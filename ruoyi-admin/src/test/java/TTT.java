import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class TTT {


    public static void main1(String[] args) {
        String html = "<p>爱戴u德哈卡阿三大苏打实打实<img src=\"https://huayin-public-1255403229.cos.ap-nanjing.myqcloud.com/common/2023/09/01/ESVoczwOS520895021eee44bd73cc4464344ccae73d9_20230901171958A013.png\" width=\"100%\"></p><p>啊大大实打实的</p><p><img src=\"https://huayin-public-1255403229.cos.ap-nanjing.myqcloud.com/common/2023/09/01/jUQNzOmr2LX4ddd2fd1a88dee238d5a2ce11c9154666_20230901172005A014.png\" width=\"100%\"></p><p><br></p><p><br></p>";

        System.out.println(Jsoup.parse(html).text());

    }


    public static String truncateHtmlWithJsoup(String html, int maxLength) {
        if (html == null || html.length() <= maxLength) {
            return html;
        }

        Document document = Jsoup.parse(html);

        Element body = document.body();
        truncateElement(body, maxLength);

        String truncatedHtml = body.html();
        if (truncatedHtml.length() > maxLength) {
            truncatedHtml = truncatedHtml.substring(0, maxLength);
        }

        return truncatedHtml + "...";
    }

    private static void truncateElement(Element element, int maxLength) {
        if (element.ownText().length() > maxLength) {
            element.text(element.ownText().substring(0, maxLength));
        }

        for (Element child : element.children()) {
            truncateElement(child, maxLength);
        }
    }


}
