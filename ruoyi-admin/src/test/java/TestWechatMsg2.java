import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.system.common.config.Custom;
import com.ruoyi.system.common.tools.OkHttp3Tools;
import okhttp3.*;

import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TestWechatMsg2 {

    @Resource
    private Custom custom;

    public static void mai1n(String[] args) {

        TestWechatMsg2 testWechatMsg2 = new TestWechatMsg2();
        try {
            String[] strings = new String[]{"语文", "数学"};
//            testWechatMsg2.weChatMiniStudentSignUpMsg("o2wmq5JJtHMh910uESWEZFHPCKrw","小明",strings,"12345678912","12333333222");

            testWechatMsg2.weChatMiniStudentAuditMsg("o2wmq5JJtHMh910uESWEZFHPCKrw", "小明", strings, "12345678912", "审核通过");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 小程序 AccessToken
     *
     * @return
     * @throws IOException
     */
    public String getWeChatMiniAccessToken() throws IOException {

        String appId = "wxe697bb38d4b86ed7";
        String secret = "fd572fd24d04011fd6fbb85949d892a2";

        Request request = new Request.Builder()
                .url("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appId + "&secret=" + secret)
                .get()
                .build();

        Call call = OkHttp3Tools.client.newCall(request);
        Response response = call.execute();
        JSONObject jsonObject = JSONObject.parseObject(response.body().string());
//        System.out.println(jsonObject.getString("access_token"));
        return jsonObject.getString("access_token");
    }


    /**
     * 小程序
     * 学员报名成功通知
     * 模板 SbrB1nbVaa8TCOqXe8s4HNr21xYGfQwUZePhW3syERs
     */
    public void weChatMiniStudentSignUpMsg(String openId, String userName, String[] subjects, String tel, String parentTel) throws IOException {
        //openid

        //报名学生
        //报名时间
        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式(年-月-日-时-分-秒)
        String createTime = dateFormat.format(now);//格式化然后放入字符串中
        //课程名称
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < subjects.length; i++) {
            stringBuilder.append(subjects[i]);

            if (i < subjects.length - 1) {
                stringBuilder.append("，");
            }
        }
        String subjectsstr = stringBuilder.toString();
        //联系方式
        //家长电话

        HttpURLConnection httpConn = null;
        InputStream is = null;
        BufferedReader rd = null;
        String accessToken = null;
        String str = null;
        try {
            //1.就要有一在小程序中允许通知的openid
            //2.调用接口
            //获取token  小程序全局唯一后台接口调用凭据
            accessToken = getWeChatMiniAccessToken();

            JSONObject xmlData = new JSONObject();
            xmlData.put("touser", openId);//接收者（用户）的 openid
            xmlData.put("template_id", "SbrB1nbVaa8TCOqXe8s4HNr21xYGfQwUZePhW3syERs");//所需下发的订阅模板id
            xmlData.put("page", "/pages/index/index");//点击模板卡片后的跳转页面，仅限本小程序内的页面该字段不填则模板无跳转
            xmlData.put("miniprogram_state", "developer");//跳转小程序类型：developer为开发版；trial为体验版；formal为正式版；默认为正式版
            xmlData.put("lang", "zh_CN");//进入小程序查看”的语言类型，支持zh_CN(简体中文)、en_US(英文)、zh_HK(繁体中文)、zh_TW(繁体中文)，默认为zh_CN返回值

            JSONObject data = new JSONObject();
            //报名学生
            JSONObject name3 = new JSONObject();//
            name3.put("value", userName);
            data.put("name3", name3);
            //课程名称
            JSONObject thing1 = new JSONObject();
            thing1.put("value", subjectsstr);
            data.put("thing1", thing1);
            //报名时间
            JSONObject time12 = new JSONObject();
            time12.put("value", createTime);
            data.put("time12", time12);
            //联系方式
            JSONObject phone_number5 = new JSONObject();
            phone_number5.put("value", tel);
            data.put("phone_number5", phone_number5);
            //家长电话
            JSONObject phone_number13 = new JSONObject();
            phone_number13.put("value", parentTel);
            data.put("phone_number13", phone_number13);

            xmlData.put("data", data);//小程序模板数据

//            System.out.println("发送模板消息xmlData:" + xmlData);

            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), xmlData.toString());

            Request request = new Request.Builder()
                    .url("https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=" + accessToken)
                    .post(requestBody)
                    .build();

            Call call = OkHttp3Tools.client.newCall(request);
            //4.执行同步请求，获取响应体Response对象
            Response response = call.execute();

            System.out.println(response.body().string());

        } catch (Exception e) {
            System.out.println("发送模板消息失败.." + e.getMessage());
        }


    }


    /**
     * 小程序
     * 学员报名审核通知
     * 模板 F9UiQnCPgWgBMffssKdn7wmLw6l6Y27N0avJL3mDwX0
     */
    public void weChatMiniStudentAuditMsg(String openId, String userName, String[] subjects, String tel, String auditResult) throws IOException {

        //当前系统时间
        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式(年-月-日-时-分-秒)
        String createTime = dateFormat.format(now);//格式化然后放入字符串中
        //课程名称
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < subjects.length; i++) {
            stringBuilder.append(subjects[i]);
            if (i < subjects.length - 1) {
                stringBuilder.append("，");
            }
        }
        String subjectsstr = stringBuilder.toString();

        String accessToken = null;
        try {
            //1.就要有一在小程序中允许通知的openid
            //2.调用接口
            //获取token  小程序全局唯一后台接口调用凭据
            accessToken = getWeChatMiniAccessToken();

            JSONObject xmlData = new JSONObject();
            xmlData.put("touser", openId);//接收者（用户）的 openid
            xmlData.put("template_id", "F9UiQnCPgWgBMffssKdn7wmLw6l6Y27N0avJL3mDwX0");//所需下发的订阅模板id
            xmlData.put("page", "/pages/index/index");//点击模板卡片后的跳转页面，仅限本小程序内的页面该字段不填则模板无跳转
            xmlData.put("miniprogram_state", "developer");//跳转小程序类型：developer为开发版；trial为体验版；formal为正式版；默认为正式版
            xmlData.put("lang", "zh_CN");//进入小程序查看”的语言类型，支持zh_CN(简体中文)、en_US(英文)、zh_HK(繁体中文)、zh_TW(繁体中文)，默认为zh_CN返回值

            JSONObject data = new JSONObject();
            //报名学生
            JSONObject name2 = new JSONObject();//
            name2.put("value", userName);
            data.put("name2", name2);
            //课程名称
            JSONObject thing1 = new JSONObject();
            thing1.put("value", subjectsstr);
            data.put("thing1", thing1);
            //联系电话
            JSONObject phone_number4 = new JSONObject();
            phone_number4.put("value", tel);
            data.put("phone_number4", phone_number4);
            //审核结果
            JSONObject phrase7 = new JSONObject();
            phrase7.put("value", auditResult);
            data.put("phrase7", phrase7);
            //审核时间
            JSONObject time6 = new JSONObject();
            time6.put("value", createTime);
            data.put("time6", time6);

            xmlData.put("data", data);//小程序模板数据

//            System.out.println("发送模板消息xmlData:" + xmlData);

            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), xmlData.toString());

            Request request = new Request.Builder()
                    .url("https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=" + accessToken)
                    .post(requestBody)
                    .build();

            Call call = OkHttp3Tools.client.newCall(request);
            //4.执行同步请求，获取响应体Response对象
            Response response = call.execute();

            System.out.println(response.body().string());

        } catch (Exception e) {
            System.out.println("发送模板消息失败.." + e.getMessage());
        }


    }


}
