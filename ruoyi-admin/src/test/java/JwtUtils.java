import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;

public class JwtUtils {

    private static final String SECRET_KEY = "your-secret-key";

    /**
     * 创建令牌
     *
     * @param subject 令牌主题, 自定义对外公布的信息
     * @return 返回加密的令牌
     */
    public static String generateToken(String subject) {
        long expirationTimeInMillis = 1000 * 60 * 60 * 48; // 设置过期时间为48小时

        return Jwts.builder()
                .setSubject(subject)
                .setExpiration(new Date(System.currentTimeMillis() + expirationTimeInMillis))
                .signWith(SignatureAlgorithm.HS512, SECRET_KEY)
                .compact();
    }

    /**
     * 验证令牌
     *
     * @param token 令牌字符串
     * @return 返回是否验证成功
     */
    public static boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token);
            return true;
        } catch (JwtException | IllegalArgumentException e) {
            // 处理token过期或者验证失败的情况
            return false;
        }
    }

    /**
     * 提前令牌主题
     *
     * @param token 令牌字符串
     * @return 返回主题字符串
     */
    public static String extractSubjectFromToken(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(SECRET_KEY)
                .parseClaimsJws(token)
                .getBody();

        return claims.getSubject();
    }
}