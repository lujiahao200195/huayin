import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.system.common.tools.OkHttp3Tools;
import com.ruoyi.system.common.wechatmsg.TagMsg;
import okhttp3.*;

import java.io.IOException;
import java.util.List;

public class TestWechatMsg {

    /**
     * 获取微信公众access_token
     */
    public static String getWechatArticleAccessToken() throws IOException {

        String appId = "wxa6ac992b3135d1fa";
        String secret = "6031c09a011deb4b094e44313ddce771";

        Request request = new Request.Builder()
                .url("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appId + "&secret=" + secret)
                .get()
                .build();

        Call call = OkHttp3Tools.client.newCall(request);
        Response response = call.execute();
        JSONObject jsonObject = JSONObject.parseObject(response.body().string());
        return jsonObject.getString("access_token");
    }

    /**
     * 测试微信小程序登入接口
     */
    public static void test01() throws IOException {
        Request request = new Request.Builder()
                .url("https://api.weixin.qq.com/sns/jscode2session?appid=wxe697bb38d4b86ed7&secret=fd572fd24d04011fd6fbb85949d892a2&js_code=0c1Fg6Ha1UCERF0w4IHa1ukOgi0Fg6Hh&grant_type=authorization_code")//URL地址
                .get()
                .build();//构建

        Call call = OkHttp3Tools.client.newCall(request);
        //4.执行同步请求，获取响应体Response对象
        Response response = call.execute();
        System.out.println(response.body().string());
        // 登入成功: {"session_key":"Vi29L0ytFE3rwoH1owv3Xg==","openid":"o2wmq5L6ExGms20q9oxZPmlPDgqE"}
    }

    /**
     * 获取小程序AccessToken
     *
     * @return
     * @throws IOException
     */
    public String getWechatMiniAccessToken() throws IOException {
        String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wxe697bb38d4b86ed7&secret=fd572fd24d04011fd6fbb85949d892a2";

        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();

        Call call = OkHttp3Tools.client.newCall(request);
        //4.执行同步请求，获取响应体Response对象
        Response response = call.execute();
        return JSONObject.parseObject(response.body().string()).getString("access_token");

    }

    /**
     * 后台给客服发通知
     */
//    @Test
    public void test07() throws IOException {
        sendCustomerService("o2wmq5J4EQt1q5X-tmT6eaHG_6xk", "12221");

    }

    public void sendCustomerService(String userOpenId, String msg) throws IOException {
        String url = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=" + getWechatMiniAccessToken();
        String requestDatas = "{\n" +
                "  \"touser\":\"" + userOpenId + "\",\n" +
                "  \"msgtype\":\"text\",\n" +
                "  \"text\":\n" +
                "  {\n" +
                "    \"content\":\"" + msg + "\"\n" +
                "  }\n" +
                "}";
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), requestDatas);
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        Call call = OkHttp3Tools.client.newCall(request);
        //4.执行同步请求，获取响应体Response对象
        Response response = call.execute();
        JSONObject jsonObject = JSONObject.parseObject(response.body().string());
        Integer errcode = jsonObject.getInteger("errcode");
        if (errcode != 0) {
            throw new RuntimeException("发送失败: " + jsonObject.getString("errmsg"));
        }
    }

    /**
     * 公众号模板消息
     *
     * @throws IOException
     */
//    @Test
    public void test06() throws IOException {
        String requestDatas = "{\n" +
                "    \"touser\": \"ooL2_t76f_SvEAX5k7e3Cgy2RG-I\",\n" +
                "    \"template_id\": \"k6cOfJ8gBlItmf5bWaNQ9QF2hJDwaaOBNd5RqxHowto\",\n" +
//                "    \"url\": \"http://weixin.qq.com/download\",\n" +
                "    \"data\": {\n" +
                "        \"keyword1\": {\n" +
                "            \"value\": \"小明\"\n" +
                "        },\n" +
                "        \"keyword2\": {\n" +
                "            \"value\": \"12345678912\"\n" +
                "        },\n" +
                "        \"keyword3\": {\n" +
                "            \"value\": \"数学，语文\"\n" +
                "        },\n" +
                "        \"keyword4\": {\n" +
                "            \"value\": \"2022年1月1日 22:22:22\"\n" +
                "        }\n" +
                "    }\n" +
                "}";


//            String requestDatas = "{\n" +
//                    "  \"touser\": \"ooL2_t21paFJl6RFWJ7Tu-ycsRNM\",\n" +
//                    "  \"template_id\": \"71Ma68Kw8jnK6ZtheDRMZA0kt1HYIXrXavjWQ7SgVBM\",\n" +
//                    "   \"data\": {\n" +
//                    "        \"thing5\": {\n" +
//                    "            \"value\": \"测试\"\n" +
//                    "        },\n" +
//                    "        \"thing1\":{\n" +
//                    "            \"value\": \"小明\"\n" +
//                    "        },\n" +
//                    "        \"phone_number8\": {\n" +
//                    "            \"value\": \"1122331122\"\n" +
//                    "        }\n" +
//                    "    }\n" +
//                    "}";

        System.out.println(requestDatas);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), requestDatas);


        String accessToken = getWechatArticleAccessToken();


        Request request = new Request.Builder()
                .url("https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" + accessToken)
                .post(requestBody)
                .build();

        Call call = OkHttp3Tools.client.newCall(request);
        //4.执行同步请求，获取响应体Response对象
        Response response = call.execute();

        System.out.println(response.body().string());


    }

    /**
     * 获取标签tagid
     *
     * @throws IOException
     */
//    @Test
    public void test04() throws IOException {


        String accessToken = getWechatArticleAccessToken();
//        String accessToken = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ7XCJpZFwiOjYsXCJ0ZWFjaGVySW5mb1wiOntcInRlYWNoZXJDb3VudHJ5UmVnaW9uXCI6XCLnpo_lu7rnnIHnpo_lt57luILpvJPmpbzljLpcIixcInRlYWNoZXJFZHVjYXRpb25cIjpcIuWvueixoVwiLFwidGVhY2hlckVtYWlsXCI6XCIxMzEyMUBxcS5jb21cIixcInRlYWNoZXJFeHBlcmllbmNlXCI6XCJcIixcInRlYWNoZXJJZFwiOjU3LFwidGVhY2hlck1ham9yXCI6XCLlrabnlJ9cIixcInRlYWNoZXJOYW1lXCI6XCLku7tcIixcInRlYWNoZXJOYXRpb25hbGl0eVwiOlwiXCIsXCJ0ZWFjaGVyUGFydHlcIjpcIlwiLFwidGVhY2hlclBob25lXCI6XCIxMjMxMjMyMTExXCIsXCJ0ZWFjaGVyUGljdHVyZVwiOlwiXCIsXCJ0ZWFjaGVyU2Nob29sXCI6XCJcIixcInVzZXJJZFwiOjZ9LFwidXNlckdlbmRlclwiOlwiXCIsXCJ1c2VyTmFtZVwiOlwi5Lu7XCIsXCJ3ZWNoYXRNaW5pT3BlbklkXCI6XCJvMndtcTVKSnRITWg5MTB1RVNXRVpGSFBDS3J3XCJ9IiwiZXhwIjoxNjkzMzY4MDQ1fQ.A05VXsB-DmFY3RL7xfI-VTZ1p-vduz7lLXo3dNnm8RRE2osM_lXjLT5j04U3vk1F7M7dTMMKGd6Vb0OaLwL0qQ";

        Request request = new Request.Builder()
                .url("https://api.weixin.qq.com/cgi-bin/tags/get?access_token=" + accessToken)//URL地址
                .get()
                .build();//构建

        Call call = OkHttp3Tools.client.newCall(request);
        //4.执行同步请求，获取响应体Response对象
        Response response = call.execute();
//       Tag tag1 = JSONObject.parseObject(response.body().string(), Tag.class);
//       System.out.println(tag1.getTags());
        System.out.println(response.body().string());

    }

    /**
     * 获取100标签里的用户
     *
     * @throws IOException
     */
//    @Test
    public void test05() throws IOException {

        String requestDatas = "{\n" +
                "    \"tagid\": 101,\n" +
                "    \"next_openid\": \"\"\n" +
                "}";


        System.out.println(requestDatas);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), requestDatas);

        String accessToken = getWechatArticleAccessToken();

        Request request = new Request.Builder()
                .url("https://api.weixin.qq.com/cgi-bin/user/tag/get?access_token=" + accessToken)//URL地址
                .post(requestBody)
                .build();//构建

        Call call = OkHttp3Tools.client.newCall(request);
        //4.执行同步请求，获取响应体Response对象
        Response response = call.execute();
//        System.out.println(response.body().string());
        String string = response.body().string();
        System.out.println(string);
        TagMsg tagMsg = JSONObject.parseObject(string, TagMsg.class);
        List<String> openid = tagMsg.getData().getOpenid();
        System.out.println(openid);

    }

    /**
     * 获取微信access_token
     */
//    @Test
    public void test03() throws IOException {
        // 微信公众号的
        String appId = "wxa6ac992b3135d1fa";
        String secret = "6031c09a011deb4b094e44313ddce771";

        Request request = new Request.Builder()
                .url("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appId + "&secret=" + secret)
                .get()
                .build();

        Call call = OkHttp3Tools.client.newCall(request);
        Response response = call.execute();
        System.out.println(response.body().string());
        // 71_KX0DDZrzonqv_y0RZkgyTu8sPyctAslId38sUev7-YI129qi_G8Qrh0OcY_lHIL_ORYVpUD_RMpeVzm7yal0BYmpEGISiG4YFh4du6WW5UTFIAmZFj1VrWgnXwsWGUcAHASCQ
//        return "";

    }

    /**
     * 测试微信小程序订阅通知接口
     */
//    @Test
    public void test02() throws IOException {

        String requestDatas = "{\n" +
                "    \"touser\": \"o2wmq5JJtHMh910uESWEZFHPCKrw\",\n" +
                "    \"template_id\": \"0RPdlcSqvMsXFinecHBpyATXyh6wRey6lshKE9jR2Uc\",\n" +
                "    \"miniprogram\": {\n" +
                "        \"appid\": \"wxe697bb38d4b86ed7\",\n" +
                "        \"pagepath\": \"index?foo=bar\"\n" +
                "    },\n" +
                "    \"data\": {\n" +
                "        \"name3\": {\n" +
                "            \"value\": \"测试\"\n" +
                "        },\n" +
                "        \"phone_number5\": {\n" +
                "            \"value\": 123456\n" +
                "        },\n" +
                "        \"thing1\": {\n" +
                "            \"value\": \"测试课程\"\n" +
                "        },\n" +
                "        \"time12\": {\n" +
                "            \"value\": \"2020-07-21 18:40:00\"\n" +
                "        }\n" +
                "    }\n" +
                "}";

        System.out.println(requestDatas);


        JSONObject xmlData = new JSONObject();
        xmlData.put("touser", "ooL2_t76f_SvEAX5k7e3Cgy2RG-I");//接收者（用户）的 openid
        xmlData.put("template_id", "SbrB1nbVaa8TCOqXe8s4HNr21xYGfQwUZePhW3syERs");//所需下发的订阅模板id
//            xmlData.put("page", "/index");//点击模板卡片后的跳转页面，仅限本小程序内的页面该字段不填则模板无跳转
        xmlData.put("miniprogram_state", "developer");//跳转小程序类型：developer为开发版；trial为体验版；formal为正式版；默认为正式版
        xmlData.put("lang", "zh_CN");//进入小程序查看”的语言类型，支持zh_CN(简体中文)、en_US(英文)、zh_HK(繁体中文)、zh_TW(繁体中文)，默认为zh_CN返回值


        JSONObject data = new JSONObject();
        //报名学生
        JSONObject name3 = new JSONObject();//
        name3.put("value", "小明");
        data.put("name3", name3);
        //课程名称
        JSONObject thing1 = new JSONObject();
        thing1.put("value", "数学，语文");
        data.put("thing1", thing1);
        //报名时间
        JSONObject time12 = new JSONObject();
        time12.put("value", "2023-08-16 17:31");
        data.put("time12", time12);
        //联系方式
        JSONObject phone_number5 = new JSONObject();
        phone_number5.put("value", "12345678901");
        data.put("phone_number5", phone_number5);
        //家长电话
        JSONObject phone_number13 = new JSONObject();
        phone_number13.put("value", "98745612302");
        data.put("phone_number13", phone_number13);

        xmlData.put("data", data);//小程序模板数据


        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), requestDatas);


        String accessToken = getWechatArticleAccessToken();


        Request request = new Request.Builder()
                .url("https://api.weixin.qq.com/cgi-bin/message/subscribe/bizsend?access_token=" + accessToken)
                .post(requestBody)
                .build();

        Call call = OkHttp3Tools.client.newCall(request);
        //4.执行同步请求，获取响应体Response对象
        Response response = call.execute();
        System.out.println(response.body().string());
    }


}
