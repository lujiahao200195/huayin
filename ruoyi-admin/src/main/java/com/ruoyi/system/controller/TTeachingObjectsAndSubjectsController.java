package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TTeachingObjectsAndSubjects;
import com.ruoyi.system.service.ITTeachingObjectsAndSubjectsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 教师意向教学Controller
 *
 * @author ruoyi
 * @date 2023-08-17
 */
@RestController
@RequestMapping("/system/tTeachingObjectsAndSubjects")
public class TTeachingObjectsAndSubjectsController extends BaseController {
    @Autowired
    private ITTeachingObjectsAndSubjectsService tTeachingObjectsAndSubjectsService;

    /**
     * 查询教师意向教学列表
     */
    @PreAuthorize("@ss.hasPermi('system:tTeachingObjectsAndSubjects:list')")
    @GetMapping("/list")
    public TableDataInfo list(TTeachingObjectsAndSubjects tTeachingObjectsAndSubjects) {
        startPage();
        List<TTeachingObjectsAndSubjects> list = tTeachingObjectsAndSubjectsService.selectTTeachingObjectsAndSubjectsList(tTeachingObjectsAndSubjects);
        return getDataTable(list);
    }

    /**
     * 导出教师意向教学列表
     */
    @PreAuthorize("@ss.hasPermi('system:tTeachingObjectsAndSubjects:export')")
    @Log(title = "教师意向教学", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TTeachingObjectsAndSubjects tTeachingObjectsAndSubjects) {
        List<TTeachingObjectsAndSubjects> list = tTeachingObjectsAndSubjectsService.selectTTeachingObjectsAndSubjectsList(tTeachingObjectsAndSubjects);
        ExcelUtil<TTeachingObjectsAndSubjects> util = new ExcelUtil<TTeachingObjectsAndSubjects>(TTeachingObjectsAndSubjects.class);
        util.exportExcel(response, list, "教师意向教学数据");
    }

    /**
     * 获取教师意向教学详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tTeachingObjectsAndSubjects:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tTeachingObjectsAndSubjectsService.selectTTeachingObjectsAndSubjectsById(id));
    }

    /**
     * 新增教师意向教学
     */
    @PreAuthorize("@ss.hasPermi('system:tTeachingObjectsAndSubjects:add')")
    @Log(title = "教师意向教学", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TTeachingObjectsAndSubjects tTeachingObjectsAndSubjects) {
        return toAjax(tTeachingObjectsAndSubjectsService.insertTTeachingObjectsAndSubjects(tTeachingObjectsAndSubjects));
    }

    /**
     * 修改教师意向教学
     */
    @PreAuthorize("@ss.hasPermi('system:tTeachingObjectsAndSubjects:edit')")
    @Log(title = "教师意向教学", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TTeachingObjectsAndSubjects tTeachingObjectsAndSubjects) {
        return toAjax(tTeachingObjectsAndSubjectsService.updateTTeachingObjectsAndSubjects(tTeachingObjectsAndSubjects));
    }

    /**
     * 删除教师意向教学
     */
    @PreAuthorize("@ss.hasPermi('system:tTeachingObjectsAndSubjects:remove')")
    @Log(title = "教师意向教学", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tTeachingObjectsAndSubjectsService.deleteTTeachingObjectsAndSubjectsByIds(ids));
    }
}
