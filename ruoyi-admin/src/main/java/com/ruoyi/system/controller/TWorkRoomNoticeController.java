package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TWorkRoomNotice;
import com.ruoyi.system.service.ITWorkRoomNoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 工作室公告Controller
 *
 * @author ruoyi
 * @date 2023-08-18
 */
@RestController
@RequestMapping("/system/tWorkRoomNotice")
public class TWorkRoomNoticeController extends BaseController {
    @Autowired
    private ITWorkRoomNoticeService tWorkRoomNoticeService;

    /**
     * 查询工作室公告列表
     */
    @PreAuthorize("@ss.hasPermi('system:tWorkRoomNotice:list')")
    @GetMapping("/list")
    public TableDataInfo list(TWorkRoomNotice tWorkRoomNotice) {
        startPage();
        List<TWorkRoomNotice> list = tWorkRoomNoticeService.selectTWorkRoomNoticeList(tWorkRoomNotice);
        return getDataTable(list);
    }

    /**
     * 导出工作室公告列表
     */
    @PreAuthorize("@ss.hasPermi('system:tWorkRoomNotice:export')")
    @Log(title = "工作室公告", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TWorkRoomNotice tWorkRoomNotice) {
        List<TWorkRoomNotice> list = tWorkRoomNoticeService.selectTWorkRoomNoticeList(tWorkRoomNotice);
        ExcelUtil<TWorkRoomNotice> util = new ExcelUtil<TWorkRoomNotice>(TWorkRoomNotice.class);
        util.exportExcel(response, list, "工作室公告数据");
    }

    /**
     * 获取工作室公告详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tWorkRoomNotice:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tWorkRoomNoticeService.selectTWorkRoomNoticeById(id));
    }

    /**
     * 新增工作室公告
     */
    @PreAuthorize("@ss.hasPermi('system:tWorkRoomNotice:add')")
    @Log(title = "工作室公告", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TWorkRoomNotice tWorkRoomNotice) {
        return toAjax(tWorkRoomNoticeService.insertTWorkRoomNotice(tWorkRoomNotice));
    }

    /**
     * 修改工作室公告
     */
    @PreAuthorize("@ss.hasPermi('system:tWorkRoomNotice:edit')")
    @Log(title = "工作室公告", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TWorkRoomNotice tWorkRoomNotice) {
        return toAjax(tWorkRoomNoticeService.updateTWorkRoomNotice(tWorkRoomNotice));
    }

    /**
     * 删除工作室公告
     */
    @PreAuthorize("@ss.hasPermi('system:tWorkRoomNotice:remove')")
    @Log(title = "工作室公告", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tWorkRoomNoticeService.deleteTWorkRoomNoticeByIds(ids));
    }
}
