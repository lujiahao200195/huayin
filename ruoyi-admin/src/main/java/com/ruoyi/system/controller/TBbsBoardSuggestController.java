package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TBbsBoardSuggest;
import com.ruoyi.system.service.ITBbsBoardSuggestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 版主推荐Controller
 *
 * @author ruoyi
 * @date 2023-08-29
 */
@RestController
@RequestMapping("/system/tBbsBoardSuggest")
public class TBbsBoardSuggestController extends BaseController {
    @Autowired
    private ITBbsBoardSuggestService tBbsBoardSuggestService;

    /**
     * 查询版主推荐列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsBoardSuggest:list')")
    @GetMapping("/list")
    public TableDataInfo list(TBbsBoardSuggest tBbsBoardSuggest) {
        startPage();
        List<TBbsBoardSuggest> list = tBbsBoardSuggestService.selectTBbsBoardSuggestList(tBbsBoardSuggest);
        return getDataTable(list);
    }

    /**
     * 导出版主推荐列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsBoardSuggest:export')")
    @Log(title = "版主推荐", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TBbsBoardSuggest tBbsBoardSuggest) {
        List<TBbsBoardSuggest> list = tBbsBoardSuggestService.selectTBbsBoardSuggestList(tBbsBoardSuggest);
        ExcelUtil<TBbsBoardSuggest> util = new ExcelUtil<TBbsBoardSuggest>(TBbsBoardSuggest.class);
        util.exportExcel(response, list, "版主推荐数据");
    }

    /**
     * 获取版主推荐详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsBoardSuggest:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tBbsBoardSuggestService.selectTBbsBoardSuggestById(id));
    }

    /**
     * 新增版主推荐
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsBoardSuggest:add')")
    @Log(title = "版主推荐", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TBbsBoardSuggest tBbsBoardSuggest) {
        return toAjax(tBbsBoardSuggestService.insertTBbsBoardSuggest(tBbsBoardSuggest));
    }

    /**
     * 修改版主推荐
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsBoardSuggest:edit')")
    @Log(title = "版主推荐", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TBbsBoardSuggest tBbsBoardSuggest) {
        return toAjax(tBbsBoardSuggestService.updateTBbsBoardSuggest(tBbsBoardSuggest));
    }

    /**
     * 删除版主推荐
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsBoardSuggest:remove')")
    @Log(title = "版主推荐", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tBbsBoardSuggestService.deleteTBbsBoardSuggestByIds(ids));
    }
}
