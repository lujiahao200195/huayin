package com.ruoyi.system.controller.socket.msg;

/**
 * 会话类型
 */
public class UserConsultSessionType {

    /**
     * 咨询教师会话
     */
    public static final int CONSULT_TEACHER_SESSION = 1;

    /**
     * 用户会话
     */
    public static final int USER_SESSION = 2;
}
