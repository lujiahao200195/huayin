package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TAddress;
import com.ruoyi.system.service.ITAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 地区Controller
 *
 * @author ruoyi
 * @date 2023-08-21
 */
@RestController
@RequestMapping("/system/tAddress")
public class TAddressController extends BaseController {
    @Autowired
    private ITAddressService tAddressService;

    /**
     * 查询地区列表
     */
    @PreAuthorize("@ss.hasPermi('system:tAddress:list')")
    @GetMapping("/list")
    public TableDataInfo list(TAddress tAddress) {
        startPage();
        List<TAddress> list = tAddressService.selectTAddressList(tAddress);
        return getDataTable(list);
    }

    /**
     * 导出地区列表
     */
    @PreAuthorize("@ss.hasPermi('system:tAddress:export')")
    @Log(title = "地区", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TAddress tAddress) {
        List<TAddress> list = tAddressService.selectTAddressList(tAddress);
        ExcelUtil<TAddress> util = new ExcelUtil<TAddress>(TAddress.class);
        util.exportExcel(response, list, "地区数据");
    }

    /**
     * 获取地区详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tAddress:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tAddressService.selectTAddressById(id));
    }

    /**
     * 新增地区
     */
    @PreAuthorize("@ss.hasPermi('system:tAddress:add')")
    @Log(title = "地区", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TAddress tAddress) {
        return toAjax(tAddressService.insertTAddress(tAddress));
    }

    /**
     * 修改地区
     */
    @PreAuthorize("@ss.hasPermi('system:tAddress:edit')")
    @Log(title = "地区", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TAddress tAddress) {
        return toAjax(tAddressService.updateTAddress(tAddress));
    }

    /**
     * 删除地区
     */
    @PreAuthorize("@ss.hasPermi('system:tAddress:remove')")
    @Log(title = "地区", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tAddressService.deleteTAddressByIds(ids));
    }
}
