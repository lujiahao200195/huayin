package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TArticleTypeRelation;
import com.ruoyi.system.service.ITArticleTypeRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 文章和文章分类关系Controller
 *
 * @author ruoyi
 * @date 2023-08-16
 */
@RestController
@RequestMapping("/system/tArticleTypeRelation")
public class TArticleTypeRelationController extends BaseController {
    @Autowired
    private ITArticleTypeRelationService tArticleTypeRelationService;

    /**
     * 查询文章和文章分类关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tArticleTypeRelation:list')")
    @GetMapping("/list")
    public TableDataInfo list(TArticleTypeRelation tArticleTypeRelation) {
        startPage();
        List<TArticleTypeRelation> list = tArticleTypeRelationService.selectTArticleTypeRelationList(tArticleTypeRelation);
        return getDataTable(list);
    }

    /**
     * 导出文章和文章分类关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tArticleTypeRelation:export')")
    @Log(title = "文章和文章分类关系", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TArticleTypeRelation tArticleTypeRelation) {
        List<TArticleTypeRelation> list = tArticleTypeRelationService.selectTArticleTypeRelationList(tArticleTypeRelation);
        ExcelUtil<TArticleTypeRelation> util = new ExcelUtil<TArticleTypeRelation>(TArticleTypeRelation.class);
        util.exportExcel(response, list, "文章和文章分类关系数据");
    }

    /**
     * 获取文章和文章分类关系详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tArticleTypeRelation:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tArticleTypeRelationService.selectTArticleTypeRelationById(id));
    }

    /**
     * 新增文章和文章分类关系
     */
    @PreAuthorize("@ss.hasPermi('system:tArticleTypeRelation:add')")
    @Log(title = "文章和文章分类关系", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TArticleTypeRelation tArticleTypeRelation) {
        return toAjax(tArticleTypeRelationService.insertTArticleTypeRelation(tArticleTypeRelation));
    }

    /**
     * 修改文章和文章分类关系
     */
    @PreAuthorize("@ss.hasPermi('system:tArticleTypeRelation:edit')")
    @Log(title = "文章和文章分类关系", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TArticleTypeRelation tArticleTypeRelation) {
        return toAjax(tArticleTypeRelationService.updateTArticleTypeRelation(tArticleTypeRelation));
    }

    /**
     * 删除文章和文章分类关系
     */
    @PreAuthorize("@ss.hasPermi('system:tArticleTypeRelation:remove')")
    @Log(title = "文章和文章分类关系", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tArticleTypeRelationService.deleteTArticleTypeRelationByIds(ids));
    }
}
