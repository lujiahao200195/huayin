package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TBbsBoard;
import com.ruoyi.system.service.ITBbsBoardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 论坛版块Controller
 *
 * @author ruoyi
 * @date 2023-09-08
 */
@RestController
@RequestMapping("/system/tBbsBoard")
public class TBbsBoardController extends BaseController {
    @Autowired
    private ITBbsBoardService tBbsBoardService;

    /**
     * 查询论坛版块列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsBoard:list')")
    @GetMapping("/list")
    public TableDataInfo list(TBbsBoard tBbsBoard) {
        startPage();
        List<TBbsBoard> list = tBbsBoardService.selectTBbsBoardList(tBbsBoard);
        return getDataTable(list);
    }

    /**
     * 导出论坛版块列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsBoard:export')")
    @Log(title = "论坛版块", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TBbsBoard tBbsBoard) {
        List<TBbsBoard> list = tBbsBoardService.selectTBbsBoardList(tBbsBoard);
        ExcelUtil<TBbsBoard> util = new ExcelUtil<TBbsBoard>(TBbsBoard.class);
        util.exportExcel(response, list, "论坛版块数据");
    }

    /**
     * 获取论坛版块详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsBoard:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tBbsBoardService.selectTBbsBoardById(id));
    }

    /**
     * 新增论坛版块
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsBoard:add')")
    @Log(title = "论坛版块", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TBbsBoard tBbsBoard) {
        return toAjax(tBbsBoardService.insertTBbsBoard(tBbsBoard));
    }

    /**
     * 修改论坛版块
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsBoard:edit')")
    @Log(title = "论坛版块", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TBbsBoard tBbsBoard) {
        return toAjax(tBbsBoardService.updateTBbsBoard(tBbsBoard));
    }

    /**
     * 删除论坛版块
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsBoard:remove')")
    @Log(title = "论坛版块", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tBbsBoardService.deleteTBbsBoardByIds(ids));
    }
}
