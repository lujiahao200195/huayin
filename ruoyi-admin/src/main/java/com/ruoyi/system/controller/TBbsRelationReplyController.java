package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TBbsRelationReply;
import com.ruoyi.system.service.ITBbsRelationReplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 回复关系Controller
 *
 * @author ruoyi
 * @date 2023-08-31
 */
@RestController
@RequestMapping("/system/tBbsRelationReply")
public class TBbsRelationReplyController extends BaseController {
    @Autowired
    private ITBbsRelationReplyService tBbsRelationReplyService;

    /**
     * 查询回复关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationReply:list')")
    @GetMapping("/list")
    public TableDataInfo list(TBbsRelationReply tBbsRelationReply) {
        startPage();
        List<TBbsRelationReply> list = tBbsRelationReplyService.selectTBbsRelationReplyList(tBbsRelationReply);
        return getDataTable(list);
    }

    /**
     * 导出回复关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationReply:export')")
    @Log(title = "回复关系", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TBbsRelationReply tBbsRelationReply) {
        List<TBbsRelationReply> list = tBbsRelationReplyService.selectTBbsRelationReplyList(tBbsRelationReply);
        ExcelUtil<TBbsRelationReply> util = new ExcelUtil<TBbsRelationReply>(TBbsRelationReply.class);
        util.exportExcel(response, list, "回复关系数据");
    }

    /**
     * 获取回复关系详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationReply:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tBbsRelationReplyService.selectTBbsRelationReplyById(id));
    }

    /**
     * 新增回复关系
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationReply:add')")
    @Log(title = "回复关系", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TBbsRelationReply tBbsRelationReply) {
        return toAjax(tBbsRelationReplyService.insertTBbsRelationReply(tBbsRelationReply));
    }

    /**
     * 修改回复关系
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationReply:edit')")
    @Log(title = "回复关系", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TBbsRelationReply tBbsRelationReply) {
        return toAjax(tBbsRelationReplyService.updateTBbsRelationReply(tBbsRelationReply));
    }

    /**
     * 删除回复关系
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationReply:remove')")
    @Log(title = "回复关系", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tBbsRelationReplyService.deleteTBbsRelationReplyByIds(ids));
    }
}
