package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TBbsBoardTag;
import com.ruoyi.system.service.ITBbsBoardTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 版块标签Controller
 *
 * @author ruoyi
 * @date 2023-08-29
 */
@RestController
@RequestMapping("/system/tBbsBoardTag")
public class TBbsBoardTagController extends BaseController {
    @Autowired
    private ITBbsBoardTagService tBbsBoardTagService;

    /**
     * 查询版块标签列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsBoardTag:list')")
    @GetMapping("/list")
    public TableDataInfo list(TBbsBoardTag tBbsBoardTag) {
        startPage();
        List<TBbsBoardTag> list = tBbsBoardTagService.selectTBbsBoardTagList(tBbsBoardTag);
        return getDataTable(list);
    }

    /**
     * 导出版块标签列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsBoardTag:export')")
    @Log(title = "版块标签", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TBbsBoardTag tBbsBoardTag) {
        List<TBbsBoardTag> list = tBbsBoardTagService.selectTBbsBoardTagList(tBbsBoardTag);
        ExcelUtil<TBbsBoardTag> util = new ExcelUtil<TBbsBoardTag>(TBbsBoardTag.class);
        util.exportExcel(response, list, "版块标签数据");
    }

    /**
     * 获取版块标签详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsBoardTag:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tBbsBoardTagService.selectTBbsBoardTagById(id));
    }

    /**
     * 新增版块标签
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsBoardTag:add')")
    @Log(title = "版块标签", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TBbsBoardTag tBbsBoardTag) {
        return toAjax(tBbsBoardTagService.insertTBbsBoardTag(tBbsBoardTag));
    }

    /**
     * 修改版块标签
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsBoardTag:edit')")
    @Log(title = "版块标签", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TBbsBoardTag tBbsBoardTag) {
        return toAjax(tBbsBoardTagService.updateTBbsBoardTag(tBbsBoardTag));
    }

    /**
     * 删除版块标签
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsBoardTag:remove')")
    @Log(title = "版块标签", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tBbsBoardTagService.deleteTBbsBoardTagByIds(ids));
    }
}
