package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TWorkRoomMessage;
import com.ruoyi.system.service.ITWorkRoomMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 工作室消息Controller
 *
 * @author ruoyi
 * @date 2023-08-23
 */
@RestController
@RequestMapping("/system/tWorkRoomMessage")
public class TWorkRoomMessageController extends BaseController {
    @Autowired
    private ITWorkRoomMessageService tWorkRoomMessageService;

    /**
     * 查询工作室消息列表
     */
    @PreAuthorize("@ss.hasPermi('system:tWorkRoomMessage:list')")
    @GetMapping("/list")
    public TableDataInfo list(TWorkRoomMessage tWorkRoomMessage) {
        startPage();
        List<TWorkRoomMessage> list = tWorkRoomMessageService.selectTWorkRoomMessageList(tWorkRoomMessage);
        return getDataTable(list);
    }

    /**
     * 导出工作室消息列表
     */
    @PreAuthorize("@ss.hasPermi('system:tWorkRoomMessage:export')")
    @Log(title = "工作室消息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TWorkRoomMessage tWorkRoomMessage) {
        List<TWorkRoomMessage> list = tWorkRoomMessageService.selectTWorkRoomMessageList(tWorkRoomMessage);
        ExcelUtil<TWorkRoomMessage> util = new ExcelUtil<TWorkRoomMessage>(TWorkRoomMessage.class);
        util.exportExcel(response, list, "工作室消息数据");
    }

    /**
     * 获取工作室消息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tWorkRoomMessage:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tWorkRoomMessageService.selectTWorkRoomMessageById(id));
    }

    /**
     * 新增工作室消息
     */
    @PreAuthorize("@ss.hasPermi('system:tWorkRoomMessage:add')")
    @Log(title = "工作室消息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TWorkRoomMessage tWorkRoomMessage) {
        return toAjax(tWorkRoomMessageService.insertTWorkRoomMessage(tWorkRoomMessage));
    }

    /**
     * 修改工作室消息
     */
    @PreAuthorize("@ss.hasPermi('system:tWorkRoomMessage:edit')")
    @Log(title = "工作室消息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TWorkRoomMessage tWorkRoomMessage) {
        return toAjax(tWorkRoomMessageService.updateTWorkRoomMessage(tWorkRoomMessage));
    }

    /**
     * 删除工作室消息
     */
    @PreAuthorize("@ss.hasPermi('system:tWorkRoomMessage:remove')")
    @Log(title = "工作室消息", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tWorkRoomMessageService.deleteTWorkRoomMessageByIds(ids));
    }
}
