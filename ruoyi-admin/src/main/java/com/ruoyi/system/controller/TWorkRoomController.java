package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TWorkRoom;
import com.ruoyi.system.service.ITWorkRoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 工作室Controller
 *
 * @author ruoyi
 * @date 2023-08-22
 */
@RestController
@RequestMapping("/system/tWorkRoom")
public class TWorkRoomController extends BaseController {
    @Autowired
    private ITWorkRoomService tWorkRoomService;

    /**
     * 查询工作室列表
     */
    @PreAuthorize("@ss.hasPermi('system:tWorkRoom:list')")
    @GetMapping("/list")
    public TableDataInfo list(TWorkRoom tWorkRoom) {
        startPage();
        List<TWorkRoom> list = tWorkRoomService.selectTWorkRoomList(tWorkRoom);
        return getDataTable(list);
    }

    /**
     * 导出工作室列表
     */
    @PreAuthorize("@ss.hasPermi('system:tWorkRoom:export')")
    @Log(title = "工作室", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TWorkRoom tWorkRoom) {
        List<TWorkRoom> list = tWorkRoomService.selectTWorkRoomList(tWorkRoom);
        ExcelUtil<TWorkRoom> util = new ExcelUtil<TWorkRoom>(TWorkRoom.class);
        util.exportExcel(response, list, "工作室数据");
    }

    /**
     * 获取工作室详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tWorkRoom:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tWorkRoomService.selectTWorkRoomById(id));
    }

    /**
     * 新增工作室
     */
    @PreAuthorize("@ss.hasPermi('system:tWorkRoom:add')")
    @Log(title = "工作室", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TWorkRoom tWorkRoom) {
        return toAjax(tWorkRoomService.insertTWorkRoom(tWorkRoom));
    }

    /**
     * 修改工作室
     */
    @PreAuthorize("@ss.hasPermi('system:tWorkRoom:edit')")
    @Log(title = "工作室", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TWorkRoom tWorkRoom) {
        return toAjax(tWorkRoomService.updateTWorkRoom(tWorkRoom));
    }

    /**
     * 删除工作室
     */
    @PreAuthorize("@ss.hasPermi('system:tWorkRoom:remove')")
    @Log(title = "工作室", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tWorkRoomService.deleteTWorkRoomByIds(ids));
    }
}
