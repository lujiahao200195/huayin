package com.ruoyi.system.controller.socket.msg;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 用户咨询消息对象
 */
@Data
@Accessors(chain = true)
public class UserConsultMsg {

    /**
     * 消息类型():
     * 1. 初始化消息
     * 2. 咨询消息
     * 3. 群组广播消息
     */
    private Long msgType;

    /**
     * 消息json字符串
     */
    private String msgJson;


}
