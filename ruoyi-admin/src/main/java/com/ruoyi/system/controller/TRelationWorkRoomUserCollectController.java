package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TRelationWorkRoomUserCollect;
import com.ruoyi.system.service.ITRelationWorkRoomUserCollectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 工作室用户关系-收藏用Controller
 *
 * @author ruoyi
 * @date 2023-08-21
 */
@RestController
@RequestMapping("/system/tRelationWorkRoomUserCollect")
public class TRelationWorkRoomUserCollectController extends BaseController {
    @Autowired
    private ITRelationWorkRoomUserCollectService tRelationWorkRoomUserCollectService;

    /**
     * 查询工作室用户关系-收藏用列表
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationWorkRoomUserCollect:list')")
    @GetMapping("/list")
    public TableDataInfo list(TRelationWorkRoomUserCollect tRelationWorkRoomUserCollect) {
        startPage();
        List<TRelationWorkRoomUserCollect> list = tRelationWorkRoomUserCollectService.selectTRelationWorkRoomUserCollectList(tRelationWorkRoomUserCollect);
        return getDataTable(list);
    }

    /**
     * 导出工作室用户关系-收藏用列表
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationWorkRoomUserCollect:export')")
    @Log(title = "工作室用户关系-收藏用", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TRelationWorkRoomUserCollect tRelationWorkRoomUserCollect) {
        List<TRelationWorkRoomUserCollect> list = tRelationWorkRoomUserCollectService.selectTRelationWorkRoomUserCollectList(tRelationWorkRoomUserCollect);
        ExcelUtil<TRelationWorkRoomUserCollect> util = new ExcelUtil<TRelationWorkRoomUserCollect>(TRelationWorkRoomUserCollect.class);
        util.exportExcel(response, list, "工作室用户关系-收藏用数据");
    }

    /**
     * 获取工作室用户关系-收藏用详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationWorkRoomUserCollect:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tRelationWorkRoomUserCollectService.selectTRelationWorkRoomUserCollectById(id));
    }

    /**
     * 新增工作室用户关系-收藏用
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationWorkRoomUserCollect:add')")
    @Log(title = "工作室用户关系-收藏用", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TRelationWorkRoomUserCollect tRelationWorkRoomUserCollect) {
        return toAjax(tRelationWorkRoomUserCollectService.insertTRelationWorkRoomUserCollect(tRelationWorkRoomUserCollect));
    }

    /**
     * 修改工作室用户关系-收藏用
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationWorkRoomUserCollect:edit')")
    @Log(title = "工作室用户关系-收藏用", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TRelationWorkRoomUserCollect tRelationWorkRoomUserCollect) {
        return toAjax(tRelationWorkRoomUserCollectService.updateTRelationWorkRoomUserCollect(tRelationWorkRoomUserCollect));
    }

    /**
     * 删除工作室用户关系-收藏用
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationWorkRoomUserCollect:remove')")
    @Log(title = "工作室用户关系-收藏用", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tRelationWorkRoomUserCollectService.deleteTRelationWorkRoomUserCollectByIds(ids));
    }
}
