package com.ruoyi.system.controller.backend.slideshow;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TSlideshow;
import com.ruoyi.system.service.backend.SlideshowService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 轮播图Controller
 */
@RestController
@RequestMapping("/system/backend/slideshow")
public class SlideshowController extends BaseController {
    @Resource
    private SlideshowService slideshowService;

    /**
     * 查询轮播图列表
     */
    @PostMapping("/slideshow-list")
    public TableDataInfo list(@RequestBody TSlideshow tSlideshow) {
        startPage();
        List<TSlideshow> list = slideshowService.selectTSlideshowList(tSlideshow);
        return getDataTable(list);
    }

    /**
     * 导出轮播图列表
     */
    @PostMapping("/export")
    public void export(HttpServletResponse response, @RequestBody TSlideshow tSlideshow) {
        List<TSlideshow> list = slideshowService.selectTSlideshowList(tSlideshow);
        ExcelUtil<TSlideshow> util = new ExcelUtil<TSlideshow>(TSlideshow.class);
        util.exportExcel(response, list, "轮播图数据");
    }

    /**
     * 获取轮播图详细信息
     */
    @PostMapping("slideshow-info")
    public AjaxResult getInfo(@RequestBody Map<String, Long> jsonMap) {
        Long id = jsonMap.get("id");
        return AjaxResult.success(slideshowService.selectTSlideshowById(id));
    }

    /**
     * 新增轮播图
     */
    @PostMapping("slideshow-add")
    public AjaxResult add(@RequestBody TSlideshow tSlideshow) {
        return toAjax(slideshowService.insertTSlideshow(tSlideshow));
    }

    /**
     * 修改轮播图
     */
    @PostMapping("slideshow-edit")
    public AjaxResult edit(@RequestBody TSlideshow tSlideshow) {
        return toAjax(slideshowService.updateTSlideshow(tSlideshow));
    }

    /**
     * 删除轮播图
     */
    @PostMapping("slideshow-remove")
    public AjaxResult remove(@RequestBody Map<String, Long[]> jsonMap) {
        Long[] ids = jsonMap.get("ids");
        return toAjax(slideshowService.deleteTSlideshowByIds(ids));
    }


    /**
     * 论坛轮播图
     *
     * @return
     */
    @PostMapping("/bbs-slideshow-list")
    public AjaxResult listBbs() {

        return AjaxResult.success();
    }


}
