package com.ruoyi.system.controller.rs;


import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.bo.wechat.SearchResourceDo;
import com.ruoyi.system.bo.wechat.UpdateResourceDo;
import com.ruoyi.system.bo.wechat.UploadResourceDo;
import com.ruoyi.system.common.tools.NTools;
import com.ruoyi.system.domain.TResourceRelation;
import com.ruoyi.system.service.RsService;
import com.ruoyi.system.vo.wechat.rs.ResourceVo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/system/rsController")
public class RsController  extends BaseController {


    @Resource
    private RsService rsService;

    /**
     * 搜索资料
     *
     * @param searchResourceDo
     * @return
     */
    @RequestMapping("/wechat-mini-search-resource-list")
    public AjaxResult wechatMiniSearchResourceList(SearchResourceDo searchResourceDo) {
        System.out.println(searchResourceDo.toString());
        startPage();
        List<ResourceVo> resourceList = rsService.wechatMiniSearchResourceList(searchResourceDo);
//        return AjaxResult.success(rsService.wechatMiniSearchResourceList(searchResourceDo));
        return AjaxResult.success(getDataTable(resourceList));
    }

    /**
     * 获取标签列表
     *
     * @return
     */
    @RequestMapping("/wechat-mini-resource-tag-list")
    public AjaxResult wechatMiniResourceTagList() {
        return AjaxResult.success(rsService.wechatMiniResourceTagList());
    }

    /**
     * 上传资源文件
     *
     * @param uploadResourceDo
     * @return
     * @throws IOException
     */
    @RequestMapping("/wechat-mini-upload-resource")
    public AjaxResult wechatMiniUploadResource(@RequestBody UploadResourceDo uploadResourceDo) throws IOException {
        // 上传文件
        return AjaxResult.success(rsService.wechatMiniUploadResource(uploadResourceDo));
    }

    /**
     * 添加资源标签
     *
     * @param parmMap
     * @return
     */
    @RequestMapping("/wechat-mini-add-resource-tag")
    public AjaxResult wechatMiniAddResourceTag(@RequestBody Map<String, String> parmMap) {
        String tagName = NTools.mustData(parmMap.get("tagName"), String.class, "标签数据错误");
        return AjaxResult.success(rsService.wechatMiniAddResourceTag(tagName));
    }

    /**
     * 搜索标签
     *
     * @param parmMap
     * @return
     */
    @RequestMapping("/wechat-mini-search-resource-tag")
    public AjaxResult wechatMiniSearchResourceTag(@RequestBody Map<String, String> parmMap) {
        String tagName = NTools.mustData(parmMap.get("tagName"), String.class, "标签数据错误");
        return AjaxResult.success(rsService.wechatMiniSearchResourceTag(tagName));
    }

    /**
     * 删除资源
     *
     * @param parmMap
     * @return
     */
    @RequestMapping("/wechat-mini-delete-resource")
    public AjaxResult wechatMiniDeleteResource(@RequestBody Map<String, Long> parmMap) {
        Long resourceId = NTools.mustData(parmMap.get("resourceId"), Long.class, "资源数据异常");
        return AjaxResult.success(rsService.wechatMiniDeleteResource(resourceId));
    }

    /**
     * 修改资源
     *
     * @param updateResourceDo
     * @return
     */
    @RequestMapping("/wechat-mini-update-resource")
    public AjaxResult wechatMiniUpdateResource(@RequestBody UpdateResourceDo updateResourceDo) {
        return AjaxResult.success(rsService.wechatMiniUpdateResource(updateResourceDo));
    }

    /**
     * 资源下载
     *
     * @param parmMap
     * @param response
     * @throws IOException
     */
    @RequestMapping("/wechat-mini-download-resource")
    public void wechatMiniDownloadResource(@RequestBody Map<String, Long> parmMap, HttpServletResponse response) throws IOException {
        Long resourceId = NTools.mustData(parmMap.get("resourceId"), Long.class, "资源数据异常");
        rsService.wechatMiniDownloadResource(resourceId, response);
    }

    /**
     * 分享资源给学生
     *
     * @param parmMap
     * @return
     */
    @RequestMapping("/wechat-mini-share-resource-student")
    public AjaxResult wechatMiniShareResourceStudent(@RequestBody Map<String, Long> parmMap) {
        Long studentId = NTools.mustData(parmMap.get("studentId"), Long.class, "学生信息错误");
        Long resourceId = NTools.mustData(parmMap.get("resourceId"), Long.class, "资源信息错误");
        return AjaxResult.success(rsService.wechatMiniShareResourceStudent(studentId, resourceId));
    }

    /**
     * 分享资源给用户
     *
     * @param parmMap
     * @return
     */
    @RequestMapping("/wechat-mini-share-resource-user")
    public AjaxResult wechatMiniShareResourceUser(@RequestBody Map<String, Long> parmMap) {
        Long resourceId = NTools.mustData(parmMap.get("resourceId"), Long.class, "资源信息错误");
        Long userId = NTools.mustData(parmMap.get("userId"), Long.class, "用户信息错误");
        return AjaxResult.success(rsService.wechatMiniShareResourceUser(userId, resourceId));
    }

    /**
     * 用户接受分享资料
     *
     * @param parmMap
     * @return
     */
    @RequestMapping("/wechat-mini-user-accept-resource")
    public AjaxResult wechatMiniUserAcceptResource(@RequestBody Map<String, Long> parmMap) {
        Long resourceId = NTools.mustData(parmMap.get("resourceId"), Long.class, "资源信息错误");
        return AjaxResult.success(rsService.wechatMiniUserAcceptResource(resourceId));
    }

    /**
     * 教师搜索学生
     * @param parmMap
     * @return
     */
    @RequestMapping("/wechat-mini-teacher-search-student")
    public AjaxResult wechatMiniTeacherSearchStudent(@RequestBody Map<String, String> parmMap){
        String studentName = NTools.mustData(parmMap.get("studentName"), String.class, "学生姓名");
        return AjaxResult.success(rsService.wechatMiniTeacherSearchStudent(studentName));
    }

    /**
     * 小程序资料下载计数接口
     * @param parmMap
     * @return
     */
    @RequestMapping("/wechat-mini-rs-count")
    public AjaxResult wechatMiniRsCount(@RequestBody Map<String,Long> parmMap){
        Long resourceId = NTools.mustData(parmMap.get("resourceId"), Long.class, "资料数据错误");
        return  AjaxResult.success(rsService.wechatMiniRsCount(resourceId));
    }

    /**
     * 收藏接口
     * @return
     */
    @RequestMapping("/wechat-mini-collect-resource")
    public AjaxResult wechatMiniCollectResource(@RequestBody Map<String,Long> parmMap){
        //数据不为空
        Long resourceId = NTools.mustData(parmMap.get("resourceId"), Long.class, "资源数据错误");
        //返回结果为已收藏进行取消收藏删除操作
        if(!(rsService.wechatMiniQueryCollectStatus(resourceId).getId() == null)){
            int result = rsService.wechatMiniCancelCollect(resourceId);
            if(result > 0 ){
                return AjaxResult.success("取消收藏");
            }else{
                return AjaxResult.success("取消失败");
            }

        }
        return AjaxResult.success(rsService.wechatMiniCollectResource(resourceId));
    }

    /**
     * 查询资源对应改用户收藏状态
     */
    @RequestMapping("/wechat-mini-query-collect-resource-status")
    public AjaxResult WechatMiniQueryCollectStatus(@RequestBody Map<String,Long> parmMap){
        //数据不为空
        Long resourceId = NTools.mustData(parmMap.get("resourceId"), Long.class, "资源数据错误");
        return AjaxResult.success(rsService.wechatMiniQueryCollectStatus(resourceId));
    }
}
