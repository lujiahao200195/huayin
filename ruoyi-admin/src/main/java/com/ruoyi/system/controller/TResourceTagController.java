package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TResourceTag;
import com.ruoyi.system.service.ITResourceTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 资源标签Controller
 *
 * @author ruoyi
 * @date 2023-09-08
 */
@RestController
@RequestMapping("/system/tResourceTag")
public class TResourceTagController extends BaseController {
    @Autowired
    private ITResourceTagService tResourceTagService;

    /**
     * 查询资源标签列表
     */
    @PreAuthorize("@ss.hasPermi('system:tResourceTag:list')")
    @GetMapping("/list")
    public TableDataInfo list(TResourceTag tResourceTag) {
        startPage();
        List<TResourceTag> list = tResourceTagService.selectTResourceTagList(tResourceTag);
        return getDataTable(list);
    }

    /**
     * 导出资源标签列表
     */
    @PreAuthorize("@ss.hasPermi('system:tResourceTag:export')")
    @Log(title = "资源标签", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TResourceTag tResourceTag) {
        List<TResourceTag> list = tResourceTagService.selectTResourceTagList(tResourceTag);
        ExcelUtil<TResourceTag> util = new ExcelUtil<TResourceTag>(TResourceTag.class);
        util.exportExcel(response, list, "资源标签数据");
    }

    /**
     * 获取资源标签详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tResourceTag:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tResourceTagService.selectTResourceTagById(id));
    }

    /**
     * 新增资源标签
     */
    @PreAuthorize("@ss.hasPermi('system:tResourceTag:add')")
    @Log(title = "资源标签", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TResourceTag tResourceTag) {
        return toAjax(tResourceTagService.insertTResourceTag(tResourceTag));
    }

    /**
     * 修改资源标签
     */
    @PreAuthorize("@ss.hasPermi('system:tResourceTag:edit')")
    @Log(title = "资源标签", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TResourceTag tResourceTag) {
        return toAjax(tResourceTagService.updateTResourceTag(tResourceTag));
    }

    /**
     * 删除资源标签
     */
    @PreAuthorize("@ss.hasPermi('system:tResourceTag:remove')")
    @Log(title = "资源标签", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tResourceTagService.deleteTResourceTagByIds(ids));
    }
}
