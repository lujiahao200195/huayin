package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TTeacherAdvantagesType;
import com.ruoyi.system.service.ITTeacherAdvantagesTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 教师个人优势分类Controller
 *
 * @author ruoyi
 * @date 2023-08-15
 */
@RestController
@RequestMapping("/system/tTeacherAdvantagesType")
public class TTeacherAdvantagesTypeController extends BaseController {
    @Autowired
    private ITTeacherAdvantagesTypeService tTeacherAdvantagesTypeService;

    /**
     * 查询教师个人优势分类列表
     */
    @PreAuthorize("@ss.hasPermi('system:tTeacherAdvantagesType:list')")
    @GetMapping("/list")
    public TableDataInfo list(TTeacherAdvantagesType tTeacherAdvantagesType) {
        startPage();
        List<TTeacherAdvantagesType> list = tTeacherAdvantagesTypeService.selectTTeacherAdvantagesTypeList(tTeacherAdvantagesType);
        return getDataTable(list);
    }

    /**
     * 导出教师个人优势分类列表
     */
    @PreAuthorize("@ss.hasPermi('system:tTeacherAdvantagesType:export')")
    @Log(title = "教师个人优势分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TTeacherAdvantagesType tTeacherAdvantagesType) {
        List<TTeacherAdvantagesType> list = tTeacherAdvantagesTypeService.selectTTeacherAdvantagesTypeList(tTeacherAdvantagesType);
        ExcelUtil<TTeacherAdvantagesType> util = new ExcelUtil<TTeacherAdvantagesType>(TTeacherAdvantagesType.class);
        util.exportExcel(response, list, "教师个人优势分类数据");
    }

    /**
     * 获取教师个人优势分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tTeacherAdvantagesType:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tTeacherAdvantagesTypeService.selectTTeacherAdvantagesTypeById(id));
    }

    /**
     * 新增教师个人优势分类
     */
    @PreAuthorize("@ss.hasPermi('system:tTeacherAdvantagesType:add')")
    @Log(title = "教师个人优势分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TTeacherAdvantagesType tTeacherAdvantagesType) {
        return toAjax(tTeacherAdvantagesTypeService.insertTTeacherAdvantagesType(tTeacherAdvantagesType));
    }

    /**
     * 修改教师个人优势分类
     */
    @PreAuthorize("@ss.hasPermi('system:tTeacherAdvantagesType:edit')")
    @Log(title = "教师个人优势分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TTeacherAdvantagesType tTeacherAdvantagesType) {
        return toAjax(tTeacherAdvantagesTypeService.updateTTeacherAdvantagesType(tTeacherAdvantagesType));
    }

    /**
     * 删除教师个人优势分类
     */
    @PreAuthorize("@ss.hasPermi('system:tTeacherAdvantagesType:remove')")
    @Log(title = "教师个人优势分类", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tTeacherAdvantagesTypeService.deleteTTeacherAdvantagesTypeByIds(ids));
    }
}
