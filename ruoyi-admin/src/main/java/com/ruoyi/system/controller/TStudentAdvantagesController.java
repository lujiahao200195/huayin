package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TStudentAdvantages;
import com.ruoyi.system.service.ITStudentAdvantagesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 学生优势科目Controller
 *
 * @author ruoyi
 * @date 2023-08-30
 */
@RestController
@RequestMapping("/system/tStudentAdvantages")
public class TStudentAdvantagesController extends BaseController {
    @Autowired
    private ITStudentAdvantagesService tStudentAdvantagesService;

    /**
     * 查询学生优势科目列表
     */
    @PreAuthorize("@ss.hasPermi('system:tStudentAdvantages:list')")
    @GetMapping("/list")
    public TableDataInfo list(TStudentAdvantages tStudentAdvantages) {
        startPage();
        List<TStudentAdvantages> list = tStudentAdvantagesService.selectTStudentAdvantagesList(tStudentAdvantages);
        return getDataTable(list);
    }

    /**
     * 导出学生优势科目列表
     */
    @PreAuthorize("@ss.hasPermi('system:tStudentAdvantages:export')")
    @Log(title = "学生优势科目", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TStudentAdvantages tStudentAdvantages) {
        List<TStudentAdvantages> list = tStudentAdvantagesService.selectTStudentAdvantagesList(tStudentAdvantages);
        ExcelUtil<TStudentAdvantages> util = new ExcelUtil<TStudentAdvantages>(TStudentAdvantages.class);
        util.exportExcel(response, list, "学生优势科目数据");
    }

    /**
     * 获取学生优势科目详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tStudentAdvantages:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tStudentAdvantagesService.selectTStudentAdvantagesById(id));
    }

    /**
     * 新增学生优势科目
     */
    @PreAuthorize("@ss.hasPermi('system:tStudentAdvantages:add')")
    @Log(title = "学生优势科目", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TStudentAdvantages tStudentAdvantages) {
        return toAjax(tStudentAdvantagesService.insertTStudentAdvantages(tStudentAdvantages));
    }

    /**
     * 修改学生优势科目
     */
    @PreAuthorize("@ss.hasPermi('system:tStudentAdvantages:edit')")
    @Log(title = "学生优势科目", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TStudentAdvantages tStudentAdvantages) {
        return toAjax(tStudentAdvantagesService.updateTStudentAdvantages(tStudentAdvantages));
    }

    /**
     * 删除学生优势科目
     */
    @PreAuthorize("@ss.hasPermi('system:tStudentAdvantages:remove')")
    @Log(title = "学生优势科目", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tStudentAdvantagesService.deleteTStudentAdvantagesByIds(ids));
    }
}
