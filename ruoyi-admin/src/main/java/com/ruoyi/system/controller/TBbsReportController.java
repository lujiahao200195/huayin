package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TBbsReport;
import com.ruoyi.system.service.ITBbsReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 举报Controller
 *
 * @author ruoyi
 * @date 2023-08-29
 */
@RestController
@RequestMapping("/system/tBbsReport")
public class TBbsReportController extends BaseController {
    @Autowired
    private ITBbsReportService tBbsReportService;

    /**
     * 查询举报列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsReport:list')")
    @GetMapping("/list")
    public TableDataInfo list(TBbsReport tBbsReport) {
        startPage();
        List<TBbsReport> list = tBbsReportService.selectTBbsReportList(tBbsReport);
        return getDataTable(list);
    }

    /**
     * 导出举报列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsReport:export')")
    @Log(title = "举报", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TBbsReport tBbsReport) {
        List<TBbsReport> list = tBbsReportService.selectTBbsReportList(tBbsReport);
        ExcelUtil<TBbsReport> util = new ExcelUtil<TBbsReport>(TBbsReport.class);
        util.exportExcel(response, list, "举报数据");
    }

    /**
     * 获取举报详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsReport:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tBbsReportService.selectTBbsReportById(id));
    }

    /**
     * 新增举报
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsReport:add')")
    @Log(title = "举报", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TBbsReport tBbsReport) {
        return toAjax(tBbsReportService.insertTBbsReport(tBbsReport));
    }

    /**
     * 修改举报
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsReport:edit')")
    @Log(title = "举报", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TBbsReport tBbsReport) {
        return toAjax(tBbsReportService.updateTBbsReport(tBbsReport));
    }

    /**
     * 删除举报
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsReport:remove')")
    @Log(title = "举报", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tBbsReportService.deleteTBbsReportByIds(ids));
    }
}
