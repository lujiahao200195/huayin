package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TBbsPostImage;
import com.ruoyi.system.service.ITBbsPostImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 帖子图片Controller
 *
 * @author ruoyi
 * @date 2023-08-28
 */
@RestController
@RequestMapping("/system/tBbsPostImage")
public class TBbsPostImageController extends BaseController {
    @Autowired
    private ITBbsPostImageService tBbsPostImageService;

    /**
     * 查询帖子图片列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsPostImage:list')")
    @GetMapping("/list")
    public TableDataInfo list(TBbsPostImage tBbsPostImage) {
        startPage();
        List<TBbsPostImage> list = tBbsPostImageService.selectTBbsPostImageList(tBbsPostImage);
        return getDataTable(list);
    }

    /**
     * 导出帖子图片列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsPostImage:export')")
    @Log(title = "帖子图片", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TBbsPostImage tBbsPostImage) {
        List<TBbsPostImage> list = tBbsPostImageService.selectTBbsPostImageList(tBbsPostImage);
        ExcelUtil<TBbsPostImage> util = new ExcelUtil<TBbsPostImage>(TBbsPostImage.class);
        util.exportExcel(response, list, "帖子图片数据");
    }

    /**
     * 获取帖子图片详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsPostImage:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tBbsPostImageService.selectTBbsPostImageById(id));
    }

    /**
     * 新增帖子图片
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsPostImage:add')")
    @Log(title = "帖子图片", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TBbsPostImage tBbsPostImage) {
        return toAjax(tBbsPostImageService.insertTBbsPostImage(tBbsPostImage));
    }

    /**
     * 修改帖子图片
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsPostImage:edit')")
    @Log(title = "帖子图片", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TBbsPostImage tBbsPostImage) {
        return toAjax(tBbsPostImageService.updateTBbsPostImage(tBbsPostImage));
    }

    /**
     * 删除帖子图片
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsPostImage:remove')")
    @Log(title = "帖子图片", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tBbsPostImageService.deleteTBbsPostImageByIds(ids));
    }
}
