package com.ruoyi.system.controller.backend.course;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.annotation.RepeatSubmit;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.bo.wechat.AddTCourse;
import com.ruoyi.system.domain.TCourse;
import com.ruoyi.system.service.ITCourseService;
import com.ruoyi.system.service.IWechatMiniService;
import com.ruoyi.system.service.backend.CourseService;
import com.ruoyi.system.vo.wechat.backend.CourseTeacherAndStudent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/system/backend/course")
public class CourseController extends BaseController {
    @Resource
    private CourseService courseService;
    @Resource
    private IWechatMiniService wechatMiniService;

    /**
     * 查询课程安排列表
     */
    @RepeatSubmit(interval = 1000, message = "请求过于频繁")
    @PostMapping("/course-list")
    public TableDataInfo list(TCourse tCourse) {
        startPage();
        List<TCourse> list = courseService.selectTCourseList(tCourse);
        return getDataTable(list);
    }

    /**
     * 导出课程安排列表
     */
    @PostMapping("/export")
    public void export(HttpServletResponse response,@RequestBody TCourse tCourse) {
        List<TCourse> list = courseService.selectTCourseList(tCourse);
        ExcelUtil<TCourse> util = new ExcelUtil<TCourse>(TCourse.class);
        util.exportExcel(response, list, "课程安排数据");
    }

    /**
     * 获取课程安排详细信息
     */
    @PostMapping("/course-info")
    public AjaxResult getInfo(@RequestBody Map<String, Long> parmMap) {
        Long id = parmMap.get("id");
        return AjaxResult.success(courseService.selectTCourseById(id));
    }

    /**
     * 新增课程安排
     */
    @PostMapping("/course-add")
    public AjaxResult add(@RequestBody AddTCourse addTCourse) {
        return toAjax(courseService.insertTCourse(addTCourse));
    }

    /**
     * 修改课程安排
     */
    @PostMapping("/course-edit")
    public AjaxResult edit(@RequestBody AddTCourse addTCourse) {
        return toAjax(courseService.updateTCourse(addTCourse));
    }

    /**
     * 删除课程安排
     */
    @PostMapping("/course-remove")
    public AjaxResult remove(@RequestBody Map<String, Long[]> jsCodeMap) {
        Long[] ids = jsCodeMap.get("ids");
        return toAjax(courseService.deleteTCourseByIds(ids));
    }

    /**
     * 课程表
     * @param jsonMap
     * @return
     */
    @PostMapping("/get-course")
    public AjaxResult getCourse(@RequestBody Map<String, String> jsonMap){
        String date = jsonMap.get("date");
        String id = jsonMap.get("id");
        String type = jsonMap.get("type");
        if(date==null||"".equals(date)){
            throw new RuntimeException("date参数错误");
        }
       return AjaxResult.success(courseService.getCourse(id,date,type));
    }

    @PostMapping("/get-student-and-teacher-course")
    public TableDataInfo getCourseTeacherAndStudent(@RequestBody Map<String, String> jsonMap){
        String type = jsonMap.get("type");
//        String pageSize = jsonMap.get("pageSize");
//        String pageNum = jsonMap.get("pageNum");

        startPage();
        List<CourseTeacherAndStudent> courseTeacherAndStudent = courseService.getCourseTeacherAndStudent(type);
        return getDataTable(courseTeacherAndStudent);

    }



//    //
//    @PostMapping("/get-student-course")
//    public AjaxResult wechatMiniStudentCourse(@RequestBody Map<String, String> jsonMap) {
//        String date = jsonMap.get("date");
//        String studentId = jsonMap.get("studentId");
//        if(date==null){
//            throw new RuntimeException("date参数错误");
//        }
//        return AjaxResult.success(courseService.wechatMiniStudentCourse(studentId,date));
//    }
//
//    //
//    @PostMapping("/get-teacher-course")
//    public AjaxResult wechatMiniTeacherCourse(@RequestBody Map<String, String> jsonMap) {
//        String date = jsonMap.get("date");
//        String teacherId = jsonMap.get("teacherId");
//        if(date==null){
//            throw new RuntimeException("date参数错误");
//        }
//        return AjaxResult.success(courseService.wechatMiniTeacherCourse(teacherId,date));
//    }



}
