package com.ruoyi.system.controller.backend.teacher;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TTeacher;
import com.ruoyi.system.service.backend.TeacherService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 教师Controller
 */
@RestController
@RequestMapping("/system/backend/teacher")
public class TeacherController extends BaseController {
    @Resource
    private TeacherService teacherService;

    /**
     * 查询教师列表
     */
    @PostMapping("/teacher-list")
    public TableDataInfo list(@RequestBody TTeacher tTeacher) {
        startPage();
        List<TTeacher> list = teacherService.selectTTeacherList(tTeacher);
        return getDataTable(list);
    }

    /**
     * 导出教师列表
     */
    @PostMapping("/export")
    public void export(HttpServletResponse response, @RequestBody TTeacher tTeacher) {
        List<TTeacher> list = teacherService.selectTTeacherList(tTeacher);
        ExcelUtil<TTeacher> util = new ExcelUtil<TTeacher>(TTeacher.class);
        util.exportExcel(response, list, "教师数据");
    }

    /**
     * 获取教师详细信息
     */
    @PostMapping("teacher-info")
    public AjaxResult getInfo(@RequestBody Map<String, Long> parmMap) {
        Long id = parmMap.get("id");
        return AjaxResult.success(teacherService.selectTTeacherById(id));
    }

    /**
     * 新增教师
     */
    @PostMapping("teacher-add")
    public AjaxResult add(@RequestBody TTeacher tTeacher) {
        return toAjax(teacherService.insertTTeacher(tTeacher));
    }

    /**
     * 修改教师
     */
    @PostMapping("teacher-edit")
    public AjaxResult edit(@RequestBody TTeacher tTeacher) {
        return toAjax(teacherService.updateTTeacher(tTeacher));
    }

    /**
     * 删除教师
     */
    @PostMapping("teacher-remove")
    public AjaxResult remove(@RequestBody Map<String, Long[]> parmMap) {
        Long[] ids = parmMap.get("ids");
        return toAjax(teacherService.deleteTTeacherByIds(ids));
    }

    /**
     * 后台管理修改教师审核状态
     *
     * @param jsCodeMap
     * @return
     */
//    @BgLog(methodType = "修改", moduleContent = "修改教师审核状态")
    @PostMapping("/change-teacher-state")
    public AjaxResult changeTeacherState(@RequestBody Map<String, String> jsCodeMap) {
        //教师表id
        String id = jsCodeMap.get("id");
        //要修改成的状态
        String stateStr = jsCodeMap.get("state");
        //原因
        String stateResult = jsCodeMap.get("stateResult");
        return AjaxResult.success(teacherService.changeTeacherState(id, stateStr, stateResult));
    }

}
