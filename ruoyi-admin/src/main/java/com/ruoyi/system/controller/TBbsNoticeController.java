package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TBbsNotice;
import com.ruoyi.system.service.ITBbsNoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 版块公告Controller
 *
 * @author ruoyi
 * @date 2023-08-28
 */
@RestController
@RequestMapping("/system/tBbsNotice")
public class TBbsNoticeController extends BaseController {
    @Autowired
    private ITBbsNoticeService tBbsNoticeService;

    /**
     * 查询版块公告列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsNotice:list')")
    @GetMapping("/list")
    public TableDataInfo list(TBbsNotice tBbsNotice) {
        startPage();
        List<TBbsNotice> list = tBbsNoticeService.selectTBbsNoticeList(tBbsNotice);
        return getDataTable(list);
    }

    /**
     * 导出版块公告列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsNotice:export')")
    @Log(title = "版块公告", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TBbsNotice tBbsNotice) {
        List<TBbsNotice> list = tBbsNoticeService.selectTBbsNoticeList(tBbsNotice);
        ExcelUtil<TBbsNotice> util = new ExcelUtil<TBbsNotice>(TBbsNotice.class);
        util.exportExcel(response, list, "版块公告数据");
    }

    /**
     * 获取版块公告详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsNotice:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tBbsNoticeService.selectTBbsNoticeById(id));
    }

    /**
     * 新增版块公告
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsNotice:add')")
    @Log(title = "版块公告", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TBbsNotice tBbsNotice) {
        return toAjax(tBbsNoticeService.insertTBbsNotice(tBbsNotice));
    }

    /**
     * 修改版块公告
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsNotice:edit')")
    @Log(title = "版块公告", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TBbsNotice tBbsNotice) {
        return toAjax(tBbsNoticeService.updateTBbsNotice(tBbsNotice));
    }

    /**
     * 删除版块公告
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsNotice:remove')")
    @Log(title = "版块公告", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tBbsNoticeService.deleteTBbsNoticeByIds(ids));
    }
}
