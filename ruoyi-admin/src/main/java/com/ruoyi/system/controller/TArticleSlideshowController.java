package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.TArticleSlideshow;
import com.ruoyi.system.service.ITArticleSlideshowService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 轮播图Controller
 * 
 * @author ruoyi
 * @date 2024-05-11
 */
@RestController
@RequestMapping("/huayin/slideshow")
public class TArticleSlideshowController extends BaseController
{
    @Autowired
    private ITArticleSlideshowService tArticleSlideshowService;

    /**
     * 查询轮播图列表
     */
    @PreAuthorize("@ss.hasPermi('huayin:slideshow:list')")
    @GetMapping("/list")
    public TableDataInfo list(TArticleSlideshow tArticleSlideshow)
    {
        startPage();
        List<TArticleSlideshow> list = tArticleSlideshowService.selectTArticleSlideshowList(tArticleSlideshow);
        return getDataTable(list);
    }

    /**
     * 导出轮播图列表
     */
    @PreAuthorize("@ss.hasPermi('huayin:slideshow:export')")
    @Log(title = "轮播图", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TArticleSlideshow tArticleSlideshow)
    {
        List<TArticleSlideshow> list = tArticleSlideshowService.selectTArticleSlideshowList(tArticleSlideshow);
        ExcelUtil<TArticleSlideshow> util = new ExcelUtil<TArticleSlideshow>(TArticleSlideshow.class);
        util.exportExcel(response, list, "轮播图数据");
    }

    /**
     * 获取轮播图详细信息
     */
    @PreAuthorize("@ss.hasPermi('huayin:slideshow:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(tArticleSlideshowService.selectTArticleSlideshowById(id));
    }

    /**
     * 新增轮播图
     */
    @PreAuthorize("@ss.hasPermi('huayin:slideshow:add')")
    @Log(title = "轮播图", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TArticleSlideshow tArticleSlideshow)
    {
        return toAjax(tArticleSlideshowService.insertTArticleSlideshow(tArticleSlideshow));
    }

    /**
     * 修改轮播图
     */
    @PreAuthorize("@ss.hasPermi('huayin:slideshow:edit')")
    @Log(title = "轮播图", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TArticleSlideshow tArticleSlideshow)
    {
        return toAjax(tArticleSlideshowService.updateTArticleSlideshow(tArticleSlideshow));
    }

    /**
     * 删除轮播图
     */
    @PreAuthorize("@ss.hasPermi('huayin:slideshow:remove')")
    @Log(title = "轮播图", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tArticleSlideshowService.deleteTArticleSlideshowByIds(ids));
    }
}
