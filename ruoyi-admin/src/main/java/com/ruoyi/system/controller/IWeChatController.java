package com.ruoyi.system.controller;


import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.bo.wechat.*;
import com.ruoyi.system.common.abs.WxMiniLog;
import com.ruoyi.system.common.tools.NTools;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.mapper.TTeacherCertificatesMapper;
import com.ruoyi.system.mapper.WechatMiniMapper;
import com.ruoyi.system.service.*;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * 微信相关接口对接
 */
@RestController
@RequestMapping("/system/iWeChatController")
public class IWeChatController extends BaseController {


    @Resource
    private IWechatMiniService wechatMiniService;

    @Resource
    private ITSubjectService subjectService;

    @Resource
    private ITSlideshowService slideshowService;

    @Resource
    private ITPhaseOfStudyingService phaseOfStudyingService;

    @Resource
    private ITTeacherAdvantagesTypeService teacherAdvantagesTypeService;

    @Resource
    private NTools nTools;


    @Resource
    private TTeacherCertificatesMapper tTeacherCertificatesMapper;

    @Resource
    private WechatMiniMapper wechatMiniMapper;

    @Resource
    private ITQuestionsService itQuestionsService;


    /**
     * 微信小程序登入
     */
    @PostMapping("/wechat-mini-login")
    public AjaxResult WeChatMiniLogin(@RequestBody Map<String, String> jsCodeMap) {// @RequestBody String jsCode
        return  AjaxResult.success("登入成功", wechatMiniService.selectTUserByWechatMiniOpenId(jsCodeMap));
    }


    /**
     * 微信小程序学生报名
     *
     * @return
     */
    @WxMiniLog(methodDesc = "新增", moduleName = "小程序学生报名")
    @PostMapping("/wechat-mini-student-sign-up")
    public AjaxResult studentSignUp(@RequestBody StudentSignUp studentSignUp) {
        return AjaxResult.success("报名成功", wechatMiniService.studentSignUp(studentSignUp));
    }

    /**
     * 订阅学生报名成功  审核信息
     * 学生id
     *
     * @return
     */
    @PostMapping("/wechat-mini-student-sign-up-msg")
    public AjaxResult studentSignUpMsg(@RequestBody Map<String, String> map) {
        String studentId = map.get("studentId");
        return AjaxResult.success(wechatMiniService.studentSignUpMsg(studentId));
    }

    /**
     * 获取所有审核通过以及自己创建的科目
     *
     * @return
     */
    @PostMapping("/wechat-mini-subject-list-and-own-create")
    public AjaxResult wechatMiniSubjectListAndOwnCreate() {
        return AjaxResult.success(wechatMiniService.wechatMiniSubjectListAndOwnCreate());
    }

    /**
     * 获取学段
     *
     * @return
     */
    @PostMapping("/wechat-mini-study-phase-list")
    public AjaxResult phaseList() {
        return AjaxResult.success(phaseOfStudyingService.selectTPhaseOfStudyingList(new TPhaseOfStudying()));
    }


    /**
     * 首页轮播图图片资源
     *
     * @return
     */
    @PostMapping("/wechat-mini-study-slideshow-list")
    public AjaxResult slideshowList() {
        return AjaxResult.success(wechatMiniService.selectTSlideshowList());
    }

    /**
     * 精选文章轮播图
     */
    @PostMapping("/wechat-mini-study-article-slideshow-list")
    public AjaxResult slideArticleShowList() {
        return AjaxResult.success(wechatMiniService.selectArticleTSlideshowList());
    }
    /**
     * 首页通告展示数据, 最新16学生个报名数据
     *
     * @return
     */
    @PostMapping("/wechat-mini-last-student-list")
    public AjaxResult lastStudentList() {
        return AjaxResult.success(wechatMiniService.lastStudentList());
    }

    /**
     * 微信小程序首页文章获取
     * 学校平台资讯文章
     *
     * @return
     */
    @PostMapping("/wechat-mini-index-article-list")
    public AjaxResult indexArticleList() {
        return AjaxResult.success(wechatMiniService.indexArticleList());
    }

    /**
     * 微信小程序-个人更新页面-获取用户信息
     */
    @WxMiniLog(methodDesc = "查询", moduleName = "用户获取个人信息")
    @PostMapping("/wechat-mini-get-userinfo")
    public AjaxResult getUserinfo() {
        return AjaxResult.success(wechatMiniService.getUserInfo());
    }

    /**
     * 微信小程序获取学生个人信息
     */
    @WxMiniLog(methodDesc = "查询", moduleName = "获取学生个人信息")
    @PostMapping("/wechat-mini-get-student-info")
    public AjaxResult getStudentInfo(@RequestBody Map<String, String> parmMap) {
        String openId = parmMap.get("openId");
        return AjaxResult.success(wechatMiniService.getStudentInfo(openId));
    }

    /**
     * 教师报名系统
     */
    @WxMiniLog(methodDesc = "新增", moduleName = "小程序教师报名")
    @PostMapping("/wechat-mini-teacher-sign-up")
    public AjaxResult teacherSignUp(@RequestBody TeacherSignUp teacherSignUp) {
        return AjaxResult.success("报名成功", wechatMiniService.teacherSignUp(teacherSignUp));
    }

    /**
     * 订阅教师报名成功  审核信息
     *
     * @return
     */
    @PostMapping("/wechat-mini-teacher-sign-up-msg")
    public AjaxResult teacherSignUpMsg(@RequestBody Map<String, String> map) {
        String teacherId = map.get("teacherId");
        return AjaxResult.success(wechatMiniService.teacherSignUpMsg(teacherId));
    }


    /**
     * 小程序查询可提供选择的教师优势
     *
     * @return
     */
    @PostMapping("/wechat-mini-advantage-type-list")
    public AjaxResult wechatMiniAdvantageTypeList() {
        return AjaxResult.success(teacherAdvantagesTypeService.selectTTeacherAdvantagesTypeList(new TTeacherAdvantagesType()));
    }


    /**
     * 新增课程
     *
     * @param parmMap
     * @return
     */
    @WxMiniLog(methodDesc = "新增", moduleName = "教师新增课程")
    @PostMapping("/wechat-mini-add-subject")
    public AjaxResult wechatMiniAddSubject(@RequestBody Map<String, String> parmMap) {
        TUser tUserByMiniOpenId = wechatMiniService.getTUserByMiniOpenId(nTools.getMiniOpenId());
        TSubject tSubject = new TSubject();
        tSubject.setSubjectName(parmMap.get("subjectName"));
        tSubject.setSubjectSort(10L);
        tSubject.setAuditStatus("待处理");
        tSubject.setCreateUserId(tUserByMiniOpenId.getId());
        subjectService.insertTSubject(tSubject);
        tSubject = subjectService.selectTSubjectList(tSubject).get(0);
        return AjaxResult.success(tSubject);
    }

    /**
     * 教师证书数据
     *
     * @param addTeacherCertificate
     * @return
     */
    @WxMiniLog(methodDesc = "新增", moduleName = "教师新增证书")
    @PostMapping("/wechat-mini-add-teacher-certificate")
    public AjaxResult wechatMiniAddTeacherCertificate(@RequestBody AddTeacherCertificate addTeacherCertificate) {
        return AjaxResult.success(wechatMiniService.wechatMiniAddTeacherCertificate(addTeacherCertificate));
    }

    /**
     * 教师查询自己的证书列表
     *
     * @param parmMap
     * @return
     */
    @WxMiniLog(methodDesc = "查询", moduleName = "教师查询证书列表")
    @PostMapping("/wechat-mini-teacher-own-certificate-list")
    public AjaxResult wechatMiniTeacherOwnCertificateList(@RequestBody Map<String, String> parmMap) {
        String openId = parmMap.get("openId");
        String teacherIdStr = parmMap.get("teacherId");
        Long teacherId = Long.parseLong(teacherIdStr);
        return AjaxResult.success(wechatMiniService.wechatMiniTeacherOwnCertificateList(openId, teacherId));
    }


    /**
     * 教师查询自己的所有证书包含待审核, 审核通过, 审核未通过的所有证书
     *
     * @param parmMap
     * @return
     */
    @WxMiniLog(methodDesc = "查询", moduleName = "教师所有查询证书列表")
    @PostMapping("/wechat-mini-teacher-all-certificate-list")
    public AjaxResult wechatMiniTeacherAllCertificateList(@RequestBody Map<String, String> parmMap) {
        String openId = parmMap.get("openId");
        String teacherIdStr = parmMap.get("teacherId");
        Long teacherId = Long.parseLong(teacherIdStr);
        return AjaxResult.success(wechatMiniService.wechatMiniTeacherAllCertificateList(openId, teacherId));
    }

    /**
     * 微信小程序首页报名人数
     *
     * @return
     */
    @PostMapping("/wechat-mini-index-signUp-number")
    public AjaxResult wechatMiniIndexSignUpNumber() {
        return AjaxResult.success(wechatMiniService.wechatMiniIndexSignUpNumber());
    }

    /**
     * 通过科目id获取文章列表
     *
     * @return
     */
//    @PostMapping("/wechat-mini-subject-article-list")
//    public AjaxResult wechatMiniSubjectArticleList(@RequestBody Map<String, Long> parmMap) {
//        Long subjectId = parmMap.get("subjectId");
//        return AjaxResult.success(wechatMiniService.wechatMiniSubjectArticleList(subjectId));
//    }


    /**
     * 通过科目id获取文章列表
     *
     * @return
     */
//    @GetMapping("/wechat-mini-subject-article-list")
//    //Map<String, Long> parmMap
//    public AjaxResult wechatMiniSubjectArticleList(QueryArticleDo queryArticleDo) {
//        startPage();
//        Integer subjectId = queryArticleDo.getSubjectId();
//        List<TArticle> list = wechatMiniService.wechatMiniSubjectArticleList(subjectId.longValue());
//        return AjaxResult.success(getDataTable(list));
//    }
    @GetMapping("/wechat-mini-subject-article-list")
    //Map<String, Long> parmMap
    public AjaxResult wechatMiniSubjectArticleList(QueryArticleDo queryArticleDo) {
        startPage();
        Integer subjectId = queryArticleDo.getSubjectId();
        List<TArticle> list = wechatMiniService.wechatMiniSubjectArticleList1(subjectId.longValue(),queryArticleDo.getArticleTwoTypeId().longValue());
        return AjaxResult.success(getDataTable(list));
    }

    @GetMapping("/wechat-mini-article-type-list")
    public AjaxResult wechatMiniArticleTypeList(){
        return AjaxResult.success(wechatMiniService.wechatMiniArticleTypeList());
    }

    /**
     * 获取首页科目列表
     *
     * @return
     */
    @PostMapping("/wechat-mini-index-subject-list")
    public AjaxResult wechatMiniIndexSubjectList() {
        return AjaxResult.success(wechatMiniService.wechatMiniIndexSubjectList());
    }

    /**
     * 获取证书列表
     *
     * @return
     */
    @PostMapping("/wechat-mini-certificate-list")
    public AjaxResult wechatMiniCertificateList() {
        return AjaxResult.success(wechatMiniService.wechatMiniCertificateList());
    }

    /**
     * 微信小程序获取全部年段表
     *
     * @return
     */
    @PostMapping("/wechat-mini-phaseOfStudying-list")
    public AjaxResult wechatMiniPhaseOfStudyingList() {
        return AjaxResult.success(phaseOfStudyingService.selectTPhaseOfStudyingList(new TPhaseOfStudying()));
    }

    /**
     * 获取正常的工作室列表(公共)
     *
     * @return
     */
    @PostMapping("/wechat-mini-public-work-room-list")
    public AjaxResult wechatMiniPublicWorkRoomList() {
        return AjaxResult.success(wechatMiniService.wechatMiniPublicWorkRoomList());
    }

    /**
     * 获取用户自己加入的工作室列表
     *
     * @return
     */
    @PostMapping("/wechat-mini-self-join-work-room-list")
    public AjaxResult wechatMiniSelfJoinWorkRoomList() {
        return AjaxResult.success(wechatMiniService.wechatMiniSelfJoinWorkRoomList());
    }

    /**
     * 创建工作室
     *
     * @param createWorkRoom
     * @return
     */
    @PostMapping("/wechat-mini-create-work-room")
    public AjaxResult wechatMiniCreateWorkRoom(@RequestBody CreateWorkRoom createWorkRoom) {
        return AjaxResult.success(wechatMiniService.wechatMiniCreateWorkRoom(createWorkRoom));
    }

    /**
     * 学生/教师/用户申请加入工作室
     *
     * @param joinWorkRoom
     * @return
     */
    @PostMapping("/wechat-mini-join-work-room")
    public AjaxResult wechatMiniJoinWorkRoom(@RequestBody JoinWorkRoom joinWorkRoom) {
        return AjaxResult.success(wechatMiniService.wechatMiniJoinWorkRoom(joinWorkRoom));
    }

    /**
     * 创建工作室邀请码
     *
     * @return
     */
    @PostMapping("/wechat-mini-create-work-invite-code")
    public AjaxResult wechatMiniCreateWorkInviteCode(@RequestBody Map<String, String> parmMap) {
        //创建邀请码的教师id
//        String teacherId = parmMap.get("teacherId");
        //创建邀请码的工作室id
        String workRoomId = parmMap.get("workRoomId");
        //邀请码的有效期
        String time = parmMap.get("time");
        return AjaxResult.success(wechatMiniService.wechatMiniCreateWorkInviteCode(workRoomId, Integer.parseInt(time)));
    }

    /**
     * 删除工作室邀请码
     *
     * @return
     */
    @PostMapping("/wechat-mini-delete-work-invite-code")
    public AjaxResult wechatMiniDeleteWorkInviteCode(@RequestBody Map<String, String> parmMap) {
//        //删除邀请码的教师id
//        String teacherId = parmMap.get("teacherId");
        //删除邀请码的邀请码id
        String workRoomId = parmMap.get("workRoomInviteId");
        return AjaxResult.success(wechatMiniService.wechatMiniDeleteWorkInviteCode(workRoomId));
    }

    /**
     * 查询工作室的邀请码列表
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-work-invite-code-list")
    public AjaxResult wechatMiniWorkInviteCodeList(@RequestBody Map<String, String> parmMap) {
        String workRoomId = parmMap.get("workRoomId");
        if (workRoomId == null) {
            throw new RuntimeException("工作室id为空");
        }
        return AjaxResult.success(wechatMiniService.wechatMiniWorkInviteCodeList(workRoomId));
    }

    /**
     * 使用工作室邀请码加入工作室
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-use-work-invite-code")
    public AjaxResult wechatMiniUseInviteCode(@RequestBody Map<String, String> parmMap) {
        String code = parmMap.get("code");
        String type = "教师";
        return AjaxResult.success(wechatMiniService.wechatMiniUseInviteCode(type, code));
    }

    /**
     * 获取工作室当前在线状态
     *
     * @return
     */
    @PostMapping("/wechat-mini-work-room-online-teacher")
    public AjaxResult wechatMiniWorkRoomOnline(@RequestBody Map<String, String> jsonMap) {
        String workRoomId = jsonMap.get("workRoomId");
        return AjaxResult.success(wechatMiniService.wechatMiniWorkRoomOnline(workRoomId));
    }


    /**
     * 获取消息数量列表
     *
     * @return
     */
    @PostMapping("/wechat-mini-work-room-show-msg")
    public AjaxResult wechatMiniWorkRoomMsg() {
        return AjaxResult.success(wechatMiniService.wechatMiniWorkRoomMsg());
    }


    /**
     * 获取工作室教师个人信息卡列表
     * 有多个教师个人信息卡
     *
     * @param jsonMap
     * @return
     */
    @RequestMapping("/wechat-mini-work-teacher-info")
    public AjaxResult wechatMiniWorkRoomTeacherInfo(@RequestBody Map<String, String> jsonMap) {
        String workRoomId = NTools.mustData(jsonMap.get("workRoomId"), String.class, "工作室信息错误");
        return AjaxResult.success(wechatMiniService.wechatMiniWorkRoomTeacherInfo(workRoomId));
    }

    /**
     * 教师个人信息卡详情
     *
     * @param jsonMap
     * @return
     */
    @PostMapping("/wechat-mini-work-teacher-card-details")
    public AjaxResult wechatMiniWorkRoomTeaCardDetails(@RequestBody Map<String, Long> jsonMap) {
        Long id = jsonMap.get("id");
        return AjaxResult.success(wechatMiniService.wechatMiniWorkRoomTeaCardDetails(id));
    }


    /**
     * 新增工作室教师个人信息卡
     *
     * @param workRoomTeacherInfo
     * @return
     */
    @PostMapping("/wechat-mini-work-insert-teacher-info")
    public AjaxResult wechatMiniInsertWorkRoomTeacherInfo(@RequestBody WorkRoomTeacherInfo workRoomTeacherInfo) {
        return AjaxResult.success(wechatMiniService.wechatMiniInsertWorkRoomTeacherInfo(workRoomTeacherInfo));
    }

    /**
     * 删除工作室教师个人信息卡
     *
     * @param jsonMap
     * @return
     */
    @RequestMapping("/wechat-mini-work-delete-teacher-info")
    public AjaxResult wechatMiniDeleteWorkRoomTeacherInfo(@RequestBody Map<String, String> jsonMap) {
        String workRoomId = jsonMap.get("workRoomId");
        String id = jsonMap.get("id");//表id
        return AjaxResult.success(wechatMiniService.wechatMiniDeleteWorkRoomTeacherInfo(id, workRoomId));
    }

    /**
     * 修改工作室教师个人信息卡
     *
     * @param
     * @return
     */
    @RequestMapping("/wechat-mini-work-update-teacher-info")
    public AjaxResult wechatMiniUpdateWorkRoomTeacherInfo(@RequestBody WorkRoomTeacherInfo workRoomTeacherInfo) {
        return AjaxResult.success(wechatMiniService.wechatMiniUpdateWorkRoomTeacherInfo(workRoomTeacherInfo));
    }

//    /**
//     * 修改工作室展示文章ID或更改整个工作室
//     *
//     * @param workRoomInfo
//     * @return
//     */
//    @RequestMapping("/wechat-mini-work-update-info")
//    public AjaxResult wechatMiniUpdateWorkRoomInfo(@RequestBody WorkRoomInfo workRoomInfo) {
//        return AjaxResult.success(wechatMiniService.wechatMiniUpdateWorkRoomInfo(workRoomInfo));
//    }

    /**
     * 工作室模块搜索
     *
     * @param workRoomSearch
     * @return
     */
    @PostMapping("/wechat-mini-work-room-search")
    public AjaxResult wechatMiniWorkRoomSearch(@RequestBody WorkRoomSearch workRoomSearch) {
        return AjaxResult.success(wechatMiniService.wechatMiniWorkRoomSearch(workRoomSearch));
    }


    /**
     * 通过科目查询相应的文章列表
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-article-list-by-subject-id")
    public AjaxResult wechatMiniArticleListBySubjectId(@RequestBody Map<String, Long> parmMap) {
        Long subjectId = parmMap.get("subjectId");
        if (subjectId == null) {
            throw new RuntimeException("科目数据异常");
        }
        return AjaxResult.success(wechatMiniService.wechatMiniArticleListBySubjectId(subjectId));
    }

    /**
     * 微信小程序工作室申请审核列表查询
     *
     * @param jsonmap
     * @return
     */
    @PostMapping("/wechat-mini-audit-join-work-room-list")
    public AjaxResult wechatMiniAuditJoinWorkRoomList(@RequestBody Map<String, String> jsonmap) {
        String workRoomId = jsonmap.get("workRoomId");
        return AjaxResult.success(wechatMiniService.wechatMiniAuditJoinWorkRoomList(workRoomId));
    }

    /**
     * 微信小程序审核工作室加入请求
     *
     * @param jsonmap
     * @return
     */
    @PostMapping("/wechat-mini-audit-join-work-room")
    public AjaxResult wechatMiniAuditJoinWorkRoom(@RequestBody Map<String, String> jsonmap) {
        String id = jsonmap.get("id");//审核表id
        String auditStatus = jsonmap.get("auditStatus");//审核状态
        String auditResult = jsonmap.get("auditResult");//审核结果原因

        return AjaxResult.success(wechatMiniService.wechatMiniAuditJoinWorkRoom(id, auditStatus, auditResult));
    }

    /**
     * 修改用户资料
     *
     * @param changeUserInfo
     * @return
     */
    @PostMapping("/wechat-mini-change-user-info")
    public AjaxResult wechatMiniChangeUserInfo(@RequestBody ChangeUserInfo changeUserInfo) {
        return AjaxResult.success(wechatMiniService.wechatMiniChangeUserInfo(changeUserInfo));
    }

    /**
     * 微信小程序用户获取与指定工作室的历史消息
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-message-history-list-by-work-room")
    public AjaxResult wechatMiniMessageHistoryListByWorkRoom(@RequestBody Map<String, Long> parmMap) {
        Long workRoomId = parmMap.get("workRoomId");
        if (workRoomId == null) {
            throw new RuntimeException("工作室信息异常");
        }
        return AjaxResult.success(wechatMiniService.wechatMiniMessageHistoryListByWorkRoom(workRoomId));
    }

    /**
     * 教师查询自己文章的接口
     *
     * @return
     */
// todo   @PostMapping("/wechat-mini-message-Oneself-Article")
    public AjaxResult wechatMiniOneselfArticle() {
        return AjaxResult.success(wechatMiniService.wechatMiniOneselfArticle());
    }


//    /**
//     * 查询自己/工作室文章
//     *
//     * @return
//     */
//    @PostMapping("/wechat-mini-own-Article")
//    public AjaxResult wechatMiniOwnArticle(@Nullable @RequestBody Map<String, Long> parmMap) {
//        Long workRoomId = null;
//        if (parmMap != null) {
//            workRoomId = parmMap.get("workRoomId");
//        }
//
//        return AjaxResult.success(wechatMiniService.selectOwnArticleList(workRoomId));
//    }

    /**
     * 查询工作室展示的文章
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-work-room-show-article")
    public AjaxResult wechatMiniWorkRoomShowArticle(@RequestBody Map<String, Long> parmMap) {
        Long workRoomId = parmMap.get("workRoomId");
        if (workRoomId == null) {
            throw new RuntimeException("工作室信息异常");
        }
        return AjaxResult.success(wechatMiniService.wechatMiniWorkRoomShowArticle(workRoomId));
    }


    /**
     * 工作室获取成员列表
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-work-room-get-member-list")
    public AjaxResult wechatMiniWorkRoomGetMemberList(@RequestBody Map<String, Long> parmMap) {
        Long workRoomId = parmMap.get("workRoomId");
        if (workRoomId == null) {
            throw new RuntimeException("工作室信息异常");
        }
        return AjaxResult.success(wechatMiniService.selectWorkRoomGetMemberList(workRoomId));
    }

    /**
     * 判断自己是否加入工作室
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-check-join-or-not-work-room")
    public AjaxResult wechatMiniCheckJoinOrNotWorkRoom(@RequestBody Map<String, Long> parmMap) {
        Long workRoomId = parmMap.get("workRoomId");
        if (workRoomId == null) {
            throw new RuntimeException("工作室信息错误");
        }
        return AjaxResult.success(wechatMiniService.wechatMiniCheckJoinOrNotWorkRoom(workRoomId, "教师"));
    }


    /**
     * 工作室查询成员简介
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-work-room-member-details")
    public AjaxResult wechatMiniWorkRoomMemberDetails(@RequestBody Map<String, String> parmMap) {
        String id = parmMap.get("id");
        String type = parmMap.get("type");
        return AjaxResult.success(wechatMiniService.wechatMiniWorkRoomMemberDetails(id, type));
    }

    /**
     * 退出工作室
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-quit-work-room")
    public AjaxResult wechatMiniQuitWorkRoom(@RequestBody Map<String, String> parmMap) {
        String workRoomId = parmMap.get("workRoomId");
        String joinType = null;
        String id = null;
        return AjaxResult.success(wechatMiniService.wechatMiniQuitWorkRoom(id, workRoomId, joinType));
    }


    /**
     * 自己创建的工作室列表
     *
     * @return
     */
    @PostMapping("/wechat-mini-own-create-work-room")
    public AjaxResult wechatMiniOwnCreateWorkRoom() {
        return AjaxResult.success(wechatMiniService.wechatMiniOwnCreateWorkRoom());
    }


    /**
     * 测试
     *
     * @return
     * @throws IOException
     */
//    @PostMapping("/test")
//    public AjaxResult test(@Validated @RequestBody JoinWorkRoom joinWorkRoom) {
//        return AjaxResult.success(joinWorkRoom);
//    }
    @PostMapping("/test")
    public AjaxResult test() {

//        PageHelper.startPage(2, 3);
//        List<TTeacherCertificates> tTeacherCertificates = tTeacherCertificatesMapper.selectTTeacherCertificatesList(new TTeacherCertificates());
//        System.out.println(tTeacherCertificates);
//        PageInfo<TTeacherCertificates> pageInfo = new PageInfo<>(tTeacherCertificates);
//        System.out.println(pageInfo);
//        System.out.println(pageInfo.getNextPage());  //下一页
//        System.out.println(pageInfo.getPrePage());  //上一页
//        System.out.println(pageInfo.getPageNum());  //当前页
//        System.out.println(pageInfo.getPageSize());  //每页多少条
//        System.out.println(pageInfo.getSize());  //当前页的条数
//        System.out.println(pageInfo.getTotal());  //总条数
//        System.out.println(pageInfo.getPages());  //总页数


        String tTeacher = wechatMiniMapper.selectOpenIdByTableId("t_teacher", 40L);

        return AjaxResult.success(tTeacher);
    }

    /**
     * 用户修改教师教师个人信息
     *
     * @param selfUpdateTeacherInfoDo
     * @return
     */
    @PostMapping("/wechat-mini-self-update-teacher-info")
    public AjaxResult wechatMiniSelfUpdateTeacherInfo(@RequestBody SelfUpdateTeacherInfoDo selfUpdateTeacherInfoDo) {
        return AjaxResult.success(wechatMiniService.wechatMiniSelfUpdateTeacherInfo(selfUpdateTeacherInfoDo));
    }


    /**
     * 用户修改个人学生信息
     *
     * @param selfUpdateStudentInfoDo
     * @return
     */
    @PostMapping("/wechat-mini-self-update-student-info")
    public AjaxResult wechatMiniSelfUpdateStudentInfo(@RequestBody SelfUpdateStudentInfoDo selfUpdateStudentInfoDo) {
        return AjaxResult.success(wechatMiniService.wechatMiniSelfUpdateStudentInfo(selfUpdateStudentInfoDo));
    }

    /**
     * 查看报名记录
     *
     * @return
     */
    @PostMapping("/wechat-mini-sign-up-record")
    public AjaxResult wechatMiniSignUpRecord() {
        return AjaxResult.success(wechatMiniService.wechatMiniSignUpRecord());
    }

    /**
     * 学生注意事项分类的文章
     *
     * @return
     */
    @PostMapping("/wechat-mini-student-look-out")
    public AjaxResult wechatMiniStudentLookOut() {
        return AjaxResult.success(wechatMiniService.wechatMiniStudentLookOut());
    }

    /**
     * 教师注意事项分类的文章
     *
     * @return
     */
    @PostMapping("/wechat-mini-teacher-look-out")
    public AjaxResult wechatMiniTeacherLookOut() {
        return AjaxResult.success(wechatMiniService.wechatMiniTeacherLookOut());
    }


    /**
     * 显示教师报名数据 审核
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-show-teacher-sign-up-info")
    public AjaxResult wechatMiniShowTeacherSignUpInfo(@RequestBody Map<String, String> parmMap) {
        String token = parmMap.get("token");
        if (token == null) {
            throw new RuntimeException("令牌错误");
        }
        return AjaxResult.success(wechatMiniService.ShowTeacherSignUpInfo(token));
    }

    /**
     * 显示学生报名数据 审核
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-show-student-sign-up-info")
    public AjaxResult wechatMiniShowStudentSignUpInfo(@RequestBody Map<String, String> parmMap) {
        String token = parmMap.get("token");
        if (token == null) {
            throw new RuntimeException("令牌错误");
        }
        return AjaxResult.success(wechatMiniService.ShowStudentSignUpInfo(token));
    }

    /**
     * 首页根据分类ID获取贴子
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-bbs-index-type-list")
    public AjaxResult bbsIndexGetArticleList(@RequestBody Map<String, Integer> parmMap) {
        Integer typeId = parmMap.get("typeId");
        Integer page = parmMap.get("page");
        Integer size = parmMap.get("size");
        if (typeId == null) {
            throw new RuntimeException("typeId为空");
        }
        return AjaxResult.success(wechatMiniService.bbsGetArticleList(page, size, Long.valueOf(typeId)));
    }

    /**
     * 用户阅读文章
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-user-article-read")
    public AjaxResult wechatMiniUserArticleRead(@RequestBody Map<String, Long> parmMap) {
        Long articleId = parmMap.get("articleId");
        if (articleId == null) {
            throw new RuntimeException("文章数据错误");
        }
        return AjaxResult.success(wechatMiniService.wechatMiniUserArticleRead(articleId));
    }

//    /**
//     * 获取工作室审核列表
//     *  根据openid查询审核列表
//     * @return
//     */
//    //todo
//    @PostMapping("/wechat-mini-get-work-room-audit-list")
//    public  AjaxResult wechatMiniGetWorkRoomAuditList(){
//        return AjaxResult.success(wechatMiniService.wechatMiniGetWorkRoomAuditList());
//
//    }

    /**
     * 管理员管理 报名审核结果 通知
     *
     * @return
     */
    @PostMapping("/wechat-mini-get-work-audit-result")
    public AjaxResult wechatMiniAuditResult(@RequestBody WechatMiniAuditResult wechatMiniAuditResult) {
        return AjaxResult.success(wechatMiniService.wechatMiniAuditResult(wechatMiniAuditResult));

    }

    /**
     * 删除自己创建的工作室
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-delete-own-work-room")
    public AjaxResult wechatMiniDeleteOwnWorkRoom(@RequestBody Map<String, Long> parmMap) {
        Long workRoomId = parmMap.get("workRoomId");
        if (workRoomId == null) {
            throw new RuntimeException("工作室信息异常");
        }
        return AjaxResult.success(wechatMiniService.wechatMiniDeleteOwnWorkRoom(workRoomId));
    }

    /**
     * 修改工作室
     *
     * @param updateWorkRoomDo
     * @return
     */
    @PostMapping("/wechat-mini-update-work-room")
    public AjaxResult wechatMiniUpdateWorkRoom(@RequestBody UpdateWorkRoomDo updateWorkRoomDo) {
        return AjaxResult.success(wechatMiniService.wechatMiniUpdateWorkRoom(updateWorkRoomDo));
    }

    /**
     * 用户修改在工作室内的名字
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-update-work-room-teacher-name")
    public AjaxResult wechatMiniUpdateWorkRoomTeacherName(@RequestBody Map<String, String> parmMap) {
        String workRoomId = parmMap.get("workRoomId");
        String teacherName = parmMap.get("teacherName");
        if (workRoomId == null || workRoomId == "") {
            throw new RuntimeException("工作室id 错误");

        }
        return AjaxResult.success(wechatMiniService.wechatMiniUpdateWorkRoomTeacherName(workRoomId, teacherName));
    }


    /**
     * 自己申请加入工作室记录
     *
     * @return
     */
    @PostMapping("/wechat-mini-own-join-work-room")
    public AjaxResult wechatMiniOwnJoinWorkRoom() {
        return AjaxResult.success(wechatMiniService.wechatMiniOwnJoinWorkRoom());
    }

    /**
     * 微信小程序后台发送消息给用户客服界面
     *
     * @return
     */
    @PostMapping("/wechat-mini-send-customer-service")
    public AjaxResult wechatMiniSendCustomerService(@RequestBody Map<String, String> parmMap) {
        String workRoomName = NTools.mustData(parmMap.get("workRoomName"), String.class, "工作室信息错误");
        return AjaxResult.success(wechatMiniService.wechatMiniSendCustomerService(workRoomName));
    }


    /**
     * 用户阅读隐私条例
     *
     * @return
     */
    @PostMapping("/wechat-mini-user-read-privacy-policy")
    public AjaxResult wechatMiniUserReadPrivacyPolicy() {
        return AjaxResult.success(wechatMiniService.wechatMiniUserReadPrivacyPolicy());
    }

    //todo 学生查看课程表
    @PostMapping("/wechat-mini-get-student-course")
    public AjaxResult wechatMiniStudentCourse(@RequestBody Map<String, String> jsonMap) {
        String date = jsonMap.get("date");
        if(date==null){
            throw new RuntimeException("date参数错误");
        }
        return AjaxResult.success(wechatMiniService.wechatMiniStudentCourse(date));
    }

    //todo 教师查看课程表
    @PostMapping("/wechat-mini-get-teacher-course")
    public AjaxResult wechatMiniTeacherCourse(@RequestBody Map<String, String> jsonMap) {
        String date = jsonMap.get("date");
        if(date==null){
            throw new RuntimeException("date参数错误");
        }
        return AjaxResult.success(wechatMiniService.wechatMiniTeacherCourse(date));
    }

    /**
     * 获取教师访问量
     * @return
     */
    @PostMapping("/wechat-mini-get-visit-num")
    public AjaxResult wechatMiniGetVisitNum(){
        return AjaxResult.success(wechatMiniService.wechatMiniGetVisitNum());
    }

    /**
     * 用户访问小程序
     */
    @PostMapping("/wechat-mini-user-visit")
    public AjaxResult wechatMiniUserVisit(){
        return AjaxResult.success(wechatMiniService.wechatMiniUserVisit());
    }

    /**
     * 全局搜索
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-global-search")
    public AjaxResult wechatMiniGlobalSearch(@RequestBody Map<String, Object> parmMap){
        String search = (String) parmMap.get("search");
        if (search == null){
            search = "";
        }
        Long startCount = NTools.data(parmMap.get("startCount"), Long.class);
        Long endCount = NTools.data(parmMap.get("endCount"), Long.class);
        return AjaxResult.success(wechatMiniService.wechatMiniGlobalSearch(search, startCount, endCount));
    }

    /**
     * 注销
     * @return
     */
    @DeleteMapping("/wechat-mini-logout")
    public AjaxResult logout(){
        wechatMiniService.logout();
        return AjaxResult.success();
    }

    /**
     * 获取试题
     * @return
     */
    @GetMapping("/wechat-mini-get-question")
    public AjaxResult getQuestions(){

        return AjaxResult.success(itQuestionsService.getQuestions());
    }

    /**
     * 比对答案返回错题解析和分数
     * @param answer
     * @return
     */
    @PostMapping("/wechat-mini-submit-question")
    public AjaxResult submitQuestion(@RequestBody Map<Long,String> answer){
        Map map = itQuestionsService.submitAnswer(answer);
        return AjaxResult.success(map);
    }

}
