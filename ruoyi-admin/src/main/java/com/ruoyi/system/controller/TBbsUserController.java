package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TBbsUser;
import com.ruoyi.system.service.ITBbsUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 论坛用户数据Controller
 *
 * @author ruoyi
 * @date 2023-08-29
 */
@RestController
@RequestMapping("/system/tBbsUser")
public class TBbsUserController extends BaseController {
    @Autowired
    private ITBbsUserService tBbsUserService;

    /**
     * 查询论坛用户数据列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsUser:list')")
    @GetMapping("/list")
    public TableDataInfo list(TBbsUser tBbsUser) {
        startPage();
        List<TBbsUser> list = tBbsUserService.selectTBbsUserList(tBbsUser);
        return getDataTable(list);
    }

    /**
     * 导出论坛用户数据列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsUser:export')")
    @Log(title = "论坛用户数据", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TBbsUser tBbsUser) {
        List<TBbsUser> list = tBbsUserService.selectTBbsUserList(tBbsUser);
        ExcelUtil<TBbsUser> util = new ExcelUtil<TBbsUser>(TBbsUser.class);
        util.exportExcel(response, list, "论坛用户数据数据");
    }

    /**
     * 获取论坛用户数据详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsUser:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tBbsUserService.selectTBbsUserById(id));
    }

    /**
     * 新增论坛用户数据
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsUser:add')")
    @Log(title = "论坛用户数据", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TBbsUser tBbsUser) {
        return toAjax(tBbsUserService.insertTBbsUser(tBbsUser));
    }

    /**
     * 修改论坛用户数据
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsUser:edit')")
    @Log(title = "论坛用户数据", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TBbsUser tBbsUser) {
        return toAjax(tBbsUserService.updateTBbsUser(tBbsUser));
    }

    /**
     * 删除论坛用户数据
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsUser:remove')")
    @Log(title = "论坛用户数据", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tBbsUserService.deleteTBbsUserByIds(ids));
    }
}
