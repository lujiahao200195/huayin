package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TStudentSchedule;
import com.ruoyi.system.service.ITStudentScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 学生可安排时间Controller
 *
 * @author ruoyi
 * @date 2023-08-17
 */
@RestController
@RequestMapping("/system/tStudentSchedule")
public class TStudentScheduleController extends BaseController {
    @Autowired
    private ITStudentScheduleService tStudentScheduleService;

    /**
     * 查询学生可安排时间列表
     */
    @PreAuthorize("@ss.hasPermi('system:tStudentSchedule:list')")
    @GetMapping("/list")
    public TableDataInfo list(TStudentSchedule tStudentSchedule) {
        startPage();
        List<TStudentSchedule> list = tStudentScheduleService.selectTStudentScheduleList(tStudentSchedule);
        return getDataTable(list);
    }

    /**
     * 导出学生可安排时间列表
     */
    @PreAuthorize("@ss.hasPermi('system:tStudentSchedule:export')")
    @Log(title = "学生可安排时间", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TStudentSchedule tStudentSchedule) {
        List<TStudentSchedule> list = tStudentScheduleService.selectTStudentScheduleList(tStudentSchedule);
        ExcelUtil<TStudentSchedule> util = new ExcelUtil<TStudentSchedule>(TStudentSchedule.class);
        util.exportExcel(response, list, "学生可安排时间数据");
    }

    /**
     * 获取学生可安排时间详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tStudentSchedule:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tStudentScheduleService.selectTStudentScheduleById(id));
    }

    /**
     * 新增学生可安排时间
     */
    @PreAuthorize("@ss.hasPermi('system:tStudentSchedule:add')")
    @Log(title = "学生可安排时间", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TStudentSchedule tStudentSchedule) {
        return toAjax(tStudentScheduleService.insertTStudentSchedule(tStudentSchedule));
    }

    /**
     * 修改学生可安排时间
     */
    @PreAuthorize("@ss.hasPermi('system:tStudentSchedule:edit')")
    @Log(title = "学生可安排时间", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TStudentSchedule tStudentSchedule) {
        return toAjax(tStudentScheduleService.updateTStudentSchedule(tStudentSchedule));
    }

    /**
     * 删除学生可安排时间
     */
    @PreAuthorize("@ss.hasPermi('system:tStudentSchedule:remove')")
    @Log(title = "学生可安排时间", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tStudentScheduleService.deleteTStudentScheduleByIds(ids));
    }
}
