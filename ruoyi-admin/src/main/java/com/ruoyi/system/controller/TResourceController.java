package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.TResource;
import com.ruoyi.system.service.ITResourceService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 资源Controller
 * 
 * @author ruoyi
 * @date 2023-09-21
 */
@RestController
@RequestMapping("/system/tResource")
public class TResourceController extends BaseController
{
    @Autowired
    private ITResourceService tResourceService;

    /**
     * 查询资源列表
     */
    @PreAuthorize("@ss.hasPermi('system:tResource:list')")
    @GetMapping("/list")
    public TableDataInfo list(TResource tResource)
    {
        startPage();
        List<TResource> list = tResourceService.selectTResourceList(tResource);
        return getDataTable(list);
    }

    /**
     * 导出资源列表
     */
    @PreAuthorize("@ss.hasPermi('system:tResource:export')")
    @Log(title = "资源", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TResource tResource)
    {
        List<TResource> list = tResourceService.selectTResourceList(tResource);
        ExcelUtil<TResource> util = new ExcelUtil<TResource>(TResource.class);
        util.exportExcel(response, list, "资源数据");
    }

    /**
     * 获取资源详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tResource:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(tResourceService.selectTResourceById(id));
    }

    /**
     * 新增资源
     */
    @PreAuthorize("@ss.hasPermi('system:tResource:add')")
    @Log(title = "资源", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TResource tResource)
    {
        return toAjax(tResourceService.insertTResource(tResource));
    }

    /**
     * 修改资源
     */
    @PreAuthorize("@ss.hasPermi('system:tResource:edit')")
    @Log(title = "资源", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TResource tResource)
    {
        return toAjax(tResourceService.updateTResource(tResource));
    }

    /**
     * 删除资源
     */
    @PreAuthorize("@ss.hasPermi('system:tResource:remove')")
    @Log(title = "资源", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tResourceService.deleteTResourceByIds(ids));
    }
}
