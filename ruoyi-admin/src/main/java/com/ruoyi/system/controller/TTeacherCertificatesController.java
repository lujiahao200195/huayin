package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TTeacherCertificates;
import com.ruoyi.system.service.ITTeacherCertificatesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 教师证书Controller
 *
 * @author ruoyi
 * @date 2023-08-16
 */
@RestController
@RequestMapping("/system/tTeacherCertificates")
public class TTeacherCertificatesController extends BaseController {
    @Autowired
    private ITTeacherCertificatesService tTeacherCertificatesService;

    /**
     * 查询教师证书列表
     */
    @PreAuthorize("@ss.hasPermi('system:tTeacherCertificates:list')")
    @GetMapping("/list")
    public TableDataInfo list(TTeacherCertificates tTeacherCertificates) {
        startPage();
        List<TTeacherCertificates> list = tTeacherCertificatesService.selectTTeacherCertificatesList(tTeacherCertificates);
        return getDataTable(list);
    }

    /**
     * 导出教师证书列表
     */
    @PreAuthorize("@ss.hasPermi('system:tTeacherCertificates:export')")
    @Log(title = "教师证书", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TTeacherCertificates tTeacherCertificates) {
        List<TTeacherCertificates> list = tTeacherCertificatesService.selectTTeacherCertificatesList(tTeacherCertificates);
        ExcelUtil<TTeacherCertificates> util = new ExcelUtil<TTeacherCertificates>(TTeacherCertificates.class);
        util.exportExcel(response, list, "教师证书数据");
    }

    /**
     * 获取教师证书详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tTeacherCertificates:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tTeacherCertificatesService.selectTTeacherCertificatesById(id));
    }

    /**
     * 新增教师证书
     */
    @PreAuthorize("@ss.hasPermi('system:tTeacherCertificates:add')")
    @Log(title = "教师证书", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TTeacherCertificates tTeacherCertificates) {
        return toAjax(tTeacherCertificatesService.insertTTeacherCertificates(tTeacherCertificates));
    }

    /**
     * 修改教师证书
     */
    @PreAuthorize("@ss.hasPermi('system:tTeacherCertificates:edit')")
    @Log(title = "教师证书", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TTeacherCertificates tTeacherCertificates) {
        return toAjax(tTeacherCertificatesService.updateTTeacherCertificates(tTeacherCertificates));
    }

    /**
     * 删除教师证书
     */
    @PreAuthorize("@ss.hasPermi('system:tTeacherCertificates:remove')")
    @Log(title = "教师证书", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tTeacherCertificatesService.deleteTTeacherCertificatesByIds(ids));
    }
}
