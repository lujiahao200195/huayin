package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TRelationTeacherCertificates;
import com.ruoyi.system.service.ITRelationTeacherCertificatesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 教师和教师证书关系Controller
 *
 * @author ruoyi
 * @date 2023-08-17
 */
@RestController
@RequestMapping("/system/tRelationTeacherCertificates")
public class TRelationTeacherCertificatesController extends BaseController {
    @Autowired
    private ITRelationTeacherCertificatesService tRelationTeacherCertificatesService;

    /**
     * 查询教师和教师证书关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationTeacherCertificates:list')")
    @GetMapping("/list")
    public TableDataInfo list(TRelationTeacherCertificates tRelationTeacherCertificates) {
        startPage();
        List<TRelationTeacherCertificates> list = tRelationTeacherCertificatesService.selectTRelationTeacherCertificatesList(tRelationTeacherCertificates);
        return getDataTable(list);
    }

    /**
     * 导出教师和教师证书关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationTeacherCertificates:export')")
    @Log(title = "教师和教师证书关系", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TRelationTeacherCertificates tRelationTeacherCertificates) {
        List<TRelationTeacherCertificates> list = tRelationTeacherCertificatesService.selectTRelationTeacherCertificatesList(tRelationTeacherCertificates);
        ExcelUtil<TRelationTeacherCertificates> util = new ExcelUtil<TRelationTeacherCertificates>(TRelationTeacherCertificates.class);
        util.exportExcel(response, list, "教师和教师证书关系数据");
    }

    /**
     * 获取教师和教师证书关系详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationTeacherCertificates:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tRelationTeacherCertificatesService.selectTRelationTeacherCertificatesById(id));
    }

    /**
     * 新增教师和教师证书关系
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationTeacherCertificates:add')")
    @Log(title = "教师和教师证书关系", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TRelationTeacherCertificates tRelationTeacherCertificates) {
        return toAjax(tRelationTeacherCertificatesService.insertTRelationTeacherCertificates(tRelationTeacherCertificates));
    }

    /**
     * 修改教师和教师证书关系
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationTeacherCertificates:edit')")
    @Log(title = "教师和教师证书关系", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TRelationTeacherCertificates tRelationTeacherCertificates) {
        return toAjax(tRelationTeacherCertificatesService.updateTRelationTeacherCertificates(tRelationTeacherCertificates));
    }

    /**
     * 删除教师和教师证书关系
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationTeacherCertificates:remove')")
    @Log(title = "教师和教师证书关系", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tRelationTeacherCertificatesService.deleteTRelationTeacherCertificatesByIds(ids));
    }
}
