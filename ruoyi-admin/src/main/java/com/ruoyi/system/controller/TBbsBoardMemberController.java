package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TBbsBoardMember;
import com.ruoyi.system.service.ITBbsBoardMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 版块成员Controller
 *
 * @author ruoyi
 * @date 2023-08-28
 */
@RestController
@RequestMapping("/system/tBbsBoardMember")
public class TBbsBoardMemberController extends BaseController {
    @Autowired
    private ITBbsBoardMemberService tBbsBoardMemberService;

    /**
     * 查询版块成员列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsBoardMember:list')")
    @GetMapping("/list")
    public TableDataInfo list(TBbsBoardMember tBbsBoardMember) {
        startPage();
        List<TBbsBoardMember> list = tBbsBoardMemberService.selectTBbsBoardMemberList(tBbsBoardMember);
        return getDataTable(list);
    }

    /**
     * 导出版块成员列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsBoardMember:export')")
    @Log(title = "版块成员", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TBbsBoardMember tBbsBoardMember) {
        List<TBbsBoardMember> list = tBbsBoardMemberService.selectTBbsBoardMemberList(tBbsBoardMember);
        ExcelUtil<TBbsBoardMember> util = new ExcelUtil<TBbsBoardMember>(TBbsBoardMember.class);
        util.exportExcel(response, list, "版块成员数据");
    }

    /**
     * 获取版块成员详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsBoardMember:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tBbsBoardMemberService.selectTBbsBoardMemberById(id));
    }

    /**
     * 新增版块成员
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsBoardMember:add')")
    @Log(title = "版块成员", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TBbsBoardMember tBbsBoardMember) {
        return toAjax(tBbsBoardMemberService.insertTBbsBoardMember(tBbsBoardMember));
    }

    /**
     * 修改版块成员
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsBoardMember:edit')")
    @Log(title = "版块成员", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TBbsBoardMember tBbsBoardMember) {
        return toAjax(tBbsBoardMemberService.updateTBbsBoardMember(tBbsBoardMember));
    }

    /**
     * 删除版块成员
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsBoardMember:remove')")
    @Log(title = "版块成员", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tBbsBoardMemberService.deleteTBbsBoardMemberByIds(ids));
    }
}
