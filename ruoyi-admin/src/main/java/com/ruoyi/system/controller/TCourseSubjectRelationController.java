package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.TCourseSubjectRelation;
import com.ruoyi.system.service.ITCourseSubjectRelationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 课程和科目关系Controller
 * 
 * @author ruoyi
 * @date 2023-09-15
 */
@RestController
@RequestMapping("/system/tCourseSubjectRelation")
public class TCourseSubjectRelationController extends BaseController
{
    @Autowired
    private ITCourseSubjectRelationService tCourseSubjectRelationService;

    /**
     * 查询课程和科目关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tCourseSubjectRelation:list')")
    @GetMapping("/list")
    public TableDataInfo list(TCourseSubjectRelation tCourseSubjectRelation)
    {
        startPage();
        List<TCourseSubjectRelation> list = tCourseSubjectRelationService.selectTCourseSubjectRelationList(tCourseSubjectRelation);
        return getDataTable(list);
    }

    /**
     * 导出课程和科目关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tCourseSubjectRelation:export')")
    @Log(title = "课程和科目关系", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TCourseSubjectRelation tCourseSubjectRelation)
    {
        List<TCourseSubjectRelation> list = tCourseSubjectRelationService.selectTCourseSubjectRelationList(tCourseSubjectRelation);
        ExcelUtil<TCourseSubjectRelation> util = new ExcelUtil<TCourseSubjectRelation>(TCourseSubjectRelation.class);
        util.exportExcel(response, list, "课程和科目关系数据");
    }

    /**
     * 获取课程和科目关系详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tCourseSubjectRelation:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(tCourseSubjectRelationService.selectTCourseSubjectRelationById(id));
    }

    /**
     * 新增课程和科目关系
     */
    @PreAuthorize("@ss.hasPermi('system:tCourseSubjectRelation:add')")
    @Log(title = "课程和科目关系", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TCourseSubjectRelation tCourseSubjectRelation)
    {
        return toAjax(tCourseSubjectRelationService.insertTCourseSubjectRelation(tCourseSubjectRelation));
    }

    /**
     * 修改课程和科目关系
     */
    @PreAuthorize("@ss.hasPermi('system:tCourseSubjectRelation:edit')")
    @Log(title = "课程和科目关系", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TCourseSubjectRelation tCourseSubjectRelation)
    {
        return toAjax(tCourseSubjectRelationService.updateTCourseSubjectRelation(tCourseSubjectRelation));
    }

    /**
     * 删除课程和科目关系
     */
    @PreAuthorize("@ss.hasPermi('system:tCourseSubjectRelation:remove')")
    @Log(title = "课程和科目关系", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tCourseSubjectRelationService.deleteTCourseSubjectRelationByIds(ids));
    }
}
