package com.ruoyi.system.controller.backend.backenddata;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.service.backend.BackendDataService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/system/backend/data")
public class BackendDataController {

    @Resource
    private BackendDataService backendDataService;

    /**
     * 获取总报名人数
     *
     * @return
     */
    @PostMapping("/get-signup-number")
    public AjaxResult getSignUpNumber() {
        return AjaxResult.success(backendDataService.getSignUpNumber());
    }

    /**
     * 获取学生报名人数
     *
     * @return
     */
    @PostMapping("/get-student-signup-number")
    public AjaxResult getStudentSignUpNumber() {
        return AjaxResult.success(backendDataService.getStudentSignUpNumber());
    }

    /**
     * 获取教师报名人数
     *
     * @return
     */
    @PostMapping("/get-teacher-signup-number")
    public AjaxResult getTeacherSignUpNumber() {
        return AjaxResult.success(backendDataService.getTeacherSignUpNumber());
    }

    //


}
