package com.ruoyi.system.controller.socket.msg;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 咨询消息
 */
@Data
@Accessors(chain = true)
public class UserConsultConsultMsg {

    /**
     * 工作室Id
     */
    private Long workRoomId;

    /**
     * 用户Id
     */
    private Long userId;

    /**
     * 发送类型
     * 1. 咨询教师发送信息给用户
     * 2. 用户发送给咨询教师工作室
     * 3.
     */
    private Long sendType;

    /**
     * 发送消息
     */
    private String message;


}
