package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TSubject;
import com.ruoyi.system.service.ITSubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 教学科目Controller
 *
 * @author ruoyi
 * @date 2023-08-18
 */
@RestController
@RequestMapping("/system/tSubject")
public class TSubjectController extends BaseController {
    @Autowired
    private ITSubjectService tSubjectService;

    /**
     * 查询教学科目列表
     */
    @PreAuthorize("@ss.hasPermi('system:tSubject:list')")
    @GetMapping("/list")
    public TableDataInfo list(TSubject tSubject) {
        startPage();
        List<TSubject> list = tSubjectService.selectTSubjectList(tSubject);
        return getDataTable(list);
    }

    /**
     * 导出教学科目列表
     */
    @PreAuthorize("@ss.hasPermi('system:tSubject:export')")
    @Log(title = "教学科目", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TSubject tSubject) {
        List<TSubject> list = tSubjectService.selectTSubjectList(tSubject);
        ExcelUtil<TSubject> util = new ExcelUtil<TSubject>(TSubject.class);
        util.exportExcel(response, list, "教学科目数据");
    }

    /**
     * 获取教学科目详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tSubject:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tSubjectService.selectTSubjectById(id));
    }

    /**
     * 新增教学科目
     */
    @PreAuthorize("@ss.hasPermi('system:tSubject:add')")
    @Log(title = "教学科目", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TSubject tSubject) {
        return toAjax(tSubjectService.insertTSubject(tSubject));
    }

    /**
     * 修改教学科目
     */
    @PreAuthorize("@ss.hasPermi('system:tSubject:edit')")
    @Log(title = "教学科目", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TSubject tSubject) {
        return toAjax(tSubjectService.updateTSubject(tSubject));
    }

    /**
     * 删除教学科目
     */
    @PreAuthorize("@ss.hasPermi('system:tSubject:remove')")
    @Log(title = "教学科目", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tSubjectService.deleteTSubjectByIds(ids));
    }
}
