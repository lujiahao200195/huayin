package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TBbsRelationBoardTag;
import com.ruoyi.system.service.ITBbsRelationBoardTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 版块标签关系Controller
 *
 * @author ruoyi
 * @date 2023-08-29
 */
@RestController
@RequestMapping("/system/tBbsRelationBoardTag")
public class TBbsRelationBoardTagController extends BaseController {
    @Autowired
    private ITBbsRelationBoardTagService tBbsRelationBoardTagService;

    /**
     * 查询版块标签关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationBoardTag:list')")
    @GetMapping("/list")
    public TableDataInfo list(TBbsRelationBoardTag tBbsRelationBoardTag) {
        startPage();
        List<TBbsRelationBoardTag> list = tBbsRelationBoardTagService.selectTBbsRelationBoardTagList(tBbsRelationBoardTag);
        return getDataTable(list);
    }

    /**
     * 导出版块标签关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationBoardTag:export')")
    @Log(title = "版块标签关系", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TBbsRelationBoardTag tBbsRelationBoardTag) {
        List<TBbsRelationBoardTag> list = tBbsRelationBoardTagService.selectTBbsRelationBoardTagList(tBbsRelationBoardTag);
        ExcelUtil<TBbsRelationBoardTag> util = new ExcelUtil<TBbsRelationBoardTag>(TBbsRelationBoardTag.class);
        util.exportExcel(response, list, "版块标签关系数据");
    }

    /**
     * 获取版块标签关系详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationBoardTag:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tBbsRelationBoardTagService.selectTBbsRelationBoardTagById(id));
    }

    /**
     * 新增版块标签关系
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationBoardTag:add')")
    @Log(title = "版块标签关系", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TBbsRelationBoardTag tBbsRelationBoardTag) {
        return toAjax(tBbsRelationBoardTagService.insertTBbsRelationBoardTag(tBbsRelationBoardTag));
    }

    /**
     * 修改版块标签关系
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationBoardTag:edit')")
    @Log(title = "版块标签关系", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TBbsRelationBoardTag tBbsRelationBoardTag) {
        return toAjax(tBbsRelationBoardTagService.updateTBbsRelationBoardTag(tBbsRelationBoardTag));
    }

    /**
     * 删除版块标签关系
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationBoardTag:remove')")
    @Log(title = "版块标签关系", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tBbsRelationBoardTagService.deleteTBbsRelationBoardTagByIds(ids));
    }
}
