package com.ruoyi.system.controller.bbs;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.bo.wechat.*;
import com.ruoyi.system.common.tools.NTools;
import com.ruoyi.system.service.BbsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 论坛公共模块
 */
@RestController
@RequestMapping("/system/wechatMiniBbsCommonController")
public class WechatMiniBbsCommonController extends BaseController {

    @Resource
    public BbsService bbsService;

    /**
     * 论坛用户数据表(用户ID, 积分数, 经验数)
     *
     * @return
     */
    @PostMapping("/wechat-mini-bbs-user-info")
    public AjaxResult wechatMiniBbsUserInfo() {
        return AjaxResult.success(bbsService.wechatMiniBbsUserInfo());
    }

    /**
     * 获取首页帖子列表
     *
     * @return
     */
    @PostMapping("/wechat-mini-bbs-index-post-list")
    public AjaxResult wechatMiniBbsIndexPostList(@RequestBody Map<String, Long> parmMap) {
        Long page = NTools.mustData(parmMap.get("pageIndex"), Long.class, "分页数据异常");
        Long pageSize = NTools.mustData(parmMap.get("pageSize"), Long.class, "分页数据异常");
        // todo 按热点获取, 按最新获取, 等...
        return AjaxResult.success(bbsService.wechatMiniBbsIndexPostList(page, pageSize));
    }

    /**
     * 发送帖子(帖子, 文章, 视频)
     *
     * @param bbsSendPostDo
     * @return
     */
    @PostMapping("/wechat-mini-bbs-send-post")
    public AjaxResult wechatMiniBbsSendPost(@RequestBody BbsSendPostDo bbsSendPostDo) {
        return AjaxResult.success(bbsService.wechatMiniBbsSendPost(bbsSendPostDo));
    }

    /**
     * 关注版块
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-follow-board")
    public AjaxResult wechatMiniFollowBoard(@RequestBody Map<String, Long> parmMap) {
        Long boardId = NTools.mustData(parmMap.get("boardId"), Long.class, "版块信息异常");
        return AjaxResult.success(bbsService.wechatMiniFollowBoard(boardId));
    }


    /**
     * 关注用户
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-follow-user")
    public AjaxResult wechatMiniFollowUser(@RequestBody Map<String, Long> parmMap) {
        Long userId = NTools.mustData(parmMap.get("userId"), Long.class, "关注用户信息错误");
        return AjaxResult.success(bbsService.wechatMiniFollowUser(userId));
    }

    /**
     * 文章点赞
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-post-up")
    public AjaxResult wechatMiniPostUp(@RequestBody Map<String, Long> parmMap) {
        Long postId = NTools.mustData(parmMap.get("postId"), Long.class, "文章数据异常");
        return AjaxResult.success(bbsService.wechatMiniPostUp(postId));
    }

    /**
     * 查询文章是否点赞
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-post-is-up")
    public AjaxResult wechatMiniPostIsUp(@RequestBody Map<String, Long> parmMap) {
        Long postId = NTools.mustData(parmMap.get("postId"), Long.class, "文章数据异常");
        return AjaxResult.success(bbsService.wechatMiniPostIsUp(postId));
    }

    /**
     * 获取评论回复列表(回复按点赞最高排序, 最多显示5个, 排序: 热门, 正序, 倒序)
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-get-reply-on-reply-list")
    public AjaxResult wechatMiniGetReplyOnReplyList(@RequestBody Map<String, Long> parmMap) {
        Long replyId = NTools.mustData(parmMap.get("replyId"), Long.class, "回复数据异常");
        return AjaxResult.success(bbsService.wechatMiniGetReplyOnReplyList(replyId));
    }

    /**
     * 发布评论或回复
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-send-command-or-reply")
    public AjaxResult wechatMiniSendCommandOrReply(@RequestBody Map<String, Object> parmMap) {
        Long postId = NTools.mustData(parmMap.get("postId"), Long.class, "文章数据异常");
        Long replyId = NTools.data(parmMap.get("replyId"), Long.class);
        String replyContent = NTools.mustData(parmMap.get("replyContent"), String.class, "评论内容异常");
        return AjaxResult.success(bbsService.wechatMiniSendCommandOrReply(postId, replyId, replyContent));
    }


    /**
     * 评论点赞
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-reply-up")
    public AjaxResult wechatMiniCommandUp(@RequestBody Map<String, Long> parmMap) {
        Long replayId = NTools.mustData(parmMap.get("replyId"), Long.class, "评论数据错误");
        return AjaxResult.success(bbsService.wechatMiniReplyUp(replayId));
    }

    /**
     * 查看评论点赞是否点赞
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-reply-is-up")
    public AjaxResult wechatMiniReplyIsUp(@RequestBody Map<String, Long> parmMap) {
        Long replayId = NTools.mustData(parmMap.get("replyId"), Long.class, "回复数据错误");
        return AjaxResult.success(bbsService.wechatMiniReplyIsUp(replayId));
    }

    /**
     * 评论点踩
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-reply-down")
    public AjaxResult wechatMiniReplyDown(@RequestBody Map<String, Long> parmMap) {
        Long replyId = NTools.mustData(parmMap.get("replyId"), Long.class, "回复数据错误");
        return AjaxResult.success(bbsService.wechatMiniReplyDown(replyId));
    }

    /**
     * 评论是否点踩
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-reply-is-down")
    public AjaxResult wechatMiniReplyIsDown(@RequestBody Map<String, Long> parmMap) {
        Long replyId = NTools.mustData(parmMap.get("replyId"), Long.class, "回复数据错误");
        return AjaxResult.success(bbsService.wechatMiniReplyIsDown(replyId));
    }

    /**
     * 获取论坛文章详情
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-post-details")
    public AjaxResult wechatMiniPostDetails(@RequestBody Map<String, Long> parmMap) {
        Long postId = NTools.mustData(parmMap.get("postId"), Long.class, "文章数据错误");
        return AjaxResult.success(bbsService.wechatMiniPostDetails(postId));
    }

    /**
     * 查看版块信息(版块版规, 版块置顶列表)
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-board-info")
    public AjaxResult wechatMiniBoardInfo(@RequestBody Map<String, Long> parmMap) {
        Long boardId = NTools.mustData(parmMap.get("boardId"), Long.class, "版块信息错误");
        return AjaxResult.success(bbsService.getBbsBoardInfoByBoardId(boardId));
    }

    /**
     * 获取文章评论列表
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-get-post-reply-list")
    public AjaxResult wechatMiniGetPostReplyList(@RequestBody Map<String, Long> parmMap) {
        Long postId = NTools.mustData(parmMap.get("postId"), Long.class, "文章信息错误");
        return AjaxResult.success(bbsService.wechatMiniGetPostReplyList(postId));
    }

    /**
     * 创建版块
     *
     * @param bbsCreateBoardDo
     * @return
     */
    @PostMapping("/wechat-mini-create-board")
    public AjaxResult wechatMiniCreateBoard(@RequestBody BbsCreateBoardDo bbsCreateBoardDo) {
        return AjaxResult.success(bbsService.wechatMiniCreateBoard(bbsCreateBoardDo));
    }


    /**
     * 查看版块帖子列表
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-board-post-list")
    public AjaxResult wechatMiniBoardPostList(@RequestBody Map<String, Long> parmMap) {
        // todo 按热点获取, 按最新获取, 等...
        Long boardId = NTools.data(parmMap.get("boardId"), Long.class);
        return AjaxResult.success(bbsService.wechatMiniBoardPostList(boardId));
    }


    // todo 查看版块视频列表
    @PostMapping("/wechat-mini-board-video")
    public AjaxResult wechatMiniBoardVideo() {
        // todo
        return AjaxResult.success();
    }


    /**
     * 查看吧主推荐
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-board-suggest-list")
    public AjaxResult wechatMiniBoardSuggestList(@RequestBody Map<String, Long> parmMap) {
        Long boardId = NTools.mustData(parmMap.get("boardId"), Long.class, "版块信息异常");
        return AjaxResult.success(bbsService.wechatMiniBoardSuggestList(boardId));
    }

    /**
     * 查询版主推荐标签
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-get-board-suggest-tag-list")
    public AjaxResult wechatMiniGetBoardSuggestTagList(@RequestBody Map<String, Long> parmMap) {
        Long boardId = NTools.mustData(parmMap.get("boardId"), Long.class, "版块信息错误");
        return AjaxResult.success(bbsService.wechatMiniGetBoardSuggestTagList(boardId));
    }

    /**
     * 查看版块成员列表
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-board-member-list")
    public AjaxResult wechatMiniBoardMemberList(@RequestBody Map<String, Long> parmMap) {
        Long boardId = NTools.mustData(parmMap.get("boardId"), Long.class, "版块信息错误");
        return AjaxResult.success(bbsService.wechatMiniBoardMemberList(boardId));
    }


    /**
     * 用户申请加入版块成员
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-apply-to-join-board-member")
    public AjaxResult wechatMiniApplyToJoinBoardMember(@RequestBody Map<String, Long> parmMap) {
        Long boardId = NTools.mustData(parmMap.get("boardId"), Long.class, "版块信息错误");
        return AjaxResult.success(bbsService.wechatMiniApplyToJoinBoardMember(boardId));
    }

    /**
     * 版主审核版块成员加入
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-audit-board-member-join")
    public AjaxResult wechatMiniAuditBoardMemberJoin(@RequestBody Map<String, Object> parmMap) {
        Long boardMemberId = NTools.mustData(parmMap.get("boardMemberId"), Long.class, "版块成员信息错误");
        String auditStatus = NTools.mustData(parmMap.get("auditStatus"), String.class, "审核状态");
        return AjaxResult.success(bbsService.wechatMiniAuditBoardMemberJoin(boardMemberId, auditStatus));
    }

    /**
     * 查看版块成员申请加入列表
     *
     * @param parmMap
     * @return
     */
    @PostMapping("/wechat-mini-board-member-join-list")
    public AjaxResult wechatMiniBoardMemberJoinList(@RequestBody Map<String, Long> parmMap) {
        Long boardId = parmMap.get("boardId");
        return AjaxResult.success(bbsService.wechatMiniBoardMemberJoinList(boardId));
    }


    /**
     * 修改版块信息
     *
     * @param updateBoardInfoDo
     * @return
     */
    @PostMapping("/wechat-mini-update-board-info")
    public AjaxResult wechatMiniUpdateBoardIntro(@RequestBody UpdateBoardInfoDo updateBoardInfoDo) {
        return AjaxResult.success(bbsService.wechatMiniUpdateBoardIntro(updateBoardInfoDo));
    }


    /**
     * 修改版规
     *
     * @param updateBoardRuleDo
     * @return
     */
    @PostMapping("/wechat-mini-update-board-rule")
    public AjaxResult wechatMiniUpdateBoardRule(@RequestBody UpdateBoardRuleDo updateBoardRuleDo) {
        return AjaxResult.success(bbsService.wechatMiniUpdateBoardRule(updateBoardRuleDo));
    }

    /**
     * 添加版块推荐文章/置顶(可加标签)
     *
     * @return
     */
    @PostMapping("/wechat-mini-add-board-suggest-post")
    public AjaxResult wechatMiniAddBoardSuggestPost(@RequestBody AddBoardSuggestPostDo addBoardSuggestPostDo) {
        return AjaxResult.success(bbsService.wechatMiniAddBoardSuggestPost(addBoardSuggestPostDo));
    }

    /**
     * 获取版块列表
     *
     * @return
     */
    @PostMapping("/wechat-mini-board-list")
    public AjaxResult wechatMiniBoardList() {
        return AjaxResult.success(bbsService.wechatMiniBoardList());
    }

}
