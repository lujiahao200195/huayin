package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TRelationSubjectType;
import com.ruoyi.system.service.ITRelationSubjectTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 科目和分类的关系Controller
 *
 * @author ruoyi
 * @date 2023-08-17
 */
@RestController
@RequestMapping("/system/tRelationSubjectType")
public class TRelationSubjectTypeController extends BaseController {
    @Autowired
    private ITRelationSubjectTypeService tRelationSubjectTypeService;

    /**
     * 查询科目和分类的关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationSubjectType:list')")
    @GetMapping("/list")
    public TableDataInfo list(TRelationSubjectType tRelationSubjectType) {
        startPage();
        List<TRelationSubjectType> list = tRelationSubjectTypeService.selectTRelationSubjectTypeList(tRelationSubjectType);
        return getDataTable(list);
    }

    /**
     * 导出科目和分类的关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationSubjectType:export')")
    @Log(title = "科目和分类的关系", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TRelationSubjectType tRelationSubjectType) {
        List<TRelationSubjectType> list = tRelationSubjectTypeService.selectTRelationSubjectTypeList(tRelationSubjectType);
        ExcelUtil<TRelationSubjectType> util = new ExcelUtil<TRelationSubjectType>(TRelationSubjectType.class);
        util.exportExcel(response, list, "科目和分类的关系数据");
    }

    /**
     * 获取科目和分类的关系详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationSubjectType:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tRelationSubjectTypeService.selectTRelationSubjectTypeById(id));
    }

    /**
     * 新增科目和分类的关系
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationSubjectType:add')")
    @Log(title = "科目和分类的关系", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TRelationSubjectType tRelationSubjectType) {
        return toAjax(tRelationSubjectTypeService.insertTRelationSubjectType(tRelationSubjectType));
    }

    /**
     * 修改科目和分类的关系
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationSubjectType:edit')")
    @Log(title = "科目和分类的关系", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TRelationSubjectType tRelationSubjectType) {
        return toAjax(tRelationSubjectTypeService.updateTRelationSubjectType(tRelationSubjectType));
    }

    /**
     * 删除科目和分类的关系
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationSubjectType:remove')")
    @Log(title = "科目和分类的关系", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tRelationSubjectTypeService.deleteTRelationSubjectTypeByIds(ids));
    }
}
