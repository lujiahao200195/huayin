package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TBackendLog;
import com.ruoyi.system.service.ITBackendLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 管理端日志Controller
 *
 * @author ruoyi
 * @date 2023-08-18
 */
@RestController
@RequestMapping("/system/tBackendLog")
public class TBackendLogController extends BaseController {
    @Autowired
    private ITBackendLogService tBackendLogService;

    /**
     * 查询管理端日志列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBackendLog:list')")
    @GetMapping("/list")
    public TableDataInfo list(TBackendLog tBackendLog) {
        startPage();
        List<TBackendLog> list = tBackendLogService.selectTBackendLogList(tBackendLog);
        return getDataTable(list);
    }

    /**
     * 导出管理端日志列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBackendLog:export')")
    @Log(title = "管理端日志", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TBackendLog tBackendLog) {
        List<TBackendLog> list = tBackendLogService.selectTBackendLogList(tBackendLog);
        ExcelUtil<TBackendLog> util = new ExcelUtil<TBackendLog>(TBackendLog.class);
        util.exportExcel(response, list, "管理端日志数据");
    }

    /**
     * 获取管理端日志详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tBackendLog:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tBackendLogService.selectTBackendLogById(id));
    }

    /**
     * 新增管理端日志
     */
    @PreAuthorize("@ss.hasPermi('system:tBackendLog:add')")
    @Log(title = "管理端日志", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TBackendLog tBackendLog) {
        return toAjax(tBackendLogService.insertTBackendLog(tBackendLog));
    }

    /**
     * 修改管理端日志
     */
    @PreAuthorize("@ss.hasPermi('system:tBackendLog:edit')")
    @Log(title = "管理端日志", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TBackendLog tBackendLog) {
        return toAjax(tBackendLogService.updateTBackendLog(tBackendLog));
    }

    /**
     * 删除管理端日志
     */
    @PreAuthorize("@ss.hasPermi('system:tBackendLog:remove')")
    @Log(title = "管理端日志", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tBackendLogService.deleteTBackendLogByIds(ids));
    }
}
