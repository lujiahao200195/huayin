package com.ruoyi.system.controller.socket.msg;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.websocket.Session;

@Data
@Accessors(chain = true)
public class UserConsultSession {

    /**
     * 会话类型(整数)
     */
    private Long sessionType;

    /**
     * 工作室Id
     */
    private Long workRoomId;

    /**
     * 用户Id
     */
    private Long userId;

    /**
     * 会话对象
     */
    private Session session;

    /**
     * 上次发心跳表的时间戳
     */
    private Long lastHeartBeat;


}
