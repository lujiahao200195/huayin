package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TBbsHelp;
import com.ruoyi.system.service.ITBbsHelpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 论坛求助Controller
 *
 * @author ruoyi
 * @date 2023-08-30
 */
@RestController
@RequestMapping("/system/tBbsHelp")
public class TBbsHelpController extends BaseController {
    @Autowired
    private ITBbsHelpService tBbsHelpService;

    /**
     * 查询论坛求助列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsHelp:list')")
    @GetMapping("/list")
    public TableDataInfo list(TBbsHelp tBbsHelp) {
        startPage();
        List<TBbsHelp> list = tBbsHelpService.selectTBbsHelpList(tBbsHelp);
        return getDataTable(list);
    }

    /**
     * 导出论坛求助列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsHelp:export')")
    @Log(title = "论坛求助", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TBbsHelp tBbsHelp) {
        List<TBbsHelp> list = tBbsHelpService.selectTBbsHelpList(tBbsHelp);
        ExcelUtil<TBbsHelp> util = new ExcelUtil<TBbsHelp>(TBbsHelp.class);
        util.exportExcel(response, list, "论坛求助数据");
    }

    /**
     * 获取论坛求助详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsHelp:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tBbsHelpService.selectTBbsHelpById(id));
    }

    /**
     * 新增论坛求助
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsHelp:add')")
    @Log(title = "论坛求助", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TBbsHelp tBbsHelp) {
        return toAjax(tBbsHelpService.insertTBbsHelp(tBbsHelp));
    }

    /**
     * 修改论坛求助
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsHelp:edit')")
    @Log(title = "论坛求助", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TBbsHelp tBbsHelp) {
        return toAjax(tBbsHelpService.updateTBbsHelp(tBbsHelp));
    }

    /**
     * 删除论坛求助
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsHelp:remove')")
    @Log(title = "论坛求助", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tBbsHelpService.deleteTBbsHelpByIds(ids));
    }
}
