package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TBbsRelationFollow;
import com.ruoyi.system.service.ITBbsRelationFollowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 关注关系Controller
 *
 * @author ruoyi
 * @date 2023-08-29
 */
@RestController
@RequestMapping("/system/tBbsRelationFollow")
public class TBbsRelationFollowController extends BaseController {
    @Autowired
    private ITBbsRelationFollowService tBbsRelationFollowService;

    /**
     * 查询关注关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationFollow:list')")
    @GetMapping("/list")
    public TableDataInfo list(TBbsRelationFollow tBbsRelationFollow) {
        startPage();
        List<TBbsRelationFollow> list = tBbsRelationFollowService.selectTBbsRelationFollowList(tBbsRelationFollow);
        return getDataTable(list);
    }

    /**
     * 导出关注关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationFollow:export')")
    @Log(title = "关注关系", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TBbsRelationFollow tBbsRelationFollow) {
        List<TBbsRelationFollow> list = tBbsRelationFollowService.selectTBbsRelationFollowList(tBbsRelationFollow);
        ExcelUtil<TBbsRelationFollow> util = new ExcelUtil<TBbsRelationFollow>(TBbsRelationFollow.class);
        util.exportExcel(response, list, "关注关系数据");
    }

    /**
     * 获取关注关系详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationFollow:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tBbsRelationFollowService.selectTBbsRelationFollowById(id));
    }

    /**
     * 新增关注关系
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationFollow:add')")
    @Log(title = "关注关系", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TBbsRelationFollow tBbsRelationFollow) {
        return toAjax(tBbsRelationFollowService.insertTBbsRelationFollow(tBbsRelationFollow));
    }

    /**
     * 修改关注关系
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationFollow:edit')")
    @Log(title = "关注关系", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TBbsRelationFollow tBbsRelationFollow) {
        return toAjax(tBbsRelationFollowService.updateTBbsRelationFollow(tBbsRelationFollow));
    }

    /**
     * 删除关注关系
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationFollow:remove')")
    @Log(title = "关注关系", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tBbsRelationFollowService.deleteTBbsRelationFollowByIds(ids));
    }
}
