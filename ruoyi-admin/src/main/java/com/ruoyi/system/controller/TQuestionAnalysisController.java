package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.TQuestionAnalysis;
import com.ruoyi.system.service.ITQuestionAnalysisService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 试题解析Controller
 * 
 * @author ruoyi
 * @date 2024-04-28
 */
@RestController
@RequestMapping("/tool/analysis")
public class TQuestionAnalysisController extends BaseController
{
    @Autowired
    private ITQuestionAnalysisService tQuestionAnalysisService;

    /**
     * 查询试题解析列表
     */
    @PreAuthorize("@ss.hasPermi('tool:analysis:list')")
    @GetMapping("/list")
    public TableDataInfo list(TQuestionAnalysis tQuestionAnalysis)
    {
        startPage();
        List<TQuestionAnalysis> list = tQuestionAnalysisService.selectTQuestionAnalysisList(tQuestionAnalysis);
        return getDataTable(list);
    }

    /**
     * 导出试题解析列表
     */
    @PreAuthorize("@ss.hasPermi('tool:analysis:export')")
    @Log(title = "试题解析", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TQuestionAnalysis tQuestionAnalysis)
    {
        List<TQuestionAnalysis> list = tQuestionAnalysisService.selectTQuestionAnalysisList(tQuestionAnalysis);
        ExcelUtil<TQuestionAnalysis> util = new ExcelUtil<TQuestionAnalysis>(TQuestionAnalysis.class);
        util.exportExcel(response, list, "试题解析数据");
    }

    /**
     * 获取试题解析详细信息
     */
    @PreAuthorize("@ss.hasPermi('tool:analysis:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(tQuestionAnalysisService.selectTQuestionAnalysisById(id));
    }

    /**
     * 新增试题解析
     */
    @PreAuthorize("@ss.hasPermi('tool:analysis:add')")
    @Log(title = "试题解析", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TQuestionAnalysis tQuestionAnalysis)
    {
        return toAjax(tQuestionAnalysisService.insertTQuestionAnalysis(tQuestionAnalysis));
    }

    /**
     * 修改试题解析
     */
    @PreAuthorize("@ss.hasPermi('tool:analysis:edit')")
    @Log(title = "试题解析", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TQuestionAnalysis tQuestionAnalysis)
    {
        return toAjax(tQuestionAnalysisService.updateTQuestionAnalysis(tQuestionAnalysis));
    }

    /**
     * 删除试题解析
     */
    @PreAuthorize("@ss.hasPermi('tool:analysis:remove')")
    @Log(title = "试题解析", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tQuestionAnalysisService.deleteTQuestionAnalysisByIds(ids));
    }
}
