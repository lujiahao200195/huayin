package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TBbsRelationAudit;
import com.ruoyi.system.service.ITBbsRelationAuditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 审核关系Controller
 *
 * @author ruoyi
 * @date 2023-08-28
 */
@RestController
@RequestMapping("/system/tBbsRelationAudit")
public class TBbsRelationAuditController extends BaseController {
    @Autowired
    private ITBbsRelationAuditService tBbsRelationAuditService;

    /**
     * 查询审核关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationAudit:list')")
    @GetMapping("/list")
    public TableDataInfo list(TBbsRelationAudit tBbsRelationAudit) {
        startPage();
        List<TBbsRelationAudit> list = tBbsRelationAuditService.selectTBbsRelationAuditList(tBbsRelationAudit);
        return getDataTable(list);
    }

    /**
     * 导出审核关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationAudit:export')")
    @Log(title = "审核关系", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TBbsRelationAudit tBbsRelationAudit) {
        List<TBbsRelationAudit> list = tBbsRelationAuditService.selectTBbsRelationAuditList(tBbsRelationAudit);
        ExcelUtil<TBbsRelationAudit> util = new ExcelUtil<TBbsRelationAudit>(TBbsRelationAudit.class);
        util.exportExcel(response, list, "审核关系数据");
    }

    /**
     * 获取审核关系详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationAudit:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tBbsRelationAuditService.selectTBbsRelationAuditById(id));
    }

    /**
     * 新增审核关系
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationAudit:add')")
    @Log(title = "审核关系", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TBbsRelationAudit tBbsRelationAudit) {
        return toAjax(tBbsRelationAuditService.insertTBbsRelationAudit(tBbsRelationAudit));
    }

    /**
     * 修改审核关系
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationAudit:edit')")
    @Log(title = "审核关系", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TBbsRelationAudit tBbsRelationAudit) {
        return toAjax(tBbsRelationAuditService.updateTBbsRelationAudit(tBbsRelationAudit));
    }

    /**
     * 删除审核关系
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationAudit:remove')")
    @Log(title = "审核关系", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tBbsRelationAuditService.deleteTBbsRelationAuditByIds(ids));
    }
}
