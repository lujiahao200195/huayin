package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TSubjectArticleRelation;
import com.ruoyi.system.service.ITSubjectArticleRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 科目文件关系Controller
 *
 * @author ruoyi
 * @date 2023-08-15
 */
@RestController
@RequestMapping("/system/tSubjectArticleRelation")
public class TSubjectArticleRelationController extends BaseController {
    @Autowired
    private ITSubjectArticleRelationService tSubjectArticleRelationService;

    /**
     * 查询科目文件关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tSubjectArticleRelation:list')")
    @GetMapping("/list")
    public TableDataInfo list(TSubjectArticleRelation tSubjectArticleRelation) {
        startPage();
        List<TSubjectArticleRelation> list = tSubjectArticleRelationService.selectTSubjectArticleRelationList(tSubjectArticleRelation);
        return getDataTable(list);
    }

    /**
     * 导出科目文件关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tSubjectArticleRelation:export')")
    @Log(title = "科目文件关系", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TSubjectArticleRelation tSubjectArticleRelation) {
        List<TSubjectArticleRelation> list = tSubjectArticleRelationService.selectTSubjectArticleRelationList(tSubjectArticleRelation);
        ExcelUtil<TSubjectArticleRelation> util = new ExcelUtil<TSubjectArticleRelation>(TSubjectArticleRelation.class);
        util.exportExcel(response, list, "科目文件关系数据");
    }

    /**
     * 获取科目文件关系详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tSubjectArticleRelation:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tSubjectArticleRelationService.selectTSubjectArticleRelationById(id));
    }

    /**
     * 新增科目文件关系
     */
    @PreAuthorize("@ss.hasPermi('system:tSubjectArticleRelation:add')")
    @Log(title = "科目文件关系", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TSubjectArticleRelation tSubjectArticleRelation) {
        return toAjax(tSubjectArticleRelationService.insertTSubjectArticleRelation(tSubjectArticleRelation));
    }

    /**
     * 修改科目文件关系
     */
    @PreAuthorize("@ss.hasPermi('system:tSubjectArticleRelation:edit')")
    @Log(title = "科目文件关系", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TSubjectArticleRelation tSubjectArticleRelation) {
        return toAjax(tSubjectArticleRelationService.updateTSubjectArticleRelation(tSubjectArticleRelation));
    }

    /**
     * 删除科目文件关系
     */
    @PreAuthorize("@ss.hasPermi('system:tSubjectArticleRelation:remove')")
    @Log(title = "科目文件关系", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tSubjectArticleRelationService.deleteTSubjectArticleRelationByIds(ids));
    }
}
