package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TRelationWorkRoomAddress;
import com.ruoyi.system.service.ITRelationWorkRoomAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 工作室与地区关系Controller
 *
 * @author ruoyi
 * @date 2023-08-21
 */
@RestController
@RequestMapping("/system/tRelationWorkRoomAddress")
public class TRelationWorkRoomAddressController extends BaseController {
    @Autowired
    private ITRelationWorkRoomAddressService tRelationWorkRoomAddressService;

    /**
     * 查询工作室与地区关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationWorkRoomAddress:list')")
    @GetMapping("/list")
    public TableDataInfo list(TRelationWorkRoomAddress tRelationWorkRoomAddress) {
        startPage();
        List<TRelationWorkRoomAddress> list = tRelationWorkRoomAddressService.selectTRelationWorkRoomAddressList(tRelationWorkRoomAddress);
        return getDataTable(list);
    }

    /**
     * 导出工作室与地区关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationWorkRoomAddress:export')")
    @Log(title = "工作室与地区关系", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TRelationWorkRoomAddress tRelationWorkRoomAddress) {
        List<TRelationWorkRoomAddress> list = tRelationWorkRoomAddressService.selectTRelationWorkRoomAddressList(tRelationWorkRoomAddress);
        ExcelUtil<TRelationWorkRoomAddress> util = new ExcelUtil<TRelationWorkRoomAddress>(TRelationWorkRoomAddress.class);
        util.exportExcel(response, list, "工作室与地区关系数据");
    }

    /**
     * 获取工作室与地区关系详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationWorkRoomAddress:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tRelationWorkRoomAddressService.selectTRelationWorkRoomAddressById(id));
    }

    /**
     * 新增工作室与地区关系
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationWorkRoomAddress:add')")
    @Log(title = "工作室与地区关系", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TRelationWorkRoomAddress tRelationWorkRoomAddress) {
        return toAjax(tRelationWorkRoomAddressService.insertTRelationWorkRoomAddress(tRelationWorkRoomAddress));
    }

    /**
     * 修改工作室与地区关系
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationWorkRoomAddress:edit')")
    @Log(title = "工作室与地区关系", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TRelationWorkRoomAddress tRelationWorkRoomAddress) {
        return toAjax(tRelationWorkRoomAddressService.updateTRelationWorkRoomAddress(tRelationWorkRoomAddress));
    }

    /**
     * 删除工作室与地区关系
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationWorkRoomAddress:remove')")
    @Log(title = "工作室与地区关系", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tRelationWorkRoomAddressService.deleteTRelationWorkRoomAddressByIds(ids));
    }
}
