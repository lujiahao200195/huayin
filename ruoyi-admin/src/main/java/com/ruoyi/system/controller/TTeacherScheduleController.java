package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TTeacherSchedule;
import com.ruoyi.system.service.ITTeacherScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 教师可安排时间段Controller
 *
 * @author ruoyi
 * @date 2023-08-17
 */
@RestController
@RequestMapping("/system/tTeacherSchedule")
public class TTeacherScheduleController extends BaseController {
    @Autowired
    private ITTeacherScheduleService tTeacherScheduleService;

    /**
     * 查询教师可安排时间段列表
     */
    @PreAuthorize("@ss.hasPermi('system:tTeacherSchedule:list')")
    @GetMapping("/list")
    public TableDataInfo list(TTeacherSchedule tTeacherSchedule) {
        startPage();
        List<TTeacherSchedule> list = tTeacherScheduleService.selectTTeacherScheduleList(tTeacherSchedule);
        return getDataTable(list);
    }

    /**
     * 导出教师可安排时间段列表
     */
    @PreAuthorize("@ss.hasPermi('system:tTeacherSchedule:export')")
    @Log(title = "教师可安排时间段", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TTeacherSchedule tTeacherSchedule) {
        List<TTeacherSchedule> list = tTeacherScheduleService.selectTTeacherScheduleList(tTeacherSchedule);
        ExcelUtil<TTeacherSchedule> util = new ExcelUtil<TTeacherSchedule>(TTeacherSchedule.class);
        util.exportExcel(response, list, "教师可安排时间段数据");
    }

    /**
     * 获取教师可安排时间段详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tTeacherSchedule:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tTeacherScheduleService.selectTTeacherScheduleById(id));
    }

    /**
     * 新增教师可安排时间段
     */
    @PreAuthorize("@ss.hasPermi('system:tTeacherSchedule:add')")
    @Log(title = "教师可安排时间段", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TTeacherSchedule tTeacherSchedule) {
        return toAjax(tTeacherScheduleService.insertTTeacherSchedule(tTeacherSchedule));
    }

    /**
     * 修改教师可安排时间段
     */
    @PreAuthorize("@ss.hasPermi('system:tTeacherSchedule:edit')")
    @Log(title = "教师可安排时间段", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TTeacherSchedule tTeacherSchedule) {
        return toAjax(tTeacherScheduleService.updateTTeacherSchedule(tTeacherSchedule));
    }

    /**
     * 删除教师可安排时间段
     */
    @PreAuthorize("@ss.hasPermi('system:tTeacherSchedule:remove')")
    @Log(title = "教师可安排时间段", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tTeacherScheduleService.deleteTTeacherScheduleByIds(ids));
    }
}
