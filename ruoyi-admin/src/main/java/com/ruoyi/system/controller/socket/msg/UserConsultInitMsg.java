package com.ruoyi.system.controller.socket.msg;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 初始化消息
 */
@Data
@Accessors(chain = true)
public class UserConsultInitMsg {

    /**
     * 会话类型
     */
    private Long sessionType;

    /**
     * 用户Id
     */
    private String miniOpenId;


    /**
     * 工作室Id
     */
    private Long workRoomId;
}
