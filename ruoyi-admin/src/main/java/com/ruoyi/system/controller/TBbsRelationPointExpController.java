package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TBbsRelationPointExp;
import com.ruoyi.system.service.ITBbsRelationPointExpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 积分经验关系Controller
 *
 * @author ruoyi
 * @date 2023-08-28
 */
@RestController
@RequestMapping("/system/tBbsRelationPointExp")
public class TBbsRelationPointExpController extends BaseController {
    @Autowired
    private ITBbsRelationPointExpService tBbsRelationPointExpService;

    /**
     * 查询积分经验关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationPointExp:list')")
    @GetMapping("/list")
    public TableDataInfo list(TBbsRelationPointExp tBbsRelationPointExp) {
        startPage();
        List<TBbsRelationPointExp> list = tBbsRelationPointExpService.selectTBbsRelationPointExpList(tBbsRelationPointExp);
        return getDataTable(list);
    }

    /**
     * 导出积分经验关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationPointExp:export')")
    @Log(title = "积分经验关系", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TBbsRelationPointExp tBbsRelationPointExp) {
        List<TBbsRelationPointExp> list = tBbsRelationPointExpService.selectTBbsRelationPointExpList(tBbsRelationPointExp);
        ExcelUtil<TBbsRelationPointExp> util = new ExcelUtil<TBbsRelationPointExp>(TBbsRelationPointExp.class);
        util.exportExcel(response, list, "积分经验关系数据");
    }

    /**
     * 获取积分经验关系详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationPointExp:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tBbsRelationPointExpService.selectTBbsRelationPointExpById(id));
    }

    /**
     * 新增积分经验关系
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationPointExp:add')")
    @Log(title = "积分经验关系", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TBbsRelationPointExp tBbsRelationPointExp) {
        return toAjax(tBbsRelationPointExpService.insertTBbsRelationPointExp(tBbsRelationPointExp));
    }

    /**
     * 修改积分经验关系
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationPointExp:edit')")
    @Log(title = "积分经验关系", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TBbsRelationPointExp tBbsRelationPointExp) {
        return toAjax(tBbsRelationPointExpService.updateTBbsRelationPointExp(tBbsRelationPointExp));
    }

    /**
     * 删除积分经验关系
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationPointExp:remove')")
    @Log(title = "积分经验关系", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tBbsRelationPointExpService.deleteTBbsRelationPointExpByIds(ids));
    }
}
