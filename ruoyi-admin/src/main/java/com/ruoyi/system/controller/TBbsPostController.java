package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TBbsPost;
import com.ruoyi.system.service.ITBbsPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 论坛帖Controller
 *
 * @author ruoyi
 * @date 2023-08-30
 */
@RestController
@RequestMapping("/system/tBbsPost")
public class TBbsPostController extends BaseController {
    @Autowired
    private ITBbsPostService tBbsPostService;

    /**
     * 查询论坛帖列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsPost:list')")
    @GetMapping("/list")
    public TableDataInfo list(TBbsPost tBbsPost) {
        startPage();
        List<TBbsPost> list = tBbsPostService.selectTBbsPostList(tBbsPost);
        return getDataTable(list);
    }

    /**
     * 导出论坛帖列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsPost:export')")
    @Log(title = "论坛帖", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TBbsPost tBbsPost) {
        List<TBbsPost> list = tBbsPostService.selectTBbsPostList(tBbsPost);
        ExcelUtil<TBbsPost> util = new ExcelUtil<TBbsPost>(TBbsPost.class);
        util.exportExcel(response, list, "论坛帖数据");
    }

    /**
     * 获取论坛帖详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsPost:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tBbsPostService.selectTBbsPostById(id));
    }

    /**
     * 新增论坛帖
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsPost:add')")
    @Log(title = "论坛帖", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TBbsPost tBbsPost) {
        return toAjax(tBbsPostService.insertTBbsPost(tBbsPost));
    }

    /**
     * 修改论坛帖
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsPost:edit')")
    @Log(title = "论坛帖", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TBbsPost tBbsPost) {
        return toAjax(tBbsPostService.updateTBbsPost(tBbsPost));
    }

    /**
     * 删除论坛帖
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsPost:remove')")
    @Log(title = "论坛帖", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tBbsPostService.deleteTBbsPostByIds(ids));
    }
}
