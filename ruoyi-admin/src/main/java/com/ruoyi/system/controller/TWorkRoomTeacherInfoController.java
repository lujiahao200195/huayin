package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TWorkRoomTeacherInfo;
import com.ruoyi.system.service.ITWorkRoomTeacherInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 工作室教师信息卡Controller
 *
 * @author ruoyi
 * @date 2023-08-18
 */
@RestController
@RequestMapping("/system/tWorkRoomTeacherInfo")
public class TWorkRoomTeacherInfoController extends BaseController {
    @Autowired
    private ITWorkRoomTeacherInfoService tWorkRoomTeacherInfoService;

    /**
     * 查询工作室教师信息卡列表
     */
    @PreAuthorize("@ss.hasPermi('system:tWorkRoomTeacherInfo:list')")
    @GetMapping("/list")
    public TableDataInfo list(TWorkRoomTeacherInfo tWorkRoomTeacherInfo) {
        startPage();
        List<TWorkRoomTeacherInfo> list = tWorkRoomTeacherInfoService.selectTWorkRoomTeacherInfoList(tWorkRoomTeacherInfo);
        return getDataTable(list);
    }

    /**
     * 导出工作室教师信息卡列表
     */
    @PreAuthorize("@ss.hasPermi('system:tWorkRoomTeacherInfo:export')")
    @Log(title = "工作室教师信息卡", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TWorkRoomTeacherInfo tWorkRoomTeacherInfo) {
        List<TWorkRoomTeacherInfo> list = tWorkRoomTeacherInfoService.selectTWorkRoomTeacherInfoList(tWorkRoomTeacherInfo);
        ExcelUtil<TWorkRoomTeacherInfo> util = new ExcelUtil<TWorkRoomTeacherInfo>(TWorkRoomTeacherInfo.class);
        util.exportExcel(response, list, "工作室教师信息卡数据");
    }

    /**
     * 获取工作室教师信息卡详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tWorkRoomTeacherInfo:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tWorkRoomTeacherInfoService.selectTWorkRoomTeacherInfoById(id));
    }

    /**
     * 新增工作室教师信息卡
     */
    @PreAuthorize("@ss.hasPermi('system:tWorkRoomTeacherInfo:add')")
    @Log(title = "工作室教师信息卡", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TWorkRoomTeacherInfo tWorkRoomTeacherInfo) {
        return toAjax(tWorkRoomTeacherInfoService.insertTWorkRoomTeacherInfo(tWorkRoomTeacherInfo));
    }

    /**
     * 修改工作室教师信息卡
     */
    @PreAuthorize("@ss.hasPermi('system:tWorkRoomTeacherInfo:edit')")
    @Log(title = "工作室教师信息卡", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TWorkRoomTeacherInfo tWorkRoomTeacherInfo) {
        return toAjax(tWorkRoomTeacherInfoService.updateTWorkRoomTeacherInfo(tWorkRoomTeacherInfo));
    }

    /**
     * 删除工作室教师信息卡
     */
    @PreAuthorize("@ss.hasPermi('system:tWorkRoomTeacherInfo:remove')")
    @Log(title = "工作室教师信息卡", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tWorkRoomTeacherInfoService.deleteTWorkRoomTeacherInfoByIds(ids));
    }
}
