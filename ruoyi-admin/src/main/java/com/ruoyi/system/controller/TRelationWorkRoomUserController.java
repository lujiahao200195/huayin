package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TRelationWorkRoomUser;
import com.ruoyi.system.service.ITRelationWorkRoomUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 工作室用户关系Controller
 *
 * @author ruoyi
 * @date 2023-08-30
 */
@RestController
@RequestMapping("/system/tRelationWorkRoomUser")
public class TRelationWorkRoomUserController extends BaseController {
    @Autowired
    private ITRelationWorkRoomUserService tRelationWorkRoomUserService;

    /**
     * 查询工作室用户关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationWorkRoomUser:list')")
    @GetMapping("/list")
    public TableDataInfo list(TRelationWorkRoomUser tRelationWorkRoomUser) {
        startPage();
        List<TRelationWorkRoomUser> list = tRelationWorkRoomUserService.selectTRelationWorkRoomUserList(tRelationWorkRoomUser);
        return getDataTable(list);
    }

    /**
     * 导出工作室用户关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationWorkRoomUser:export')")
    @Log(title = "工作室用户关系", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TRelationWorkRoomUser tRelationWorkRoomUser) {
        List<TRelationWorkRoomUser> list = tRelationWorkRoomUserService.selectTRelationWorkRoomUserList(tRelationWorkRoomUser);
        ExcelUtil<TRelationWorkRoomUser> util = new ExcelUtil<TRelationWorkRoomUser>(TRelationWorkRoomUser.class);
        util.exportExcel(response, list, "工作室用户关系数据");
    }

    /**
     * 获取工作室用户关系详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationWorkRoomUser:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tRelationWorkRoomUserService.selectTRelationWorkRoomUserById(id));
    }

    /**
     * 新增工作室用户关系
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationWorkRoomUser:add')")
    @Log(title = "工作室用户关系", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TRelationWorkRoomUser tRelationWorkRoomUser) {
        return toAjax(tRelationWorkRoomUserService.insertTRelationWorkRoomUser(tRelationWorkRoomUser));
    }

    /**
     * 修改工作室用户关系
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationWorkRoomUser:edit')")
    @Log(title = "工作室用户关系", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TRelationWorkRoomUser tRelationWorkRoomUser) {
        return toAjax(tRelationWorkRoomUserService.updateTRelationWorkRoomUser(tRelationWorkRoomUser));
    }

    /**
     * 删除工作室用户关系
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationWorkRoomUser:remove')")
    @Log(title = "工作室用户关系", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tRelationWorkRoomUserService.deleteTRelationWorkRoomUserByIds(ids));
    }
}
