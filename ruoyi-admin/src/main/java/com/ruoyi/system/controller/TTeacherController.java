package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TTeacher;
import com.ruoyi.system.service.ITTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 教师Controller
 *
 * @author ruoyi
 * @date 2023-09-13
 */
@RestController
@RequestMapping("/system/tTeacher")
public class TTeacherController extends BaseController {
    @Autowired
    private ITTeacherService tTeacherService;

    /**
     * 查询教师列表
     */
    @PreAuthorize("@ss.hasPermi('system:tTeacher:list')")
    @GetMapping("/list")
    public TableDataInfo list(TTeacher tTeacher) {
        startPage();
        List<TTeacher> list = tTeacherService.selectTTeacherList(tTeacher);
        return getDataTable(list);
    }

    /**
     * 导出教师列表
     */
    @PreAuthorize("@ss.hasPermi('system:tTeacher:export')")
    @Log(title = "教师", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TTeacher tTeacher) {
        List<TTeacher> list = tTeacherService.selectTTeacherList(tTeacher);
        ExcelUtil<TTeacher> util = new ExcelUtil<TTeacher>(TTeacher.class);
        util.exportExcel(response, list, "教师数据");
    }

    /**
     * 获取教师详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tTeacher:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tTeacherService.selectTTeacherById(id));
    }

    /**
     * 新增教师
     */
    @PreAuthorize("@ss.hasPermi('system:tTeacher:add')")
    @Log(title = "教师", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TTeacher tTeacher) {
        return toAjax(tTeacherService.insertTTeacher(tTeacher));
    }

    /**
     * 修改教师
     */
    @PreAuthorize("@ss.hasPermi('system:tTeacher:edit')")
    @Log(title = "教师", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TTeacher tTeacher) {
        return toAjax(tTeacherService.updateTTeacher(tTeacher));
    }

    /**
     * 删除教师
     */
    @PreAuthorize("@ss.hasPermi('system:tTeacher:remove')")
    @Log(title = "教师", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tTeacherService.deleteTTeacherByIds(ids));
    }
}
