package com.ruoyi.system.controller.socket.msg;

public class UserConsultMsgType {

    /**
     * 初始化连接
     */
    public static final int INIT_SESSION = 1;

    /**
     * 咨询消息
     */
    public static final int CONSULT_MESSAGE = 2;

    /**
     * 工作室群众广播消息
     */
    public static final int GROUP_MESSAGE = 3;

    /**
     * 心跳包空消息
     */
    public static final int HEART_BEAT = 4;
}
