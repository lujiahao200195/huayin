package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TTeacherAdvantages;
import com.ruoyi.system.service.ITTeacherAdvantagesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 教师个人优势Controller
 *
 * @author ruoyi
 * @date 2023-08-16
 */
@RestController
@RequestMapping("/system/tTeacherAdvantages")
public class TTeacherAdvantagesController extends BaseController {
    @Autowired
    private ITTeacherAdvantagesService tTeacherAdvantagesService;

    /**
     * 查询教师个人优势列表
     */
    @PreAuthorize("@ss.hasPermi('system:tTeacherAdvantages:list')")
    @GetMapping("/list")
    public TableDataInfo list(TTeacherAdvantages tTeacherAdvantages) {
        startPage();
        List<TTeacherAdvantages> list = tTeacherAdvantagesService.selectTTeacherAdvantagesList(tTeacherAdvantages);
        return getDataTable(list);
    }

    /**
     * 导出教师个人优势列表
     */
    @PreAuthorize("@ss.hasPermi('system:tTeacherAdvantages:export')")
    @Log(title = "教师个人优势", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TTeacherAdvantages tTeacherAdvantages) {
        List<TTeacherAdvantages> list = tTeacherAdvantagesService.selectTTeacherAdvantagesList(tTeacherAdvantages);
        ExcelUtil<TTeacherAdvantages> util = new ExcelUtil<TTeacherAdvantages>(TTeacherAdvantages.class);
        util.exportExcel(response, list, "教师个人优势数据");
    }

    /**
     * 获取教师个人优势详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tTeacherAdvantages:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tTeacherAdvantagesService.selectTTeacherAdvantagesById(id));
    }

    /**
     * 新增教师个人优势
     */
    @PreAuthorize("@ss.hasPermi('system:tTeacherAdvantages:add')")
    @Log(title = "教师个人优势", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TTeacherAdvantages tTeacherAdvantages) {
        return toAjax(tTeacherAdvantagesService.insertTTeacherAdvantages(tTeacherAdvantages));
    }

    /**
     * 修改教师个人优势
     */
    @PreAuthorize("@ss.hasPermi('system:tTeacherAdvantages:edit')")
    @Log(title = "教师个人优势", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TTeacherAdvantages tTeacherAdvantages) {
        return toAjax(tTeacherAdvantagesService.updateTTeacherAdvantages(tTeacherAdvantages));
    }

    /**
     * 删除教师个人优势
     */
    @PreAuthorize("@ss.hasPermi('system:tTeacherAdvantages:remove')")
    @Log(title = "教师个人优势", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tTeacherAdvantagesService.deleteTTeacherAdvantagesByIds(ids));
    }
}
