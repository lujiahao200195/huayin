package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TSlideshow;
import com.ruoyi.system.service.ITSlideshowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 轮播图Controller
 *
 * @author ruoyi
 * @date 2023-08-15
 */
@RestController
@RequestMapping("/system/tSlideshow")
public class TSlideshowController extends BaseController {
    @Autowired
    private ITSlideshowService tSlideshowService;

    /**
     * 查询轮播图列表
     */
    @PreAuthorize("@ss.hasPermi('system:tSlideshow:list')")
    @GetMapping("/list")
    public TableDataInfo list(TSlideshow tSlideshow) {
        startPage();
        List<TSlideshow> list = tSlideshowService.selectTSlideshowList(tSlideshow);
        return getDataTable(list);
    }

    /**
     * 导出轮播图列表
     */
    @PreAuthorize("@ss.hasPermi('system:tSlideshow:export')")
    @Log(title = "轮播图", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TSlideshow tSlideshow) {
        List<TSlideshow> list = tSlideshowService.selectTSlideshowList(tSlideshow);
        ExcelUtil<TSlideshow> util = new ExcelUtil<TSlideshow>(TSlideshow.class);
        util.exportExcel(response, list, "轮播图数据");
    }

    /**
     * 获取轮播图详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tSlideshow:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tSlideshowService.selectTSlideshowById(id));
    }

    /**
     * 新增轮播图
     */
    @PreAuthorize("@ss.hasPermi('system:tSlideshow:add')")
    @Log(title = "轮播图", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TSlideshow tSlideshow) {
        return toAjax(tSlideshowService.insertTSlideshow(tSlideshow));
    }

    /**
     * 修改轮播图
     */
    @PreAuthorize("@ss.hasPermi('system:tSlideshow:edit')")
    @Log(title = "轮播图", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TSlideshow tSlideshow) {
        return toAjax(tSlideshowService.updateTSlideshow(tSlideshow));
    }

    /**
     * 删除轮播图
     */
    @PreAuthorize("@ss.hasPermi('system:tSlideshow:remove')")
    @Log(title = "轮播图", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tSlideshowService.deleteTSlideshowByIds(ids));
    }
}
