package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TWechatMiniLog;
import com.ruoyi.system.service.ITWechatMiniLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 微信小程序日志Controller
 *
 * @author ruoyi
 * @date 2023-08-18
 */
@RestController
@RequestMapping("/system/tWechatMiniLog")
public class TWechatMiniLogController extends BaseController {
    @Autowired
    private ITWechatMiniLogService tWechatMiniLogService;

    /**
     * 查询微信小程序日志列表
     */
    @PreAuthorize("@ss.hasPermi('system:tWechatMiniLog:list')")
    @GetMapping("/list")
    public TableDataInfo list(TWechatMiniLog tWechatMiniLog) {
        startPage();
        List<TWechatMiniLog> list = tWechatMiniLogService.selectTWechatMiniLogList(tWechatMiniLog);
        return getDataTable(list);
    }

    /**
     * 导出微信小程序日志列表
     */
    @PreAuthorize("@ss.hasPermi('system:tWechatMiniLog:export')")
    @Log(title = "微信小程序日志", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TWechatMiniLog tWechatMiniLog) {
        List<TWechatMiniLog> list = tWechatMiniLogService.selectTWechatMiniLogList(tWechatMiniLog);
        ExcelUtil<TWechatMiniLog> util = new ExcelUtil<TWechatMiniLog>(TWechatMiniLog.class);
        util.exportExcel(response, list, "微信小程序日志数据");
    }

    /**
     * 获取微信小程序日志详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tWechatMiniLog:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tWechatMiniLogService.selectTWechatMiniLogById(id));
    }

    /**
     * 新增微信小程序日志
     */
    @PreAuthorize("@ss.hasPermi('system:tWechatMiniLog:add')")
    @Log(title = "微信小程序日志", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TWechatMiniLog tWechatMiniLog) {
        return toAjax(tWechatMiniLogService.insertTWechatMiniLog(tWechatMiniLog));
    }

    /**
     * 修改微信小程序日志
     */
    @PreAuthorize("@ss.hasPermi('system:tWechatMiniLog:edit')")
    @Log(title = "微信小程序日志", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TWechatMiniLog tWechatMiniLog) {
        return toAjax(tWechatMiniLogService.updateTWechatMiniLog(tWechatMiniLog));
    }

    /**
     * 删除微信小程序日志
     */
    @PreAuthorize("@ss.hasPermi('system:tWechatMiniLog:remove')")
    @Log(title = "微信小程序日志", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tWechatMiniLogService.deleteTWechatMiniLogByIds(ids));
    }
}
