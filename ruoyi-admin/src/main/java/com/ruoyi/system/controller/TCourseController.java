package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.TCourse;
import com.ruoyi.system.service.ITCourseService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 课程安排Controller
 * 
 * @author ruoyi
 * @date 2023-09-15
 */
@RestController
@RequestMapping("/system/tCourse")
public class TCourseController extends BaseController
{
    @Autowired
    private ITCourseService tCourseService;

    /**
     * 查询课程安排列表
     */
    @PreAuthorize("@ss.hasPermi('system:tCourse:list')")
    @GetMapping("/list")
    public TableDataInfo list(TCourse tCourse)
    {
        startPage();
        List<TCourse> list = tCourseService.selectTCourseList(tCourse);
        return getDataTable(list);
    }

    /**
     * 导出课程安排列表
     */
    @PreAuthorize("@ss.hasPermi('system:tCourse:export')")
    @Log(title = "课程安排", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TCourse tCourse)
    {
        List<TCourse> list = tCourseService.selectTCourseList(tCourse);
        ExcelUtil<TCourse> util = new ExcelUtil<TCourse>(TCourse.class);
        util.exportExcel(response, list, "课程安排数据");
    }

    /**
     * 获取课程安排详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tCourse:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(tCourseService.selectTCourseById(id));
    }

    /**
     * 新增课程安排
     */
    @PreAuthorize("@ss.hasPermi('system:tCourse:add')")
    @Log(title = "课程安排", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TCourse tCourse)
    {
        return toAjax(tCourseService.insertTCourse(tCourse));
    }

    /**
     * 修改课程安排
     */
    @PreAuthorize("@ss.hasPermi('system:tCourse:edit')")
    @Log(title = "课程安排", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TCourse tCourse)
    {
        return toAjax(tCourseService.updateTCourse(tCourse));
    }

    /**
     * 删除课程安排
     */
    @PreAuthorize("@ss.hasPermi('system:tCourse:remove')")
    @Log(title = "课程安排", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tCourseService.deleteTCourseByIds(ids));
    }
}
