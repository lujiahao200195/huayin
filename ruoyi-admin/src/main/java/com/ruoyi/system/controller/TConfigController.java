package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.TConfig;
import com.ruoyi.system.service.ITConfigService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 配置Controller
 * 
 * @author ruoyi
 * @date 2023-09-19
 */
@RestController
@RequestMapping("/system/tConfig")
public class TConfigController extends BaseController
{
    @Autowired
    private ITConfigService tConfigService;

    /**
     * 查询配置列表
     */
    @PreAuthorize("@ss.hasPermi('system:tConfig:list')")
    @GetMapping("/list")
    public TableDataInfo list(TConfig tConfig)
    {
        startPage();
        List<TConfig> list = tConfigService.selectTConfigList(tConfig);
        return getDataTable(list);
    }

    /**
     * 导出配置列表
     */
    @PreAuthorize("@ss.hasPermi('system:tConfig:export')")
    @Log(title = "配置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TConfig tConfig)
    {
        List<TConfig> list = tConfigService.selectTConfigList(tConfig);
        ExcelUtil<TConfig> util = new ExcelUtil<TConfig>(TConfig.class);
        util.exportExcel(response, list, "配置数据");
    }

    /**
     * 获取配置详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tConfig:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(tConfigService.selectTConfigById(id));
    }

    /**
     * 新增配置
     */
    @PreAuthorize("@ss.hasPermi('system:tConfig:add')")
    @Log(title = "配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TConfig tConfig)
    {
        return toAjax(tConfigService.insertTConfig(tConfig));
    }

    /**
     * 修改配置
     */
    @PreAuthorize("@ss.hasPermi('system:tConfig:edit')")
    @Log(title = "配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TConfig tConfig)
    {
        return toAjax(tConfigService.updateTConfig(tConfig));
    }

    /**
     * 删除配置
     */
    @PreAuthorize("@ss.hasPermi('system:tConfig:remove')")
    @Log(title = "配置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(tConfigService.deleteTConfigByIds(ids));
    }
}
