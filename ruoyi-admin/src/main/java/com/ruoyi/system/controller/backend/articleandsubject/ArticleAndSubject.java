package com.ruoyi.system.controller.backend.articleandsubject;


import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.system.domain.TSubjectArticleRelation;
import com.ruoyi.system.service.ITSubjectArticleRelationService;
import com.ruoyi.system.service.backend.IArticleAndSubjectService;
import com.ruoyi.system.vo.wechat.backend.ArticleAndSubjectVo;
import com.ruoyi.system.vo.wechat.backend.ArticleIdsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/system/backend/articleAndSubject")
public class ArticleAndSubject extends BaseController {

    /*文章Service和文章与科目关系Service导入*/
    @Autowired
    private IArticleAndSubjectService articleAndSubjectService;

    @Autowired
    private ITSubjectArticleRelationService tSubjectArticleRelationService;



    /*获取文章列表*/
//    @PreAuthorize("@ss.hasPermi('system:tArticle:list')")
    @GetMapping("/articleList")
//    @PostMapping("/articleList")
    public TableDataInfo articleList(ArticleAndSubjectVo articleAndSubjectVo){

        startPage();
        List<ArticleAndSubjectVo> list = articleAndSubjectService.selectArticleAndSubject(articleAndSubjectVo);
        return getDataTable(list);
    }



    /*获取单篇详细情况*/
//    @PreAuthorize("@ss.hasPermi('system:tArticle:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id){
        return AjaxResult.success(articleAndSubjectService.selectArticleById(id));
    }




    /*添加文章*/
//    @PreAuthorize("@ss.hasPermi('system:tArticle:add')")
//    @Log(title = "文章", businessType = BusinessType.INSERT)
    @PostMapping("/insertArticle")
    public AjaxResult add(@RequestBody ArticleAndSubjectVo articleAndSubjectVo) {

        //文章插入及主键ID获取,判断是否成功
        int result = articleAndSubjectService.insertArticle(articleAndSubjectVo);

        //进行文章关系与科目连接插入
        TSubjectArticleRelation subjectArticleRelation = new TSubjectArticleRelation();
        subjectArticleRelation.setSubjectId(articleAndSubjectVo.getSubjectId());
        subjectArticleRelation.setArticleId(articleAndSubjectVo.getId().toString());
        return toAjax(tSubjectArticleRelationService.insertTSubjectArticleRelation(subjectArticleRelation));
    }


    /**
     * 删除文章内容
     */

//    @PreAuthorize("@ss.hasPermi('system:articleAndSubject:remove')")
    @DeleteMapping("/{ids}")
    public AjaxResult deleteArticle(@PathVariable Long[] ids){
        //根据relation获取文章主键ID集合
        List<ArticleIdsVo> articleIdVos = tSubjectArticleRelationService.getArticleIds(ids);
        //数据转换List变为数组
        ArrayList<Long> list = new ArrayList<>();
        for(ArticleIdsVo articleResult: articleIdVos){
            list.add(articleResult.getArticleId());
        }
        Long[] articleDeleteIds = list.toArray(new Long[list.size()]);
        //删除文章信息
        articleAndSubjectService.deleteArticle(articleDeleteIds);
        //删除关联信息    1.导入relationServie  2.根据ids删除相应数据
        return AjaxResult.success(tSubjectArticleRelationService.deleteTSubjectArticleRelationByIds(ids));

    }






}
