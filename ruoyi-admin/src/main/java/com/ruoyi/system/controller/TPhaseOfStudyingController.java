package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TPhaseOfStudying;
import com.ruoyi.system.service.ITPhaseOfStudyingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 学段Controller
 *
 * @author ruoyi
 * @date 2023-08-15
 */
@RestController
@RequestMapping("/system/tPhaseOfStudying")
public class TPhaseOfStudyingController extends BaseController {
    @Autowired
    private ITPhaseOfStudyingService tPhaseOfStudyingService;

    /**
     * 查询学段列表
     */
    @PreAuthorize("@ss.hasPermi('system:tPhaseOfStudying:list')")
    @GetMapping("/list")
    public TableDataInfo list(TPhaseOfStudying tPhaseOfStudying) {
        startPage();
        List<TPhaseOfStudying> list = tPhaseOfStudyingService.selectTPhaseOfStudyingList(tPhaseOfStudying);
        return getDataTable(list);
    }

    /**
     * 导出学段列表
     */
    @PreAuthorize("@ss.hasPermi('system:tPhaseOfStudying:export')")
    @Log(title = "学段", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TPhaseOfStudying tPhaseOfStudying) {
        List<TPhaseOfStudying> list = tPhaseOfStudyingService.selectTPhaseOfStudyingList(tPhaseOfStudying);
        ExcelUtil<TPhaseOfStudying> util = new ExcelUtil<TPhaseOfStudying>(TPhaseOfStudying.class);
        util.exportExcel(response, list, "学段数据");
    }

    /**
     * 获取学段详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tPhaseOfStudying:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tPhaseOfStudyingService.selectTPhaseOfStudyingById(id));
    }

    /**
     * 新增学段
     */
    @PreAuthorize("@ss.hasPermi('system:tPhaseOfStudying:add')")
    @Log(title = "学段", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TPhaseOfStudying tPhaseOfStudying) {
        return toAjax(tPhaseOfStudyingService.insertTPhaseOfStudying(tPhaseOfStudying));
    }

    /**
     * 修改学段
     */
    @PreAuthorize("@ss.hasPermi('system:tPhaseOfStudying:edit')")
    @Log(title = "学段", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TPhaseOfStudying tPhaseOfStudying) {
        return toAjax(tPhaseOfStudyingService.updateTPhaseOfStudying(tPhaseOfStudying));
    }

    /**
     * 删除学段
     */
    @PreAuthorize("@ss.hasPermi('system:tPhaseOfStudying:remove')")
    @Log(title = "学段", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tPhaseOfStudyingService.deleteTPhaseOfStudyingByIds(ids));
    }
}
