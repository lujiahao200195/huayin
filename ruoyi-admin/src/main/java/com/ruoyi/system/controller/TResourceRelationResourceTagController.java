package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TResourceRelationResourceTag;
import com.ruoyi.system.service.ITResourceRelationResourceTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 资源标签关系Controller
 *
 * @author ruoyi
 * @date 2023-09-08
 */
@RestController
@RequestMapping("/system/tResourceRelationResourceTag")
public class TResourceRelationResourceTagController extends BaseController {
    @Autowired
    private ITResourceRelationResourceTagService tResourceRelationResourceTagService;

    /**
     * 查询资源标签关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tResourceRelationResourceTag:list')")
    @GetMapping("/list")
    public TableDataInfo list(TResourceRelationResourceTag tResourceRelationResourceTag) {
        startPage();
        List<TResourceRelationResourceTag> list = tResourceRelationResourceTagService.selectTResourceRelationResourceTagList(tResourceRelationResourceTag);
        return getDataTable(list);
    }

    /**
     * 导出资源标签关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tResourceRelationResourceTag:export')")
    @Log(title = "资源标签关系", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TResourceRelationResourceTag tResourceRelationResourceTag) {
        List<TResourceRelationResourceTag> list = tResourceRelationResourceTagService.selectTResourceRelationResourceTagList(tResourceRelationResourceTag);
        ExcelUtil<TResourceRelationResourceTag> util = new ExcelUtil<TResourceRelationResourceTag>(TResourceRelationResourceTag.class);
        util.exportExcel(response, list, "资源标签关系数据");
    }

    /**
     * 获取资源标签关系详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tResourceRelationResourceTag:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tResourceRelationResourceTagService.selectTResourceRelationResourceTagById(id));
    }

    /**
     * 新增资源标签关系
     */
    @PreAuthorize("@ss.hasPermi('system:tResourceRelationResourceTag:add')")
    @Log(title = "资源标签关系", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TResourceRelationResourceTag tResourceRelationResourceTag) {
        return toAjax(tResourceRelationResourceTagService.insertTResourceRelationResourceTag(tResourceRelationResourceTag));
    }

    /**
     * 修改资源标签关系
     */
    @PreAuthorize("@ss.hasPermi('system:tResourceRelationResourceTag:edit')")
    @Log(title = "资源标签关系", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TResourceRelationResourceTag tResourceRelationResourceTag) {
        return toAjax(tResourceRelationResourceTagService.updateTResourceRelationResourceTag(tResourceRelationResourceTag));
    }

    /**
     * 删除资源标签关系
     */
    @PreAuthorize("@ss.hasPermi('system:tResourceRelationResourceTag:remove')")
    @Log(title = "资源标签关系", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tResourceRelationResourceTagService.deleteTResourceRelationResourceTagByIds(ids));
    }
}
