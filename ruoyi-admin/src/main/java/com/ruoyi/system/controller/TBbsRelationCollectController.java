package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TBbsRelationCollect;
import com.ruoyi.system.service.ITBbsRelationCollectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 收藏关系Controller
 *
 * @author ruoyi
 * @date 2023-08-28
 */
@RestController
@RequestMapping("/system/tBbsRelationCollect")
public class TBbsRelationCollectController extends BaseController {
    @Autowired
    private ITBbsRelationCollectService tBbsRelationCollectService;

    /**
     * 查询收藏关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationCollect:list')")
    @GetMapping("/list")
    public TableDataInfo list(TBbsRelationCollect tBbsRelationCollect) {
        startPage();
        List<TBbsRelationCollect> list = tBbsRelationCollectService.selectTBbsRelationCollectList(tBbsRelationCollect);
        return getDataTable(list);
    }

    /**
     * 导出收藏关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationCollect:export')")
    @Log(title = "收藏关系", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TBbsRelationCollect tBbsRelationCollect) {
        List<TBbsRelationCollect> list = tBbsRelationCollectService.selectTBbsRelationCollectList(tBbsRelationCollect);
        ExcelUtil<TBbsRelationCollect> util = new ExcelUtil<TBbsRelationCollect>(TBbsRelationCollect.class);
        util.exportExcel(response, list, "收藏关系数据");
    }

    /**
     * 获取收藏关系详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationCollect:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tBbsRelationCollectService.selectTBbsRelationCollectById(id));
    }

    /**
     * 新增收藏关系
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationCollect:add')")
    @Log(title = "收藏关系", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TBbsRelationCollect tBbsRelationCollect) {
        return toAjax(tBbsRelationCollectService.insertTBbsRelationCollect(tBbsRelationCollect));
    }

    /**
     * 修改收藏关系
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationCollect:edit')")
    @Log(title = "收藏关系", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TBbsRelationCollect tBbsRelationCollect) {
        return toAjax(tBbsRelationCollectService.updateTBbsRelationCollect(tBbsRelationCollect));
    }

    /**
     * 删除收藏关系
     */
    @PreAuthorize("@ss.hasPermi('system:tBbsRelationCollect:remove')")
    @Log(title = "收藏关系", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tBbsRelationCollectService.deleteTBbsRelationCollectByIds(ids));
    }
}
