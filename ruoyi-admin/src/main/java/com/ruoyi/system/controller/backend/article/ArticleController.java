package com.ruoyi.system.controller.backend.article;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TArticle;
import com.ruoyi.system.service.backend.ArticleService;
import com.ruoyi.system.vo.wechat.backend.ExportSchoolArticle;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 文章Controller
 *
 * @author ruoyi
 * @date 2023-08-17
 */
@RestController
@RequestMapping("/system/backend/article")

public class ArticleController extends BaseController {
    @Resource
    private ArticleService articleService;

    /**
     * 查询文章列表
     */
    @PostMapping("/article-list")
    public TableDataInfo list(@RequestBody TArticle tArticle) {
        startPage();
        List<TArticle> list = articleService.selectTArticleList(tArticle);
        return getDataTable(list);
    }

    /**
     * 导出文章列表
     */
    @PostMapping("/export")
    public void export(HttpServletResponse response, TArticle tArticle) {
        List<TArticle> list = articleService.selectTArticleList(tArticle);
        ExcelUtil<TArticle> util = new ExcelUtil<TArticle>(TArticle.class);
        util.exportExcel(response, list, "文章数据");
    }

    /**
     * 获取文章详细信息
     */
    @PostMapping("/article-info")
    public AjaxResult getInfo(@RequestBody Map<String, Long> jsonMap) {
        Long id = jsonMap.get("id");
        return AjaxResult.success(articleService.selectTArticleById(id));
    }

    /**
     * 新增文章
     */
    @PostMapping("/article-add")
    public AjaxResult add(@Validated @RequestBody  TArticle tArticle) {
        return toAjax(articleService.insertTArticle(tArticle));
    }

    /**
     * 修改文章
     */
    @PostMapping("/article-edit")
    public AjaxResult edit(@Validated @RequestBody TArticle tArticle) {
        return toAjax(articleService.updateTArticle(tArticle));
    }

    /**
     * 删除文章
     */
    @PostMapping("/article-remove")
    public AjaxResult remove(@RequestBody Map<String, Long[]> jsonMap) {
        Long[] ids = jsonMap.get("ids");
        return toAjax(articleService.deleteTArticleByIds(ids));
    }

    /**
     * 导出学校文章列表
     */
    @PostMapping("/school-export")
    public void exportSchool(HttpServletResponse response,@RequestBody Map<String, Long[]> jsonMap) {
        Long[] ids = jsonMap.get("ids");
        if(ids.length==0){
            throw new RuntimeException("请勾选要导出的文章");
        }
        List<ExportSchoolArticle> exportSchoolArticles = articleService.exportSchoolArticle(ids);
        ExcelUtil<ExportSchoolArticle> util = new ExcelUtil<ExportSchoolArticle>(ExportSchoolArticle.class);
        util.exportExcel(response, exportSchoolArticles, "文章数据");
    }


    /**
     * 学校咨询文章查询
     *
     * @return
     */
    @PostMapping("/school-article-list")
    public TableDataInfo getSchoolArticle(TArticle article) {
        startPage();
        List<TArticle> list = articleService.getSchoolArticle(article);
        return getDataTable(list);
    }

    /**
     * 新增学校咨询文章
     *
     * @param tArticle
     * @return
     */
    @PostMapping("/school-article-add")
    public AjaxResult addSchoolArticle(@Validated @RequestBody TArticle tArticle) {
        return AjaxResult.success(articleService.addSchoolArticle(tArticle));
    }

    /**
     * 删除学校咨询文章
     *
     * @param jsonMap
     * @return
     */
    @PostMapping("/school-article-remove")
    public AjaxResult removeSchoolArticle(@RequestBody Map<String, Long[]> jsonMap) {
        Long[] articleIds = jsonMap.get("ids");
        return AjaxResult.success(articleService.removeSchoolArticle(articleIds));
    }



}
