package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TStudentIntensify;
import com.ruoyi.system.service.ITStudentIntensifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 学生强化科目Controller
 *
 * @author ruoyi
 * @date 2023-08-30
 */
@RestController
@RequestMapping("/system/tStudentIntensify")
public class TStudentIntensifyController extends BaseController {
    @Autowired
    private ITStudentIntensifyService tStudentIntensifyService;

    /**
     * 查询学生强化科目列表
     */
    @PreAuthorize("@ss.hasPermi('system:tStudentIntensify:list')")
    @GetMapping("/list")
    public TableDataInfo list(TStudentIntensify tStudentIntensify) {
        startPage();
        List<TStudentIntensify> list = tStudentIntensifyService.selectTStudentIntensifyList(tStudentIntensify);
        return getDataTable(list);
    }

    /**
     * 导出学生强化科目列表
     */
    @PreAuthorize("@ss.hasPermi('system:tStudentIntensify:export')")
    @Log(title = "学生强化科目", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TStudentIntensify tStudentIntensify) {
        List<TStudentIntensify> list = tStudentIntensifyService.selectTStudentIntensifyList(tStudentIntensify);
        ExcelUtil<TStudentIntensify> util = new ExcelUtil<TStudentIntensify>(TStudentIntensify.class);
        util.exportExcel(response, list, "学生强化科目数据");
    }

    /**
     * 获取学生强化科目详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tStudentIntensify:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tStudentIntensifyService.selectTStudentIntensifyById(id));
    }

    /**
     * 新增学生强化科目
     */
    @PreAuthorize("@ss.hasPermi('system:tStudentIntensify:add')")
    @Log(title = "学生强化科目", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TStudentIntensify tStudentIntensify) {
        return toAjax(tStudentIntensifyService.insertTStudentIntensify(tStudentIntensify));
    }

    /**
     * 修改学生强化科目
     */
    @PreAuthorize("@ss.hasPermi('system:tStudentIntensify:edit')")
    @Log(title = "学生强化科目", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TStudentIntensify tStudentIntensify) {
        return toAjax(tStudentIntensifyService.updateTStudentIntensify(tStudentIntensify));
    }

    /**
     * 删除学生强化科目
     */
    @PreAuthorize("@ss.hasPermi('system:tStudentIntensify:remove')")
    @Log(title = "学生强化科目", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tStudentIntensifyService.deleteTStudentIntensifyByIds(ids));
    }
}
