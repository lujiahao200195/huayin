package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TSubjectType;
import com.ruoyi.system.service.ITSubjectTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 科目分类Controller
 *
 * @author ruoyi
 * @date 2023-08-17
 */
@RestController
@RequestMapping("/system/tSubjectType")
public class TSubjectTypeController extends BaseController {
    @Autowired
    private ITSubjectTypeService tSubjectTypeService;

    /**
     * 查询科目分类列表
     */
    @PreAuthorize("@ss.hasPermi('system:tSubjectType:list')")
    @GetMapping("/list")
    public TableDataInfo list(TSubjectType tSubjectType) {
        startPage();
        List<TSubjectType> list = tSubjectTypeService.selectTSubjectTypeList(tSubjectType);
        return getDataTable(list);
    }

    /**
     * 导出科目分类列表
     */
    @PreAuthorize("@ss.hasPermi('system:tSubjectType:export')")
    @Log(title = "科目分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TSubjectType tSubjectType) {
        List<TSubjectType> list = tSubjectTypeService.selectTSubjectTypeList(tSubjectType);
        ExcelUtil<TSubjectType> util = new ExcelUtil<TSubjectType>(TSubjectType.class);
        util.exportExcel(response, list, "科目分类数据");
    }

    /**
     * 获取科目分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tSubjectType:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tSubjectTypeService.selectTSubjectTypeById(id));
    }

    /**
     * 新增科目分类
     */
    @PreAuthorize("@ss.hasPermi('system:tSubjectType:add')")
    @Log(title = "科目分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TSubjectType tSubjectType) {
        return toAjax(tSubjectTypeService.insertTSubjectType(tSubjectType));
    }

    /**
     * 修改科目分类
     */
    @PreAuthorize("@ss.hasPermi('system:tSubjectType:edit')")
    @Log(title = "科目分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TSubjectType tSubjectType) {
        return toAjax(tSubjectTypeService.updateTSubjectType(tSubjectType));
    }

    /**
     * 删除科目分类
     */
    @PreAuthorize("@ss.hasPermi('system:tSubjectType:remove')")
    @Log(title = "科目分类", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tSubjectTypeService.deleteTSubjectTypeByIds(ids));
    }
}
