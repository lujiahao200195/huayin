package com.ruoyi.system.controller.backend.subject;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.bo.wechat.AddSubjectArticleDo;
import com.ruoyi.system.domain.TSubject;
import com.ruoyi.system.service.backend.SubjectService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 教学科目Controller
 *
 * @author ruoyi
 * @date 2023-08-18
 */
@RestController
@RequestMapping("/system/backend/subject")
public class SubjectController extends BaseController {
    @Resource
    private SubjectService subjectService;

    /**
     * 查询教学科目列表
     */
    @PostMapping("/subject-list")
    public TableDataInfo list(@RequestBody TSubject tSubject) {
        startPage();
        List<TSubject> list = subjectService.selectTSubjectList(tSubject);
        return getDataTable(list);
    }

    /**
     * 导出教学科目列表
     */
    @PostMapping("/export")
    public void export(HttpServletResponse response, @RequestBody TSubject tSubject) {
        List<TSubject> list = subjectService.selectTSubjectList(tSubject);
        ExcelUtil<TSubject> util = new ExcelUtil<TSubject>(TSubject.class);
        util.exportExcel(response, list, "教学科目数据");
    }

    /**
     * 获取教学科目详细信息
     */
    @PostMapping("/subject-info")
    public AjaxResult getInfo(@RequestBody Map<String, Long> jsonMap) {
        Long id = jsonMap.get("id");
        return AjaxResult.success(subjectService.selectTSubjectById(id));
    }

    /**
     * 新增教学科目
     */
    @PostMapping("subject-add")
    public AjaxResult add(@RequestBody TSubject tSubject) {
        return toAjax(subjectService.insertTSubject(tSubject));
    }

    /**
     * 修改教学科目
     */
    @PostMapping("subject-edit")
    public AjaxResult edit(@RequestBody TSubject tSubject) {
        return toAjax(subjectService.updateTSubject(tSubject));
    }

    /**
     * 删除教学科目
     */
    @PostMapping("/subject-remove")
    public AjaxResult remove(@RequestBody Map<String, Long[]> jsonMap) {
        Long[] ids = jsonMap.get("ids");
        return toAjax(subjectService.deleteTSubjectByIds(ids));
    }

    /**
     * 添加科目文章
     *
     * @param addSubjectArticleDo
     * @return
     */
    @PostMapping("/add-subject-article")
    public AjaxResult addSubjectArticle(@RequestBody AddSubjectArticleDo addSubjectArticleDo) {
        return AjaxResult.success(subjectService.addSubjectArticle(addSubjectArticleDo));
    }

    /**
     * 删除科目文章
     *
     * @param jsonMap
     * @return
     */
    @PostMapping("/remove-subject-article")
    public AjaxResult removeSubjectArticle(@RequestBody Map<String, Long> jsonMap) {
        Long subjectId = jsonMap.get("subjectId");
        Long articleId = jsonMap.get("articleId");
        return AjaxResult.success(subjectService.removeSubjectArticle(subjectId, articleId));
    }

    /**
     * 根据科目id获取科目文章
     *
     * @param jsonMap
     * @return
     */
    @PostMapping("/get-subject-article")
    public AjaxResult getSubjectArticle(@RequestBody Map<String, Long> jsonMap) {
        Long id = jsonMap.get("id");
        return AjaxResult.success(subjectService.getSubjectArticles(id));
    }


}
