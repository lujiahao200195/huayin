package com.ruoyi.system.controller.socket;


import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.system.controller.socket.msg.*;
import com.ruoyi.system.domain.TUser;
import com.ruoyi.system.domain.TWorkRoomMessage;
import com.ruoyi.system.mapper.TWorkRoomMessageMapper;
import com.ruoyi.system.mapper.WechatMiniMapper;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.websocket.OnMessage;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * 用户咨询服务
 */
@Component
@ServerEndpoint(value = "/user-consult-server")
public class UserConsultServer {

    /**
     * 记录在线连接
     * key: 工作室Id或用户ID的组合字符串
     * value: 会话对象
     */
    public static final Map<String, UserConsultSession> sessionMap = new ConcurrentHashMap<>();
    private static WechatMiniMapper wechatMiniMapper;

    // 心跳包(连接保活)
    static {
        new Thread(() -> {
            // 每阁2分钟清除过期连接
            while (true) {
                try {
                    Thread.sleep(1000 * 60);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                for (Map.Entry<String, UserConsultSession> stringUserConsultSessionEntry : sessionMap.entrySet()) {
                    // 超过1分钟没有返回心跳包则视为死亡
                    if ((new Date().getTime() - stringUserConsultSessionEntry.getValue().getLastHeartBeat()) > 1000 * 60) {
                        sessionMap.remove(stringUserConsultSessionEntry.getKey());
                    }
                }
            }
        }).start();


    }

    @Resource
    private TWorkRoomMessageMapper workRoomMessageMapper;

    /**
     * 获取当前在线人数
     *
     * @return
     */
    private static synchronized int getOnlineCount() {
        return sessionMap.size();
    }

    /**
     * 获取工作室在线状态
     */
    public static String getWorkRoomOnlineStatus(Long workRoomId) {
        UserConsultSession userConsultSession = sessionMap.get("CONSULT_TEACHER_SESSION:" + workRoomId);
        if (userConsultSession != null) {
            return "在线";
        }
        return "不在线";
    }

    /**
     * 获取用户在线状态
     */
    public static String getUserOnlineStatus(String openId) {
        TUser tUser = wechatMiniMapper.selectTUserByWechatMiniOpenId(openId);
        UserConsultSession userConsultSession = sessionMap.get("USER_SESSION:" + tUser.getId());
        if (userConsultSession != null) {
            return "在线";
        }

        return "不在线";
    }

    @Resource
    public void WechatMiniMapper(WechatMiniMapper wechatMiniMapper) {
        UserConsultServer.wechatMiniMapper = wechatMiniMapper;
    }

    /**
     * 收到客户端消息后调用的方法
     *
     * @param message 接收消息
     */
    @OnMessage
    public void onMessage(Session session, String message) {
        UserConsultMsg userConsultMsg = JSONObject.parseObject(message, UserConsultMsg.class);
        Long msgType = userConsultMsg.getMsgType();
        String msgJson = userConsultMsg.getMsgJson();
        UserConsultSession userConsultSession = null;
        try {
            switch (msgType.intValue()) {
                case UserConsultMsgType.INIT_SESSION:
                    // 初始化连接
                    initSession(session, msgJson);
                    break;
                case UserConsultMsgType.CONSULT_MESSAGE:
                    // 咨询消息
                    UserConsultConsultMsg userConsultConsultMsg = JSONObject.parseObject(msgJson, UserConsultConsultMsg.class);
                    sendMsg(userConsultConsultMsg);
                    break;
                case UserConsultMsgType.GROUP_MESSAGE:
                    // todo 工作室群组消息
                    break;
                case UserConsultMsgType.HEART_BEAT:
                    // 心跳包更新
                    UserConsultConsultMsg userConsultConsultMsg1 = JSONObject.parseObject(msgJson, UserConsultConsultMsg.class);
                    switch (userConsultConsultMsg1.getSendType().intValue()) {
                        case 1:
                            // 咨询教师发送
                            userConsultSession = sessionMap.get("CONSULT_TEACHER_SESSION:" + userConsultConsultMsg1.getWorkRoomId());
                            userConsultSession.setLastHeartBeat(new Date().getTime());
                            break;
                        case 2:
                            // 用户发送
                            userConsultSession = sessionMap.get("USER_SESSION:" + userConsultConsultMsg1.getWorkRoomId());
                            userConsultSession.setLastHeartBeat(new Date().getTime());
                            break;
                    }
                    break;
            }
        } catch (Exception e) {
            // todo 处理异常

        }

    }

    /**
     * 初始化会话连接
     *
     * @param session
     * @param msgJson
     */
    private void initSession(Session session, String msgJson) throws Exception {
        UserConsultInitMsg userConsultInitMsg = JSONObject.parseObject(msgJson, UserConsultInitMsg.class);
        TUser tUser = wechatMiniMapper.selectTUserByWechatMiniOpenId(userConsultInitMsg.getMiniOpenId());
        if (tUser == null) {
            throw new Exception("用户信息异常");
        }
        UserConsultSession userConsultSession = new UserConsultSession();
        userConsultSession.setSessionType(userConsultInitMsg.getSessionType());
        userConsultSession.setSession(session);
        userConsultSession.setWorkRoomId(userConsultInitMsg.getWorkRoomId());
        userConsultSession.setUserId(tUser.getId());
        userConsultSession.setLastHeartBeat(new Date().getTime());
        switch (userConsultInitMsg.getSessionType().intValue()) {
            case UserConsultSessionType.CONSULT_TEACHER_SESSION:
                sessionMap.put("CONSULT_TEACHER_SESSION:" + userConsultInitMsg.getWorkRoomId(), userConsultSession);
                break;
            case UserConsultSessionType.USER_SESSION:
                sessionMap.put("USER_SESSION:" + userConsultInitMsg.getWorkRoomId(), userConsultSession);
                break;
        }
    }

    /**
     * 发送咨询消息
     *
     * @param userConsultConsultMsg
     * @throws IOException
     */
    private void sendMsg(UserConsultConsultMsg userConsultConsultMsg) throws IOException {
        TWorkRoomMessage tWorkRoomMessage = new TWorkRoomMessage();
        tWorkRoomMessage.setWorkRoomId(userConsultConsultMsg.getWorkRoomId());
        tWorkRoomMessage.setMessageContent(userConsultConsultMsg.getMessage());
        tWorkRoomMessage.setMessageRead("未读");
        Long workRoomId = userConsultConsultMsg.getWorkRoomId();
        Long userId = userConsultConsultMsg.getUserId();
        switch (userConsultConsultMsg.getSendType().intValue()) {
            case 1:
                // 咨询教师发送信息给用户, 完善tWorkRoomMessage
                tWorkRoomMessage.setMessageType("咨询教师");
                tWorkRoomMessage.setTargetId(userConsultConsultMsg.getUserId());
                break;
            case 2:
                // 用户发送给咨询教师工作室, 完善tWorkRoomMessage
                tWorkRoomMessage.setMessageType("用户");
                tWorkRoomMessage.setTargetId(userConsultConsultMsg.getUserId());
                break;
        }
        // 发送消息给用户
        UserConsultSession userConsultSession1 = sessionMap.get("USER_SESSION:" + userId);
        if (userConsultSession1 != null) {
            userConsultSession1.setLastHeartBeat(new Date().getTime());
            userConsultSession1.getSession().getBasicRemote().sendText(JSONObject.toJSONString(userConsultConsultMsg));
        }
        // 发送给咨询教师
        UserConsultSession userConsultSession2 = sessionMap.get("CONSULT_TEACHER_SESSION:" + workRoomId);
        if (userConsultSession2 != null) {
            userConsultSession2.setLastHeartBeat(new Date().getTime());
            userConsultSession2.getSession().getBasicRemote().sendText(JSONObject.toJSONString(userConsultConsultMsg));
        }
        // 加入数据到数据库
        workRoomMessageMapper.insertTWorkRoomMessage(tWorkRoomMessage);
    }

}
