package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TWorkRoomInvite;
import com.ruoyi.system.service.ITWorkRoomInviteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 工作室邀请Controller
 *
 * @author ruoyi
 * @date 2023-08-18
 */
@RestController
@RequestMapping("/system/tWorkRoomInvite")
public class TWorkRoomInviteController extends BaseController {
    @Autowired
    private ITWorkRoomInviteService tWorkRoomInviteService;

    /**
     * 查询工作室邀请列表
     */
    @PreAuthorize("@ss.hasPermi('system:tWorkRoomInvite:list')")
    @GetMapping("/list")
    public TableDataInfo list(TWorkRoomInvite tWorkRoomInvite) {
        startPage();
        List<TWorkRoomInvite> list = tWorkRoomInviteService.selectTWorkRoomInviteList(tWorkRoomInvite);
        return getDataTable(list);
    }

    /**
     * 导出工作室邀请列表
     */
    @PreAuthorize("@ss.hasPermi('system:tWorkRoomInvite:export')")
    @Log(title = "工作室邀请", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TWorkRoomInvite tWorkRoomInvite) {
        List<TWorkRoomInvite> list = tWorkRoomInviteService.selectTWorkRoomInviteList(tWorkRoomInvite);
        ExcelUtil<TWorkRoomInvite> util = new ExcelUtil<TWorkRoomInvite>(TWorkRoomInvite.class);
        util.exportExcel(response, list, "工作室邀请数据");
    }

    /**
     * 获取工作室邀请详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tWorkRoomInvite:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tWorkRoomInviteService.selectTWorkRoomInviteById(id));
    }

    /**
     * 新增工作室邀请
     */
    @PreAuthorize("@ss.hasPermi('system:tWorkRoomInvite:add')")
    @Log(title = "工作室邀请", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TWorkRoomInvite tWorkRoomInvite) {
        return toAjax(tWorkRoomInviteService.insertTWorkRoomInvite(tWorkRoomInvite));
    }

    /**
     * 修改工作室邀请
     */
    @PreAuthorize("@ss.hasPermi('system:tWorkRoomInvite:edit')")
    @Log(title = "工作室邀请", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TWorkRoomInvite tWorkRoomInvite) {
        return toAjax(tWorkRoomInviteService.updateTWorkRoomInvite(tWorkRoomInvite));
    }

    /**
     * 删除工作室邀请
     */
    @PreAuthorize("@ss.hasPermi('system:tWorkRoomInvite:remove')")
    @Log(title = "工作室邀请", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tWorkRoomInviteService.deleteTWorkRoomInviteByIds(ids));
    }
}
