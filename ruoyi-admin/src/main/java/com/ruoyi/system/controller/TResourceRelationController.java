package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TResourceRelation;
import com.ruoyi.system.service.ITResourceRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 资源关系Controller
 *
 * @author ruoyi
 * @date 2023-09-11
 */
@RestController
@RequestMapping("/system/tResourceRelation")
public class TResourceRelationController extends BaseController {
    @Autowired
    private ITResourceRelationService tResourceRelationService;

    /**
     * 查询资源关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tResourceRelation:list')")
    @GetMapping("/list")
    public TableDataInfo list(TResourceRelation tResourceRelation) {
        startPage();
        List<TResourceRelation> list = tResourceRelationService.selectTResourceRelationList(tResourceRelation);
        return getDataTable(list);
    }

    /**
     * 导出资源关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tResourceRelation:export')")
    @Log(title = "资源关系", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TResourceRelation tResourceRelation) {
        List<TResourceRelation> list = tResourceRelationService.selectTResourceRelationList(tResourceRelation);
        ExcelUtil<TResourceRelation> util = new ExcelUtil<TResourceRelation>(TResourceRelation.class);
        util.exportExcel(response, list, "资源关系数据");
    }

    /**
     * 获取资源关系详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tResourceRelation:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tResourceRelationService.selectTResourceRelationById(id));
    }

    /**
     * 新增资源关系
     */
    @PreAuthorize("@ss.hasPermi('system:tResourceRelation:add')")
    @Log(title = "资源关系", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TResourceRelation tResourceRelation) {
        return toAjax(tResourceRelationService.insertTResourceRelation(tResourceRelation));
    }

    /**
     * 修改资源关系
     */
    @PreAuthorize("@ss.hasPermi('system:tResourceRelation:edit')")
    @Log(title = "资源关系", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TResourceRelation tResourceRelation) {
        return toAjax(tResourceRelationService.updateTResourceRelation(tResourceRelation));
    }

    /**
     * 删除资源关系
     */
    @PreAuthorize("@ss.hasPermi('system:tResourceRelation:remove')")
    @Log(title = "资源关系", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tResourceRelationService.deleteTResourceRelationByIds(ids));
    }
}
