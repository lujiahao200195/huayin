package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TRelationWorkRoomSubject;
import com.ruoyi.system.service.ITRelationWorkRoomSubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 工作室与科目关系Controller
 *
 * @author ruoyi
 * @date 2023-08-21
 */
@RestController
@RequestMapping("/system/tRelationWorkRoomSubject")
public class TRelationWorkRoomSubjectController extends BaseController {
    @Autowired
    private ITRelationWorkRoomSubjectService tRelationWorkRoomSubjectService;

    /**
     * 查询工作室与科目关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationWorkRoomSubject:list')")
    @GetMapping("/list")
    public TableDataInfo list(TRelationWorkRoomSubject tRelationWorkRoomSubject) {
        startPage();
        List<TRelationWorkRoomSubject> list = tRelationWorkRoomSubjectService.selectTRelationWorkRoomSubjectList(tRelationWorkRoomSubject);
        return getDataTable(list);
    }

    /**
     * 导出工作室与科目关系列表
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationWorkRoomSubject:export')")
    @Log(title = "工作室与科目关系", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, TRelationWorkRoomSubject tRelationWorkRoomSubject) {
        List<TRelationWorkRoomSubject> list = tRelationWorkRoomSubjectService.selectTRelationWorkRoomSubjectList(tRelationWorkRoomSubject);
        ExcelUtil<TRelationWorkRoomSubject> util = new ExcelUtil<TRelationWorkRoomSubject>(TRelationWorkRoomSubject.class);
        util.exportExcel(response, list, "工作室与科目关系数据");
    }

    /**
     * 获取工作室与科目关系详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationWorkRoomSubject:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id) {
        return AjaxResult.success(tRelationWorkRoomSubjectService.selectTRelationWorkRoomSubjectById(id));
    }

    /**
     * 新增工作室与科目关系
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationWorkRoomSubject:add')")
    @Log(title = "工作室与科目关系", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TRelationWorkRoomSubject tRelationWorkRoomSubject) {
        return toAjax(tRelationWorkRoomSubjectService.insertTRelationWorkRoomSubject(tRelationWorkRoomSubject));
    }

    /**
     * 修改工作室与科目关系
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationWorkRoomSubject:edit')")
    @Log(title = "工作室与科目关系", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TRelationWorkRoomSubject tRelationWorkRoomSubject) {
        return toAjax(tRelationWorkRoomSubjectService.updateTRelationWorkRoomSubject(tRelationWorkRoomSubject));
    }

    /**
     * 删除工作室与科目关系
     */
    @PreAuthorize("@ss.hasPermi('system:tRelationWorkRoomSubject:remove')")
    @Log(title = "工作室与科目关系", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids) {
        return toAjax(tRelationWorkRoomSubjectService.deleteTRelationWorkRoomSubjectByIds(ids));
    }
}
