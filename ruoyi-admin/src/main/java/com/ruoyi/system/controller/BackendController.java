package com.ruoyi.system.controller;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.system.common.abs.BgLog;
import com.ruoyi.system.domain.TSlideshow;
import com.ruoyi.system.domain.TStudent;
import com.ruoyi.system.service.BackendService;
import com.ruoyi.system.service.ITSlideshowService;
import com.ruoyi.system.service.ITStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/system/backendController")
public class BackendController extends BaseController {

    @Resource
    private BackendService backendService;

    @Resource
    private ITSlideshowService itSlideshowService;

    @Autowired
    private ITStudentService tStudentService;


    /**
     * 查询学生接口
     *
     * @return
     */
    public AjaxResult changeStudentState() {
        return AjaxResult.success();
    }

    /**
     * 后台管理修改学生审核状态
     *
     * @param jsCodeMap
     * @return
     */
    @BgLog(methodType = "修改", moduleContent = "修改学生审核状态")
    @PostMapping("/change-student-state")
    public AjaxResult changeStudentState(@RequestBody Map<String, String> jsCodeMap) {
        //学生表id
        String id = jsCodeMap.get("id");
        //要修改成的状态
        String stateStr = jsCodeMap.get("state");
        backendService.changeStudentState(id, stateStr);
        return AjaxResult.success();
    }

    /**
     * 后台管理修改教师审核状态
     *
     * @param jsCodeMap
     * @return
     */
    @BgLog(methodType = "修改", moduleContent = "修改教师审核状态")
    @PostMapping("/change-teacher-state")
    public AjaxResult changeTeacherState(@RequestBody Map<String, String> jsCodeMap) {
        //教师表id
        String id = jsCodeMap.get("id");
        //要修改成的状态
        String stateStr = jsCodeMap.get("state");
        backendService.changeTeacherState(id, stateStr);
        return AjaxResult.success();
    }

    /**
     * 测试
     *
     * @return
     */
//    @WxMiniLog(methodDesc="查询",moduleName = "查询信息")
    @BgLog(methodType = "测试", moduleContent = "测试管理员日志")
    @PostMapping("/test")
    public AjaxResult test() {
        return AjaxResult.success("返回数据");
    }

    //todo 小程序轮播图
    // 新增轮播图 删除轮播图

    /**
     * 轮播图查询
     *
     * @return
     */
    @PostMapping("/all-slide-show")
    public AjaxResult allSlideShow() {
        return AjaxResult.success(itSlideshowService.selectTSlideshowList(new TSlideshow()));
    }


    /**
     * 根据学生id 查学生的优势科目和需强化科目
     *
     * @param jsCodeMap
     * @return
     */
    @PostMapping("/student-subject-details")
    public AjaxResult studentSubjectDetails(@RequestBody Map<String, Long> jsCodeMap) {
        Long studentId = jsCodeMap.get("studentId");
        if (studentId == null) {
            throw new RuntimeException("studentId为空");
        }
        return AjaxResult.success(backendService.studentSubjectDetails(studentId));
    }

    /**
     * 后台管理学生信息
     *
     * @param tStudent
     * @return
     */
    @PostMapping("/student-info-list")
    public TableDataInfo studentInfo(@RequestBody TStudent tStudent) {
        startPage();
        List<TStudent> list = tStudentService.selectTStudentList(tStudent);
        return getDataTable(list);
    }


}
