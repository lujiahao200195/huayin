package com.ruoyi.system.controller.backend.student;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.TStudent;
import com.ruoyi.system.service.backend.StudentService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * 后台管理学生管理
 */
@RestController
@RequestMapping("/system/backend/student")
public class StudentController extends BaseController {


    @Resource
    private StudentService studentService;

    /**
     * 学生列表
     *
     * @param tStudent
     * @return
     */
    @PostMapping("/student-list")
    public TableDataInfo list(@RequestBody TStudent tStudent) {
        startPage();
        List<TStudent> list = studentService.selectTStudentList(tStudent);
        return getDataTable(list);
    }

    /**
     * 导出学生列表
     */
    @PostMapping("/export")
    public void export(HttpServletResponse response, @RequestBody TStudent tStudent) {
        List<TStudent> list = studentService.selectTStudentList(tStudent);
        ExcelUtil<TStudent> util = new ExcelUtil<TStudent>(TStudent.class);
        util.exportExcel(response, list, "学生数据");
    }

    /**
     * 获取学生详细信息
     */
    @PostMapping("/student-info")
    public AjaxResult getInfo(@RequestBody Map<String, Long> parmMap) {
        Long id = parmMap.get("id");
        return AjaxResult.success(studentService.selectTStudentById(id));
    }

    /**
     * 根据学生id 查学生的优势科目和需强化科目
     *
     * @param jsCodeMap
     * @return
     */
    @PostMapping("/student-subject-details")
    public AjaxResult studentSubjectDetails(@RequestBody Map<String, Long> jsCodeMap) {
        Long studentId = jsCodeMap.get("id");
        if (studentId == null) {
            throw new RuntimeException("studentId为空");
        }
        return AjaxResult.success(studentService.studentSubjectDetails(studentId));
    }

    /**
     * 新增学生
     */
    @PostMapping("/student-add")
    public AjaxResult add(@RequestBody TStudent tStudent) {
        return toAjax(studentService.insertTStudent(tStudent));
    }

    /**
     * 修改学生
     */
    @PostMapping("/student-edit")
    public AjaxResult edit(@RequestBody TStudent tStudent) {
        return toAjax(studentService.updateTStudent(tStudent));
    }

    /**
     * 删除学生
     */
    @PostMapping("/student-remove")
    public AjaxResult remove(@RequestBody Map<String, Long[]> jsCodeMap) {
        Long[] ids = jsCodeMap.get("ids");
        return toAjax(studentService.deleteTStudentByIds(ids));
    }

    /**
     * 后台管理修改学生审核状态
     *
     * @param jsCodeMap
     * @return
     */
//    @BgLog(methodType = "修改", moduleContent = "修改学生审核状态")
    @PostMapping("/change-student-state")
    public AjaxResult changeStudentState(@RequestBody Map<String, String> jsCodeMap) {
        //学生表id
        String id = jsCodeMap.get("id");
        //要修改成的状态
        String stateStr = jsCodeMap.get("state");
        //原因
        String stateResult = jsCodeMap.get("stateResult");
        studentService.changeStudentState(id, stateStr, stateResult);
        return AjaxResult.success();
    }




}
