package com.ruoyi.system.service;

import com.ruoyi.system.domain.TBbsRelationPointExp;

import java.util.List;

/**
 * 积分经验关系Service接口
 *
 * @author ruoyi
 * @date 2023-08-28
 */
public interface ITBbsRelationPointExpService {
    /**
     * 查询积分经验关系
     *
     * @param id 积分经验关系主键
     * @return 积分经验关系
     */
    public TBbsRelationPointExp selectTBbsRelationPointExpById(Long id);

    /**
     * 查询积分经验关系列表
     *
     * @param tBbsRelationPointExp 积分经验关系
     * @return 积分经验关系集合
     */
    public List<TBbsRelationPointExp> selectTBbsRelationPointExpList(TBbsRelationPointExp tBbsRelationPointExp);

    /**
     * 新增积分经验关系
     *
     * @param tBbsRelationPointExp 积分经验关系
     * @return 结果
     */
    public int insertTBbsRelationPointExp(TBbsRelationPointExp tBbsRelationPointExp);

    /**
     * 修改积分经验关系
     *
     * @param tBbsRelationPointExp 积分经验关系
     * @return 结果
     */
    public int updateTBbsRelationPointExp(TBbsRelationPointExp tBbsRelationPointExp);

    /**
     * 批量删除积分经验关系
     *
     * @param ids 需要删除的积分经验关系主键集合
     * @return 结果
     */
    public int deleteTBbsRelationPointExpByIds(Long[] ids);

    /**
     * 删除积分经验关系信息
     *
     * @param id 积分经验关系主键
     * @return 结果
     */
    public int deleteTBbsRelationPointExpById(Long id);
}
