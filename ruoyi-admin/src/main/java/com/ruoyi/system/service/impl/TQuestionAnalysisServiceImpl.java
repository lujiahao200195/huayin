package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TQuestionAnalysisMapper;
import com.ruoyi.system.domain.TQuestionAnalysis;
import com.ruoyi.system.service.ITQuestionAnalysisService;

/**
 * 试题解析Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-28
 */
@Service
public class TQuestionAnalysisServiceImpl implements ITQuestionAnalysisService 
{
    @Autowired
    private TQuestionAnalysisMapper tQuestionAnalysisMapper;

    /**
     * 查询试题解析
     * 
     * @param id 试题解析主键
     * @return 试题解析
     */
    @Override
    public TQuestionAnalysis selectTQuestionAnalysisById(Long id)
    {
        return tQuestionAnalysisMapper.selectTQuestionAnalysisById(id);
    }

    /**
     * 查询试题解析列表
     * 
     * @param tQuestionAnalysis 试题解析
     * @return 试题解析
     */
    @Override
    public List<TQuestionAnalysis> selectTQuestionAnalysisList(TQuestionAnalysis tQuestionAnalysis)
    {
        return tQuestionAnalysisMapper.selectTQuestionAnalysisList(tQuestionAnalysis);
    }

    /**
     * 新增试题解析
     * 
     * @param tQuestionAnalysis 试题解析
     * @return 结果
     */
    @Override
    public int insertTQuestionAnalysis(TQuestionAnalysis tQuestionAnalysis)
    {
        return tQuestionAnalysisMapper.insertTQuestionAnalysis(tQuestionAnalysis);
    }

    /**
     * 修改试题解析
     * 
     * @param tQuestionAnalysis 试题解析
     * @return 结果
     */
    @Override
    public int updateTQuestionAnalysis(TQuestionAnalysis tQuestionAnalysis)
    {
        return tQuestionAnalysisMapper.updateTQuestionAnalysis(tQuestionAnalysis);
    }

    /**
     * 批量删除试题解析
     * 
     * @param ids 需要删除的试题解析主键
     * @return 结果
     */
    @Override
    public int deleteTQuestionAnalysisByIds(Long[] ids)
    {
        return tQuestionAnalysisMapper.deleteTQuestionAnalysisByIds(ids);
    }

    /**
     * 删除试题解析信息
     * 
     * @param id 试题解析主键
     * @return 结果
     */
    @Override
    public int deleteTQuestionAnalysisById(Long id)
    {
        return tQuestionAnalysisMapper.deleteTQuestionAnalysisById(id);
    }
}
