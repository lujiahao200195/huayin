package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TBbsUser;
import com.ruoyi.system.mapper.TBbsUserMapper;
import com.ruoyi.system.service.ITBbsUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 论坛用户数据Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-29
 */
@Service
public class TBbsUserServiceImpl implements ITBbsUserService {
    @Autowired
    private TBbsUserMapper tBbsUserMapper;

    /**
     * 查询论坛用户数据
     *
     * @param id 论坛用户数据主键
     * @return 论坛用户数据
     */
    @Override
    public TBbsUser selectTBbsUserById(Long id) {
        return tBbsUserMapper.selectTBbsUserById(id);
    }

    /**
     * 查询论坛用户数据列表
     *
     * @param tBbsUser 论坛用户数据
     * @return 论坛用户数据
     */
    @Override
    public List<TBbsUser> selectTBbsUserList(TBbsUser tBbsUser) {
        return tBbsUserMapper.selectTBbsUserList(tBbsUser);
    }

    /**
     * 新增论坛用户数据
     *
     * @param tBbsUser 论坛用户数据
     * @return 结果
     */
    @Override
    public int insertTBbsUser(TBbsUser tBbsUser) {
        return tBbsUserMapper.insertTBbsUser(tBbsUser);
    }

    /**
     * 修改论坛用户数据
     *
     * @param tBbsUser 论坛用户数据
     * @return 结果
     */
    @Override
    public int updateTBbsUser(TBbsUser tBbsUser) {
        return tBbsUserMapper.updateTBbsUser(tBbsUser);
    }

    /**
     * 批量删除论坛用户数据
     *
     * @param ids 需要删除的论坛用户数据主键
     * @return 结果
     */
    @Override
    public int deleteTBbsUserByIds(Long[] ids) {
        return tBbsUserMapper.deleteTBbsUserByIds(ids);
    }

    /**
     * 删除论坛用户数据信息
     *
     * @param id 论坛用户数据主键
     * @return 结果
     */
    @Override
    public int deleteTBbsUserById(Long id) {
        return tBbsUserMapper.deleteTBbsUserById(id);
    }
}
