package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.TArticleTwoType;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2024-04-17
 */
public interface ITArticleTwoTypeService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public TArticleTwoType selectTArticleTwoTypeById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param tArticleTwoType 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<TArticleTwoType> selectTArticleTwoTypeList(TArticleTwoType tArticleTwoType);

    /**
     * 新增【请填写功能名称】
     * 
     * @param tArticleTwoType 【请填写功能名称】
     * @return 结果
     */
    public int insertTArticleTwoType(TArticleTwoType tArticleTwoType);

    /**
     * 修改【请填写功能名称】
     * 
     * @param tArticleTwoType 【请填写功能名称】
     * @return 结果
     */
    public int updateTArticleTwoType(TArticleTwoType tArticleTwoType);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteTArticleTwoTypeByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteTArticleTwoTypeById(Long id);

    List<TArticleTwoType> getTwoTypeList();


}
