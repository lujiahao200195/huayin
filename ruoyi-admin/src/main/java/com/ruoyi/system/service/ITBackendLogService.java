package com.ruoyi.system.service;

import com.ruoyi.system.domain.TBackendLog;

import java.util.List;

/**
 * 管理端日志Service接口
 *
 * @author ruoyi
 * @date 2023-08-18
 */
public interface ITBackendLogService {
    /**
     * 查询管理端日志
     *
     * @param id 管理端日志主键
     * @return 管理端日志
     */
    public TBackendLog selectTBackendLogById(Long id);

    /**
     * 查询管理端日志列表
     *
     * @param tBackendLog 管理端日志
     * @return 管理端日志集合
     */
    public List<TBackendLog> selectTBackendLogList(TBackendLog tBackendLog);

    /**
     * 新增管理端日志
     *
     * @param tBackendLog 管理端日志
     * @return 结果
     */
    public int insertTBackendLog(TBackendLog tBackendLog);

    /**
     * 修改管理端日志
     *
     * @param tBackendLog 管理端日志
     * @return 结果
     */
    public int updateTBackendLog(TBackendLog tBackendLog);

    /**
     * 批量删除管理端日志
     *
     * @param ids 需要删除的管理端日志主键集合
     * @return 结果
     */
    public int deleteTBackendLogByIds(Long[] ids);

    /**
     * 删除管理端日志信息
     *
     * @param id 管理端日志主键
     * @return 结果
     */
    public int deleteTBackendLogById(Long id);
}
