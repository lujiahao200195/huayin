package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TBackendLog;
import com.ruoyi.system.mapper.TBackendLogMapper;
import com.ruoyi.system.service.ITBackendLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 管理端日志Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-18
 */
@Service
public class TBackendLogServiceImpl implements ITBackendLogService {
    @Autowired
    private TBackendLogMapper tBackendLogMapper;

    /**
     * 查询管理端日志
     *
     * @param id 管理端日志主键
     * @return 管理端日志
     */
    @Override
    public TBackendLog selectTBackendLogById(Long id) {
        return tBackendLogMapper.selectTBackendLogById(id);
    }

    /**
     * 查询管理端日志列表
     *
     * @param tBackendLog 管理端日志
     * @return 管理端日志
     */
    @Override
    public List<TBackendLog> selectTBackendLogList(TBackendLog tBackendLog) {
        return tBackendLogMapper.selectTBackendLogList(tBackendLog);
    }

    /**
     * 新增管理端日志
     *
     * @param tBackendLog 管理端日志
     * @return 结果
     */
    @Override
    public int insertTBackendLog(TBackendLog tBackendLog) {
        return tBackendLogMapper.insertTBackendLog(tBackendLog);
    }

    /**
     * 修改管理端日志
     *
     * @param tBackendLog 管理端日志
     * @return 结果
     */
    @Override
    public int updateTBackendLog(TBackendLog tBackendLog) {
        return tBackendLogMapper.updateTBackendLog(tBackendLog);
    }

    /**
     * 批量删除管理端日志
     *
     * @param ids 需要删除的管理端日志主键
     * @return 结果
     */
    @Override
    public int deleteTBackendLogByIds(Long[] ids) {
        return tBackendLogMapper.deleteTBackendLogByIds(ids);
    }

    /**
     * 删除管理端日志信息
     *
     * @param id 管理端日志主键
     * @return 结果
     */
    @Override
    public int deleteTBackendLogById(Long id) {
        return tBackendLogMapper.deleteTBackendLogById(id);
    }
}
