package com.ruoyi.system.service.backend.impl;

import com.ruoyi.system.domain.TArticle;
import com.ruoyi.system.mapper.backend.ArticleMapper;
import com.ruoyi.system.service.backend.ArticleService;
import com.ruoyi.system.vo.wechat.backend.ExportSchoolArticle;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 文章Service业务层处理
 */
@Service
public class ArticleServiceImpl implements ArticleService {
    @Resource
    private ArticleMapper articleMapper;

    /**
     * 查询文章
     *
     * @param id 文章主键
     * @return 文章
     */
    @Override
    public TArticle selectTArticleById(Long id) {
        return articleMapper.selectTArticleById(id);
    }

    /**
     * 查询文章列表
     *
     * @param tArticle 文章
     * @return 文章
     */
    @Override
    public List<TArticle> selectTArticleList(TArticle tArticle) {
        return articleMapper.selectTArticleList(tArticle);
    }

    @Override
    public List<ExportSchoolArticle> exportSchoolArticle(Long[] ids) {
        List<ExportSchoolArticle> exportSchoolArticles = articleMapper.exportSchoolArticle(ids);
        return exportSchoolArticles;
    }

    /**
     * 新增文章
     *
     * @param tArticle 文章
     * @return 结果
     */
    @Override
    public int insertTArticle(TArticle tArticle) {
        return articleMapper.insertTArticle(tArticle);
    }

    /**
     * 修改文章
     *
     * @param tArticle 文章
     * @return 结果
     */
    @Override
    public int updateTArticle(TArticle tArticle) {
        return articleMapper.updateTArticle(tArticle);
    }

    /**
     * 批量删除文章
     *
     * @param ids 需要删除的文章主键
     * @return 结果
     */
    @Override
    public int deleteTArticleByIds(Long[] ids) {
        return articleMapper.deleteTArticleByIds(ids);
    }

    /**
     * 删除文章信息
     *
     * @param id 文章主键
     * @return 结果
     */
    @Override
    public int deleteTArticleById(Long id) {
        return articleMapper.deleteTArticleById(id);
    }

    @Override
    public List<TArticle> getSchoolArticle(TArticle article) {
        List<TArticle> TArticles = articleMapper.getXXArticle(article,"微信小程序学校资讯");
        return TArticles;
    }

    @Override
    public String addSchoolArticle(TArticle tArticle) {
        if(tArticle==null){
            throw new RuntimeException("文章对象空");
        }
        int i = articleMapper.insertTArticle(tArticle);
        if (i == 0) {
            throw new RuntimeException("新增文章失败");
        }
        int i1 = articleMapper.insertArticleTypeRelation(tArticle.getId(), "微信小程序学校资讯");
        if (i1 == 0) {
            throw new RuntimeException("新增关系表失败");
        }
        return "新增成功";
    }

    @Override
    public String removeSchoolArticle(Long[] ids) {
        if (ids.length == 0) {
            throw new RuntimeException("文章id为空");
        }
        int i = articleMapper.deleteTArticleByIds(ids);
        if (i == 0) {
            throw new RuntimeException("删除文章失败");
        }
        int i1 = articleMapper.deleteArticleTypeRelation(ids, "微信小程序学校资讯");
        if (i1 == 0) {
            throw new RuntimeException("删除文章类型关系失败");
        }
        return "删除成功";
    }
}
