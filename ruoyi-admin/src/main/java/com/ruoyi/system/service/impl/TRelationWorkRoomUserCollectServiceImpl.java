package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TRelationWorkRoomUserCollect;
import com.ruoyi.system.mapper.TRelationWorkRoomUserCollectMapper;
import com.ruoyi.system.service.ITRelationWorkRoomUserCollectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 工作室用户关系-收藏用Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-21
 */
@Service
public class TRelationWorkRoomUserCollectServiceImpl implements ITRelationWorkRoomUserCollectService {
    @Autowired
    private TRelationWorkRoomUserCollectMapper tRelationWorkRoomUserCollectMapper;

    /**
     * 查询工作室用户关系-收藏用
     *
     * @param id 工作室用户关系-收藏用主键
     * @return 工作室用户关系-收藏用
     */
    @Override
    public TRelationWorkRoomUserCollect selectTRelationWorkRoomUserCollectById(Long id) {
        return tRelationWorkRoomUserCollectMapper.selectTRelationWorkRoomUserCollectById(id);
    }

    /**
     * 查询工作室用户关系-收藏用列表
     *
     * @param tRelationWorkRoomUserCollect 工作室用户关系-收藏用
     * @return 工作室用户关系-收藏用
     */
    @Override
    public List<TRelationWorkRoomUserCollect> selectTRelationWorkRoomUserCollectList(TRelationWorkRoomUserCollect tRelationWorkRoomUserCollect) {
        return tRelationWorkRoomUserCollectMapper.selectTRelationWorkRoomUserCollectList(tRelationWorkRoomUserCollect);
    }

    /**
     * 新增工作室用户关系-收藏用
     *
     * @param tRelationWorkRoomUserCollect 工作室用户关系-收藏用
     * @return 结果
     */
    @Override
    public int insertTRelationWorkRoomUserCollect(TRelationWorkRoomUserCollect tRelationWorkRoomUserCollect) {
        return tRelationWorkRoomUserCollectMapper.insertTRelationWorkRoomUserCollect(tRelationWorkRoomUserCollect);
    }

    /**
     * 修改工作室用户关系-收藏用
     *
     * @param tRelationWorkRoomUserCollect 工作室用户关系-收藏用
     * @return 结果
     */
    @Override
    public int updateTRelationWorkRoomUserCollect(TRelationWorkRoomUserCollect tRelationWorkRoomUserCollect) {
        return tRelationWorkRoomUserCollectMapper.updateTRelationWorkRoomUserCollect(tRelationWorkRoomUserCollect);
    }

    /**
     * 批量删除工作室用户关系-收藏用
     *
     * @param ids 需要删除的工作室用户关系-收藏用主键
     * @return 结果
     */
    @Override
    public int deleteTRelationWorkRoomUserCollectByIds(Long[] ids) {
        return tRelationWorkRoomUserCollectMapper.deleteTRelationWorkRoomUserCollectByIds(ids);
    }

    /**
     * 删除工作室用户关系-收藏用信息
     *
     * @param id 工作室用户关系-收藏用主键
     * @return 结果
     */
    @Override
    public int deleteTRelationWorkRoomUserCollectById(Long id) {
        return tRelationWorkRoomUserCollectMapper.deleteTRelationWorkRoomUserCollectById(id);
    }
}
