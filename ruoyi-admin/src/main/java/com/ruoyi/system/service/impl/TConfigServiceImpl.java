package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TConfigMapper;
import com.ruoyi.system.domain.TConfig;
import com.ruoyi.system.service.ITConfigService;

/**
 * 配置Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-09-19
 */
@Service
public class TConfigServiceImpl implements ITConfigService 
{
    @Autowired
    private TConfigMapper tConfigMapper;

    /**
     * 查询配置
     * 
     * @param id 配置主键
     * @return 配置
     */
    @Override
    public TConfig selectTConfigById(Long id)
    {
        return tConfigMapper.selectTConfigById(id);
    }

    /**
     * 查询配置列表
     * 
     * @param tConfig 配置
     * @return 配置
     */
    @Override
    public List<TConfig> selectTConfigList(TConfig tConfig)
    {
        return tConfigMapper.selectTConfigList(tConfig);
    }

    /**
     * 新增配置
     * 
     * @param tConfig 配置
     * @return 结果
     */
    @Override
    public int insertTConfig(TConfig tConfig)
    {
        return tConfigMapper.insertTConfig(tConfig);
    }

    /**
     * 修改配置
     * 
     * @param tConfig 配置
     * @return 结果
     */
    @Override
    public int updateTConfig(TConfig tConfig)
    {
        return tConfigMapper.updateTConfig(tConfig);
    }

    /**
     * 批量删除配置
     * 
     * @param ids 需要删除的配置主键
     * @return 结果
     */
    @Override
    public int deleteTConfigByIds(Long[] ids)
    {
        return tConfigMapper.deleteTConfigByIds(ids);
    }

    /**
     * 删除配置信息
     * 
     * @param id 配置主键
     * @return 结果
     */
    @Override
    public int deleteTConfigById(Long id)
    {
        return tConfigMapper.deleteTConfigById(id);
    }
}
