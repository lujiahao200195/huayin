package com.ruoyi.system.service.impl;

import java.util.List;

import com.ruoyi.system.mapper.TArticleTwoTypeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.domain.TArticleTwoType;
import com.ruoyi.system.service.ITArticleTwoTypeService;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-17
 */
@Service
public class TArticleTwoTypeServiceImpl implements ITArticleTwoTypeService 
{
    @Autowired
    private TArticleTwoTypeMapper tArticleTwoTypeMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public TArticleTwoType selectTArticleTwoTypeById(Long id)
    {
        return tArticleTwoTypeMapper.selectTArticleTwoTypeById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param tArticleTwoType 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<TArticleTwoType> selectTArticleTwoTypeList(TArticleTwoType tArticleTwoType)
    {
        return tArticleTwoTypeMapper.selectTArticleTwoTypeList(tArticleTwoType);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param tArticleTwoType 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertTArticleTwoType(TArticleTwoType tArticleTwoType)
    {
        return tArticleTwoTypeMapper.insertTArticleTwoType(tArticleTwoType);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param tArticleTwoType 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateTArticleTwoType(TArticleTwoType tArticleTwoType)
    {
        return tArticleTwoTypeMapper.updateTArticleTwoType(tArticleTwoType);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteTArticleTwoTypeByIds(Long[] ids)
    {
        return tArticleTwoTypeMapper.deleteTArticleTwoTypeByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteTArticleTwoTypeById(Long id)
    {
        return tArticleTwoTypeMapper.deleteTArticleTwoTypeById(id);
    }

    @Override
    public List<TArticleTwoType> getTwoTypeList() {
        return tArticleTwoTypeMapper.selectTwoTypeList();
    }
}
