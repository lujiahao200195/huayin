package com.ruoyi.system.service;

import com.ruoyi.system.domain.TTeacherAdvantages;

import java.util.List;

/**
 * 教师个人优势Service接口
 *
 * @author ruoyi
 * @date 2023-08-16
 */
public interface ITTeacherAdvantagesService {
    /**
     * 查询教师个人优势
     *
     * @param id 教师个人优势主键
     * @return 教师个人优势
     */
    public TTeacherAdvantages selectTTeacherAdvantagesById(Long id);

    /**
     * 查询教师个人优势列表
     *
     * @param tTeacherAdvantages 教师个人优势
     * @return 教师个人优势集合
     */
    public List<TTeacherAdvantages> selectTTeacherAdvantagesList(TTeacherAdvantages tTeacherAdvantages);

    /**
     * 新增教师个人优势
     *
     * @param tTeacherAdvantages 教师个人优势
     * @return 结果
     */
    public int insertTTeacherAdvantages(TTeacherAdvantages tTeacherAdvantages);

    /**
     * 修改教师个人优势
     *
     * @param tTeacherAdvantages 教师个人优势
     * @return 结果
     */
    public int updateTTeacherAdvantages(TTeacherAdvantages tTeacherAdvantages);

    /**
     * 批量删除教师个人优势
     *
     * @param ids 需要删除的教师个人优势主键集合
     * @return 结果
     */
    public int deleteTTeacherAdvantagesByIds(Long[] ids);

    /**
     * 删除教师个人优势信息
     *
     * @param id 教师个人优势主键
     * @return 结果
     */
    public int deleteTTeacherAdvantagesById(Long id);
}
