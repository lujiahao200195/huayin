package com.ruoyi.system.service;

import com.ruoyi.system.domain.TResourceTag;

import java.util.List;

/**
 * 资源标签Service接口
 *
 * @author ruoyi
 * @date 2023-09-08
 */
public interface ITResourceTagService {
    /**
     * 查询资源标签
     *
     * @param id 资源标签主键
     * @return 资源标签
     */
    public TResourceTag selectTResourceTagById(Long id);

    /**
     * 查询资源标签列表
     *
     * @param tResourceTag 资源标签
     * @return 资源标签集合
     */
    public List<TResourceTag> selectTResourceTagList(TResourceTag tResourceTag);

    /**
     * 新增资源标签
     *
     * @param tResourceTag 资源标签
     * @return 结果
     */
    public int insertTResourceTag(TResourceTag tResourceTag);

    /**
     * 修改资源标签
     *
     * @param tResourceTag 资源标签
     * @return 结果
     */
    public int updateTResourceTag(TResourceTag tResourceTag);

    /**
     * 批量删除资源标签
     *
     * @param ids 需要删除的资源标签主键集合
     * @return 结果
     */
    public int deleteTResourceTagByIds(Long[] ids);

    /**
     * 删除资源标签信息
     *
     * @param id 资源标签主键
     * @return 结果
     */
    public int deleteTResourceTagById(Long id);
}
