package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TResourceTag;
import com.ruoyi.system.mapper.TResourceTagMapper;
import com.ruoyi.system.service.ITResourceTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 资源标签Service业务层处理
 *
 * @author ruoyi
 * @date 2023-09-08
 */
@Service
public class TResourceTagServiceImpl implements ITResourceTagService {
    @Autowired
    private TResourceTagMapper tResourceTagMapper;

    /**
     * 查询资源标签
     *
     * @param id 资源标签主键
     * @return 资源标签
     */
    @Override
    public TResourceTag selectTResourceTagById(Long id) {
        return tResourceTagMapper.selectTResourceTagById(id);
    }

    /**
     * 查询资源标签列表
     *
     * @param tResourceTag 资源标签
     * @return 资源标签
     */
    @Override
    public List<TResourceTag> selectTResourceTagList(TResourceTag tResourceTag) {
        return tResourceTagMapper.selectTResourceTagList(tResourceTag);
    }

    /**
     * 新增资源标签
     *
     * @param tResourceTag 资源标签
     * @return 结果
     */
    @Override
    public int insertTResourceTag(TResourceTag tResourceTag) {
        return tResourceTagMapper.insertTResourceTag(tResourceTag);
    }

    /**
     * 修改资源标签
     *
     * @param tResourceTag 资源标签
     * @return 结果
     */
    @Override
    public int updateTResourceTag(TResourceTag tResourceTag) {
        return tResourceTagMapper.updateTResourceTag(tResourceTag);
    }

    /**
     * 批量删除资源标签
     *
     * @param ids 需要删除的资源标签主键
     * @return 结果
     */
    @Override
    public int deleteTResourceTagByIds(Long[] ids) {
        return tResourceTagMapper.deleteTResourceTagByIds(ids);
    }

    /**
     * 删除资源标签信息
     *
     * @param id 资源标签主键
     * @return 结果
     */
    @Override
    public int deleteTResourceTagById(Long id) {
        return tResourceTagMapper.deleteTResourceTagById(id);
    }
}
