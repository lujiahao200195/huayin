package com.ruoyi.system.service;

import com.ruoyi.system.domain.TBbsRelationAudit;

import java.util.List;

/**
 * 审核关系Service接口
 *
 * @author ruoyi
 * @date 2023-08-28
 */
public interface ITBbsRelationAuditService {
    /**
     * 查询审核关系
     *
     * @param id 审核关系主键
     * @return 审核关系
     */
    public TBbsRelationAudit selectTBbsRelationAuditById(Long id);

    /**
     * 查询审核关系列表
     *
     * @param tBbsRelationAudit 审核关系
     * @return 审核关系集合
     */
    public List<TBbsRelationAudit> selectTBbsRelationAuditList(TBbsRelationAudit tBbsRelationAudit);

    /**
     * 新增审核关系
     *
     * @param tBbsRelationAudit 审核关系
     * @return 结果
     */
    public int insertTBbsRelationAudit(TBbsRelationAudit tBbsRelationAudit);

    /**
     * 修改审核关系
     *
     * @param tBbsRelationAudit 审核关系
     * @return 结果
     */
    public int updateTBbsRelationAudit(TBbsRelationAudit tBbsRelationAudit);

    /**
     * 批量删除审核关系
     *
     * @param ids 需要删除的审核关系主键集合
     * @return 结果
     */
    public int deleteTBbsRelationAuditByIds(Long[] ids);

    /**
     * 删除审核关系信息
     *
     * @param id 审核关系主键
     * @return 结果
     */
    public int deleteTBbsRelationAuditById(Long id);
}
