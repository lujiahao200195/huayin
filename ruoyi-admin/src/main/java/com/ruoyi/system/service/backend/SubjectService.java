package com.ruoyi.system.service.backend;

import com.ruoyi.system.bo.wechat.AddSubjectArticleDo;
import com.ruoyi.system.domain.TArticle;
import com.ruoyi.system.domain.TSubject;

import java.util.List;

/**
 * 教学科目Service接口
 *
 * @author ruoyi
 * @date 2023-08-18
 */
public interface SubjectService {
    /**
     * 查询教学科目
     *
     * @param id 教学科目主键
     * @return 教学科目
     */
    public TSubject selectTSubjectById(Long id);

    /**
     * 查询教学科目列表
     *
     * @param tSubject 教学科目
     * @return 教学科目集合
     */
    public List<TSubject> selectTSubjectList(TSubject tSubject);

    /**
     * 新增教学科目
     *
     * @param tSubject 教学科目
     * @return 结果
     */
    public int insertTSubject(TSubject tSubject);

    /**
     * 修改教学科目
     *
     * @param tSubject 教学科目
     * @return 结果
     */
    public int updateTSubject(TSubject tSubject);

    /**
     * 批量删除教学科目
     *
     * @param ids 需要删除的教学科目主键集合
     * @return 结果
     */
    public int deleteTSubjectByIds(Long[] ids);

    /**
     * 删除教学科目信息
     *
     * @param id 教学科目主键
     * @return 结果
     */
    public int deleteTSubjectById(Long id);

    /**
     * 添加科目文章
     *
     * @param addSubjectArticleDo
     * @return
     */
    String addSubjectArticle(AddSubjectArticleDo addSubjectArticleDo);

    /**
     * 根据科目id获取文章列表
     *
     * @param id
     * @return
     */
    List<TArticle> getSubjectArticles(Long id);

    /**
     * 删除科目文章
     *
     * @param subjectId
     * @param articleId
     * @return
     */
    String removeSubjectArticle(Long subjectId, Long articleId);
}
