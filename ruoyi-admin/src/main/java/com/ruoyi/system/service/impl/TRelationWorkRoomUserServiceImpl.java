package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TRelationWorkRoomUser;
import com.ruoyi.system.mapper.TRelationWorkRoomUserMapper;
import com.ruoyi.system.service.ITRelationWorkRoomUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 工作室用户关系Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-30
 */
@Service
public class TRelationWorkRoomUserServiceImpl implements ITRelationWorkRoomUserService {
    @Autowired
    private TRelationWorkRoomUserMapper tRelationWorkRoomUserMapper;

    /**
     * 查询工作室用户关系
     *
     * @param id 工作室用户关系主键
     * @return 工作室用户关系
     */
    @Override
    public TRelationWorkRoomUser selectTRelationWorkRoomUserById(Long id) {
        return tRelationWorkRoomUserMapper.selectTRelationWorkRoomUserById(id);
    }

    /**
     * 查询工作室用户关系列表
     *
     * @param tRelationWorkRoomUser 工作室用户关系
     * @return 工作室用户关系
     */
    @Override
    public List<TRelationWorkRoomUser> selectTRelationWorkRoomUserList(TRelationWorkRoomUser tRelationWorkRoomUser) {
        return tRelationWorkRoomUserMapper.selectTRelationWorkRoomUserList(tRelationWorkRoomUser);
    }

    /**
     * 新增工作室用户关系
     *
     * @param tRelationWorkRoomUser 工作室用户关系
     * @return 结果
     */
    @Override
    public int insertTRelationWorkRoomUser(TRelationWorkRoomUser tRelationWorkRoomUser) {
        return tRelationWorkRoomUserMapper.insertTRelationWorkRoomUser(tRelationWorkRoomUser);
    }

    /**
     * 修改工作室用户关系
     *
     * @param tRelationWorkRoomUser 工作室用户关系
     * @return 结果
     */
    @Override
    public int updateTRelationWorkRoomUser(TRelationWorkRoomUser tRelationWorkRoomUser) {
        return tRelationWorkRoomUserMapper.updateTRelationWorkRoomUser(tRelationWorkRoomUser);
    }

    /**
     * 批量删除工作室用户关系
     *
     * @param ids 需要删除的工作室用户关系主键
     * @return 结果
     */
    @Override
    public int deleteTRelationWorkRoomUserByIds(Long[] ids) {
        return tRelationWorkRoomUserMapper.deleteTRelationWorkRoomUserByIds(ids);
    }

    /**
     * 删除工作室用户关系信息
     *
     * @param id 工作室用户关系主键
     * @return 结果
     */
    @Override
    public int deleteTRelationWorkRoomUserById(Long id) {
        return tRelationWorkRoomUserMapper.deleteTRelationWorkRoomUserById(id);
    }
}
