package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TResourceMapper;
import com.ruoyi.system.domain.TResource;
import com.ruoyi.system.service.ITResourceService;

/**
 * 资源Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-09-21
 */
@Service
public class TResourceServiceImpl implements ITResourceService 
{
    @Autowired
    private TResourceMapper tResourceMapper;

    /**
     * 查询资源
     * 
     * @param id 资源主键
     * @return 资源
     */
    @Override
    public TResource selectTResourceById(Long id)
    {
        return tResourceMapper.selectTResourceById(id);
    }

    /**
     * 查询资源列表
     * 
     * @param tResource 资源
     * @return 资源
     */
    @Override
    public List<TResource> selectTResourceList(TResource tResource)
    {
        return tResourceMapper.selectTResourceList(tResource);
    }

    /**
     * 新增资源
     * 
     * @param tResource 资源
     * @return 结果
     */
    @Override
    public int insertTResource(TResource tResource)
    {
        return tResourceMapper.insertTResource(tResource);
    }

    /**
     * 修改资源
     * 
     * @param tResource 资源
     * @return 结果
     */
    @Override
    public int updateTResource(TResource tResource)
    {
        return tResourceMapper.updateTResource(tResource);
    }

    /**
     * 批量删除资源
     * 
     * @param ids 需要删除的资源主键
     * @return 结果
     */
    @Override
    public int deleteTResourceByIds(Long[] ids)
    {
        return tResourceMapper.deleteTResourceByIds(ids);
    }

    /**
     * 删除资源信息
     * 
     * @param id 资源主键
     * @return 结果
     */
    @Override
    public int deleteTResourceById(Long id)
    {
        return tResourceMapper.deleteTResourceById(id);
    }
}
