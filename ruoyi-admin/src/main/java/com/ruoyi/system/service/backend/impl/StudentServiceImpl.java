package com.ruoyi.system.service.backend.impl;

import com.ruoyi.system.common.abs.wechatmini.MiniAuditData;
import com.ruoyi.system.common.config.Custom;
import com.ruoyi.system.common.tools.NTools;
import com.ruoyi.system.common.tools.WxUtil;
import com.ruoyi.system.domain.TStudent;
import com.ruoyi.system.mapper.backend.StudentMapper;
import com.ruoyi.system.service.backend.StudentService;
import com.ruoyi.system.vo.wechat.StudentSubjectDetails;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class StudentServiceImpl implements StudentService {


    @Resource
    WxUtil wxUtil;
    @Resource
    private StudentMapper studentMapper;

    @Resource
    Custom custom;

    /**
     * 查询学生
     *
     * @param id 学生主键
     * @return 学生
     */
    @Override
    public TStudent selectTStudentById(Long id) {
        return studentMapper.selectTStudentById(id);
    }

    @Override
    public StudentSubjectDetails studentSubjectDetails(Long id) {
        StudentSubjectDetails studentSubjectDetails = new StudentSubjectDetails();
        studentSubjectDetails.setAdvantageSubject(NTools.arrayToStr(studentMapper.selectObjectsByStudentId(id)));
        studentSubjectDetails.setNeedSubject(studentMapper.selectNeedSubjectByStudentId(id));
        return studentSubjectDetails;
    }

    /**
     * 查询学生列表
     *
     * @param tStudent 学生
     * @return 学生
     */
    @Override
    public List<TStudent> selectTStudentList(TStudent tStudent) {
        return studentMapper.selectTStudentList(tStudent);
    }

    /**
     * 新增学生
     *
     * @param tStudent 学生
     * @return 结果
     */
    @Override
    public int insertTStudent(TStudent tStudent) {
        return studentMapper.insertTStudent(tStudent);
    }

    /**
     * 修改学生
     *
     * @param tStudent 学生
     * @return 结果
     */
    @Override
    public int updateTStudent(TStudent tStudent) {
        return studentMapper.updateTStudent(tStudent);
    }

    /**
     * 批量删除学生
     *
     * @param ids 需要删除的学生主键
     * @return 结果
     */
    @Override
    public int deleteTStudentByIds(Long[] ids) {
        return studentMapper.deleteTStudentByIds(ids);
    }

    /**
     * 删除学生信息
     *
     * @param id 学生主键
     * @return 结果
     */
    @Override
    public int deleteTStudentById(Long id) {
        return studentMapper.deleteTStudentById(id);
    }

    @Override
    public String changeStudentState(String id, String stateStr, String stateResult) {
        //判断是否有存在记录
        Integer count = studentMapper.count("t_student", Integer.valueOf(id));
        if (count > 0) {
            //修改状态
            Integer integer = studentMapper.updateStudentStateById(Integer.valueOf(id), stateStr, stateResult);
            if (integer == 0) {
                throw new RuntimeException("修改状态失败");
            }
            //通过id查学生名
//            GetStudentAndOpenId getStudentAndOpenId = studentMapper.selectStudentById(Integer.valueOf(id));
            TStudent tStudent = studentMapper.selectTStudentById(Long.valueOf(id));
            //userid查小程序openid
            String openid = studentMapper.selectOpenIdById(tStudent.getUserId());

//            List<String> strings = studentMapper.selectSubjectsById(tStudent.getId());
            //需要强化的科目名称字符串数组
//            String[] subjects = new String[strings.size()];
//            //遍历插入
//            for (int i = 0; i < strings.size(); i++) {
//                subjects[i] = strings.get(i);
//            }
            //小程序通知模板
            MiniAuditData miniAuditData = new MiniAuditData();
            miniAuditData.setPhrase1(stateStr);
            miniAuditData.setThing8(tStudent.getStudentName());
            miniAuditData.setThing3(stateResult);
            miniAuditData.setTime2(NTools.createTime());
//            MiniStudentAuditData miniStudentAuditData = new MiniStudentAuditData();
//            miniStudentAuditData.setName2(getStudentAndOpenId.getStudentName());
//            miniStudentAuditData.setThing1(NTools.arrayToStr(subjects));
//            miniStudentAuditData.setPhone_number4(getStudentAndOpenId.getStudentPhone());
//            miniStudentAuditData.setPhrase7(stateStr);
//            miniStudentAuditData.setTime6(NTools.createTime());
            try {
                System.out.println("openId" + openid);
                //调用通知接口
                wxUtil.weChatMiniMsg(miniAuditData.msgTemplateData(openid, custom.AuditSuccess, custom.wechatMiniInfoLang,custom.wechatMiniPage,custom.miniprogramState));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

        }
        return "成功";
    }
}
