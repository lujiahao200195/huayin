package com.ruoyi.system.service;

import com.ruoyi.system.domain.TWorkRoom;

import java.util.List;

/**
 * 工作室Service接口
 *
 * @author ruoyi
 * @date 2023-08-22
 */
public interface ITWorkRoomService {
    /**
     * 查询工作室
     *
     * @param id 工作室主键
     * @return 工作室
     */
    public TWorkRoom selectTWorkRoomById(Long id);

    /**
     * 查询工作室列表
     *
     * @param tWorkRoom 工作室
     * @return 工作室集合
     */
    public List<TWorkRoom> selectTWorkRoomList(TWorkRoom tWorkRoom);

    /**
     * 新增工作室
     *
     * @param tWorkRoom 工作室
     * @return 结果
     */
    public int insertTWorkRoom(TWorkRoom tWorkRoom);

    /**
     * 修改工作室
     *
     * @param tWorkRoom 工作室
     * @return 结果
     */
    public int updateTWorkRoom(TWorkRoom tWorkRoom);

    /**
     * 批量删除工作室
     *
     * @param ids 需要删除的工作室主键集合
     * @return 结果
     */
    public int deleteTWorkRoomByIds(Long[] ids);

    /**
     * 删除工作室信息
     *
     * @param id 工作室主键
     * @return 结果
     */
    public int deleteTWorkRoomById(Long id);
}
