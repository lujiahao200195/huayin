package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TRelationWorkRoomSubject;
import com.ruoyi.system.mapper.TRelationWorkRoomSubjectMapper;
import com.ruoyi.system.service.ITRelationWorkRoomSubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 工作室与科目关系Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-21
 */
@Service
public class TRelationWorkRoomSubjectServiceImpl implements ITRelationWorkRoomSubjectService {
    @Autowired
    private TRelationWorkRoomSubjectMapper tRelationWorkRoomSubjectMapper;

    /**
     * 查询工作室与科目关系
     *
     * @param id 工作室与科目关系主键
     * @return 工作室与科目关系
     */
    @Override
    public TRelationWorkRoomSubject selectTRelationWorkRoomSubjectById(Long id) {
        return tRelationWorkRoomSubjectMapper.selectTRelationWorkRoomSubjectById(id);
    }

    /**
     * 查询工作室与科目关系列表
     *
     * @param tRelationWorkRoomSubject 工作室与科目关系
     * @return 工作室与科目关系
     */
    @Override
    public List<TRelationWorkRoomSubject> selectTRelationWorkRoomSubjectList(TRelationWorkRoomSubject tRelationWorkRoomSubject) {
        return tRelationWorkRoomSubjectMapper.selectTRelationWorkRoomSubjectList(tRelationWorkRoomSubject);
    }

    /**
     * 新增工作室与科目关系
     *
     * @param tRelationWorkRoomSubject 工作室与科目关系
     * @return 结果
     */
    @Override
    public int insertTRelationWorkRoomSubject(TRelationWorkRoomSubject tRelationWorkRoomSubject) {
        return tRelationWorkRoomSubjectMapper.insertTRelationWorkRoomSubject(tRelationWorkRoomSubject);
    }

    /**
     * 修改工作室与科目关系
     *
     * @param tRelationWorkRoomSubject 工作室与科目关系
     * @return 结果
     */
    @Override
    public int updateTRelationWorkRoomSubject(TRelationWorkRoomSubject tRelationWorkRoomSubject) {
        return tRelationWorkRoomSubjectMapper.updateTRelationWorkRoomSubject(tRelationWorkRoomSubject);
    }

    /**
     * 批量删除工作室与科目关系
     *
     * @param ids 需要删除的工作室与科目关系主键
     * @return 结果
     */
    @Override
    public int deleteTRelationWorkRoomSubjectByIds(Long[] ids) {
        return tRelationWorkRoomSubjectMapper.deleteTRelationWorkRoomSubjectByIds(ids);
    }

    /**
     * 删除工作室与科目关系信息
     *
     * @param id 工作室与科目关系主键
     * @return 结果
     */
    @Override
    public int deleteTRelationWorkRoomSubjectById(Long id) {
        return tRelationWorkRoomSubjectMapper.deleteTRelationWorkRoomSubjectById(id);
    }
}
