package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TBbsRelationBoardTag;
import com.ruoyi.system.mapper.TBbsRelationBoardTagMapper;
import com.ruoyi.system.service.ITBbsRelationBoardTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 版块标签关系Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-29
 */
@Service
public class TBbsRelationBoardTagServiceImpl implements ITBbsRelationBoardTagService {
    @Autowired
    private TBbsRelationBoardTagMapper tBbsRelationBoardTagMapper;

    /**
     * 查询版块标签关系
     *
     * @param id 版块标签关系主键
     * @return 版块标签关系
     */
    @Override
    public TBbsRelationBoardTag selectTBbsRelationBoardTagById(Long id) {
        return tBbsRelationBoardTagMapper.selectTBbsRelationBoardTagById(id);
    }

    /**
     * 查询版块标签关系列表
     *
     * @param tBbsRelationBoardTag 版块标签关系
     * @return 版块标签关系
     */
    @Override
    public List<TBbsRelationBoardTag> selectTBbsRelationBoardTagList(TBbsRelationBoardTag tBbsRelationBoardTag) {
        return tBbsRelationBoardTagMapper.selectTBbsRelationBoardTagList(tBbsRelationBoardTag);
    }

    /**
     * 新增版块标签关系
     *
     * @param tBbsRelationBoardTag 版块标签关系
     * @return 结果
     */
    @Override
    public int insertTBbsRelationBoardTag(TBbsRelationBoardTag tBbsRelationBoardTag) {
        return tBbsRelationBoardTagMapper.insertTBbsRelationBoardTag(tBbsRelationBoardTag);
    }

    /**
     * 修改版块标签关系
     *
     * @param tBbsRelationBoardTag 版块标签关系
     * @return 结果
     */
    @Override
    public int updateTBbsRelationBoardTag(TBbsRelationBoardTag tBbsRelationBoardTag) {
        return tBbsRelationBoardTagMapper.updateTBbsRelationBoardTag(tBbsRelationBoardTag);
    }

    /**
     * 批量删除版块标签关系
     *
     * @param ids 需要删除的版块标签关系主键
     * @return 结果
     */
    @Override
    public int deleteTBbsRelationBoardTagByIds(Long[] ids) {
        return tBbsRelationBoardTagMapper.deleteTBbsRelationBoardTagByIds(ids);
    }

    /**
     * 删除版块标签关系信息
     *
     * @param id 版块标签关系主键
     * @return 结果
     */
    @Override
    public int deleteTBbsRelationBoardTagById(Long id) {
        return tBbsRelationBoardTagMapper.deleteTBbsRelationBoardTagById(id);
    }
}
