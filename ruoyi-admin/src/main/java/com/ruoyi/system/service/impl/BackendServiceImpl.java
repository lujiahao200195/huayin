package com.ruoyi.system.service.impl;

import com.ruoyi.system.common.abs.wechatmini.MiniStudentAuditData;
import com.ruoyi.system.common.abs.wechatmini.MiniTeacherAuditData;
import com.ruoyi.system.common.config.Custom;
import com.ruoyi.system.common.tools.NTools;
import com.ruoyi.system.common.tools.WxUtil;
import com.ruoyi.system.mapper.BackendMapper;
import com.ruoyi.system.service.BackendService;
import com.ruoyi.system.vo.wechat.GetStudentAndOpenId;
import com.ruoyi.system.vo.wechat.StudentSubjectDetails;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class BackendServiceImpl implements BackendService {

    @Resource
    BackendMapper backendMapper;

    @Resource
    WxUtil wxUtil;

    @Resource
    Custom custom;

    @Resource
    NTools nTools;

    @Override
    public Integer changeStudentState(String id, String stateStr) {
        //判断是否有存在记录
        Integer count = backendMapper.count("t_student", Integer.valueOf(id));
        if (count > 0) {
            //修改状态
            Integer integer = backendMapper.updateStudentStateById(Integer.valueOf(id), stateStr);
            //通过id查学生名 手机号 微信小程序openid
            GetStudentAndOpenId getStudentAndOpenId = backendMapper.selectStudentById(Integer.valueOf(id));
            List<String> strings = backendMapper.selectSubjectsById(getStudentAndOpenId.getStudentId());
            //需要强化的科目名称字符串数组
            String[] subjects = new String[strings.size()];
            //遍历插入
            for (int i = 0; i < strings.size(); i++) {
                subjects[i] = strings.get(i);
            }
            //小程序通知模板
            MiniStudentAuditData miniStudentAuditData = new MiniStudentAuditData();
            miniStudentAuditData.setName2(getStudentAndOpenId.getStudentName());
            miniStudentAuditData.setThing1(NTools.arrayToStr(subjects));
            miniStudentAuditData.setPhone_number4(getStudentAndOpenId.getStudentPhone());
            miniStudentAuditData.setPhrase7(stateStr);
            miniStudentAuditData.setTime6(NTools.createTime());
            try {
                System.out.println("openId" + getStudentAndOpenId.getUserWxMiniOpenid());
                //调用通知接口
                wxUtil.weChatMiniMsg(miniStudentAuditData.msgTemplateData(getStudentAndOpenId.getUserWxMiniOpenid(), custom.AuditSuccess, custom.wechatMiniInfoLang,custom.wechatMiniPage,custom.miniprogramState));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

        }
        return null;
    }

    @Override
    public Integer changeTeacherState(String id, String stateStr) {
        //判断是否存在此教师
        Integer count = backendMapper.count("t_teacher", Integer.valueOf(id));
        if (count > 0) {
            //修改教师审核状态
            Integer i = backendMapper.updateTeacherStateById(Integer.valueOf(id), stateStr);
            //查询教师姓名
            String tName = backendMapper.selectTeacherNameById(Integer.valueOf(id));
            //根据教师id寻找教师小程序openid
            String openId = backendMapper.selectTeacherOpenIdById(Integer.valueOf(id));
            //构建教师审核通知
            MiniTeacherAuditData miniTeacherAuditData = new MiniTeacherAuditData();
            miniTeacherAuditData.setThing8(tName);
            miniTeacherAuditData.setPhrase1(stateStr);
            miniTeacherAuditData.setThing3("无");
            miniTeacherAuditData.setTime2(NTools.createTime());
            try {
                System.out.println("openId" + openId);
                wxUtil.weChatMiniMsg(miniTeacherAuditData.msgTemplateData(openId, custom.AuditSuccess, custom.wechatMiniInfoLang,custom.wechatMiniPage,custom.miniprogramState));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

        }

        return null;
    }

    @Override
    public StudentSubjectDetails studentSubjectDetails(Long id) {
        StudentSubjectDetails studentSubjectDetails = new StudentSubjectDetails();
        studentSubjectDetails.setAdvantageSubject(NTools.arrayToStr(backendMapper.selectObjectsByStudentId(id)));
        studentSubjectDetails.setNeedSubject(backendMapper.selectNeedSubjectByStudentId(id));
        return studentSubjectDetails;
    }

}
