package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TArticleTypeRelation;
import com.ruoyi.system.mapper.TArticleTypeRelationMapper;
import com.ruoyi.system.service.ITArticleTypeRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 文章和文章分类关系Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-16
 */
@Service
public class TArticleTypeRelationServiceImpl implements ITArticleTypeRelationService {
    @Autowired
    private TArticleTypeRelationMapper tArticleTypeRelationMapper;

    /**
     * 查询文章和文章分类关系
     *
     * @param id 文章和文章分类关系主键
     * @return 文章和文章分类关系
     */
    @Override
    public TArticleTypeRelation selectTArticleTypeRelationById(Long id) {
        return tArticleTypeRelationMapper.selectTArticleTypeRelationById(id);
    }

    /**
     * 查询文章和文章分类关系列表
     *
     * @param tArticleTypeRelation 文章和文章分类关系
     * @return 文章和文章分类关系
     */
    @Override
    public List<TArticleTypeRelation> selectTArticleTypeRelationList(TArticleTypeRelation tArticleTypeRelation) {
        return tArticleTypeRelationMapper.selectTArticleTypeRelationList(tArticleTypeRelation);
    }

    /**
     * 新增文章和文章分类关系
     *
     * @param tArticleTypeRelation 文章和文章分类关系
     * @return 结果
     */
    @Override
    public int insertTArticleTypeRelation(TArticleTypeRelation tArticleTypeRelation) {
        return tArticleTypeRelationMapper.insertTArticleTypeRelation(tArticleTypeRelation);
    }

    /**
     * 修改文章和文章分类关系
     *
     * @param tArticleTypeRelation 文章和文章分类关系
     * @return 结果
     */
    @Override
    public int updateTArticleTypeRelation(TArticleTypeRelation tArticleTypeRelation) {
        return tArticleTypeRelationMapper.updateTArticleTypeRelation(tArticleTypeRelation);
    }

    /**
     * 批量删除文章和文章分类关系
     *
     * @param ids 需要删除的文章和文章分类关系主键
     * @return 结果
     */
    @Override
    public int deleteTArticleTypeRelationByIds(Long[] ids) {
        return tArticleTypeRelationMapper.deleteTArticleTypeRelationByIds(ids);
    }

    /**
     * 删除文章和文章分类关系信息
     *
     * @param id 文章和文章分类关系主键
     * @return 结果
     */
    @Override
    public int deleteTArticleTypeRelationById(Long id) {
        return tArticleTypeRelationMapper.deleteTArticleTypeRelationById(id);
    }
}
