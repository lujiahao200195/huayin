package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TBbsRelationAudit;
import com.ruoyi.system.mapper.TBbsRelationAuditMapper;
import com.ruoyi.system.service.ITBbsRelationAuditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 审核关系Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-28
 */
@Service
public class TBbsRelationAuditServiceImpl implements ITBbsRelationAuditService {
    @Autowired
    private TBbsRelationAuditMapper tBbsRelationAuditMapper;

    /**
     * 查询审核关系
     *
     * @param id 审核关系主键
     * @return 审核关系
     */
    @Override
    public TBbsRelationAudit selectTBbsRelationAuditById(Long id) {
        return tBbsRelationAuditMapper.selectTBbsRelationAuditById(id);
    }

    /**
     * 查询审核关系列表
     *
     * @param tBbsRelationAudit 审核关系
     * @return 审核关系
     */
    @Override
    public List<TBbsRelationAudit> selectTBbsRelationAuditList(TBbsRelationAudit tBbsRelationAudit) {
        return tBbsRelationAuditMapper.selectTBbsRelationAuditList(tBbsRelationAudit);
    }

    /**
     * 新增审核关系
     *
     * @param tBbsRelationAudit 审核关系
     * @return 结果
     */
    @Override
    public int insertTBbsRelationAudit(TBbsRelationAudit tBbsRelationAudit) {
        return tBbsRelationAuditMapper.insertTBbsRelationAudit(tBbsRelationAudit);
    }

    /**
     * 修改审核关系
     *
     * @param tBbsRelationAudit 审核关系
     * @return 结果
     */
    @Override
    public int updateTBbsRelationAudit(TBbsRelationAudit tBbsRelationAudit) {
        return tBbsRelationAuditMapper.updateTBbsRelationAudit(tBbsRelationAudit);
    }

    /**
     * 批量删除审核关系
     *
     * @param ids 需要删除的审核关系主键
     * @return 结果
     */
    @Override
    public int deleteTBbsRelationAuditByIds(Long[] ids) {
        return tBbsRelationAuditMapper.deleteTBbsRelationAuditByIds(ids);
    }

    /**
     * 删除审核关系信息
     *
     * @param id 审核关系主键
     * @return 结果
     */
    @Override
    public int deleteTBbsRelationAuditById(Long id) {
        return tBbsRelationAuditMapper.deleteTBbsRelationAuditById(id);
    }
}
