package com.ruoyi.system.service;

import com.ruoyi.system.domain.TSubjectArticleRelation;
import com.ruoyi.system.vo.wechat.backend.ArticleIdsVo;

import java.util.List;

/**
 * 科目文件关系Service接口
 *
 * @author ruoyi
 * @date 2023-08-15
 */
public interface ITSubjectArticleRelationService {
    /**
     * 查询科目文件关系
     *
     * @param id 科目文件关系主键
     * @return 科目文件关系
     */
    public TSubjectArticleRelation selectTSubjectArticleRelationById(Long id);

    /**
     * 查询科目文件关系列表
     *
     * @param tSubjectArticleRelation 科目文件关系
     * @return 科目文件关系集合
     */
    public List<TSubjectArticleRelation> selectTSubjectArticleRelationList(TSubjectArticleRelation tSubjectArticleRelation);

    /**
     * 新增科目文件关系
     *
     * @param tSubjectArticleRelation 科目文件关系
     * @return 结果
     */
    public int insertTSubjectArticleRelation(TSubjectArticleRelation tSubjectArticleRelation);

    /**
     * 修改科目文件关系
     *
     * @param tSubjectArticleRelation 科目文件关系
     * @return 结果
     */
    public int updateTSubjectArticleRelation(TSubjectArticleRelation tSubjectArticleRelation);

    /**
     * 批量删除科目文件关系
     *
     * @param ids 需要删除的科目文件关系主键集合
     * @return 结果
     */
    public int deleteTSubjectArticleRelationByIds(Long[] ids);

    /**
     * 删除科目文件关系信息
     *
     * @param id 科目文件关系主键
     * @return 结果
     */
    public int deleteTSubjectArticleRelationById(Long id);

    public List<ArticleIdsVo> getArticleIds(Long[] ids);
}
