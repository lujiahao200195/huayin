package com.ruoyi.system.service;

import com.ruoyi.system.domain.TWechatMiniLog;

import java.util.List;

/**
 * 微信小程序日志Service接口
 *
 * @author ruoyi
 * @date 2023-08-18
 */
public interface ITWechatMiniLogService {
    /**
     * 查询微信小程序日志
     *
     * @param id 微信小程序日志主键
     * @return 微信小程序日志
     */
    public TWechatMiniLog selectTWechatMiniLogById(Long id);

    /**
     * 查询微信小程序日志列表
     *
     * @param tWechatMiniLog 微信小程序日志
     * @return 微信小程序日志集合
     */
    public List<TWechatMiniLog> selectTWechatMiniLogList(TWechatMiniLog tWechatMiniLog);

    /**
     * 新增微信小程序日志
     *
     * @param tWechatMiniLog 微信小程序日志
     * @return 结果
     */
    public int insertTWechatMiniLog(TWechatMiniLog tWechatMiniLog);

    /**
     * 修改微信小程序日志
     *
     * @param tWechatMiniLog 微信小程序日志
     * @return 结果
     */
    public int updateTWechatMiniLog(TWechatMiniLog tWechatMiniLog);

    /**
     * 批量删除微信小程序日志
     *
     * @param ids 需要删除的微信小程序日志主键集合
     * @return 结果
     */
    public int deleteTWechatMiniLogByIds(Long[] ids);

    /**
     * 删除微信小程序日志信息
     *
     * @param id 微信小程序日志主键
     * @return 结果
     */
    public int deleteTWechatMiniLogById(Long id);
}
