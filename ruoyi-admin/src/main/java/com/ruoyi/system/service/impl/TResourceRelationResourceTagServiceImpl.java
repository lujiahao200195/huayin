package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TResourceRelationResourceTag;
import com.ruoyi.system.mapper.TResourceRelationResourceTagMapper;
import com.ruoyi.system.service.ITResourceRelationResourceTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 资源标签关系Service业务层处理
 *
 * @author ruoyi
 * @date 2023-09-08
 */
@Service
public class TResourceRelationResourceTagServiceImpl implements ITResourceRelationResourceTagService {
    @Autowired
    private TResourceRelationResourceTagMapper tResourceRelationResourceTagMapper;

    /**
     * 查询资源标签关系
     *
     * @param id 资源标签关系主键
     * @return 资源标签关系
     */
    @Override
    public TResourceRelationResourceTag selectTResourceRelationResourceTagById(Long id) {
        return tResourceRelationResourceTagMapper.selectTResourceRelationResourceTagById(id);
    }

    /**
     * 查询资源标签关系列表
     *
     * @param tResourceRelationResourceTag 资源标签关系
     * @return 资源标签关系
     */
    @Override
    public List<TResourceRelationResourceTag> selectTResourceRelationResourceTagList(TResourceRelationResourceTag tResourceRelationResourceTag) {
        return tResourceRelationResourceTagMapper.selectTResourceRelationResourceTagList(tResourceRelationResourceTag);
    }

    /**
     * 新增资源标签关系
     *
     * @param tResourceRelationResourceTag 资源标签关系
     * @return 结果
     */
    @Override
    public int insertTResourceRelationResourceTag(TResourceRelationResourceTag tResourceRelationResourceTag) {
        return tResourceRelationResourceTagMapper.insertTResourceRelationResourceTag(tResourceRelationResourceTag);
    }

    /**
     * 修改资源标签关系
     *
     * @param tResourceRelationResourceTag 资源标签关系
     * @return 结果
     */
    @Override
    public int updateTResourceRelationResourceTag(TResourceRelationResourceTag tResourceRelationResourceTag) {
        return tResourceRelationResourceTagMapper.updateTResourceRelationResourceTag(tResourceRelationResourceTag);
    }

    /**
     * 批量删除资源标签关系
     *
     * @param ids 需要删除的资源标签关系主键
     * @return 结果
     */
    @Override
    public int deleteTResourceRelationResourceTagByIds(Long[] ids) {
        return tResourceRelationResourceTagMapper.deleteTResourceRelationResourceTagByIds(ids);
    }

    /**
     * 删除资源标签关系信息
     *
     * @param id 资源标签关系主键
     * @return 结果
     */
    @Override
    public int deleteTResourceRelationResourceTagById(Long id) {
        return tResourceRelationResourceTagMapper.deleteTResourceRelationResourceTagById(id);
    }
}
