package com.ruoyi.system.service;

import com.ruoyi.system.domain.TBbsRelationBoardTag;

import java.util.List;

/**
 * 版块标签关系Service接口
 *
 * @author ruoyi
 * @date 2023-08-29
 */
public interface ITBbsRelationBoardTagService {
    /**
     * 查询版块标签关系
     *
     * @param id 版块标签关系主键
     * @return 版块标签关系
     */
    public TBbsRelationBoardTag selectTBbsRelationBoardTagById(Long id);

    /**
     * 查询版块标签关系列表
     *
     * @param tBbsRelationBoardTag 版块标签关系
     * @return 版块标签关系集合
     */
    public List<TBbsRelationBoardTag> selectTBbsRelationBoardTagList(TBbsRelationBoardTag tBbsRelationBoardTag);

    /**
     * 新增版块标签关系
     *
     * @param tBbsRelationBoardTag 版块标签关系
     * @return 结果
     */
    public int insertTBbsRelationBoardTag(TBbsRelationBoardTag tBbsRelationBoardTag);

    /**
     * 修改版块标签关系
     *
     * @param tBbsRelationBoardTag 版块标签关系
     * @return 结果
     */
    public int updateTBbsRelationBoardTag(TBbsRelationBoardTag tBbsRelationBoardTag);

    /**
     * 批量删除版块标签关系
     *
     * @param ids 需要删除的版块标签关系主键集合
     * @return 结果
     */
    public int deleteTBbsRelationBoardTagByIds(Long[] ids);

    /**
     * 删除版块标签关系信息
     *
     * @param id 版块标签关系主键
     * @return 结果
     */
    public int deleteTBbsRelationBoardTagById(Long id);
}
