package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TBbsRelationReply;
import com.ruoyi.system.mapper.TBbsRelationReplyMapper;
import com.ruoyi.system.service.ITBbsRelationReplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 回复关系Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-31
 */
@Service
public class TBbsRelationReplyServiceImpl implements ITBbsRelationReplyService {
    @Autowired
    private TBbsRelationReplyMapper tBbsRelationReplyMapper;

    /**
     * 查询回复关系
     *
     * @param id 回复关系主键
     * @return 回复关系
     */
    @Override
    public TBbsRelationReply selectTBbsRelationReplyById(Long id) {
        return tBbsRelationReplyMapper.selectTBbsRelationReplyById(id);
    }

    /**
     * 查询回复关系列表
     *
     * @param tBbsRelationReply 回复关系
     * @return 回复关系
     */
    @Override
    public List<TBbsRelationReply> selectTBbsRelationReplyList(TBbsRelationReply tBbsRelationReply) {
        return tBbsRelationReplyMapper.selectTBbsRelationReplyList(tBbsRelationReply);
    }

    /**
     * 新增回复关系
     *
     * @param tBbsRelationReply 回复关系
     * @return 结果
     */
    @Override
    public int insertTBbsRelationReply(TBbsRelationReply tBbsRelationReply) {
        return tBbsRelationReplyMapper.insertTBbsRelationReply(tBbsRelationReply);
    }

    /**
     * 修改回复关系
     *
     * @param tBbsRelationReply 回复关系
     * @return 结果
     */
    @Override
    public int updateTBbsRelationReply(TBbsRelationReply tBbsRelationReply) {
        return tBbsRelationReplyMapper.updateTBbsRelationReply(tBbsRelationReply);
    }

    /**
     * 批量删除回复关系
     *
     * @param ids 需要删除的回复关系主键
     * @return 结果
     */
    @Override
    public int deleteTBbsRelationReplyByIds(Long[] ids) {
        return tBbsRelationReplyMapper.deleteTBbsRelationReplyByIds(ids);
    }

    /**
     * 删除回复关系信息
     *
     * @param id 回复关系主键
     * @return 结果
     */
    @Override
    public int deleteTBbsRelationReplyById(Long id) {
        return tBbsRelationReplyMapper.deleteTBbsRelationReplyById(id);
    }
}
