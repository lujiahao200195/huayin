package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TSubjectArticleRelation;
import com.ruoyi.system.mapper.TSubjectArticleRelationMapper;
import com.ruoyi.system.service.ITSubjectArticleRelationService;
import com.ruoyi.system.vo.wechat.backend.ArticleIdsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 科目文件关系Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-15
 */
@Service
public class TSubjectArticleRelationServiceImpl implements ITSubjectArticleRelationService {
    @Autowired
    private TSubjectArticleRelationMapper tSubjectArticleRelationMapper;

    /**
     * 查询科目文件关系
     *
     * @param id 科目文件关系主键
     * @return 科目文件关系
     */
    @Override
    public TSubjectArticleRelation selectTSubjectArticleRelationById(Long id) {
        return tSubjectArticleRelationMapper.selectTSubjectArticleRelationById(id);
    }

    /**
     * 查询科目文件关系列表
     *
     * @param tSubjectArticleRelation 科目文件关系
     * @return 科目文件关系
     */
    @Override
    public List<TSubjectArticleRelation> selectTSubjectArticleRelationList(TSubjectArticleRelation tSubjectArticleRelation) {
        return tSubjectArticleRelationMapper.selectTSubjectArticleRelationList(tSubjectArticleRelation);
    }

    /**
     * 新增科目文件关系
     *
     * @param tSubjectArticleRelation 科目文件关系
     * @return 结果
     */
    @Override
    public int insertTSubjectArticleRelation(TSubjectArticleRelation tSubjectArticleRelation) {
        return tSubjectArticleRelationMapper.insertTSubjectArticleRelation(tSubjectArticleRelation);
    }

    /**
     * 修改科目文件关系
     *
     * @param tSubjectArticleRelation 科目文件关系
     * @return 结果
     */
    @Override
    public int updateTSubjectArticleRelation(TSubjectArticleRelation tSubjectArticleRelation) {
        return tSubjectArticleRelationMapper.updateTSubjectArticleRelation(tSubjectArticleRelation);
    }

    /**
     * 批量删除科目文件关系
     *
     * @param ids 需要删除的科目文件关系主键
     * @return 结果
     */
    @Override
    public int deleteTSubjectArticleRelationByIds(Long[] ids) {
        return tSubjectArticleRelationMapper.deleteTSubjectArticleRelationByIds(ids);
    }

    /**
     * 删除科目文件关系信息
     *
     * @param id 科目文件关系主键
     * @return 结果
     */
    @Override
    public int deleteTSubjectArticleRelationById(Long id) {
        return tSubjectArticleRelationMapper.deleteTSubjectArticleRelationById(id);
    }


    /**
     *
     * @param ids
     * @return 文章主键ID
     */
    @Override
    public List<ArticleIdsVo> getArticleIds(Long[] ids) {
        return tSubjectArticleRelationMapper.getArticleIds(ids);
    }
}
