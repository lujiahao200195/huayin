package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TTeacher;
import com.ruoyi.system.mapper.TTeacherMapper;
import com.ruoyi.system.service.ITTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 教师Service业务层处理
 *
 * @author ruoyi
 * @date 2023-09-13
 */
@Service
public class TTeacherServiceImpl implements ITTeacherService {
    @Autowired
    private TTeacherMapper tTeacherMapper;

    /**
     * 查询教师
     *
     * @param id 教师主键
     * @return 教师
     */
    @Override
    public TTeacher selectTTeacherById(Long id) {
        return tTeacherMapper.selectTTeacherById(id);
    }

    /**
     * 查询教师列表
     *
     * @param tTeacher 教师
     * @return 教师
     */
    @Override
    public List<TTeacher> selectTTeacherList(TTeacher tTeacher) {
        return tTeacherMapper.selectTTeacherList(tTeacher);
    }

    /**
     * 新增教师
     *
     * @param tTeacher 教师
     * @return 结果
     */
    @Override
    public int insertTTeacher(TTeacher tTeacher) {
        return tTeacherMapper.insertTTeacher(tTeacher);
    }

    /**
     * 修改教师
     *
     * @param tTeacher 教师
     * @return 结果
     */
    @Override
    public int updateTTeacher(TTeacher tTeacher) {
        return tTeacherMapper.updateTTeacher(tTeacher);
    }

    /**
     * 批量删除教师
     *
     * @param ids 需要删除的教师主键
     * @return 结果
     */
    @Override
    public int deleteTTeacherByIds(Long[] ids) {
        return tTeacherMapper.deleteTTeacherByIds(ids);
    }

    /**
     * 删除教师信息
     *
     * @param id 教师主键
     * @return 结果
     */
    @Override
    public int deleteTTeacherById(Long id) {
        return tTeacherMapper.deleteTTeacherById(id);
    }
}
