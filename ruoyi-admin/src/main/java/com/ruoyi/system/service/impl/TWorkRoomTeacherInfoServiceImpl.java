package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TWorkRoomTeacherInfo;
import com.ruoyi.system.mapper.TWorkRoomTeacherInfoMapper;
import com.ruoyi.system.service.ITWorkRoomTeacherInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 工作室教师信息卡Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-18
 */
@Service
public class TWorkRoomTeacherInfoServiceImpl implements ITWorkRoomTeacherInfoService {
    @Autowired
    private TWorkRoomTeacherInfoMapper tWorkRoomTeacherInfoMapper;

    /**
     * 查询工作室教师信息卡
     *
     * @param id 工作室教师信息卡主键
     * @return 工作室教师信息卡
     */
    @Override
    public TWorkRoomTeacherInfo selectTWorkRoomTeacherInfoById(Long id) {
        return tWorkRoomTeacherInfoMapper.selectTWorkRoomTeacherInfoById(id);
    }

    /**
     * 查询工作室教师信息卡列表
     *
     * @param tWorkRoomTeacherInfo 工作室教师信息卡
     * @return 工作室教师信息卡
     */
    @Override
    public List<TWorkRoomTeacherInfo> selectTWorkRoomTeacherInfoList(TWorkRoomTeacherInfo tWorkRoomTeacherInfo) {
        return tWorkRoomTeacherInfoMapper.selectTWorkRoomTeacherInfoList(tWorkRoomTeacherInfo);
    }

    /**
     * 新增工作室教师信息卡
     *
     * @param tWorkRoomTeacherInfo 工作室教师信息卡
     * @return 结果
     */
    @Override
    public int insertTWorkRoomTeacherInfo(TWorkRoomTeacherInfo tWorkRoomTeacherInfo) {
        return tWorkRoomTeacherInfoMapper.insertTWorkRoomTeacherInfo(tWorkRoomTeacherInfo);
    }

    /**
     * 修改工作室教师信息卡
     *
     * @param tWorkRoomTeacherInfo 工作室教师信息卡
     * @return 结果
     */
    @Override
    public int updateTWorkRoomTeacherInfo(TWorkRoomTeacherInfo tWorkRoomTeacherInfo) {
        return tWorkRoomTeacherInfoMapper.updateTWorkRoomTeacherInfo(tWorkRoomTeacherInfo);
    }

    /**
     * 批量删除工作室教师信息卡
     *
     * @param ids 需要删除的工作室教师信息卡主键
     * @return 结果
     */
    @Override
    public int deleteTWorkRoomTeacherInfoByIds(Long[] ids) {
        return tWorkRoomTeacherInfoMapper.deleteTWorkRoomTeacherInfoByIds(ids);
    }

    /**
     * 删除工作室教师信息卡信息
     *
     * @param id 工作室教师信息卡主键
     * @return 结果
     */
    @Override
    public int deleteTWorkRoomTeacherInfoById(Long id) {
        return tWorkRoomTeacherInfoMapper.deleteTWorkRoomTeacherInfoById(id);
    }
}
