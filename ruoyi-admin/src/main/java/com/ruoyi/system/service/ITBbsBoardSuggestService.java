package com.ruoyi.system.service;

import com.ruoyi.system.domain.TBbsBoardSuggest;

import java.util.List;

/**
 * 版主推荐Service接口
 *
 * @author ruoyi
 * @date 2023-08-29
 */
public interface ITBbsBoardSuggestService {
    /**
     * 查询版主推荐
     *
     * @param id 版主推荐主键
     * @return 版主推荐
     */
    public TBbsBoardSuggest selectTBbsBoardSuggestById(Long id);

    /**
     * 查询版主推荐列表
     *
     * @param tBbsBoardSuggest 版主推荐
     * @return 版主推荐集合
     */
    public List<TBbsBoardSuggest> selectTBbsBoardSuggestList(TBbsBoardSuggest tBbsBoardSuggest);

    /**
     * 新增版主推荐
     *
     * @param tBbsBoardSuggest 版主推荐
     * @return 结果
     */
    public int insertTBbsBoardSuggest(TBbsBoardSuggest tBbsBoardSuggest);

    /**
     * 修改版主推荐
     *
     * @param tBbsBoardSuggest 版主推荐
     * @return 结果
     */
    public int updateTBbsBoardSuggest(TBbsBoardSuggest tBbsBoardSuggest);

    /**
     * 批量删除版主推荐
     *
     * @param ids 需要删除的版主推荐主键集合
     * @return 结果
     */
    public int deleteTBbsBoardSuggestByIds(Long[] ids);

    /**
     * 删除版主推荐信息
     *
     * @param id 版主推荐主键
     * @return 结果
     */
    public int deleteTBbsBoardSuggestById(Long id);
}
