package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TAddress;
import com.ruoyi.system.mapper.TAddressMapper;
import com.ruoyi.system.service.ITAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 地区Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-21
 */
@Service
public class TAddressServiceImpl implements ITAddressService {
    @Autowired
    private TAddressMapper tAddressMapper;

    /**
     * 查询地区
     *
     * @param id 地区主键
     * @return 地区
     */
    @Override
    public TAddress selectTAddressById(Long id) {
        return tAddressMapper.selectTAddressById(id);
    }

    /**
     * 查询地区列表
     *
     * @param tAddress 地区
     * @return 地区
     */
    @Override
    public List<TAddress> selectTAddressList(TAddress tAddress) {
        return tAddressMapper.selectTAddressList(tAddress);
    }

    /**
     * 新增地区
     *
     * @param tAddress 地区
     * @return 结果
     */
    @Override
    public int insertTAddress(TAddress tAddress) {
        return tAddressMapper.insertTAddress(tAddress);
    }

    /**
     * 修改地区
     *
     * @param tAddress 地区
     * @return 结果
     */
    @Override
    public int updateTAddress(TAddress tAddress) {
        return tAddressMapper.updateTAddress(tAddress);
    }

    /**
     * 批量删除地区
     *
     * @param ids 需要删除的地区主键
     * @return 结果
     */
    @Override
    public int deleteTAddressByIds(Long[] ids) {
        return tAddressMapper.deleteTAddressByIds(ids);
    }

    /**
     * 删除地区信息
     *
     * @param id 地区主键
     * @return 结果
     */
    @Override
    public int deleteTAddressById(Long id) {
        return tAddressMapper.deleteTAddressById(id);
    }
}
