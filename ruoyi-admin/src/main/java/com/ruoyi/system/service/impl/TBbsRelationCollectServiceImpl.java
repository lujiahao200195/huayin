package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TBbsRelationCollect;
import com.ruoyi.system.mapper.TBbsRelationCollectMapper;
import com.ruoyi.system.service.ITBbsRelationCollectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 收藏关系Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-28
 */
@Service
public class TBbsRelationCollectServiceImpl implements ITBbsRelationCollectService {
    @Autowired
    private TBbsRelationCollectMapper tBbsRelationCollectMapper;

    /**
     * 查询收藏关系
     *
     * @param id 收藏关系主键
     * @return 收藏关系
     */
    @Override
    public TBbsRelationCollect selectTBbsRelationCollectById(Long id) {
        return tBbsRelationCollectMapper.selectTBbsRelationCollectById(id);
    }

    /**
     * 查询收藏关系列表
     *
     * @param tBbsRelationCollect 收藏关系
     * @return 收藏关系
     */
    @Override
    public List<TBbsRelationCollect> selectTBbsRelationCollectList(TBbsRelationCollect tBbsRelationCollect) {
        return tBbsRelationCollectMapper.selectTBbsRelationCollectList(tBbsRelationCollect);
    }

    /**
     * 新增收藏关系
     *
     * @param tBbsRelationCollect 收藏关系
     * @return 结果
     */
    @Override
    public int insertTBbsRelationCollect(TBbsRelationCollect tBbsRelationCollect) {
        return tBbsRelationCollectMapper.insertTBbsRelationCollect(tBbsRelationCollect);
    }

    /**
     * 修改收藏关系
     *
     * @param tBbsRelationCollect 收藏关系
     * @return 结果
     */
    @Override
    public int updateTBbsRelationCollect(TBbsRelationCollect tBbsRelationCollect) {
        return tBbsRelationCollectMapper.updateTBbsRelationCollect(tBbsRelationCollect);
    }

    /**
     * 批量删除收藏关系
     *
     * @param ids 需要删除的收藏关系主键
     * @return 结果
     */
    @Override
    public int deleteTBbsRelationCollectByIds(Long[] ids) {
        return tBbsRelationCollectMapper.deleteTBbsRelationCollectByIds(ids);
    }

    /**
     * 删除收藏关系信息
     *
     * @param id 收藏关系主键
     * @return 结果
     */
    @Override
    public int deleteTBbsRelationCollectById(Long id) {
        return tBbsRelationCollectMapper.deleteTBbsRelationCollectById(id);
    }
}
