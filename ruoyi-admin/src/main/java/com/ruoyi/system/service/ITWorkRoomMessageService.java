package com.ruoyi.system.service;

import com.ruoyi.system.domain.TWorkRoomMessage;

import java.util.List;

/**
 * 工作室消息Service接口
 *
 * @author ruoyi
 * @date 2023-08-23
 */
public interface ITWorkRoomMessageService {
    /**
     * 查询工作室消息
     *
     * @param id 工作室消息主键
     * @return 工作室消息
     */
    public TWorkRoomMessage selectTWorkRoomMessageById(Long id);

    /**
     * 查询工作室消息列表
     *
     * @param tWorkRoomMessage 工作室消息
     * @return 工作室消息集合
     */
    public List<TWorkRoomMessage> selectTWorkRoomMessageList(TWorkRoomMessage tWorkRoomMessage);

    /**
     * 新增工作室消息
     *
     * @param tWorkRoomMessage 工作室消息
     * @return 结果
     */
    public int insertTWorkRoomMessage(TWorkRoomMessage tWorkRoomMessage);

    /**
     * 修改工作室消息
     *
     * @param tWorkRoomMessage 工作室消息
     * @return 结果
     */
    public int updateTWorkRoomMessage(TWorkRoomMessage tWorkRoomMessage);

    /**
     * 批量删除工作室消息
     *
     * @param ids 需要删除的工作室消息主键集合
     * @return 结果
     */
    public int deleteTWorkRoomMessageByIds(Long[] ids);

    /**
     * 删除工作室消息信息
     *
     * @param id 工作室消息主键
     * @return 结果
     */
    public int deleteTWorkRoomMessageById(Long id);
}
