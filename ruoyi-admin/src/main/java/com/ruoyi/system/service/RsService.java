package com.ruoyi.system.service;

import com.ruoyi.system.bo.wechat.SearchResourceDo;
import com.ruoyi.system.bo.wechat.UpdateResourceDo;
import com.ruoyi.system.bo.wechat.UploadResourceDo;
import com.ruoyi.system.domain.TResourceRelation;
import com.ruoyi.system.domain.TResourceTag;
import com.ruoyi.system.vo.wechat.rs.ResourceTagVo;
import com.ruoyi.system.vo.wechat.rs.ResourceVo;
import com.ruoyi.system.vo.wechat.rs.SearchStudentVo;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public interface RsService {

    /**
     * 获取标签列表
     *
     * @return
     */
    List<ResourceTagVo> wechatMiniResourceTagList();

    /**
     * 搜索资源列表
     *
     * @param searchResourceDo
     * @return
     */
    List<ResourceVo> wechatMiniSearchResourceList(SearchResourceDo searchResourceDo);

    /**
     * 添加资源标签
     *
     * @param tagName
     * @return
     */
    TResourceTag wechatMiniAddResourceTag(String tagName);

    /**
     * 上传资源
     *
     * @param uploadResourceDo
     * @return
     */
    String wechatMiniUploadResource(UploadResourceDo uploadResourceDo) throws IOException;

    /**
     * 搜索标签
     *
     * @param tagName
     * @return
     */
    List<ResourceTagVo> wechatMiniSearchResourceTag(String tagName);

    /**
     * 删除资源数据
     *
     * @param resourceId
     * @return
     */
    String wechatMiniDeleteResource(Long resourceId);

    /**
     * 下载资源
     *
     * @param resourceId
     * @param response
     */
    void wechatMiniDownloadResource(Long resourceId, HttpServletResponse response) throws IOException;

    /**
     * 修改资源
     *
     * @param updateResourceDo
     * @return
     */
    String wechatMiniUpdateResource(UpdateResourceDo updateResourceDo);

    /**
     * 用户分享资源给学生
     *
     * @param studentId
     * @param resourceId
     * @return
     */
    String wechatMiniShareResourceStudent(Long studentId, Long resourceId);

    /**
     * 用户分享资源给用户
     *
     * @param userId
     * @param resourceId
     * @return
     */
    String wechatMiniShareResourceUser(Long userId, Long resourceId);

    /**
     * 用户接受资源风险
     *
     * @param resourceId
     * @return
     */
    String wechatMiniUserAcceptResource(Long resourceId);

    /**
     * 教师搜索学生
     * @param studentName
     * @return
     */
    List<SearchStudentVo> wechatMiniTeacherSearchStudent(String studentName);

    /**
     * 微信小程序资源下载计数接口
     * @param resourceId
     * @return
     */
    String wechatMiniRsCount(Long resourceId);

    /**
     * 小程序用户收藏资料
     * @param resourceId
     * @return
     */
    String wechatMiniCollectResource(Long resourceId);


    /**
     * 小程序取消用户收藏(删除数据)
     * @param resourceId
     * @return
     */
    public int wechatMiniCancelCollect(Long resourceId);

    /**
     * 查看资源对应用户的收藏状态
     * @param resourceId
     * @return
     */
    public TResourceRelation wechatMiniQueryCollectStatus(Long resourceId);
}
