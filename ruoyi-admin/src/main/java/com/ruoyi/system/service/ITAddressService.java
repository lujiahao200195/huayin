package com.ruoyi.system.service;

import com.ruoyi.system.domain.TAddress;

import java.util.List;

/**
 * 地区Service接口
 *
 * @author ruoyi
 * @date 2023-08-21
 */
public interface ITAddressService {
    /**
     * 查询地区
     *
     * @param id 地区主键
     * @return 地区
     */
    public TAddress selectTAddressById(Long id);

    /**
     * 查询地区列表
     *
     * @param tAddress 地区
     * @return 地区集合
     */
    public List<TAddress> selectTAddressList(TAddress tAddress);

    /**
     * 新增地区
     *
     * @param tAddress 地区
     * @return 结果
     */
    public int insertTAddress(TAddress tAddress);

    /**
     * 修改地区
     *
     * @param tAddress 地区
     * @return 结果
     */
    public int updateTAddress(TAddress tAddress);

    /**
     * 批量删除地区
     *
     * @param ids 需要删除的地区主键集合
     * @return 结果
     */
    public int deleteTAddressByIds(Long[] ids);

    /**
     * 删除地区信息
     *
     * @param id 地区主键
     * @return 结果
     */
    public int deleteTAddressById(Long id);
}
