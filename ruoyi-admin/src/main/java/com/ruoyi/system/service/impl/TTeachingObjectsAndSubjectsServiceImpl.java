package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TTeachingObjectsAndSubjects;
import com.ruoyi.system.mapper.TTeachingObjectsAndSubjectsMapper;
import com.ruoyi.system.service.ITTeachingObjectsAndSubjectsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 教师意向教学Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-17
 */
@Service
public class TTeachingObjectsAndSubjectsServiceImpl implements ITTeachingObjectsAndSubjectsService {
    @Autowired
    private TTeachingObjectsAndSubjectsMapper tTeachingObjectsAndSubjectsMapper;

    /**
     * 查询教师意向教学
     *
     * @param id 教师意向教学主键
     * @return 教师意向教学
     */
    @Override
    public TTeachingObjectsAndSubjects selectTTeachingObjectsAndSubjectsById(Long id) {
        return tTeachingObjectsAndSubjectsMapper.selectTTeachingObjectsAndSubjectsById(id);
    }

    /**
     * 查询教师意向教学列表
     *
     * @param tTeachingObjectsAndSubjects 教师意向教学
     * @return 教师意向教学
     */
    @Override
    public List<TTeachingObjectsAndSubjects> selectTTeachingObjectsAndSubjectsList(TTeachingObjectsAndSubjects tTeachingObjectsAndSubjects) {
        return tTeachingObjectsAndSubjectsMapper.selectTTeachingObjectsAndSubjectsList(tTeachingObjectsAndSubjects);
    }

    /**
     * 新增教师意向教学
     *
     * @param tTeachingObjectsAndSubjects 教师意向教学
     * @return 结果
     */
    @Override
    public int insertTTeachingObjectsAndSubjects(TTeachingObjectsAndSubjects tTeachingObjectsAndSubjects) {
        return tTeachingObjectsAndSubjectsMapper.insertTTeachingObjectsAndSubjects(tTeachingObjectsAndSubjects);
    }

    /**
     * 修改教师意向教学
     *
     * @param tTeachingObjectsAndSubjects 教师意向教学
     * @return 结果
     */
    @Override
    public int updateTTeachingObjectsAndSubjects(TTeachingObjectsAndSubjects tTeachingObjectsAndSubjects) {
        return tTeachingObjectsAndSubjectsMapper.updateTTeachingObjectsAndSubjects(tTeachingObjectsAndSubjects);
    }

    /**
     * 批量删除教师意向教学
     *
     * @param ids 需要删除的教师意向教学主键
     * @return 结果
     */
    @Override
    public int deleteTTeachingObjectsAndSubjectsByIds(Long[] ids) {
        return tTeachingObjectsAndSubjectsMapper.deleteTTeachingObjectsAndSubjectsByIds(ids);
    }

    /**
     * 删除教师意向教学信息
     *
     * @param id 教师意向教学主键
     * @return 结果
     */
    @Override
    public int deleteTTeachingObjectsAndSubjectsById(Long id) {
        return tTeachingObjectsAndSubjectsMapper.deleteTTeachingObjectsAndSubjectsById(id);
    }
}
