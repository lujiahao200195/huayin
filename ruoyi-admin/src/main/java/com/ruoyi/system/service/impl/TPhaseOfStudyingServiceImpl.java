package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TPhaseOfStudying;
import com.ruoyi.system.mapper.TPhaseOfStudyingMapper;
import com.ruoyi.system.service.ITPhaseOfStudyingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 学段Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-15
 */
@Service
public class TPhaseOfStudyingServiceImpl implements ITPhaseOfStudyingService {
    @Autowired
    private TPhaseOfStudyingMapper tPhaseOfStudyingMapper;

    /**
     * 查询学段
     *
     * @param id 学段主键
     * @return 学段
     */
    @Override
    public TPhaseOfStudying selectTPhaseOfStudyingById(Long id) {
        return tPhaseOfStudyingMapper.selectTPhaseOfStudyingById(id);
    }

    /**
     * 查询学段列表
     *
     * @param tPhaseOfStudying 学段
     * @return 学段
     */
    @Override
    public List<TPhaseOfStudying> selectTPhaseOfStudyingList(TPhaseOfStudying tPhaseOfStudying) {
        return tPhaseOfStudyingMapper.selectTPhaseOfStudyingList(tPhaseOfStudying);
    }

    /**
     * 新增学段
     *
     * @param tPhaseOfStudying 学段
     * @return 结果
     */
    @Override
    public int insertTPhaseOfStudying(TPhaseOfStudying tPhaseOfStudying) {
        return tPhaseOfStudyingMapper.insertTPhaseOfStudying(tPhaseOfStudying);
    }

    /**
     * 修改学段
     *
     * @param tPhaseOfStudying 学段
     * @return 结果
     */
    @Override
    public int updateTPhaseOfStudying(TPhaseOfStudying tPhaseOfStudying) {
        return tPhaseOfStudyingMapper.updateTPhaseOfStudying(tPhaseOfStudying);
    }

    /**
     * 批量删除学段
     *
     * @param ids 需要删除的学段主键
     * @return 结果
     */
    @Override
    public int deleteTPhaseOfStudyingByIds(Long[] ids) {
        return tPhaseOfStudyingMapper.deleteTPhaseOfStudyingByIds(ids);
    }

    /**
     * 删除学段信息
     *
     * @param id 学段主键
     * @return 结果
     */
    @Override
    public int deleteTPhaseOfStudyingById(Long id) {
        return tPhaseOfStudyingMapper.deleteTPhaseOfStudyingById(id);
    }
}
