package com.ruoyi.system.service;

import com.ruoyi.system.domain.TRelationTeacherCertificates;

import java.util.List;

/**
 * 教师和教师证书关系Service接口
 *
 * @author ruoyi
 * @date 2023-08-17
 */
public interface ITRelationTeacherCertificatesService {
    /**
     * 查询教师和教师证书关系
     *
     * @param id 教师和教师证书关系主键
     * @return 教师和教师证书关系
     */
    public TRelationTeacherCertificates selectTRelationTeacherCertificatesById(Long id);

    /**
     * 查询教师和教师证书关系列表
     *
     * @param tRelationTeacherCertificates 教师和教师证书关系
     * @return 教师和教师证书关系集合
     */
    public List<TRelationTeacherCertificates> selectTRelationTeacherCertificatesList(TRelationTeacherCertificates tRelationTeacherCertificates);

    /**
     * 新增教师和教师证书关系
     *
     * @param tRelationTeacherCertificates 教师和教师证书关系
     * @return 结果
     */
    public int insertTRelationTeacherCertificates(TRelationTeacherCertificates tRelationTeacherCertificates);

    /**
     * 修改教师和教师证书关系
     *
     * @param tRelationTeacherCertificates 教师和教师证书关系
     * @return 结果
     */
    public int updateTRelationTeacherCertificates(TRelationTeacherCertificates tRelationTeacherCertificates);

    /**
     * 批量删除教师和教师证书关系
     *
     * @param ids 需要删除的教师和教师证书关系主键集合
     * @return 结果
     */
    public int deleteTRelationTeacherCertificatesByIds(Long[] ids);

    /**
     * 删除教师和教师证书关系信息
     *
     * @param id 教师和教师证书关系主键
     * @return 结果
     */
    public int deleteTRelationTeacherCertificatesById(Long id);
}
