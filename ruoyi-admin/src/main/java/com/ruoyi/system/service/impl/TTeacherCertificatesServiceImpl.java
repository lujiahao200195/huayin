package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TTeacherCertificates;
import com.ruoyi.system.mapper.TTeacherCertificatesMapper;
import com.ruoyi.system.service.ITTeacherCertificatesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 教师证书Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-16
 */
@Service
public class TTeacherCertificatesServiceImpl implements ITTeacherCertificatesService {
    @Autowired
    private TTeacherCertificatesMapper tTeacherCertificatesMapper;

    /**
     * 查询教师证书
     *
     * @param id 教师证书主键
     * @return 教师证书
     */
    @Override
    public TTeacherCertificates selectTTeacherCertificatesById(Long id) {
        return tTeacherCertificatesMapper.selectTTeacherCertificatesById(id);
    }

    /**
     * 查询教师证书列表
     *
     * @param tTeacherCertificates 教师证书
     * @return 教师证书
     */
    @Override
    public List<TTeacherCertificates> selectTTeacherCertificatesList(TTeacherCertificates tTeacherCertificates) {
        return tTeacherCertificatesMapper.selectTTeacherCertificatesList(tTeacherCertificates);
    }

    /**
     * 新增教师证书
     *
     * @param tTeacherCertificates 教师证书
     * @return 结果
     */
    @Override
    public int insertTTeacherCertificates(TTeacherCertificates tTeacherCertificates) {
        return tTeacherCertificatesMapper.insertTTeacherCertificates(tTeacherCertificates);
    }

    /**
     * 修改教师证书
     *
     * @param tTeacherCertificates 教师证书
     * @return 结果
     */
    @Override
    public int updateTTeacherCertificates(TTeacherCertificates tTeacherCertificates) {
        return tTeacherCertificatesMapper.updateTTeacherCertificates(tTeacherCertificates);
    }

    /**
     * 批量删除教师证书
     *
     * @param ids 需要删除的教师证书主键
     * @return 结果
     */
    @Override
    public int deleteTTeacherCertificatesByIds(Long[] ids) {
        return tTeacherCertificatesMapper.deleteTTeacherCertificatesByIds(ids);
    }

    /**
     * 删除教师证书信息
     *
     * @param id 教师证书主键
     * @return 结果
     */
    @Override
    public int deleteTTeacherCertificatesById(Long id) {
        return tTeacherCertificatesMapper.deleteTTeacherCertificatesById(id);
    }
}
