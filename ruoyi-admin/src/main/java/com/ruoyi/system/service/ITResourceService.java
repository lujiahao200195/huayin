package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.TResource;

/**
 * 资源Service接口
 * 
 * @author ruoyi
 * @date 2023-09-21
 */
public interface ITResourceService 
{
    /**
     * 查询资源
     * 
     * @param id 资源主键
     * @return 资源
     */
    public TResource selectTResourceById(Long id);

    /**
     * 查询资源列表
     * 
     * @param tResource 资源
     * @return 资源集合
     */
    public List<TResource> selectTResourceList(TResource tResource);

    /**
     * 新增资源
     * 
     * @param tResource 资源
     * @return 结果
     */
    public int insertTResource(TResource tResource);

    /**
     * 修改资源
     * 
     * @param tResource 资源
     * @return 结果
     */
    public int updateTResource(TResource tResource);

    /**
     * 批量删除资源
     * 
     * @param ids 需要删除的资源主键集合
     * @return 结果
     */
    public int deleteTResourceByIds(Long[] ids);

    /**
     * 删除资源信息
     * 
     * @param id 资源主键
     * @return 结果
     */
    public int deleteTResourceById(Long id);
}
