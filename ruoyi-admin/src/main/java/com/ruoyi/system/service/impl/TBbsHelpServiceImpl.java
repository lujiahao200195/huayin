package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TBbsHelp;
import com.ruoyi.system.mapper.TBbsHelpMapper;
import com.ruoyi.system.service.ITBbsHelpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 论坛求助Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-30
 */
@Service
public class TBbsHelpServiceImpl implements ITBbsHelpService {
    @Autowired
    private TBbsHelpMapper tBbsHelpMapper;

    /**
     * 查询论坛求助
     *
     * @param id 论坛求助主键
     * @return 论坛求助
     */
    @Override
    public TBbsHelp selectTBbsHelpById(Long id) {
        return tBbsHelpMapper.selectTBbsHelpById(id);
    }

    /**
     * 查询论坛求助列表
     *
     * @param tBbsHelp 论坛求助
     * @return 论坛求助
     */
    @Override
    public List<TBbsHelp> selectTBbsHelpList(TBbsHelp tBbsHelp) {
        return tBbsHelpMapper.selectTBbsHelpList(tBbsHelp);
    }

    /**
     * 新增论坛求助
     *
     * @param tBbsHelp 论坛求助
     * @return 结果
     */
    @Override
    public int insertTBbsHelp(TBbsHelp tBbsHelp) {
        return tBbsHelpMapper.insertTBbsHelp(tBbsHelp);
    }

    /**
     * 修改论坛求助
     *
     * @param tBbsHelp 论坛求助
     * @return 结果
     */
    @Override
    public int updateTBbsHelp(TBbsHelp tBbsHelp) {
        return tBbsHelpMapper.updateTBbsHelp(tBbsHelp);
    }

    /**
     * 批量删除论坛求助
     *
     * @param ids 需要删除的论坛求助主键
     * @return 结果
     */
    @Override
    public int deleteTBbsHelpByIds(Long[] ids) {
        return tBbsHelpMapper.deleteTBbsHelpByIds(ids);
    }

    /**
     * 删除论坛求助信息
     *
     * @param id 论坛求助主键
     * @return 结果
     */
    @Override
    public int deleteTBbsHelpById(Long id) {
        return tBbsHelpMapper.deleteTBbsHelpById(id);
    }
}
