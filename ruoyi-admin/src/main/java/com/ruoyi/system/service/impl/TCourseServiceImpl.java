package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TCourseMapper;
import com.ruoyi.system.domain.TCourse;
import com.ruoyi.system.service.ITCourseService;

/**
 * 课程安排Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-09-15
 */
@Service
public class TCourseServiceImpl implements ITCourseService 
{
    @Autowired
    private TCourseMapper tCourseMapper;

    /**
     * 查询课程安排
     * 
     * @param id 课程安排主键
     * @return 课程安排
     */
    @Override
    public TCourse selectTCourseById(Long id)
    {
        return tCourseMapper.selectTCourseById(id);
    }

    /**
     * 查询课程安排列表
     * 
     * @param tCourse 课程安排
     * @return 课程安排
     */
    @Override
    public List<TCourse> selectTCourseList(TCourse tCourse)
    {
        return tCourseMapper.selectTCourseList(tCourse);
    }

    /**
     * 新增课程安排
     * 
     * @param tCourse 课程安排
     * @return 结果
     */
    @Override
    public int insertTCourse(TCourse tCourse)
    {
        return tCourseMapper.insertTCourse(tCourse);
    }

    /**
     * 修改课程安排
     * 
     * @param tCourse 课程安排
     * @return 结果
     */
    @Override
    public int updateTCourse(TCourse tCourse)
    {
        return tCourseMapper.updateTCourse(tCourse);
    }

    /**
     * 批量删除课程安排
     * 
     * @param ids 需要删除的课程安排主键
     * @return 结果
     */
    @Override
    public int deleteTCourseByIds(Long[] ids)
    {
        return tCourseMapper.deleteTCourseByIds(ids);
    }

    /**
     * 删除课程安排信息
     * 
     * @param id 课程安排主键
     * @return 结果
     */
    @Override
    public int deleteTCourseById(Long id)
    {
        return tCourseMapper.deleteTCourseById(id);
    }
}
