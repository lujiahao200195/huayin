package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TWorkRoomInvite;
import com.ruoyi.system.mapper.TWorkRoomInviteMapper;
import com.ruoyi.system.service.ITWorkRoomInviteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 工作室邀请Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-18
 */
@Service
public class TWorkRoomInviteServiceImpl implements ITWorkRoomInviteService {
    @Autowired
    private TWorkRoomInviteMapper tWorkRoomInviteMapper;

    /**
     * 查询工作室邀请
     *
     * @param id 工作室邀请主键
     * @return 工作室邀请
     */
    @Override
    public TWorkRoomInvite selectTWorkRoomInviteById(Long id) {
        return tWorkRoomInviteMapper.selectTWorkRoomInviteById(id);
    }

    /**
     * 查询工作室邀请列表
     *
     * @param tWorkRoomInvite 工作室邀请
     * @return 工作室邀请
     */
    @Override
    public List<TWorkRoomInvite> selectTWorkRoomInviteList(TWorkRoomInvite tWorkRoomInvite) {
        return tWorkRoomInviteMapper.selectTWorkRoomInviteList(tWorkRoomInvite);
    }

    /**
     * 新增工作室邀请
     *
     * @param tWorkRoomInvite 工作室邀请
     * @return 结果
     */
    @Override
    public int insertTWorkRoomInvite(TWorkRoomInvite tWorkRoomInvite) {
        return tWorkRoomInviteMapper.insertTWorkRoomInvite(tWorkRoomInvite);
    }

    /**
     * 修改工作室邀请
     *
     * @param tWorkRoomInvite 工作室邀请
     * @return 结果
     */
    @Override
    public int updateTWorkRoomInvite(TWorkRoomInvite tWorkRoomInvite) {
        return tWorkRoomInviteMapper.updateTWorkRoomInvite(tWorkRoomInvite);
    }

    /**
     * 批量删除工作室邀请
     *
     * @param ids 需要删除的工作室邀请主键
     * @return 结果
     */
    @Override
    public int deleteTWorkRoomInviteByIds(Long[] ids) {
        return tWorkRoomInviteMapper.deleteTWorkRoomInviteByIds(ids);
    }

    /**
     * 删除工作室邀请信息
     *
     * @param id 工作室邀请主键
     * @return 结果
     */
    @Override
    public int deleteTWorkRoomInviteById(Long id) {
        return tWorkRoomInviteMapper.deleteTWorkRoomInviteById(id);
    }
}
