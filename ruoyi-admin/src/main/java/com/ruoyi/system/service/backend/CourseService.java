package com.ruoyi.system.service.backend;

import com.ruoyi.system.bo.wechat.AddTCourse;
import com.ruoyi.system.domain.TCourse;
import com.ruoyi.system.vo.wechat.GetCourse;
import com.ruoyi.system.vo.wechat.backend.CourseTeacherAndStudent;

import java.util.List;

/**
 * 课程安排Service接口
 *
 * @author ruoyi
 * @date 2023-09-14
 */
public interface CourseService {
    /**
     * 查询课程安排
     *
     * @param id 课程安排主键
     * @return 课程安排
     */
    public TCourse selectTCourseById(Long id);

    /**
     * 查询课程安排列表
     *
     * @param tCourse 课程安排
     * @return 课程安排集合
     */
    public List<TCourse> selectTCourseList(TCourse tCourse);

    /**
     * 新增课程安排
     *
     * @return 结果
     */
    public int insertTCourse(AddTCourse addTCoursee);

    /**
     * 修改课程安排
     * @return 结果
     */
    public int updateTCourse(AddTCourse addTCourse);

    /**
     * 批量删除课程安排
     *
     * @param ids 需要删除的课程安排主键集合
     * @return 结果
     */
    public int deleteTCourseByIds(Long[] ids);

    /**
     * 删除课程安排信息
     *
     * @param id 课程安排主键
     * @return 结果
     */
    public int deleteTCourseById(Long id);

    /**
     * 全部课程
     * @return
     */
    List<GetCourse> getCourse(String id,String date,String type);

    /**
     *
     * @return
     */
   List<CourseTeacherAndStudent> getCourseTeacherAndStudent(String type);

//    /**
//     * 学生课程表
//     * @return
//     */
//    List<GetCourse> wechatMiniStudentCourse(String studentId,String date);
//
//    /**
//     * 教师课程表
//     * @return
//     */
//    List<GetCourse> wechatMiniTeacherCourse(String teacherId,String date);
}
