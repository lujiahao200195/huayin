package com.ruoyi.system.service;

import com.ruoyi.system.domain.TBbsUser;

import java.util.List;

/**
 * 论坛用户数据Service接口
 *
 * @author ruoyi
 * @date 2023-08-29
 */
public interface ITBbsUserService {
    /**
     * 查询论坛用户数据
     *
     * @param id 论坛用户数据主键
     * @return 论坛用户数据
     */
    public TBbsUser selectTBbsUserById(Long id);

    /**
     * 查询论坛用户数据列表
     *
     * @param tBbsUser 论坛用户数据
     * @return 论坛用户数据集合
     */
    public List<TBbsUser> selectTBbsUserList(TBbsUser tBbsUser);

    /**
     * 新增论坛用户数据
     *
     * @param tBbsUser 论坛用户数据
     * @return 结果
     */
    public int insertTBbsUser(TBbsUser tBbsUser);

    /**
     * 修改论坛用户数据
     *
     * @param tBbsUser 论坛用户数据
     * @return 结果
     */
    public int updateTBbsUser(TBbsUser tBbsUser);

    /**
     * 批量删除论坛用户数据
     *
     * @param ids 需要删除的论坛用户数据主键集合
     * @return 结果
     */
    public int deleteTBbsUserByIds(Long[] ids);

    /**
     * 删除论坛用户数据信息
     *
     * @param id 论坛用户数据主键
     * @return 结果
     */
    public int deleteTBbsUserById(Long id);
}
