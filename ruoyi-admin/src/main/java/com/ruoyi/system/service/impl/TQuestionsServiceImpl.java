package com.ruoyi.system.service.impl;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.ruoyi.system.bo.wechat.TAnalysisVo;
import com.ruoyi.system.bo.wechat.TQuestionsTo;
import com.ruoyi.system.bo.wechat.TQuestionsVo;
import com.ruoyi.system.common.tools.NTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TQuestionsMapper;
import com.ruoyi.system.domain.TQuestions;
import com.ruoyi.system.service.ITQuestionsService;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-19
 */
@Service
public class TQuestionsServiceImpl implements ITQuestionsService 
{
    @Autowired
    private TQuestionsMapper tQuestionsMapper;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private NTools nTools;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public TQuestions selectTQuestionsById(Long id)
    {
        return tQuestionsMapper.selectTQuestionsById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param tQuestions 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<TQuestions> selectTQuestionsList(TQuestions tQuestions)
    {
        return tQuestionsMapper.selectTQuestionsList(tQuestions);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param tQuestions 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertTQuestions(TQuestions tQuestions)
    {
        return tQuestionsMapper.insertTQuestions(tQuestions);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param tQuestions 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateTQuestions(TQuestions tQuestions)
    {
        return tQuestionsMapper.updateTQuestions(tQuestions);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteTQuestionsByIds(Long[] ids)
    {
        return tQuestionsMapper.deleteTQuestionsByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteTQuestionsById(Long id)
    {
        return tQuestionsMapper.deleteTQuestionsById(id);
    }

    @Override
    public List<TQuestionsVo> getQuestions() {
        List<TQuestionsVo> tQuestionsVoList = new ArrayList<>();
        List<TQuestionsTo> tQuestionsToList = tQuestionsMapper.selectTQuestionAll();
        tQuestionsToList.forEach(item -> {
            TQuestionsVo tQuestionsVo = new TQuestionsVo();
            tQuestionsVo.setQuestionText(item.getQuestionText());
            tQuestionsVo.setId(item.getId());
            tQuestionsVo.setOptionText(item.gettOptionList().stream().map(option -> option.getOptionText()).collect(Collectors.toList()));
            tQuestionsVo.setLevelTypeName(item.gettDifficultyLevel().getTypeName());
            tQuestionsVo.setQuestionTypeName(item.gettQuestionType().getTypeName());
            redisTemplate.opsForSet().add("question",tQuestionsVo);
            redisTemplate.expire("question",10, TimeUnit.MINUTES);
        });
        for (int i = 0; i < 10 ; i++){
            TQuestionsVo question = (TQuestionsVo) redisTemplate.opsForSet().pop("question");
            if (question != null){

                List<String> optionText = question.getOptionText();
                if (!optionText.isEmpty() && optionText.size() > 0){

                    Collections.reverse(optionText);
                }
            }


            tQuestionsVoList.add(question);
        }
        return tQuestionsVoList;
    }

    @Override
    public Map submitAnswer(Map<Long,String> answer) {
        Map map = new HashMap();
        List<Long> wrongQuestionIds = new ArrayList<>();
        List<TAnalysisVo> tAnalysisVoList = new ArrayList<>();
        Integer score = 0;
        for (Map.Entry<Long, String> entry : answer.entrySet()) {
            Long questionId = entry.getKey();
            String option = entry.getValue();
            int count = tQuestionsMapper.selectOne(questionId,option);
            if (count > 0){
                score += 5;
                continue;
            }
            wrongQuestionIds.add(questionId);
        }
        map.put("score",score);
        tAnalysisVoList = tQuestionsMapper.selectTAnalysis(wrongQuestionIds);
        map.put("tAnalysisVoList",tAnalysisVoList);
        return map;
    }
}
