package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TBbsPostImage;
import com.ruoyi.system.mapper.TBbsPostImageMapper;
import com.ruoyi.system.service.ITBbsPostImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 帖子图片Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-28
 */
@Service
public class TBbsPostImageServiceImpl implements ITBbsPostImageService {
    @Autowired
    private TBbsPostImageMapper tBbsPostImageMapper;

    /**
     * 查询帖子图片
     *
     * @param id 帖子图片主键
     * @return 帖子图片
     */
    @Override
    public TBbsPostImage selectTBbsPostImageById(Long id) {
        return tBbsPostImageMapper.selectTBbsPostImageById(id);
    }

    /**
     * 查询帖子图片列表
     *
     * @param tBbsPostImage 帖子图片
     * @return 帖子图片
     */
    @Override
    public List<TBbsPostImage> selectTBbsPostImageList(TBbsPostImage tBbsPostImage) {
        return tBbsPostImageMapper.selectTBbsPostImageList(tBbsPostImage);
    }

    /**
     * 新增帖子图片
     *
     * @param tBbsPostImage 帖子图片
     * @return 结果
     */
    @Override
    public int insertTBbsPostImage(TBbsPostImage tBbsPostImage) {
        return tBbsPostImageMapper.insertTBbsPostImage(tBbsPostImage);
    }

    /**
     * 修改帖子图片
     *
     * @param tBbsPostImage 帖子图片
     * @return 结果
     */
    @Override
    public int updateTBbsPostImage(TBbsPostImage tBbsPostImage) {
        return tBbsPostImageMapper.updateTBbsPostImage(tBbsPostImage);
    }

    /**
     * 批量删除帖子图片
     *
     * @param ids 需要删除的帖子图片主键
     * @return 结果
     */
    @Override
    public int deleteTBbsPostImageByIds(Long[] ids) {
        return tBbsPostImageMapper.deleteTBbsPostImageByIds(ids);
    }

    /**
     * 删除帖子图片信息
     *
     * @param id 帖子图片主键
     * @return 结果
     */
    @Override
    public int deleteTBbsPostImageById(Long id) {
        return tBbsPostImageMapper.deleteTBbsPostImageById(id);
    }
}
