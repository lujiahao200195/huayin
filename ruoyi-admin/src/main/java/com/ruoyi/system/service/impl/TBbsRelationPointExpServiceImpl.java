package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TBbsRelationPointExp;
import com.ruoyi.system.mapper.TBbsRelationPointExpMapper;
import com.ruoyi.system.service.ITBbsRelationPointExpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 积分经验关系Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-28
 */
@Service
public class TBbsRelationPointExpServiceImpl implements ITBbsRelationPointExpService {
    @Autowired
    private TBbsRelationPointExpMapper tBbsRelationPointExpMapper;

    /**
     * 查询积分经验关系
     *
     * @param id 积分经验关系主键
     * @return 积分经验关系
     */
    @Override
    public TBbsRelationPointExp selectTBbsRelationPointExpById(Long id) {
        return tBbsRelationPointExpMapper.selectTBbsRelationPointExpById(id);
    }

    /**
     * 查询积分经验关系列表
     *
     * @param tBbsRelationPointExp 积分经验关系
     * @return 积分经验关系
     */
    @Override
    public List<TBbsRelationPointExp> selectTBbsRelationPointExpList(TBbsRelationPointExp tBbsRelationPointExp) {
        return tBbsRelationPointExpMapper.selectTBbsRelationPointExpList(tBbsRelationPointExp);
    }

    /**
     * 新增积分经验关系
     *
     * @param tBbsRelationPointExp 积分经验关系
     * @return 结果
     */
    @Override
    public int insertTBbsRelationPointExp(TBbsRelationPointExp tBbsRelationPointExp) {
        return tBbsRelationPointExpMapper.insertTBbsRelationPointExp(tBbsRelationPointExp);
    }

    /**
     * 修改积分经验关系
     *
     * @param tBbsRelationPointExp 积分经验关系
     * @return 结果
     */
    @Override
    public int updateTBbsRelationPointExp(TBbsRelationPointExp tBbsRelationPointExp) {
        return tBbsRelationPointExpMapper.updateTBbsRelationPointExp(tBbsRelationPointExp);
    }

    /**
     * 批量删除积分经验关系
     *
     * @param ids 需要删除的积分经验关系主键
     * @return 结果
     */
    @Override
    public int deleteTBbsRelationPointExpByIds(Long[] ids) {
        return tBbsRelationPointExpMapper.deleteTBbsRelationPointExpByIds(ids);
    }

    /**
     * 删除积分经验关系信息
     *
     * @param id 积分经验关系主键
     * @return 结果
     */
    @Override
    public int deleteTBbsRelationPointExpById(Long id) {
        return tBbsRelationPointExpMapper.deleteTBbsRelationPointExpById(id);
    }
}
