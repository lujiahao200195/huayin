package com.ruoyi.system.service.impl;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.system.bo.wechat.*;
import com.ruoyi.system.common.tools.NTools;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.mapper.*;
import com.ruoyi.system.service.BbsService;
import com.ruoyi.system.vo.wechat.LoginVo;
import com.ruoyi.system.vo.wechat.bbs.*;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Service
public class BbsServiceImpl implements BbsService {

    @Resource
    private NTools nTools;

    @Resource
    private TBbsUserMapper tBbsUserMapper;

    @Resource
    private BbsMapper bbsMapper;

    @Resource
    private TBbsPostImageMapper postImageMapper;

    @Resource
    private TBbsBoardMapper tBbsBoardMapper;

    @Resource
    private TBbsHelpMapper tBbsHelpMapper;

    @Resource
    private TBbsPostMapper tBbsPostMapper;

    @Resource
    private TBbsRelationPointExpMapper relationPointExpMapper;

    @Resource
    private TBbsRelationFollowMapper relationFollowMapper;

    @Resource
    private TUserMapper tUserMapper;

    @Resource
    private TBbsRelationReplyMapper relationReplyMapper;

    @Resource
    private TBbsBoardSuggestMapper boardSuggestMapper;

    @Resource
    private RedisTemplate redisTemplate;

    @Resource
    private TBbsBoardTagMapper boardTagMapper;

    @Resource
    private TBbsBoardMemberMapper boardMemberMapper;

    @Resource
    private TBbsRelationBoardTagMapper relationBoardTagMapper;

    @Resource
    private TBbsRelationAuditMapper relationAuditMapper;


    @Override
    public BbsUserInfo wechatMiniBbsUserInfo() {
        // 获取用户信息
        LoginVo loginInfo = nTools.getLoginInfo();
        // 查询论坛用户数据
        TBbsUser tBbsUser = new TBbsUser();
        tBbsUser.setUserId(loginInfo.getId());
        List<TBbsUser> tBbsUsers = tBbsUserMapper.selectTBbsUserList(tBbsUser);
        if (tBbsUsers.isEmpty()) {
            // 第一次进论坛创建初始化数据
            tBbsUser.setUserId(loginInfo.getId());
            tBbsUser.setExpNum(0l);
            tBbsUser.setPointNum(0l);
            tBbsUserMapper.insertTBbsUser(tBbsUser);
        } else {
            tBbsUser = tBbsUsers.get(0);
        }
        BbsUserInfo bbsUserInfo = new BbsUserInfo();
        bbsUserInfo.setUserId(tBbsUser.getUserId());
        bbsUserInfo.setBbsUserId(tBbsUser.getId());
        bbsUserInfo.setUserName(loginInfo.getUserName());
        bbsUserInfo.setUserIntro(loginInfo.getUserIntro());
        bbsUserInfo.setExpNum(tBbsUser.getExpNum());
        bbsUserInfo.setPointNum(tBbsUser.getPointNum());
        bbsUserInfo.setUserGender(loginInfo.getUserGender());
        bbsUserInfo.setUserPhoto(loginInfo.getUserPhoto());
        return bbsUserInfo;
    }

    @Override
    public List<BbsIndexPostVo> wechatMiniBbsIndexPostList(Long pageIndex, Long pageSize) {
        // 查询首页文章列表
        return bbsMapper.selectTBbsPostByLast(pageIndex, pageSize);
    }

    /**
     * 获取文章图片列表
     *
     * @param postId
     * @return
     */
    public List<BbsPostImageVo> getImageVoListBytBbsPostImage(Long postId) {
        List<BbsPostImageVo> postImageVoList = new ArrayList<>();
        String data = NTools.data(redisTemplate.opsForValue().get("wxmini:common:bbs:getImageVoListBytBbsPostImage:" + postId), String.class);
        if (data != null) {
            JSONArray objects = JSONArray.parseArray(data);
            for (int i = 0; i < objects.size(); i++) {
                BbsPostImageVo object = objects.getObject(i, BbsPostImageVo.class);
                postImageVoList.add(object);
            }
            return postImageVoList;
        }
        TBbsPostImage postImage = new TBbsPostImage();

        postImage.setPostId(postId);
        List<TBbsPostImage> tBbsPostImages = postImageMapper.selectTBbsPostImageList(postImage);
        if (!tBbsPostImages.isEmpty()) {
            for (TBbsPostImage tBbsPostImage : tBbsPostImages) {
                BbsPostImageVo bbsPostImageVo = new BbsPostImageVo();
                bbsPostImageVo.setImageId(tBbsPostImage.getId());
                bbsPostImageVo.setImageUrl(tBbsPostImage.getPostImage());
                postImageVoList.add(bbsPostImageVo);
            }
        }
        redisTemplate.opsForValue().set("wxmini:common:bbs:getImageVoListBytBbsPostImage:" + postId, JSONObject.toJSONString(postImageVoList), 1, TimeUnit.HOURS);
        return postImageVoList;
    }

    @Override
    public BbsBoardInfo getBbsBoardInfoByBoardId(Long boardId) {
        if (boardId == null) {
            return null;
        }
        String data = NTools.data(redisTemplate.opsForValue().get("wxmini:common:bbs:getBbsBoardInfoByBoardId:" + boardId), String.class);

        if (data != null && !"null".equals(data)) {
            BbsBoardInfo bbsBoardInfo = JSONObject.parseObject(data, BbsBoardInfo.class);
            return bbsBoardInfo;
        }
        BbsBoardInfo bbsBoardInfo = bbsMapper.selectBbsBoardInfoByBoardId(boardId);
        redisTemplate.opsForValue().set("wxmini:common:bbs:getBbsBoardInfoByBoardId:" + boardId, JSONObject.toJSONString(bbsBoardInfo), 1, TimeUnit.HOURS);
        return bbsBoardInfo;
    }

    @Override
    @Transactional
    public String wechatMiniBbsSendPost(BbsSendPostDo bbsSendPostDo) {
        // 发帖加经验
        Long addExp = 100l;
        // 发帖加的积分
        Long addPoint = 10l;

        LoginVo loginInfo = nTools.getLoginInfo();
        TBbsUser tBbsUser = bbsMapper.selectTBbsUserByUserId(loginInfo.getId());
        TBbsPost tBbsPost = new TBbsPost();
        tBbsPost.setBoardId(bbsSendPostDo.getBoardId());
        tBbsPost.setPostTitle(bbsSendPostDo.getPostTitle());
        tBbsPost.setPostContent(bbsSendPostDo.getPostContent());
        tBbsPost.setPostType(bbsSendPostDo.getPostType());
        tBbsPost.setUserId(loginInfo.getId());
        int i1 = tBbsPostMapper.insertTBbsPost(tBbsPost);
        if (i1 == 0) {
            throw new RuntimeException("创建帖子异常");
        }
        switch (bbsSendPostDo.getPostType()) {
            case "求助":
                // 扣除积分
                Long pointNum = tBbsUser.getPointNum();
                if (pointNum < bbsSendPostDo.getRewardPoint()) {
                    throw new RuntimeException("创建失败, 积分不足");
                }
                // 创建帮助信息
                TBbsHelp tBbsHelp = new TBbsHelp();
                tBbsHelp.setPostId(tBbsPost.getId());
                tBbsHelp.setRewardPoint(bbsSendPostDo.getRewardPoint());
                tBbsHelp.setHelpStatus("未解决");
                int i = tBbsHelpMapper.insertTBbsHelp(tBbsHelp);
                if (i == 0) {
                    throw new RuntimeException("创建帮助异常");
                }
                addPoint = addPoint - bbsSendPostDo.getRewardPoint();
                break;
            case "文章":
            case "帖子":
                break;
            default:
                throw new RuntimeException("文章类型错误");
        }
        // 更新用户数据
        tBbsUser.setExpNum(tBbsUser.getExpNum() + addExp);
        tBbsUser.setPointNum(tBbsUser.getPointNum() + addPoint);
        int i3 = tBbsUserMapper.updateTBbsUser(tBbsUser);
        if (i3 == 0) {
            throw new RuntimeException("用户数据更新失败");
        }
        // 创建积分扣除记录
        TBbsRelationPointExp relationPointExp = new TBbsRelationPointExp();
        relationPointExp.setExpNum(addExp);
        relationPointExp.setPointExpType("文章");
        relationPointExp.setUserId(loginInfo.getId());
        relationPointExp.setTargetId(tBbsPost.getId());
        relationPointExp.setPointNum(addPoint);
        int i2 = relationPointExpMapper.insertTBbsRelationPointExp(relationPointExp);
        if (i2 == 0) {
            throw new RuntimeException("积分记录异常");
        }
        // 保存图片列表信息
        if (bbsSendPostDo.getImageList() != null && !bbsSendPostDo.getImageList().isEmpty()) {
            for (String imageUrl : bbsSendPostDo.getImageList()) {
                TBbsPostImage tBbsPostImage = new TBbsPostImage();
                tBbsPostImage.setPostId(tBbsPost.getId());
                tBbsPostImage.setPostImage(imageUrl);
                int i = postImageMapper.insertTBbsPostImage(tBbsPostImage);
                if (i == 0) {
                    throw new RuntimeException("图片创建失败");
                }
            }
        }

        return "发送成功";
    }

    @Override
    public String wechatMiniFollowBoard(Long boardId) {
        LoginVo loginInfo = nTools.getLoginInfo();
        TBbsRelationFollow relationFollow = new TBbsRelationFollow();
        relationFollow.setUserId(loginInfo.getId());
        relationFollow.setFollowType("版块");
        relationFollow.setTargetId(boardId);
        int i = relationFollowMapper.insertTBbsRelationFollow(relationFollow);
        if (i == 0) {
            throw new RuntimeException("关注失败");
        }
        return "关注成功";
    }

    @Override
    public String wechatMiniFollowUser(Long userId) {
        LoginVo loginInfo = nTools.getLoginInfo();
        TBbsRelationFollow relationFollow = new TBbsRelationFollow();
        relationFollow.setUserId(loginInfo.getId());
        relationFollow.setFollowType("用户");
        TUser tUser = tUserMapper.selectTUserById(userId);
        if (tUser == null) {
            throw new RuntimeException("您所关注的用户不存在");
        }
        relationFollow.setTargetId(userId);
        int i = relationFollowMapper.insertTBbsRelationFollow(relationFollow);
        if (i == 0) {
            throw new RuntimeException("关注失败");
        }
        return "关注成功";
    }

    @Override
    @Transactional
    public String wechatMiniPostUp(Long postId) {
        LoginVo loginInfo = nTools.getLoginInfo();
        TBbsPost tBbsPost = tBbsPostMapper.selectTBbsPostById(postId);
        if (tBbsPost == null) {
            throw new RuntimeException("文章不存在");
        }
        // 从缓存中查询是否以及点赞过了
        Long data = NTools.data(redisTemplate.opsForValue().get("wxmini:bbs:post-up:" + loginInfo.getId() + ":" + postId), Long.class);
        if (data != null) {
            return "您已点赞过了";
        }
        // 文章点赞数据增加
        tBbsPost.setPostUp(tBbsPost.getPostUp() + 1);
        tBbsPostMapper.updateTBbsPost(tBbsPost);
        // 缓存点赞避免7天内第二次点赞
        redisTemplate.opsForValue().set("wxmini:bbs:post-up:" + loginInfo.getId() + ":" + postId, postId, 7, TimeUnit.DAYS);
        return "点赞成功";
    }

    @Override
    public String wechatMiniPostIsUp(Long postId) {
        LoginVo loginInfo = nTools.getLoginInfo();
        TBbsPost tBbsPost = tBbsPostMapper.selectTBbsPostById(postId);
        if (tBbsPost == null) {
            throw new RuntimeException("文章不存在");
        }
        // 从缓存中查询是否以及点赞过了
        Long data = NTools.data(redisTemplate.opsForValue().get("wxmini:bbs:post-up:" + loginInfo.getId() + ":" + postId), Long.class);
        if (data != null) {
            return "已点赞";
        }
        return "未点赞";
    }

    @Override
    public String wechatMiniReplyUp(Long replyId) {
        LoginVo loginInfo = nTools.getLoginInfo();
        TBbsRelationReply relationReply = relationReplyMapper.selectTBbsRelationReplyById(replyId);
        if (relationReply == null) {
            throw new RuntimeException("回复不存在");
        }
        // 从缓存中获取数据
        Long data = NTools.data(redisTemplate.opsForValue().get("wxmini:bbs:reply-up:" + loginInfo.getId() + ":" + replyId), Long.class);
        if (data != null) {
            return "您已经点赞过了";
        }
        // 更新数据库
        relationReply.setReplyUp(relationReply.getReplyUp() + 1);
        int i = relationReplyMapper.updateTBbsRelationReply(relationReply);
        if (i == 0) {
            throw new RuntimeException("点赞失败");
        }
        // 更新缓存
        redisTemplate.opsForValue().set("wxmini:bbs:reply-up:" + loginInfo.getId() + ":" + replyId, replyId, 7, TimeUnit.DAYS);
        return "点赞成功";
    }

    @Override
    public String wechatMiniReplyIsUp(Long replyId) {
        LoginVo loginInfo = nTools.getLoginInfo();
        TBbsRelationReply relationReply = relationReplyMapper.selectTBbsRelationReplyById(replyId);
        if (relationReply == null) {
            throw new RuntimeException("回复不存在");
        }
        // 从缓存中获取数据
        Long data = NTools.data(redisTemplate.opsForValue().get("wxmini:bbs:reply-up:" + loginInfo.getId() + ":" + replyId), Long.class);
        if (data != null) {
            return "已点赞";
        }
        return "未点赞";
    }

    @Override
    public String wechatMiniReplyDown(Long replyId) {
        LoginVo loginInfo = nTools.getLoginInfo();
        TBbsRelationReply relationReply = relationReplyMapper.selectTBbsRelationReplyById(replyId);
        if (relationReply == null) {
            throw new RuntimeException("回复不存在");
        }
        // 从缓存中获取数据
        Long data = NTools.data(redisTemplate.opsForValue().get("wxmini:bbs:reply-down:" + loginInfo.getId() + ":" + replyId), Long.class);
        if (data != null) {
            return "你已经点踩过了";
        }
        // 更新数据库
        relationReply.setReplyDown(relationReply.getReplyDown() + 1);
        relationReplyMapper.updateTBbsRelationReply(relationReply);
        // 更新缓存
        redisTemplate.opsForValue().set("wxmini:bbs:reply-down:" + loginInfo.getId() + ":" + replyId, replyId, 7, TimeUnit.DAYS);
        return "点踩成功";
    }

    @Override
    public String wechatMiniReplyIsDown(Long replyId) {
        LoginVo loginInfo = nTools.getLoginInfo();
        TBbsRelationReply relationReply = relationReplyMapper.selectTBbsRelationReplyById(replyId);
        if (relationReply == null) {
            throw new RuntimeException("回复不存在");
        }
        // 从缓存中获取数据
        Long data = NTools.data(redisTemplate.opsForValue().get("wxmini:bbs:reply-down:" + loginInfo.getId() + ":" + replyId), Long.class);
        if (data != null) {
            return "已点踩";
        }
        return "未点踩";
    }

    @Override
    public PostDetailsVo wechatMiniPostDetails(Long postId) {
        LoginVo loginInfo = nTools.getLoginInfo();
        // 查询文章
        TBbsPost tBbsPost = tBbsPostMapper.selectTBbsPostById(postId);
        if (tBbsPost == null) {
            throw new RuntimeException("文章数据异常");
        }
        // 文章详情
        PostDetailsVo postDetailsVo = new PostDetailsVo();
        // 获取版块信息
        postDetailsVo.setBoardInfo(getBbsBoardInfoByBoardId(tBbsPost.getBoardId()));
        postDetailsVo.setPostOwnUserId(loginInfo.getId());
        postDetailsVo.setPostOwnUserName(loginInfo.getUserName());
        postDetailsVo.setPostOwnUserPhoto(loginInfo.getUserPhoto());
        postDetailsVo.setPostTitle(tBbsPost.getPostTitle());
        postDetailsVo.setPostContent(tBbsPost.getPostContent());
        postDetailsVo.setPostType(tBbsPost.getPostType());
        // 查询文章图片
        postDetailsVo.setImageVoList(getImageVoListBytBbsPostImage(tBbsPost.getId()));
        return postDetailsVo;
    }

    @Override
    @Transactional
    public String wechatMiniCreateBoard(BbsCreateBoardDo bbsCreateBoardDo) {
        LoginVo loginInfo = nTools.getLoginInfo();
        TBbsBoard tBbsBoard = new TBbsBoard();
        tBbsBoard.setBoardIcon(bbsCreateBoardDo.getBoardPhoto());
        tBbsBoard.setBoardIntro(bbsCreateBoardDo.getBoardIntro());
        tBbsBoard.setBoardName(bbsCreateBoardDo.getBoardName());
        tBbsBoard.setUserId(loginInfo.getId());
        tBbsBoard.setBoardReveal("显示");
        int i = tBbsBoardMapper.insertTBbsBoard(tBbsBoard);
        if (i == 0) {
            throw new RuntimeException("版块创建失败");
        }
        // 创建默认规则帖
        TBbsPost tBbsPost = new TBbsPost();
        tBbsPost.setUserId(loginInfo.getId());
        tBbsPost.setBoardId(tBbsBoard.getId());
        tBbsPost.setPostType("规则");
        tBbsPost.setPostTitle(bbsCreateBoardDo.getBoardName() + "规则帖");
        tBbsPost.setPostContent("请在此处填写规则");
        int i1 = tBbsPostMapper.insertTBbsPost(tBbsPost);
        if (i1 == 0) {
            throw new RuntimeException("默认规则帖创建失败");
        }
        return "版块创建成功";
    }

    @Override
    public List<PostReplyVo> wechatMiniGetPostReplyList(Long postId) {
        List<TBbsRelationReply> relationReplyList = bbsMapper.selectTBbsRelationReplyListByPostIdAnd(postId);
        List<PostReplyVo> postReplyVoList = new ArrayList<>();
        if (!relationReplyList.isEmpty()) {
            for (TBbsRelationReply relationReply : relationReplyList) {
                TUser tUser = getTUserByUserId(relationReply.getUserId());
                PostReplyVo postReplyVo = new PostReplyVo();
                postReplyVo.setReplyId(relationReply.getId());
                postReplyVo.setUserId(tUser.getId());
                postReplyVo.setUserName(tUser.getUserName());
                postReplyVo.setUserPhoto(tUser.getUserPhoto());
                postReplyVo.setReplyContent(relationReply.getReplyContent());
                postReplyVo.setReplyUpNum(relationReply.getReplyUp());
                postReplyVo.setReplyTime(relationReply.getCreatedTime());
                // 查询评论最多5条回复
                List<ChildReplyVo> childReplyVoList = bbsMapper.selectReplyVoListByReplyId(relationReply.getId());
                postReplyVo.setChildReply(childReplyVoList);
                postReplyVoList.add(postReplyVo);
            }
        }
        return postReplyVoList;
    }

    @Override
    public List<BbsIndexPostVo> wechatMiniBoardPostList(Long boardId) {
        return bbsMapper.selectTBbsPostByBoardId(boardId);
    }

    @Override
    public String wechatMiniSendCommandOrReply(Long postId, Long replyId, String replyContent) {
        LoginVo loginInfo = nTools.getLoginInfo();
        TBbsRelationReply relationReply = new TBbsRelationReply();
        relationReply.setReplyId(replyId);
        relationReply.setPostId(postId);
        relationReply.setReplyContent(replyContent);
        relationReply.setUserId(loginInfo.getId());
        int i = relationReplyMapper.insertTBbsRelationReply(relationReply);
        if (i == 0) {
            throw new RuntimeException("评论发布失败");
        }
        return "评论发布成功";
    }

    @Override
    public List<BbsIndexPostVo> wechatMiniBoardSuggestList(Long boardId) {
        return bbsMapper.selectBoardSuggestByBoardId(boardId);
    }

    @Override
    public List<ChildReplyVo> wechatMiniGetReplyOnReplyList(Long replyId) {
        return bbsMapper.selectReplyVoListByReplyId(replyId);
    }

    @Override
    public String wechatMiniUpdateBoardRule(UpdateBoardRuleDo updateBoardRuleDo) {
        LoginVo loginInfo = nTools.getLoginInfo();
        TBbsPost tBbsPost = bbsMapper.selectRuleTPostByBoardId(updateBoardRuleDo.getBoardId(), loginInfo.getId());
        if (tBbsPost == null) {
            throw new RuntimeException("文章数据错误");
        }
        tBbsPost.setPostContent(updateBoardRuleDo.getBoardContent());
        int i = tBbsPostMapper.updateTBbsPost(tBbsPost);
        if (i == 0) {
            throw new RuntimeException("更新失败");
        }
        return "更新成功";
    }

    @Override
    public String wechatMiniUpdateBoardIntro(UpdateBoardInfoDo updateBoardInfoDo) {
        LoginVo loginInfo = nTools.getLoginInfo();
        TBbsBoard tBbsBoard = bbsMapper.selectTBbsBoardByUserIdAndBoardId(updateBoardInfoDo.getBoardId(), loginInfo.getId());
        tBbsBoard.setBoardName(updateBoardInfoDo.getBoardName());
        tBbsBoard.setBoardIcon(updateBoardInfoDo.getBoardIcon());
        tBbsBoard.setBoardIntro(updateBoardInfoDo.getBoardIntro());
        int i = tBbsBoardMapper.updateTBbsBoard(tBbsBoard);
        if (i == 0) {
            throw new RuntimeException("更新失败");
        }
        return "更新成功";
    }

    @Override
    @Transactional
    public String wechatMiniAddBoardSuggestPost(AddBoardSuggestPostDo addBoardSuggestPostDo) {
        LoginVo loginInfo = nTools.getLoginInfo();
        TBbsBoardSuggest tBbsBoardSuggest = new TBbsBoardSuggest();
        // 查询版块, 确保用户操作的版块是用户自己的
        TBbsBoard tBbsBoard = tBbsBoardMapper.selectTBbsBoardById(addBoardSuggestPostDo.getBoardId());
        if (!Objects.equals(tBbsBoard.getUserId(), loginInfo.getId())) {
            throw new RuntimeException("你没有权限操作.");
        }
        tBbsBoardSuggest.setBoardId(addBoardSuggestPostDo.getBoardId());
        tBbsBoardSuggest.setPostId(addBoardSuggestPostDo.getPostId());
        int i = boardSuggestMapper.insertTBbsBoardSuggest(tBbsBoardSuggest);
        if (i == 0) {
            throw new RuntimeException("添加失败");
        }
        // 添加标签
        if (addBoardSuggestPostDo.getTagIdList() != null && !addBoardSuggestPostDo.getTagIdList().isEmpty()) {
            for (Long tagId : addBoardSuggestPostDo.getTagIdList()) {
                // 验证标签是否存在
                TBbsBoardTag tBbsBoardTag = boardTagMapper.selectTBbsBoardTagById(tagId);
                if (tBbsBoardTag == null) {
                    throw new RuntimeException("该标签不存在");
                }
                TBbsRelationBoardTag tBbsRelationBoardTag = new TBbsRelationBoardTag();
                tBbsRelationBoardTag.setTagId(tagId);
                tBbsRelationBoardTag.setSuggestId(tBbsBoardSuggest.getId());
                int i1 = relationBoardTagMapper.insertTBbsRelationBoardTag(tBbsRelationBoardTag);
                if (i1 == 0) {
                    throw new RuntimeException("添加标签失败");
                }
            }
        }
        return "添加成功";
    }

    @Override
    public List<Map> wechatMiniBoardList() {

        return bbsMapper.selectBoardList();
    }

    @Override
    public List<BoardTagVo> wechatMiniGetBoardSuggestTagList(Long boardId) {
        List<BoardTagVo> boardTagVoList = new ArrayList<>();
        TBbsBoardTag tBbsBoardTag = new TBbsBoardTag();
        tBbsBoardTag.setBoardId(boardId);
        List<TBbsBoardTag> tBbsBoardTags = boardTagMapper.selectTBbsBoardTagList(tBbsBoardTag);
        if (!tBbsBoardTags.isEmpty()) {
            for (TBbsBoardTag bbsBoardTag : tBbsBoardTags) {
                BoardTagVo boardTagVo = new BoardTagVo();
                boardTagVo.setTagId(bbsBoardTag.getId());
                boardTagVo.setTagName(bbsBoardTag.getTagName());
                boardTagVoList.add(boardTagVo);
            }
        }
        return boardTagVoList;
    }

    @Override
    public List<BoardMemberVo> wechatMiniBoardMemberList(Long boardId) {
        // 必须审核通过的成员才可以被查到
        List<BoardMemberVo> boardMemberVoListRs = bbsMapper.selectBoardMemberVoListByBoardId(boardId);
        return boardMemberVoListRs;
    }

    @Override
    @Transactional
    public String wechatMiniApplyToJoinBoardMember(Long boardId) {
        LoginVo loginInfo = nTools.getLoginInfo();
        // 查询版块是否存在
        TBbsBoard tBbsBoard = tBbsBoardMapper.selectTBbsBoardById(boardId);
        if (tBbsBoard == null) {
            throw new RuntimeException("申请失败, 版块不存在");
        }
        // 查询是否已经加入
        TBbsBoardMember boardMember1 = new TBbsBoardMember();
        boardMember1.setBoardId(boardId);
        boardMember1.setUserId(loginInfo.getId());
        List<TBbsBoardMember> boardMembers = boardMemberMapper.selectTBbsBoardMemberList(boardMember1);
        if (!boardMembers.isEmpty()) {
            if (boardMembers.get(0) != null) {
                throw new RuntimeException("申请失败, 已经申请加入版块");
            }
        }
        TBbsBoardMember tBbsBoardMember = new TBbsBoardMember();
        tBbsBoardMember.setBoardId(boardId);
        tBbsBoardMember.setUserId(loginInfo.getId());
        // 插入版块成功数据
        int i = boardMemberMapper.insertTBbsBoardMember(tBbsBoardMember);
        if (i == 0) {
            throw new RuntimeException("申请失败, 版块信息错误");
        }
        // 插入审核数据
        TBbsRelationAudit tBbsRelationAudit = new TBbsRelationAudit();
        tBbsRelationAudit.setAuditType("版块成员");
        tBbsRelationAudit.setAuditStatus("待审核");
        tBbsRelationAudit.setTargetId(tBbsBoardMember.getId());
        int i1 = relationAuditMapper.insertTBbsRelationAudit(tBbsRelationAudit);
        if (i1 == 0) {
            throw new RuntimeException("申请失败, 审核数据错误");
        }
        return "申请成功";
    }

    @Override
    public List<BoardMemberJoinVo> wechatMiniBoardMemberJoinList(Long boardId) {
        LoginVo loginInfo = nTools.getLoginInfo();
        // 验证用户是否有权限查看版块申请列表
        TBbsBoard tBbsBoard = tBbsBoardMapper.selectTBbsBoardById(boardId);
        if (tBbsBoard == null) {
            throw new RuntimeException("版块信息错误");
        }
        if (!Objects.equals(tBbsBoard.getUserId(), loginInfo.getId())) {
            throw new RuntimeException("查询失败,你没有权限查询该消息");
        }
        return bbsMapper.selectBoardMemberJoinVoListByBoardId(boardId);
    }

    @Override
    public String wechatMiniAuditBoardMemberJoin(Long boardMemberId, String auditStatus) {
        LoginVo loginInfo = nTools.getLoginInfo();
        // 要查询用户是否有权限修改版块相关的信息
        BoardMemberJoinVo boardMemberJoinVo = bbsMapper.selectBoardMemberJoinVoByBoardMemberIdAndUserId(loginInfo.getId(), boardMemberId);
        if (boardMemberJoinVo == null) {
            throw new RuntimeException("版块成员信息错误");
        }
        TBbsRelationAudit tBbsRelationAudit = new TBbsRelationAudit();
        tBbsRelationAudit.setAuditType("版块成员");
        tBbsRelationAudit.setTargetId(boardMemberId);
        List<TBbsRelationAudit> tBbsRelationAudits = relationAuditMapper.selectTBbsRelationAuditList(tBbsRelationAudit);
        if (!tBbsRelationAudits.isEmpty() && tBbsRelationAudits.get(0) != null) {
            TBbsRelationAudit tBbsRelationAudit1 = tBbsRelationAudits.get(0);
            tBbsRelationAudit1.setAuditStatus(auditStatus);
            int i = relationAuditMapper.updateTBbsRelationAudit(tBbsRelationAudit1);
            if (i == 0) {
                throw new RuntimeException("修改失败");
            }
        } else {
            throw new RuntimeException("版块信息错误");
        }
        return "修改成功";
    }

    /**
     * 通过用户Id获取用户数据(缓存)
     *
     * @param userId
     * @return
     */
    private TUser getTUserByUserId(Long userId) {
        if (userId == null) {
            return null;
        }
        // 用户数据缓存获取
        redisTemplate.opsForValue().get("wxmini:bbs:user-info-cache:" + userId);
        String data = NTools.data(redisTemplate.opsForValue().get("wxmini:bbs:user-info-cache:" + userId), String.class);
        if (data != null) {
            return JSONObject.parseObject(data, TUser.class);
        }
        TUser tUser = tUserMapper.selectTUserById(userId);
        if (tUser != null) {
            // 更新缓存
            redisTemplate.opsForValue().set("wxmini:bbs:user-info-cache:" + userId, JSONObject.toJSONString(tUser), 10, TimeUnit.MINUTES);
            return tUser;
        }
        return null;
    }
}
