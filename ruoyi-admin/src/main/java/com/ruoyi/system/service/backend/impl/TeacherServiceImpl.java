package com.ruoyi.system.service.backend.impl;

import com.ruoyi.system.common.abs.wechatmini.MiniAuditData;
import com.ruoyi.system.common.config.Custom;
import com.ruoyi.system.common.tools.NTools;
import com.ruoyi.system.common.tools.WxUtil;
import com.ruoyi.system.domain.TTeacher;
import com.ruoyi.system.mapper.backend.TeacherMapper;
import com.ruoyi.system.service.backend.TeacherService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 教师Service业务层处理
 */
@Service
public class TeacherServiceImpl implements TeacherService {
    @Resource
    private TeacherMapper teacherMapper;

    @Resource
    Custom custom;

    @Resource
    private WxUtil wxUtil;

    /**
     * 查询教师
     *
     * @param id 教师主键
     * @return 教师
     */
    @Override
    public TTeacher selectTTeacherById(Long id) {
        return teacherMapper.selectTTeacherById(id);
    }

    /**
     * 查询教师列表
     *
     * @param tTeacher 教师
     * @return 教师
     */
    @Override
    public List<TTeacher> selectTTeacherList(TTeacher tTeacher) {
        return teacherMapper.selectTTeacherList(tTeacher);
    }

    /**
     * 新增教师
     *
     * @param tTeacher 教师
     * @return 结果
     */
    @Override
    public int insertTTeacher(TTeacher tTeacher) {
        return teacherMapper.insertTTeacher(tTeacher);
    }

    /**
     * 修改教师
     *
     * @param tTeacher 教师
     * @return 结果
     */
    @Override
    public int updateTTeacher(TTeacher tTeacher) {
        return teacherMapper.updateTTeacher(tTeacher);
    }

    /**
     * 批量删除教师
     *
     * @param ids 需要删除的教师主键
     * @return 结果
     */
    @Override
    public int deleteTTeacherByIds(Long[] ids) {
        return teacherMapper.deleteTTeacherByIds(ids);
    }

    /**
     * 删除教师信息
     *
     * @param id 教师主键
     * @return 结果
     */
    @Override
    public int deleteTTeacherById(Long id) {
        return teacherMapper.deleteTTeacherById(id);
    }

    @Override
    public String changeTeacherState(String id, String stateStr, String stateResult) {
        //判断是否存在此教师
        Integer count = teacherMapper.count("t_teacher", Integer.valueOf(id));
        if (count > 0) {
            //修改教师审核状态
            Integer i = teacherMapper.updateTeacherStateById(Integer.valueOf(id), stateStr, stateResult);
            if (i == 0) {
                throw new RuntimeException("修改审核状态失败");
            }
            //查询教师姓名
            String tName = teacherMapper.selectTeacherNameById(Integer.valueOf(id));
            //根据教师id寻找教师小程序openid
            String openId = teacherMapper.selectTeacherOpenIdById(Integer.valueOf(id));
            //构建教师审核通知
            MiniAuditData miniAuditData = new MiniAuditData();
            miniAuditData.setThing8(tName);
            miniAuditData.setPhrase1(stateStr);
            miniAuditData.setThing3(stateResult);
            miniAuditData.setTime2(NTools.createTime());
            try {
                System.out.println("openId" + openId);
                wxUtil.weChatMiniMsg(miniAuditData.msgTemplateData(openId, custom.AuditSuccess, custom.wechatMiniInfoLang,custom.wechatMiniPage,custom.miniprogramState));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

        }

        return "成功";
    }
}
