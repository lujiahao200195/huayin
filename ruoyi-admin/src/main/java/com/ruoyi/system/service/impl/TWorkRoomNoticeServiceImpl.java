package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TWorkRoomNotice;
import com.ruoyi.system.mapper.TWorkRoomNoticeMapper;
import com.ruoyi.system.service.ITWorkRoomNoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 工作室公告Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-18
 */
@Service
public class TWorkRoomNoticeServiceImpl implements ITWorkRoomNoticeService {
    @Autowired
    private TWorkRoomNoticeMapper tWorkRoomNoticeMapper;

    /**
     * 查询工作室公告
     *
     * @param id 工作室公告主键
     * @return 工作室公告
     */
    @Override
    public TWorkRoomNotice selectTWorkRoomNoticeById(Long id) {
        return tWorkRoomNoticeMapper.selectTWorkRoomNoticeById(id);
    }

    /**
     * 查询工作室公告列表
     *
     * @param tWorkRoomNotice 工作室公告
     * @return 工作室公告
     */
    @Override
    public List<TWorkRoomNotice> selectTWorkRoomNoticeList(TWorkRoomNotice tWorkRoomNotice) {
        return tWorkRoomNoticeMapper.selectTWorkRoomNoticeList(tWorkRoomNotice);
    }

    /**
     * 新增工作室公告
     *
     * @param tWorkRoomNotice 工作室公告
     * @return 结果
     */
    @Override
    public int insertTWorkRoomNotice(TWorkRoomNotice tWorkRoomNotice) {
        return tWorkRoomNoticeMapper.insertTWorkRoomNotice(tWorkRoomNotice);
    }

    /**
     * 修改工作室公告
     *
     * @param tWorkRoomNotice 工作室公告
     * @return 结果
     */
    @Override
    public int updateTWorkRoomNotice(TWorkRoomNotice tWorkRoomNotice) {
        return tWorkRoomNoticeMapper.updateTWorkRoomNotice(tWorkRoomNotice);
    }

    /**
     * 批量删除工作室公告
     *
     * @param ids 需要删除的工作室公告主键
     * @return 结果
     */
    @Override
    public int deleteTWorkRoomNoticeByIds(Long[] ids) {
        return tWorkRoomNoticeMapper.deleteTWorkRoomNoticeByIds(ids);
    }

    /**
     * 删除工作室公告信息
     *
     * @param id 工作室公告主键
     * @return 结果
     */
    @Override
    public int deleteTWorkRoomNoticeById(Long id) {
        return tWorkRoomNoticeMapper.deleteTWorkRoomNoticeById(id);
    }
}
