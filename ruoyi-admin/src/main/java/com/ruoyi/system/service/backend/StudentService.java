package com.ruoyi.system.service.backend;

import com.ruoyi.system.domain.TStudent;
import com.ruoyi.system.vo.wechat.StudentSubjectDetails;

import java.util.List;

/**
 * 学生管理service
 */
public interface StudentService {

    /**
     * 查询学生
     *
     * @param id 学生主键
     * @return 学生
     */
    public TStudent selectTStudentById(Long id);

    /**
     * 根据学生id 查学生的优势科目和需强化科目
     *
     * @param id
     * @return
     */
    StudentSubjectDetails studentSubjectDetails(Long id);

    /**
     * 查询学生列表
     *
     * @param tStudent 学生
     * @return 学生集合
     */
    public List<TStudent> selectTStudentList(TStudent tStudent);

    /**
     * 新增学生
     *
     * @param tStudent 学生
     * @return 结果
     */
    public int insertTStudent(TStudent tStudent);

    /**
     * 修改学生
     *
     * @param tStudent 学生
     * @return 结果
     */
    public int updateTStudent(TStudent tStudent);

    /**
     * 批量删除学生
     *
     * @param ids 需要删除的学生主键集合
     * @return 结果
     */
    public int deleteTStudentByIds(Long[] ids);

    /**
     * 删除学生信息
     *
     * @param id 学生主键
     * @return 结果
     */
    public int deleteTStudentById(Long id);

    /**
     * 后台管理修改学生审核状态码
     *
     * @return
     */
    public String changeStudentState(String id, String stateStr, String stateResult);
}
