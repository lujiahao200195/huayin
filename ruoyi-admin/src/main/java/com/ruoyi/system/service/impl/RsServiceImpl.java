package com.ruoyi.system.service.impl;

import com.ruoyi.system.bo.wechat.SearchResourceDo;
import com.ruoyi.system.bo.wechat.UpdateResourceDo;
import com.ruoyi.system.bo.wechat.UploadResourceDo;
import com.ruoyi.system.common.tools.CosFileUploadUtils;
import com.ruoyi.system.common.tools.NTools;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.mapper.*;
import com.ruoyi.system.service.RsService;
import com.ruoyi.system.vo.wechat.LoginVo;
import com.ruoyi.system.vo.wechat.rs.ResourceTagVo;
import com.ruoyi.system.vo.wechat.rs.ResourceVo;
import com.ruoyi.system.vo.wechat.rs.SearchStudentVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class RsServiceImpl implements RsService {

    @Resource
    private TResourceTagMapper resourceTagMapper;

    @Resource
    private TResourceMapper resourceMapper;

    @Resource
    private TResourceRelationResourceTagMapper relationResourceTagMapper;

    @Resource
    private TResourceRelationMapper resourceRelationMapper;

    @Resource
    private RsMapper rsMapper;

    @Resource
    private NTools nTools;


    @Override
    public List<ResourceTagVo> wechatMiniResourceTagList() {
        List<ResourceTagVo> resourceTagVos = new ArrayList<>();
        TResourceTag tResourceTag = new TResourceTag();
        tResourceTag.setAuditStatus("通过");
        List<TResourceTag> tResourceTags = resourceTagMapper.selectTResourceTagList(tResourceTag);
        for (TResourceTag resourceTag : tResourceTags) {
            ResourceTagVo resourceTagVo = new ResourceTagVo();
            resourceTagVo.setTagId(resourceTag.getId());
            resourceTagVo.setTagName(resourceTag.getResourceTagName());
            resourceTagVos.add(resourceTagVo);
        }
        return resourceTagVos;
    }

    @Override
    public List<ResourceVo> wechatMiniSearchResourceList(SearchResourceDo searchResourceDo) {
//        nanjing.myqcloud.com/huayin/common/2023/10/07/tmp_f6e85975f0d5d891563bfa3c74ca7931_20231007183450A063.docx
        LoginVo loginInfo = nTools.getLoginInfo();
        if ("学生资料".equals(searchResourceDo.getSearchType())){
            LoginVo.StudentInfo studentInfo = loginInfo.getStudentInfo();
            if (studentInfo == null){
                throw new RuntimeException("您不是学生");
            }
            return rsMapper.searchResourceVo(searchResourceDo, loginInfo.getId(), studentInfo.getStudentId());
        }
        return rsMapper.searchResourceVo(searchResourceDo, loginInfo.getId(), -1l);
    }

    @Override
    public TResourceTag wechatMiniAddResourceTag(String tagName) {
        LoginVo loginInfo = nTools.getLoginInfo();
        TResourceTag tResourceTag = new TResourceTag();
        tResourceTag.setResourceTagName(tagName);
        tResourceTag.setUserId(loginInfo.getId());
        tResourceTag.setAuditStatus("通过");
        int i = resourceTagMapper.insertTResourceTag(tResourceTag);
        if (i == 0) {
            throw new RuntimeException("创建标签失败");
        }
        return tResourceTag;
    }

    @Override
    @Transactional
    public String wechatMiniUploadResource(UploadResourceDo uploadResourceDo) throws IOException {
        LoginVo loginInfo = nTools.getLoginInfo();
        // 上传文件
        TResource tResource = new TResource();
        tResource.setUserId(loginInfo.getId());
        tResource.setResourceTitle(uploadResourceDo.getResourceTitle());
        tResource.setResourceIntro(uploadResourceDo.getResourceDetails());
        tResource.setResourceMime(uploadResourceDo.getResourceMime());
        tResource.setResourceLink(uploadResourceDo.getResourceLink());
        tResource.setFileType(uploadResourceDo.getFileExt());
        tResource.setResourceSize(CosFileUploadUtils.getFileSize(uploadResourceDo.getResourceSize()));
        tResource.setPublicStatus(uploadResourceDo.getPublicStatus());
        tResource.setResourceCover(uploadResourceDo.getResourceCover());
        tResource.setAuditStatus("通过");
        int i = resourceMapper.insertTResource(tResource);
        if (i == 0) {
            throw new RuntimeException("资源创建失败");
        }
        // 创建标签关系
        if (uploadResourceDo.getResourceTagList() != null && !uploadResourceDo.getResourceTagList().isEmpty()) {
            for (Long tagId : uploadResourceDo.getResourceTagList()) {
                TResourceRelationResourceTag tResourceRelationResourceTag = new TResourceRelationResourceTag();
                tResourceRelationResourceTag.setResourceTagId(tagId);
                tResourceRelationResourceTag.setResourceId(tResource.getId());
                int i1 = relationResourceTagMapper.insertTResourceRelationResourceTag(tResourceRelationResourceTag);
                if (i1 == 0) {
                    throw new RuntimeException("标签异常");
                }
            }
        }
        return "资源创建成功";
    }

    @Override
    public List<ResourceTagVo> wechatMiniSearchResourceTag(String tagName) {
        LoginVo loginInfo = nTools.getLoginInfo();
        return rsMapper.searchResourceTagVo(tagName, loginInfo.getId());
    }

    @Override
    public String wechatMiniDeleteResource(Long resourceId) {
        LoginVo loginInfo = nTools.getLoginInfo();
        TResource tResource = resourceMapper.selectTResourceById(resourceId);
        if (!Objects.equals(tResource.getUserId(), loginInfo.getId())) {
            throw new RuntimeException("无权操作此资源");
        }
        tResource.setDeleteTime(new Date());
        int i = resourceMapper.updateTResource(tResource);
        if (i == 0) {
            throw new RuntimeException("删除失败");
        }
        return "删除成功";
    }

    @Override
    public void wechatMiniDownloadResource(Long resourceId, HttpServletResponse response) throws IOException {
        // 判断用户是否有权限下载资源
        LoginVo loginInfo = nTools.getLoginInfo();
        TResource resource = rsMapper.selectTResourceByUserIdAndResourceId(loginInfo.getId(), resourceId);
        if (resource == null) {
            throw new RuntimeException("您没有权限下载此文件");
        }
        // 下载资源
        CosFileUploadUtils.download(resource.getResourceLink(), response);
    }

    @Override
    @Transactional
    public String wechatMiniUpdateResource(UpdateResourceDo updateResourceDo) {
        LoginVo loginInfo = nTools.getLoginInfo();
        TResource resource = resourceMapper.selectTResourceById(updateResourceDo.getResourceId());
        if (resource == null) {
            throw new RuntimeException("资源不存在");
        }
        // 判断是否有修改权限
        if (!Objects.equals(loginInfo.getId(), resource.getUserId())) {
            throw new RuntimeException("您没有权限修改此资源");
        }
        // 修改数据
        resource.setResourceTitle(updateResourceDo.getResourceTitle());
        resource.setResourceIntro(updateResourceDo.getResourceIntro());
        int i = resourceMapper.updateTResource(resource);
        if (i == 0) {
            throw new RuntimeException("修改失败");
        }
        // 修改标签关联
        // 删除标签
        relationResourceTagMapper.deleteTResourceRelationResourceTagByIds(updateResourceDo.getResourceIdList().toArray(new Long[0]));
        // 重新添加
        if (updateResourceDo.getResourceIdList() != null && !updateResourceDo.getResourceIdList().isEmpty()) {
            for (Long aLong : updateResourceDo.getResourceIdList()) {
                TResourceRelationResourceTag tResourceRelationResourceTag = new TResourceRelationResourceTag();
                tResourceRelationResourceTag.setResourceId(resource.getId());
                tResourceRelationResourceTag.setResourceTagId(aLong);
                int i1 = relationResourceTagMapper.insertTResourceRelationResourceTag(tResourceRelationResourceTag);
                if (i1 == 0) {
                    throw new RuntimeException("标签修改错误");
                }
            }
        }
        return "修改成功";
    }

    @Override
    public String wechatMiniShareResourceStudent(Long studentId, Long resourceId) {
        // 验证用户是否有权限分享资源
        LoginVo loginInfo = nTools.getLoginInfo();
        TResource resource = resourceMapper.selectTResourceById(resourceId);
        if (!Objects.equals(resource.getUserId(), loginInfo.getId())) {
            throw new RuntimeException("您没有权限分享此资料");
        }
        TResourceRelation tResourceRelation = new TResourceRelation();
        tResourceRelation.setResourceId(resourceId);
        tResourceRelation.setResourceType("学生资料");
        tResourceRelation.setTargetId(studentId);
        tResourceRelation.setAcceptStatus("是");
        int i = resourceRelationMapper.insertTResourceRelation(tResourceRelation);
        if (i == 0) {
            throw new RuntimeException("分享失败");
        }
        return "分享成功";
    }

    @Override
    public String wechatMiniShareResourceUser(Long userId, Long resourceId) {
        LoginVo loginInfo = nTools.getLoginInfo();
        // 判断用户是否有权限分享改资源
        TResource resource = resourceMapper.selectTResourceById(resourceId);
        if (resource == null) {
            throw new RuntimeException("资料信息错误");
        }
        if (!Objects.equals(resource.getUserId(), loginInfo.getId())) {
            throw new RuntimeException("您没有权限分享该文件");
        }
        // 新增分享数据
        TResourceRelation resourceRelation = new TResourceRelation();
        resourceRelation.setResourceId(resourceId);
        resourceRelation.setResourceType("共享资料");
        resourceRelation.setTargetId(userId);
        int i = resourceRelationMapper.insertTResourceRelation(resourceRelation);
        if (i == 0) {
            throw new RuntimeException("分享失败");
        }
        return "分享成功";
    }

    @Override
    public String wechatMiniUserAcceptResource(Long resourceId) {
        LoginVo loginInfo = nTools.getLoginInfo();
        // 用户接受资源
        List<TResourceRelation> tResourceRelations = resourceRelationMapper.selectTResourceRelationList(new TResourceRelation());
        if (tResourceRelations.isEmpty()) {
            throw new RuntimeException("资源关系数据不存在");
        }
        TResourceRelation tResourceRelation = tResourceRelations.get(0);
        // 判断这条数据是否属于用户
        if (!"学生资料".equals(tResourceRelation.getResourceType()) || !Objects.equals(tResourceRelation.getTargetId(), loginInfo.getId())) {
            throw new RuntimeException("您无权接受此文件");
        }
        if (tResourceRelation == null) {
            throw new RuntimeException("资源关系数据异常");
        }
        tResourceRelation.setAcceptStatus("是");
        int i = resourceRelationMapper.updateTResourceRelation(tResourceRelation);
        if (i == 0) {
            throw new RuntimeException("接受失败");
        }
        return "接受成功";
    }

    @Override
    public List<SearchStudentVo> wechatMiniTeacherSearchStudent(String studentName) {
        LoginVo.TeacherInfo teacherInfo = nTools.getTeacherInfo();
        if (teacherInfo == null){
            throw new RuntimeException("不是教师, 无权查询学生");
        }
        List<TStudent> tStudents = rsMapper.teacherSearchStudent(studentName);
        List<SearchStudentVo> searchStudentVoList = new ArrayList<>();
        if (!tStudents.isEmpty()){
            for (TStudent tStudent : tStudents) {
                SearchStudentVo searchStudentVo = new SearchStudentVo();
                searchStudentVo.setStudentId(tStudent.getId());
                searchStudentVo.setStudentName(tStudent.getStudentName());
                searchStudentVoList.add(searchStudentVo);
            }
        }
        return searchStudentVoList;
    }

    @Override
    public String wechatMiniRsCount(Long resourceId) {
        TResource resource = resourceMapper.selectTResourceById(resourceId);
        if (resource == null){
            throw new RuntimeException("资源数据不存在");
        }
        Long downloadNum = resource.getDownloadNum();
        if (downloadNum == null){
            downloadNum = 0l;
        }
        resource.setDownloadNum(downloadNum+1);
        resourceMapper.updateTResource(resource);
        return "下载成功";
    }


    /**
     * 小程序文章收藏接口
     * @param resourceId
     * @return 结果字符
     *
     */
    @Override
    public String wechatMiniCollectResource(Long resourceId) {
        //获取用户登录信息
        LoginVo loginInfo = nTools.getLoginInfo();
        //数据非空判断
        TResource resource = resourceMapper.selectTResourceById(resourceId);
        if (resource == null){
            throw new RuntimeException("资源数据错误");
        }
        //查找资源是否已经被收藏
        TResourceRelation resourceRelationRidAndTid = wechatMiniQueryCollectStatus(resourceId);
        System.out.println(resourceRelationRidAndTid.toString());
        //数据存在
        if(!(resourceRelationRidAndTid.getId() == null)) {
            return "已收藏";
        }
        //构建需更新的数据对象
        TResourceRelation tResourceRelation = new TResourceRelation();
        tResourceRelation.setResourceType("收藏");
        tResourceRelation.setTargetId(loginInfo.getId());
        tResourceRelation.setResourceId(resourceId);
        tResourceRelation.setAcceptStatus("否");
        //更新操作
        int i = resourceRelationMapper.insertTResourceRelation(tResourceRelation);
        if (i == 0){
            throw new RuntimeException("收藏失败");
        }
        return "收藏成功";
    }


    /**
     * 用户取消收藏
     */
    @Override
    public int wechatMiniCancelCollect(Long resourceId) {
        //数据非空判断
        TResource resource = resourceMapper.selectTResourceById(resourceId);
        if (resource == null){
            throw new RuntimeException("资源数据错误");
        }
        //获取目标数据
        TResourceRelation resourceRelationRidAndTid = wechatMiniQueryCollectStatus(resourceId);
        //数据存在
        if(resourceRelationRidAndTid.getId() == null) {
            return 0;
        }
        return resourceRelationMapper.deleteTResourceRelationById(resourceRelationRidAndTid.getId());
    }


    /**
     * 查看资源的对应用户的收藏状态
     * @param resourceId
     * @return
     */
    @Override
    public TResourceRelation wechatMiniQueryCollectStatus(Long resourceId) {
        //获取用户登录信息
        LoginVo loginInfo = nTools.getLoginInfo();
        //查询对象封装
        TResourceRelation resourceRelationRidAndTid = new TResourceRelation();
        resourceRelationRidAndTid.setResourceId(resourceId);
        resourceRelationRidAndTid.setTargetId(loginInfo.getId());
        resourceRelationRidAndTid.setResourceType("收藏");
        //进行查询
        TResourceRelation resourceRelation = resourceRelationMapper.selectTRRByTidAndRid(resourceRelationRidAndTid);
        if(resourceRelation == null){
            TResourceRelation tResourceRelationEmpty = new TResourceRelation();
            return tResourceRelationEmpty;
        }
        //返回结果
        return resourceRelation;
    }
}
