package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TCourseSubjectRelationMapper;
import com.ruoyi.system.domain.TCourseSubjectRelation;
import com.ruoyi.system.service.ITCourseSubjectRelationService;

/**
 * 课程和科目关系Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-09-15
 */
@Service
public class TCourseSubjectRelationServiceImpl implements ITCourseSubjectRelationService 
{
    @Autowired
    private TCourseSubjectRelationMapper tCourseSubjectRelationMapper;

    /**
     * 查询课程和科目关系
     * 
     * @param id 课程和科目关系主键
     * @return 课程和科目关系
     */
    @Override
    public TCourseSubjectRelation selectTCourseSubjectRelationById(Long id)
    {
        return tCourseSubjectRelationMapper.selectTCourseSubjectRelationById(id);
    }

    /**
     * 查询课程和科目关系列表
     * 
     * @param tCourseSubjectRelation 课程和科目关系
     * @return 课程和科目关系
     */
    @Override
    public List<TCourseSubjectRelation> selectTCourseSubjectRelationList(TCourseSubjectRelation tCourseSubjectRelation)
    {
        return tCourseSubjectRelationMapper.selectTCourseSubjectRelationList(tCourseSubjectRelation);
    }

    /**
     * 新增课程和科目关系
     * 
     * @param tCourseSubjectRelation 课程和科目关系
     * @return 结果
     */
    @Override
    public int insertTCourseSubjectRelation(TCourseSubjectRelation tCourseSubjectRelation)
    {
        return tCourseSubjectRelationMapper.insertTCourseSubjectRelation(tCourseSubjectRelation);
    }

    /**
     * 修改课程和科目关系
     * 
     * @param tCourseSubjectRelation 课程和科目关系
     * @return 结果
     */
    @Override
    public int updateTCourseSubjectRelation(TCourseSubjectRelation tCourseSubjectRelation)
    {
        return tCourseSubjectRelationMapper.updateTCourseSubjectRelation(tCourseSubjectRelation);
    }

    /**
     * 批量删除课程和科目关系
     * 
     * @param ids 需要删除的课程和科目关系主键
     * @return 结果
     */
    @Override
    public int deleteTCourseSubjectRelationByIds(Long[] ids)
    {
        return tCourseSubjectRelationMapper.deleteTCourseSubjectRelationByIds(ids);
    }

    /**
     * 删除课程和科目关系信息
     * 
     * @param id 课程和科目关系主键
     * @return 结果
     */
    @Override
    public int deleteTCourseSubjectRelationById(Long id)
    {
        return tCourseSubjectRelationMapper.deleteTCourseSubjectRelationById(id);
    }
}
