package com.ruoyi.system.service.backend.impl;

import com.ruoyi.system.mapper.backend.BackendDataMapper;
import com.ruoyi.system.service.backend.BackendDataService;
import com.ruoyi.system.vo.wechat.backend.GetSignUpNumber;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class BackendDataServiceImpl implements BackendDataService {

    @Resource
    private BackendDataMapper backendDataMapperMapper;

    @Override
    public List<GetSignUpNumber> getSignUpNumber() {
        return backendDataMapperMapper.getSignUpNumber();
    }

    @Override
    public List<GetSignUpNumber> getStudentSignUpNumber() {
        return backendDataMapperMapper.getStudentSignUpNumber();
    }

    @Override
    public List<GetSignUpNumber> getTeacherSignUpNumber() {
        return backendDataMapperMapper.getTeacherSignUpNumber();
    }

}
