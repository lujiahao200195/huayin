package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TStudentAdvantages;
import com.ruoyi.system.mapper.TStudentAdvantagesMapper;
import com.ruoyi.system.service.ITStudentAdvantagesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 学生优势科目Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-30
 */
@Service
public class TStudentAdvantagesServiceImpl implements ITStudentAdvantagesService {
    @Autowired
    private TStudentAdvantagesMapper tStudentAdvantagesMapper;

    /**
     * 查询学生优势科目
     *
     * @param id 学生优势科目主键
     * @return 学生优势科目
     */
    @Override
    public TStudentAdvantages selectTStudentAdvantagesById(Long id) {
        return tStudentAdvantagesMapper.selectTStudentAdvantagesById(id);
    }

    /**
     * 查询学生优势科目列表
     *
     * @param tStudentAdvantages 学生优势科目
     * @return 学生优势科目
     */
    @Override
    public List<TStudentAdvantages> selectTStudentAdvantagesList(TStudentAdvantages tStudentAdvantages) {
        return tStudentAdvantagesMapper.selectTStudentAdvantagesList(tStudentAdvantages);
    }

    /**
     * 新增学生优势科目
     *
     * @param tStudentAdvantages 学生优势科目
     * @return 结果
     */
    @Override
    public int insertTStudentAdvantages(TStudentAdvantages tStudentAdvantages) {
        return tStudentAdvantagesMapper.insertTStudentAdvantages(tStudentAdvantages);
    }

    /**
     * 修改学生优势科目
     *
     * @param tStudentAdvantages 学生优势科目
     * @return 结果
     */
    @Override
    public int updateTStudentAdvantages(TStudentAdvantages tStudentAdvantages) {
        return tStudentAdvantagesMapper.updateTStudentAdvantages(tStudentAdvantages);
    }

    /**
     * 批量删除学生优势科目
     *
     * @param ids 需要删除的学生优势科目主键
     * @return 结果
     */
    @Override
    public int deleteTStudentAdvantagesByIds(Long[] ids) {
        return tStudentAdvantagesMapper.deleteTStudentAdvantagesByIds(ids);
    }

    /**
     * 删除学生优势科目信息
     *
     * @param id 学生优势科目主键
     * @return 结果
     */
    @Override
    public int deleteTStudentAdvantagesById(Long id) {
        return tStudentAdvantagesMapper.deleteTStudentAdvantagesById(id);
    }
}
