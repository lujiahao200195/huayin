package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TSubjectType;
import com.ruoyi.system.mapper.TSubjectTypeMapper;
import com.ruoyi.system.service.ITSubjectTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 科目分类Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-17
 */
@Service
public class TSubjectTypeServiceImpl implements ITSubjectTypeService {
    @Autowired
    private TSubjectTypeMapper tSubjectTypeMapper;

    /**
     * 查询科目分类
     *
     * @param id 科目分类主键
     * @return 科目分类
     */
    @Override
    public TSubjectType selectTSubjectTypeById(Long id) {
        return tSubjectTypeMapper.selectTSubjectTypeById(id);
    }

    /**
     * 查询科目分类列表
     *
     * @param tSubjectType 科目分类
     * @return 科目分类
     */
    @Override
    public List<TSubjectType> selectTSubjectTypeList(TSubjectType tSubjectType) {
        return tSubjectTypeMapper.selectTSubjectTypeList(tSubjectType);
    }

    /**
     * 新增科目分类
     *
     * @param tSubjectType 科目分类
     * @return 结果
     */
    @Override
    public int insertTSubjectType(TSubjectType tSubjectType) {
        return tSubjectTypeMapper.insertTSubjectType(tSubjectType);
    }

    /**
     * 修改科目分类
     *
     * @param tSubjectType 科目分类
     * @return 结果
     */
    @Override
    public int updateTSubjectType(TSubjectType tSubjectType) {
        return tSubjectTypeMapper.updateTSubjectType(tSubjectType);
    }

    /**
     * 批量删除科目分类
     *
     * @param ids 需要删除的科目分类主键
     * @return 结果
     */
    @Override
    public int deleteTSubjectTypeByIds(Long[] ids) {
        return tSubjectTypeMapper.deleteTSubjectTypeByIds(ids);
    }

    /**
     * 删除科目分类信息
     *
     * @param id 科目分类主键
     * @return 结果
     */
    @Override
    public int deleteTSubjectTypeById(Long id) {
        return tSubjectTypeMapper.deleteTSubjectTypeById(id);
    }
}
