package com.ruoyi.system.service;

import com.ruoyi.system.domain.TSubjectType;

import java.util.List;

/**
 * 科目分类Service接口
 *
 * @author ruoyi
 * @date 2023-08-17
 */
public interface ITSubjectTypeService {
    /**
     * 查询科目分类
     *
     * @param id 科目分类主键
     * @return 科目分类
     */
    public TSubjectType selectTSubjectTypeById(Long id);

    /**
     * 查询科目分类列表
     *
     * @param tSubjectType 科目分类
     * @return 科目分类集合
     */
    public List<TSubjectType> selectTSubjectTypeList(TSubjectType tSubjectType);

    /**
     * 新增科目分类
     *
     * @param tSubjectType 科目分类
     * @return 结果
     */
    public int insertTSubjectType(TSubjectType tSubjectType);

    /**
     * 修改科目分类
     *
     * @param tSubjectType 科目分类
     * @return 结果
     */
    public int updateTSubjectType(TSubjectType tSubjectType);

    /**
     * 批量删除科目分类
     *
     * @param ids 需要删除的科目分类主键集合
     * @return 结果
     */
    public int deleteTSubjectTypeByIds(Long[] ids);

    /**
     * 删除科目分类信息
     *
     * @param id 科目分类主键
     * @return 结果
     */
    public int deleteTSubjectTypeById(Long id);
}
