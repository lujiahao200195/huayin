package com.ruoyi.system.service.backend.impl;

import com.ruoyi.system.bo.wechat.AddSubjectArticleDo;
import com.ruoyi.system.domain.TArticle;
import com.ruoyi.system.domain.TSubject;
import com.ruoyi.system.mapper.backend.ArticleMapper;
import com.ruoyi.system.mapper.backend.SubjectMapper;
import com.ruoyi.system.mapper.backend.UserMapper;
import com.ruoyi.system.service.backend.SubjectService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 教学科目Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-18
 */
@Service
public class SubjectServiceImpl implements SubjectService {
    @Resource
    private SubjectMapper subjectMapper;

    @Resource
    private ArticleMapper articleMapper;

    @Resource
    private UserMapper userMapper;


    /**
     * 查询教学科目
     *
     * @param id 教学科目主键
     * @return 教学科目
     */
    @Override
    public TSubject selectTSubjectById(Long id) {
        return subjectMapper.selectTSubjectById(id);
    }

    /**
     * 查询教学科目列表
     *
     * @param tSubject 教学科目
     * @return 教学科目
     */
    @Override
    public List<TSubject> selectTSubjectList(TSubject tSubject) {
        return subjectMapper.selectTSubjectList(tSubject);
    }

    /**
     * 新增教学科目
     *
     * @param tSubject 教学科目
     * @return 结果
     */
    @Override
    public int insertTSubject(TSubject tSubject) {
        return subjectMapper.insertTSubject(tSubject);
    }

    /**
     * 修改教学科目
     *
     * @param tSubject 教学科目
     * @return 结果
     */
    @Override
    public int updateTSubject(TSubject tSubject) {
        return subjectMapper.updateTSubject(tSubject);
    }

    /**
     * 批量删除教学科目
     *
     * @param ids 需要删除的教学科目主键
     * @return 结果
     */
    @Override
    public int deleteTSubjectByIds(Long[] ids) {
        return subjectMapper.deleteTSubjectByIds(ids);
    }

    /**
     * 删除教学科目信息
     *
     * @param id 教学科目主键
     * @return 结果
     */
    @Override
    public int deleteTSubjectById(Long id) {
        return subjectMapper.deleteTSubjectById(id);
    }

    @Override
    @Transactional
    public String addSubjectArticle(AddSubjectArticleDo addSubjectArticleDo) {
        if (addSubjectArticleDo.getSubjectId() == null) {
            throw new RuntimeException("科目参数为空请求错误");
        }
        TArticle article = new TArticle();
        article.setArticleContent(addSubjectArticleDo.getArticleContent());
        article.setArticleImage(addSubjectArticleDo.getArticleImage());
        article.setArticleLink(addSubjectArticleDo.getArticleLink());
        article.setArticleNote(addSubjectArticleDo.getArticleNote());
        int i = articleMapper.insertTArticle(article);
        if (i == 0) {
            throw new RuntimeException("新增文章失败");
        }
        int i1 = subjectMapper.insertSubjectArticleRelation(addSubjectArticleDo.getSubjectId(), article.getId());
        if (i1 == 0) {
            throw new RuntimeException("新增关系表失败");
        }

        return "新增成功";
    }

    @Override
    public List<TArticle> getSubjectArticles(Long id) {
        if (id == null) {
            throw new RuntimeException("科目id错误");
        }
        return subjectMapper.getSubjectArticles(id);
    }

    @Override
    public String removeSubjectArticle(Long subjectId, Long articleId) {
        if (subjectId == null || articleId == null) {
            throw new RuntimeException("参数错误");
        }
        int i = subjectMapper.deleteSubjectArticleRelation(subjectId, articleId);
        if (i == 0) {
            throw new RuntimeException("删除关系表失败");
        }
        int i1 = articleMapper.deleteTArticleById(articleId);
        if (i1 == 0) {
            throw new RuntimeException("删除文章失败");
        }
        return "删除成功";
    }
}
