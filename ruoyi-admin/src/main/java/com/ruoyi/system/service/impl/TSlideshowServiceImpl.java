package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TSlideshow;
import com.ruoyi.system.mapper.TSlideshowMapper;
import com.ruoyi.system.service.ITSlideshowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 轮播图Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-15
 */
@Service
public class TSlideshowServiceImpl implements ITSlideshowService {
    @Autowired
    private TSlideshowMapper tSlideshowMapper;

    /**
     * 查询轮播图
     *
     * @param id 轮播图主键
     * @return 轮播图
     */
    @Override
    public TSlideshow selectTSlideshowById(Long id) {
        return tSlideshowMapper.selectTSlideshowById(id);
    }

    /**
     * 查询轮播图列表
     *
     * @param tSlideshow 轮播图
     * @return 轮播图
     */
    @Override
    public List<TSlideshow> selectTSlideshowList(TSlideshow tSlideshow) {
        return tSlideshowMapper.selectTSlideshowList(tSlideshow);
    }

    /**
     * 新增轮播图
     *
     * @param tSlideshow 轮播图
     * @return 结果
     */
    @Override
    public int insertTSlideshow(TSlideshow tSlideshow) {
        return tSlideshowMapper.insertTSlideshow(tSlideshow);
    }

    /**
     * 修改轮播图
     *
     * @param tSlideshow 轮播图
     * @return 结果
     */
    @Override
    public int updateTSlideshow(TSlideshow tSlideshow) {
        return tSlideshowMapper.updateTSlideshow(tSlideshow);
    }

    /**
     * 批量删除轮播图
     *
     * @param ids 需要删除的轮播图主键
     * @return 结果
     */
    @Override
    public int deleteTSlideshowByIds(Long[] ids) {
        return tSlideshowMapper.deleteTSlideshowByIds(ids);
    }

    /**
     * 删除轮播图信息
     *
     * @param id 轮播图主键
     * @return 结果
     */
    @Override
    public int deleteTSlideshowById(Long id) {
        return tSlideshowMapper.deleteTSlideshowById(id);
    }
}
