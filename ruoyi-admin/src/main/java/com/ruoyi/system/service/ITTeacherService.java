package com.ruoyi.system.service;

import com.ruoyi.system.domain.TTeacher;

import java.util.List;

/**
 * 教师Service接口
 *
 * @author ruoyi
 * @date 2023-09-13
 */
public interface ITTeacherService {
    /**
     * 查询教师
     *
     * @param id 教师主键
     * @return 教师
     */
    public TTeacher selectTTeacherById(Long id);

    /**
     * 查询教师列表
     *
     * @param tTeacher 教师
     * @return 教师集合
     */
    public List<TTeacher> selectTTeacherList(TTeacher tTeacher);

    /**
     * 新增教师
     *
     * @param tTeacher 教师
     * @return 结果
     */
    public int insertTTeacher(TTeacher tTeacher);

    /**
     * 修改教师
     *
     * @param tTeacher 教师
     * @return 结果
     */
    public int updateTTeacher(TTeacher tTeacher);

    /**
     * 批量删除教师
     *
     * @param ids 需要删除的教师主键集合
     * @return 结果
     */
    public int deleteTTeacherByIds(Long[] ids);

    /**
     * 删除教师信息
     *
     * @param id 教师主键
     * @return 结果
     */
    public int deleteTTeacherById(Long id);
}
