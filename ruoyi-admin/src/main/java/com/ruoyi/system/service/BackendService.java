package com.ruoyi.system.service;

import com.ruoyi.system.vo.wechat.StudentSubjectDetails;

/**
 * 后台管理接口
 */
public interface BackendService {

    /**
     * 后台管理修改学生审核状态码
     *
     * @return
     */
    public Integer changeStudentState(String id, String stateStr);

    /**
     * 后台管理修改教师审核状态码
     *
     * @return
     */
    public Integer changeTeacherState(String id, String stateStr);

    /**
     * 根据学生id 查学生的优势科目和需强化科目
     *
     * @param id
     * @return
     */
    StudentSubjectDetails studentSubjectDetails(Long id);
}
