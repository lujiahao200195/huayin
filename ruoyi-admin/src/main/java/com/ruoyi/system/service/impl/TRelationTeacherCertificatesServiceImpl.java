package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TRelationTeacherCertificates;
import com.ruoyi.system.mapper.TRelationTeacherCertificatesMapper;
import com.ruoyi.system.service.ITRelationTeacherCertificatesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 教师和教师证书关系Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-17
 */
@Service
public class TRelationTeacherCertificatesServiceImpl implements ITRelationTeacherCertificatesService {
    @Autowired
    private TRelationTeacherCertificatesMapper tRelationTeacherCertificatesMapper;

    /**
     * 查询教师和教师证书关系
     *
     * @param id 教师和教师证书关系主键
     * @return 教师和教师证书关系
     */
    @Override
    public TRelationTeacherCertificates selectTRelationTeacherCertificatesById(Long id) {
        return tRelationTeacherCertificatesMapper.selectTRelationTeacherCertificatesById(id);
    }

    /**
     * 查询教师和教师证书关系列表
     *
     * @param tRelationTeacherCertificates 教师和教师证书关系
     * @return 教师和教师证书关系
     */
    @Override
    public List<TRelationTeacherCertificates> selectTRelationTeacherCertificatesList(TRelationTeacherCertificates tRelationTeacherCertificates) {
        return tRelationTeacherCertificatesMapper.selectTRelationTeacherCertificatesList(tRelationTeacherCertificates);
    }

    /**
     * 新增教师和教师证书关系
     *
     * @param tRelationTeacherCertificates 教师和教师证书关系
     * @return 结果
     */
    @Override
    public int insertTRelationTeacherCertificates(TRelationTeacherCertificates tRelationTeacherCertificates) {
        return tRelationTeacherCertificatesMapper.insertTRelationTeacherCertificates(tRelationTeacherCertificates);
    }

    /**
     * 修改教师和教师证书关系
     *
     * @param tRelationTeacherCertificates 教师和教师证书关系
     * @return 结果
     */
    @Override
    public int updateTRelationTeacherCertificates(TRelationTeacherCertificates tRelationTeacherCertificates) {
        return tRelationTeacherCertificatesMapper.updateTRelationTeacherCertificates(tRelationTeacherCertificates);
    }

    /**
     * 批量删除教师和教师证书关系
     *
     * @param ids 需要删除的教师和教师证书关系主键
     * @return 结果
     */
    @Override
    public int deleteTRelationTeacherCertificatesByIds(Long[] ids) {
        return tRelationTeacherCertificatesMapper.deleteTRelationTeacherCertificatesByIds(ids);
    }

    /**
     * 删除教师和教师证书关系信息
     *
     * @param id 教师和教师证书关系主键
     * @return 结果
     */
    @Override
    public int deleteTRelationTeacherCertificatesById(Long id) {
        return tRelationTeacherCertificatesMapper.deleteTRelationTeacherCertificatesById(id);
    }
}
