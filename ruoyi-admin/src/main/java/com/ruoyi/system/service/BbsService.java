package com.ruoyi.system.service;

import com.ruoyi.system.bo.wechat.*;
import com.ruoyi.system.vo.wechat.bbs.*;

import java.util.List;
import java.util.Map;

/**
 * 论坛接口
 */
public interface BbsService {

    /**
     * 获取论坛用户信息
     *
     * @return
     */
    BbsUserInfo wechatMiniBbsUserInfo();

    /**
     * 获取首页文章列表
     *
     * @return
     */
    List<BbsIndexPostVo> wechatMiniBbsIndexPostList(Long pageIndex, Long pageSize);


    /**
     * 通过boardId获取版块信息
     *
     * @param boardId
     * @return
     */
    BbsBoardInfo getBbsBoardInfoByBoardId(Long boardId);

    /**
     * 发送论坛帖
     *
     * @param bbsSendPostDo
     * @return
     */
    String wechatMiniBbsSendPost(BbsSendPostDo bbsSendPostDo);

    /**
     * 用户关注版块
     *
     * @param boardId
     * @return
     */
    String wechatMiniFollowBoard(Long boardId);

    /**
     * 用户关注用户
     *
     * @param userId
     * @return
     */
    String wechatMiniFollowUser(Long userId);

    /**
     * 用户给文章点赞
     *
     * @param postId
     * @return
     */
    String wechatMiniPostUp(Long postId);

    /**
     * 查询是否已经点过赞了
     *
     * @param postId
     * @return
     */
    String wechatMiniPostIsUp(Long postId);


    /**
     * 回复点赞
     *
     * @param replyId
     * @return
     */
    String wechatMiniReplyUp(Long replyId);

    /**
     * 回复是否已经点赞
     *
     * @param replyId
     * @return
     */
    String wechatMiniReplyIsUp(Long replyId);

    /**
     * 回复点踩
     *
     * @param replyId
     * @return
     */
    String wechatMiniReplyDown(Long replyId);

    /**
     * 回复是否已经点踩
     *
     * @param replyId
     * @return
     */
    String wechatMiniReplyIsDown(Long replyId);

    /**
     * 获取论坛详情
     *
     * @param postId
     * @return
     */
    PostDetailsVo wechatMiniPostDetails(Long postId);

    /**
     * 用户创建版块
     *
     * @param bbsCreateBoardDo
     * @return
     */
    String wechatMiniCreateBoard(BbsCreateBoardDo bbsCreateBoardDo);

    /**
     * 获取文章回复列表
     *
     * @param postId
     * @return
     */
    List<PostReplyVo> wechatMiniGetPostReplyList(Long postId);

    /**
     * 获取版块文章列表
     *
     * @param boardId
     * @return
     */
    List<BbsIndexPostVo> wechatMiniBoardPostList(Long boardId);

    /**
     * 用户发布文章的评论或回复
     *
     * @param postId
     * @param replyId
     * @param replyContent
     * @return
     */
    String wechatMiniSendCommandOrReply(Long postId, Long replyId, String replyContent);

    /**
     * 获取版块推荐
     *
     * @param boardId
     * @return
     */
    List<BbsIndexPostVo> wechatMiniBoardSuggestList(Long boardId);

    /**
     * 通过评论的回复Id获取回复列表
     *
     * @param replyId
     * @return
     */
    List<ChildReplyVo> wechatMiniGetReplyOnReplyList(Long replyId);

    /**
     * 修改版块
     *
     * @param updateBoardRuleDo
     * @return
     */
    String wechatMiniUpdateBoardRule(UpdateBoardRuleDo updateBoardRuleDo);

    /**
     * 用户更新版块信息
     *
     * @param updateBoardInfoDo
     * @return
     */
    String wechatMiniUpdateBoardIntro(UpdateBoardInfoDo updateBoardInfoDo);

    /**
     * 添加版块推荐文章
     *
     * @param addBoardSuggestPostDo
     * @return
     */
    String wechatMiniAddBoardSuggestPost(AddBoardSuggestPostDo addBoardSuggestPostDo);

    /**
     * 查询版块列表
     *
     * @return
     */
    List<Map> wechatMiniBoardList();

    /**
     * @param boardId
     * @return
     */
    List<BoardTagVo> wechatMiniGetBoardSuggestTagList(Long boardId);

    /**
     * 查询版块成员列表
     *
     * @param boardId
     * @return
     */
    List<BoardMemberVo> wechatMiniBoardMemberList(Long boardId);

    /**
     * 用户申请加入版块成员
     *
     * @param boardId
     * @return
     */
    String wechatMiniApplyToJoinBoardMember(Long boardId);

    /**
     * 版主查询版块申请加入的列表
     *
     * @param boardId
     * @return
     */
    List<BoardMemberJoinVo> wechatMiniBoardMemberJoinList(Long boardId);

    /**
     * 版主审核版块成员加入
     *
     * @param boardMemberId
     * @return
     */
    String wechatMiniAuditBoardMemberJoin(Long boardMemberId, String auditStatus);
}
