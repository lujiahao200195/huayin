package com.ruoyi.system.service;

import com.ruoyi.system.domain.TArticleTypeRelation;

import java.util.List;

/**
 * 文章和文章分类关系Service接口
 *
 * @author ruoyi
 * @date 2023-08-16
 */
public interface ITArticleTypeRelationService {
    /**
     * 查询文章和文章分类关系
     *
     * @param id 文章和文章分类关系主键
     * @return 文章和文章分类关系
     */
    public TArticleTypeRelation selectTArticleTypeRelationById(Long id);

    /**
     * 查询文章和文章分类关系列表
     *
     * @param tArticleTypeRelation 文章和文章分类关系
     * @return 文章和文章分类关系集合
     */
    public List<TArticleTypeRelation> selectTArticleTypeRelationList(TArticleTypeRelation tArticleTypeRelation);

    /**
     * 新增文章和文章分类关系
     *
     * @param tArticleTypeRelation 文章和文章分类关系
     * @return 结果
     */
    public int insertTArticleTypeRelation(TArticleTypeRelation tArticleTypeRelation);

    /**
     * 修改文章和文章分类关系
     *
     * @param tArticleTypeRelation 文章和文章分类关系
     * @return 结果
     */
    public int updateTArticleTypeRelation(TArticleTypeRelation tArticleTypeRelation);

    /**
     * 批量删除文章和文章分类关系
     *
     * @param ids 需要删除的文章和文章分类关系主键集合
     * @return 结果
     */
    public int deleteTArticleTypeRelationByIds(Long[] ids);

    /**
     * 删除文章和文章分类关系信息
     *
     * @param id 文章和文章分类关系主键
     * @return 结果
     */
    public int deleteTArticleTypeRelationById(Long id);
}
