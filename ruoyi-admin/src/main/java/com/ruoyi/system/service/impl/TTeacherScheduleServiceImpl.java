package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TTeacherSchedule;
import com.ruoyi.system.mapper.TTeacherScheduleMapper;
import com.ruoyi.system.service.ITTeacherScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 教师可安排时间段Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-17
 */
@Service
public class TTeacherScheduleServiceImpl implements ITTeacherScheduleService {
    @Autowired
    private TTeacherScheduleMapper tTeacherScheduleMapper;

    /**
     * 查询教师可安排时间段
     *
     * @param id 教师可安排时间段主键
     * @return 教师可安排时间段
     */
    @Override
    public TTeacherSchedule selectTTeacherScheduleById(Long id) {
        return tTeacherScheduleMapper.selectTTeacherScheduleById(id);
    }

    /**
     * 查询教师可安排时间段列表
     *
     * @param tTeacherSchedule 教师可安排时间段
     * @return 教师可安排时间段
     */
    @Override
    public List<TTeacherSchedule> selectTTeacherScheduleList(TTeacherSchedule tTeacherSchedule) {
        return tTeacherScheduleMapper.selectTTeacherScheduleList(tTeacherSchedule);
    }

    /**
     * 新增教师可安排时间段
     *
     * @param tTeacherSchedule 教师可安排时间段
     * @return 结果
     */
    @Override
    public int insertTTeacherSchedule(TTeacherSchedule tTeacherSchedule) {
        return tTeacherScheduleMapper.insertTTeacherSchedule(tTeacherSchedule);
    }

    /**
     * 修改教师可安排时间段
     *
     * @param tTeacherSchedule 教师可安排时间段
     * @return 结果
     */
    @Override
    public int updateTTeacherSchedule(TTeacherSchedule tTeacherSchedule) {
        return tTeacherScheduleMapper.updateTTeacherSchedule(tTeacherSchedule);
    }

    /**
     * 批量删除教师可安排时间段
     *
     * @param ids 需要删除的教师可安排时间段主键
     * @return 结果
     */
    @Override
    public int deleteTTeacherScheduleByIds(Long[] ids) {
        return tTeacherScheduleMapper.deleteTTeacherScheduleByIds(ids);
    }

    /**
     * 删除教师可安排时间段信息
     *
     * @param id 教师可安排时间段主键
     * @return 结果
     */
    @Override
    public int deleteTTeacherScheduleById(Long id) {
        return tTeacherScheduleMapper.deleteTTeacherScheduleById(id);
    }
}
