package com.ruoyi.system.service;

import com.ruoyi.system.domain.TSlideshow;

import java.util.List;

/**
 * 轮播图Service接口
 *
 * @author ruoyi
 * @date 2023-08-15
 */
public interface ITSlideshowService {
    /**
     * 查询轮播图
     *
     * @param id 轮播图主键
     * @return 轮播图
     */
    public TSlideshow selectTSlideshowById(Long id);

    /**
     * 查询轮播图列表
     *
     * @param tSlideshow 轮播图
     * @return 轮播图集合
     */
    public List<TSlideshow> selectTSlideshowList(TSlideshow tSlideshow);

    /**
     * 新增轮播图
     *
     * @param tSlideshow 轮播图
     * @return 结果
     */
    public int insertTSlideshow(TSlideshow tSlideshow);

    /**
     * 修改轮播图
     *
     * @param tSlideshow 轮播图
     * @return 结果
     */
    public int updateTSlideshow(TSlideshow tSlideshow);

    /**
     * 批量删除轮播图
     *
     * @param ids 需要删除的轮播图主键集合
     * @return 结果
     */
    public int deleteTSlideshowByIds(Long[] ids);

    /**
     * 删除轮播图信息
     *
     * @param id 轮播图主键
     * @return 结果
     */
    public int deleteTSlideshowById(Long id);
}
