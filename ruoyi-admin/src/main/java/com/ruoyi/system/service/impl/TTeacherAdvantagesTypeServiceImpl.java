package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TTeacherAdvantagesType;
import com.ruoyi.system.mapper.TTeacherAdvantagesTypeMapper;
import com.ruoyi.system.service.ITTeacherAdvantagesTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 教师个人优势分类Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-15
 */
@Service
public class TTeacherAdvantagesTypeServiceImpl implements ITTeacherAdvantagesTypeService {
    @Autowired
    private TTeacherAdvantagesTypeMapper tTeacherAdvantagesTypeMapper;

    /**
     * 查询教师个人优势分类
     *
     * @param id 教师个人优势分类主键
     * @return 教师个人优势分类
     */
    @Override
    public TTeacherAdvantagesType selectTTeacherAdvantagesTypeById(Long id) {
        return tTeacherAdvantagesTypeMapper.selectTTeacherAdvantagesTypeById(id);
    }

    /**
     * 查询教师个人优势分类列表
     *
     * @param tTeacherAdvantagesType 教师个人优势分类
     * @return 教师个人优势分类
     */
    @Override
    public List<TTeacherAdvantagesType> selectTTeacherAdvantagesTypeList(TTeacherAdvantagesType tTeacherAdvantagesType) {
        return tTeacherAdvantagesTypeMapper.selectTTeacherAdvantagesTypeList(tTeacherAdvantagesType);
    }

    /**
     * 新增教师个人优势分类
     *
     * @param tTeacherAdvantagesType 教师个人优势分类
     * @return 结果
     */
    @Override
    public int insertTTeacherAdvantagesType(TTeacherAdvantagesType tTeacherAdvantagesType) {
        return tTeacherAdvantagesTypeMapper.insertTTeacherAdvantagesType(tTeacherAdvantagesType);
    }

    /**
     * 修改教师个人优势分类
     *
     * @param tTeacherAdvantagesType 教师个人优势分类
     * @return 结果
     */
    @Override
    public int updateTTeacherAdvantagesType(TTeacherAdvantagesType tTeacherAdvantagesType) {
        return tTeacherAdvantagesTypeMapper.updateTTeacherAdvantagesType(tTeacherAdvantagesType);
    }

    /**
     * 批量删除教师个人优势分类
     *
     * @param ids 需要删除的教师个人优势分类主键
     * @return 结果
     */
    @Override
    public int deleteTTeacherAdvantagesTypeByIds(Long[] ids) {
        return tTeacherAdvantagesTypeMapper.deleteTTeacherAdvantagesTypeByIds(ids);
    }

    /**
     * 删除教师个人优势分类信息
     *
     * @param id 教师个人优势分类主键
     * @return 结果
     */
    @Override
    public int deleteTTeacherAdvantagesTypeById(Long id) {
        return tTeacherAdvantagesTypeMapper.deleteTTeacherAdvantagesTypeById(id);
    }
}
