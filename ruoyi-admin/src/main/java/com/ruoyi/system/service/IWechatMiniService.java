package com.ruoyi.system.service;

import com.ruoyi.system.bo.wechat.StudentSignUp;
import com.ruoyi.system.bo.wechat.*;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.vo.wechat.*;

import java.util.List;
import java.util.Map;

public interface IWechatMiniService {

    /**
     * 通过微信小程序openId获取用户
     *
     * @return
     */
    String selectTUserByWechatMiniOpenId(Map<String, String> jsCodeMap);

    /**
     * 获取新报名学生列表
     *
     * @return
     */
    List<LastStudentList> lastStudentList();

    /**
     * 首页轮播图
     */
    List<TSlideshow> selectTSlideshowList();

    /**
     * 首页获取文章列表
     *
     * @return
     */
    List<TArticle> indexArticleList();

    /**
     * 获取用户信息通过openId
     *
     * @param openId
     * @return
     */
    GetStudentInfo getStudentInfo(String openId);

    /**
     * 学生报名
     *
     * @param studentSignUp
     */
    com.ruoyi.system.vo.wechat.StudentSignUp studentSignUp(StudentSignUp studentSignUp);

    /**
     * 发送学生报名通知
     */
    String studentSignUpMsg(String studentId);

    /**
     * 发送教师报名通知
     *
     * @param teacherId
     */
    String teacherSignUpMsg(String teacherId);

    /**
     * 获取用户个人信息
     *
     * @return
     */
    GetUserInfo getUserInfo();

    /**
     * 通过微信小程序openId获取用户信息
     *
     * @param openId
     * @return
     */
    TUser getTUserByMiniOpenId(String openId);

    /**
     * 教师报名
     *
     * @param teacherSignUp
     */
    TeacherSignUpVo teacherSignUp(TeacherSignUp teacherSignUp);

    /**
     * 微信小程序教师添加教师证书
     *
     * @param addTeacherCertificate
     * @return
     */
    String wechatMiniAddTeacherCertificate(AddTeacherCertificate addTeacherCertificate);

    /**
     * 通过微信openId和教师Id查询教师自己拥有的证书列表
     *
     * @param openId
     * @param teacherId
     * @return
     */
    List<TTeacherCertificates> wechatMiniTeacherOwnCertificateList(String openId, Long teacherId);

    /**
     * 通过微信openId和教师Id查询教师自己所有的证书列表
     *
     * @param openId
     * @param teacherId
     * @return
     */
    List<TTeacherCertificates> wechatMiniTeacherAllCertificateList(String openId, Long teacherId);

    /**
     * 微信小程序学生报名总数
     *
     * @return
     */
    Long wechatMiniIndexSignUpNumber();

    /**
     * 通过文章Id获取
     *
     * @param subjectId
     * @return
     */
    List<TArticle> wechatMiniSubjectArticleList(Long subjectId);

    /**
     * 微信小程序获取首页文章列表
     *
     * @return
     */
    List<TArticle> wechatMiniIndexSubjectList();

    /**
     * 微信小程序获取证书列表
     *
     * @return
     */
    List<TTeacherCertificates> wechatMiniCertificateList();

    /**
     * 获取用户自己加入的工作室列表
     *
     * @return
     */
    List<OwnWorkRoomSearchVo> wechatMiniSelfJoinWorkRoomList();

    /**
     * 微信小程序查询正常的工作室列表
     *
     * @return
     */
    List<TWorkRoom> wechatMiniPublicWorkRoomList();

    /**
     * 创建工作室
     *
     * @param createWorkRoom
     * @return
     */
    String wechatMiniCreateWorkRoom(CreateWorkRoom createWorkRoom);

    /**
     * 根据工作室id查询工作室所有的邀请码列表
     *
     * @param wordRoomId
     * @return
     */
    List<TWorkRoomInvite> wechatMiniWorkInviteCodeList(String wordRoomId);

    /**
     * 使用工作室邀请码
     *
     * @return
     */
    String wechatMiniUseInviteCode(String type, String inviteCode);

    /**
     * 根据openid查询审核列表
     *
     * @return
     */
    List<GetWorkRoomAuditList> wechatMiniGetWorkRoomAuditList();

    /**
     * 用户/学生/教师加入工作室
     *
     * @param joinWorkRoom
     * @return
     */
    String wechatMiniJoinWorkRoom(JoinWorkRoom joinWorkRoom);

    /**
     * 创建工作室邀请码
     *
     * @param workRoomId
     * @return
     */
    String wechatMiniCreateWorkInviteCode(String workRoomId, int time);

    /**
     * 删除工作室邀请码
     *
     * @param workRoomInviteId
     * @return
     */
    String wechatMiniDeleteWorkInviteCode(String workRoomInviteId);

    /**
     * 获取所有正常科目以及自己创建的科目
     *
     * @return
     */
    List<TSubject> wechatMiniSubjectListAndOwnCreate();

    /**
     * 微信工作室搜索
     *
     * @param workRoomSearch
     * @return
     */
    List<WorkRoomSearchVo> wechatMiniWorkRoomSearch(WorkRoomSearch workRoomSearch);

    /**
     * 页面展示消息列表
     *
     * @return
     */
    List<WorkRoomMsgShowVo> wechatMiniWorkRoomMsg();

    /**
     * 通过科目Id查询文章列表
     *
     * @param subjectId
     * @return
     */
    List<TArticle> wechatMiniArticleListBySubjectId(Long subjectId);

    /**
     * 通过工作室id 查询工作室的教师个人信息表
     *
     * @param workRoomId
     * @return
     */
    List<TWorkRoomTeacherInfo> wechatMiniWorkRoomTeacherInfo(String workRoomId);

    /**
     * 删除工作室教师个人信息卡
     *
     * @param workRoomId
     * @return
     */
    String wechatMiniDeleteWorkRoomTeacherInfo(String id, String workRoomId);

    /**
     * 修改工作室教师个人信息表
     *
     * @param
     * @return
     */
    String wechatMiniUpdateWorkRoomTeacherInfo(WorkRoomTeacherInfo workRoomTeacherInfo);

    /**
     * 新增工作室教师个人信息表
     *
     * @return
     */
    String wechatMiniInsertWorkRoomTeacherInfo(WorkRoomTeacherInfo workRoomTeacherInfo);


//    /**
//     * 修改工作室信息
//     *
//     */
//    String wechatMiniUpdateWorkRoomInfo(WorkRoomInfo workRoomInfo);


    /**
     * 根据工作室id查工作室的审核列表
     *
     * @param workRoomId
     */
    List<GetWorkRoomAuditList> wechatMiniAuditJoinWorkRoomList(String workRoomId);

    /**
     * 微信小程序工作室加入请求审核
     *
     * @param id
     * @param auditStatus
     * @return
     */
    String wechatMiniAuditJoinWorkRoom(String id, String auditStatus, String auditResult);

    /**
     * 获取工作室当前在线状态
     *
     * @param workRoomId
     * @return
     */
    String wechatMiniWorkRoomOnline(String workRoomId);


    /**
     * 修改user资料
     *
     * @param changeUserInfo
     * @return
     */
    String wechatMiniChangeUserInfo(ChangeUserInfo changeUserInfo);

    /**
     * 用户查询与工作室的历史消息
     *
     * @param workRoomId
     * @return
     */
    List<TWorkRoomMessage> wechatMiniMessageHistoryListByWorkRoom(Long workRoomId);


    /**
     * 查询自己的文章
     *
     * @return
     */
    List<WechatMiniArticleVo> wechatMiniOneselfArticle();

    /**
     * 查询工作室的展示文章
     *
     * @param workRoomId
     * @return
     */
    WechatMiniArticleVo wechatMiniWorkRoomShowArticle(Long workRoomId);

    /**
     * 查询自己/工作室文章
     *
     * @param workRoomId
     * @return
     */
    List<WechatMiniArticleVo> selectOwnArticleList(Long workRoomId);

    /**
     * 工作室用户列表
     *
     * @param workRoomId
     * @return
     */
    List<WorkRoomGetMember> selectWorkRoomGetMemberList(Long workRoomId);

    /**
     * 查询用户当前身份是否加入工作室
     *
     * @param workRoomId
     * @return
     */
    String wechatMiniCheckJoinOrNotWorkRoom(Long workRoomId, String joinType);

    /**
     * 工作室查询成员简介
     *
     * @param id
     * @param type
     * @return
     */
    WorkRoomMemberDetails wechatMiniWorkRoomMemberDetails(String id, String type);


    /**
     * 退出工作室
     *
     * @return
     */
    String wechatMiniQuitWorkRoom(String id, String workRoomId, String joinType);

    /**
     * 查询子的工作室
     *
     * @return
     */
    List<OwnWorkRoomSearchVo> wechatMiniOwnCreateWorkRoom();

    /**
     * 获取用户报名记录
     *
     * @return
     */
    SignUpRecordVo wechatMiniSignUpRecord();

    /**
     * 查询学生注意文章
     *
     * @return
     */
    WechatMiniArticleVo wechatMiniStudentLookOut();

    /**
     * 查询教师注意文章
     *
     * @return
     */
    WechatMiniArticleVo wechatMiniTeacherLookOut();

    /**
     * 修改教师教师信息
     *
     * @param selfUpdateTeacherInfoDo
     * @return
     */
    String wechatMiniSelfUpdateTeacherInfo(SelfUpdateTeacherInfoDo selfUpdateTeacherInfoDo);

    /**
     * 修改学生信息
     *
     * @param selfUpdateStudentInfoDo
     * @return
     */
    String wechatMiniSelfUpdateStudentInfo(SelfUpdateStudentInfoDo selfUpdateStudentInfoDo);

    /**
     * 用户阅读文章
     *
     * @param articleId
     * @return
     */
    String wechatMiniUserArticleRead(Long articleId);

    /**
     * 首页根据分类ID获取贴子
     *
     * @param typeId
     * @return
     */
    List<GetArticleList> bbsGetArticleList(Integer page, Integer size, Long typeId);

    /**
     * 查询论坛最新的文章
     *
     * @return
     */
    List<GetArticleList> bbsGetNewArticleList(String page, String size);

    /**
     * 显示教师报名数据
     *
     * @param token
     * @return
     */
    SignUpTeacherInfoVo ShowTeacherSignUpInfo(String token);

    /**
     * 显示学生报名数据
     *
     * @param token
     * @return
     */
    SignUpStudentInfoVo ShowStudentSignUpInfo(String token);

    /**
     * 报名审核结果
     *
     * @return
     */
    String wechatMiniAuditResult(WechatMiniAuditResult wechatMiniAuditResult);

    /**
     * 删除工作室
     *
     * @param workRoomId
     * @return
     */
    String wechatMiniDeleteOwnWorkRoom(Long workRoomId);

    /**
     * 修改工作室
     *
     * @param updateWorkRoomDo
     * @return
     */
    String wechatMiniUpdateWorkRoom(UpdateWorkRoomDo updateWorkRoomDo);

    /**
     * 自己申请加入工作室记录
     *
     * @return
     */
    List<OwnJoinWorkRoomVo> wechatMiniOwnJoinWorkRoom();

    /**
     * 用户修改在工作室内的名字
     *
     * @return
     */
    String wechatMiniUpdateWorkRoomTeacherName(String workRoomId, String teacherName);

    /**
     * 教师个人信息卡详情
     *
     * @return
     */
    TWorkRoomTeacherInfo wechatMiniWorkRoomTeaCardDetails(Long id);

    /**
     * 微信小程序后台发送消息给用户客服界面
     *
     * @param workRoomName
     * @return
     */
    String wechatMiniSendCustomerService(String workRoomName);

    /**
     * 用户阅读隐私条例
     *
     * @return
     */
    String wechatMiniUserReadPrivacyPolicy();

    /**
     * 学生课程表
     * @param date
     * @return
     */
    List<GetCourse> wechatMiniStudentCourse(String date);

    /**
     * 教师课程表
     * @param date
     * @return
     */
    List<GetCourse> wechatMiniTeacherCourse(String date);

    /**
     * 获取访问数
     * @return
     */
    String wechatMiniGetVisitNum();


    /**
     * 用户访问
     * @return
     */
    String wechatMiniUserVisit();

    /**
     * 小程序全局搜索
     * @param search
     * @return
     */
    List<GlobalSearchVo> wechatMiniGlobalSearch(String search, Long startCount, Long endCount);

    void logout();

    List<TArticle> wechatMiniSubjectArticleList1(long l, long pageType);

    List<TArticleTwoType> wechatMiniArticleTypeList();

    List<TArticleSlideshow> selectArticleTSlideshowList();

}
