package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TBbsReport;
import com.ruoyi.system.mapper.TBbsReportMapper;
import com.ruoyi.system.service.ITBbsReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 举报Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-29
 */
@Service
public class TBbsReportServiceImpl implements ITBbsReportService {
    @Autowired
    private TBbsReportMapper tBbsReportMapper;

    /**
     * 查询举报
     *
     * @param id 举报主键
     * @return 举报
     */
    @Override
    public TBbsReport selectTBbsReportById(Long id) {
        return tBbsReportMapper.selectTBbsReportById(id);
    }

    /**
     * 查询举报列表
     *
     * @param tBbsReport 举报
     * @return 举报
     */
    @Override
    public List<TBbsReport> selectTBbsReportList(TBbsReport tBbsReport) {
        return tBbsReportMapper.selectTBbsReportList(tBbsReport);
    }

    /**
     * 新增举报
     *
     * @param tBbsReport 举报
     * @return 结果
     */
    @Override
    public int insertTBbsReport(TBbsReport tBbsReport) {
        return tBbsReportMapper.insertTBbsReport(tBbsReport);
    }

    /**
     * 修改举报
     *
     * @param tBbsReport 举报
     * @return 结果
     */
    @Override
    public int updateTBbsReport(TBbsReport tBbsReport) {
        return tBbsReportMapper.updateTBbsReport(tBbsReport);
    }

    /**
     * 批量删除举报
     *
     * @param ids 需要删除的举报主键
     * @return 结果
     */
    @Override
    public int deleteTBbsReportByIds(Long[] ids) {
        return tBbsReportMapper.deleteTBbsReportByIds(ids);
    }

    /**
     * 删除举报信息
     *
     * @param id 举报主键
     * @return 结果
     */
    @Override
    public int deleteTBbsReportById(Long id) {
        return tBbsReportMapper.deleteTBbsReportById(id);
    }
}
