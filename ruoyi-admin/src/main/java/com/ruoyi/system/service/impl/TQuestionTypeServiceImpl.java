package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TQuestionTypeMapper;
import com.ruoyi.system.domain.TQuestionType;
import com.ruoyi.system.service.ITQuestionTypeService;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-19
 */
@Service
public class TQuestionTypeServiceImpl implements ITQuestionTypeService 
{
    @Autowired
    private TQuestionTypeMapper tQuestionTypeMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public TQuestionType selectTQuestionTypeById(Long id)
    {
        return tQuestionTypeMapper.selectTQuestionTypeById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param tQuestionType 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<TQuestionType> selectTQuestionTypeList(TQuestionType tQuestionType)
    {
        return tQuestionTypeMapper.selectTQuestionTypeList(tQuestionType);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param tQuestionType 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertTQuestionType(TQuestionType tQuestionType)
    {
        return tQuestionTypeMapper.insertTQuestionType(tQuestionType);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param tQuestionType 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateTQuestionType(TQuestionType tQuestionType)
    {
        return tQuestionTypeMapper.updateTQuestionType(tQuestionType);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteTQuestionTypeByIds(Long[] ids)
    {
        return tQuestionTypeMapper.deleteTQuestionTypeByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteTQuestionTypeById(Long id)
    {
        return tQuestionTypeMapper.deleteTQuestionTypeById(id);
    }
}
