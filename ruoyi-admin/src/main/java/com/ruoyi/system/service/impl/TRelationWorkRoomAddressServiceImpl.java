package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TRelationWorkRoomAddress;
import com.ruoyi.system.mapper.TRelationWorkRoomAddressMapper;
import com.ruoyi.system.service.ITRelationWorkRoomAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 工作室与地区关系Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-21
 */
@Service
public class TRelationWorkRoomAddressServiceImpl implements ITRelationWorkRoomAddressService {
    @Autowired
    private TRelationWorkRoomAddressMapper tRelationWorkRoomAddressMapper;

    /**
     * 查询工作室与地区关系
     *
     * @param id 工作室与地区关系主键
     * @return 工作室与地区关系
     */
    @Override
    public TRelationWorkRoomAddress selectTRelationWorkRoomAddressById(Long id) {
        return tRelationWorkRoomAddressMapper.selectTRelationWorkRoomAddressById(id);
    }

    /**
     * 查询工作室与地区关系列表
     *
     * @param tRelationWorkRoomAddress 工作室与地区关系
     * @return 工作室与地区关系
     */
    @Override
    public List<TRelationWorkRoomAddress> selectTRelationWorkRoomAddressList(TRelationWorkRoomAddress tRelationWorkRoomAddress) {
        return tRelationWorkRoomAddressMapper.selectTRelationWorkRoomAddressList(tRelationWorkRoomAddress);
    }

    /**
     * 新增工作室与地区关系
     *
     * @param tRelationWorkRoomAddress 工作室与地区关系
     * @return 结果
     */
    @Override
    public int insertTRelationWorkRoomAddress(TRelationWorkRoomAddress tRelationWorkRoomAddress) {
        return tRelationWorkRoomAddressMapper.insertTRelationWorkRoomAddress(tRelationWorkRoomAddress);
    }

    /**
     * 修改工作室与地区关系
     *
     * @param tRelationWorkRoomAddress 工作室与地区关系
     * @return 结果
     */
    @Override
    public int updateTRelationWorkRoomAddress(TRelationWorkRoomAddress tRelationWorkRoomAddress) {
        return tRelationWorkRoomAddressMapper.updateTRelationWorkRoomAddress(tRelationWorkRoomAddress);
    }

    /**
     * 批量删除工作室与地区关系
     *
     * @param ids 需要删除的工作室与地区关系主键
     * @return 结果
     */
    @Override
    public int deleteTRelationWorkRoomAddressByIds(Long[] ids) {
        return tRelationWorkRoomAddressMapper.deleteTRelationWorkRoomAddressByIds(ids);
    }

    /**
     * 删除工作室与地区关系信息
     *
     * @param id 工作室与地区关系主键
     * @return 结果
     */
    @Override
    public int deleteTRelationWorkRoomAddressById(Long id) {
        return tRelationWorkRoomAddressMapper.deleteTRelationWorkRoomAddressById(id);
    }
}
