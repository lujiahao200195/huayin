package com.ruoyi.system.service.backend;

import com.ruoyi.system.domain.TArticle;
import com.ruoyi.system.vo.wechat.backend.ExportSchoolArticle;

import java.util.List;

/**
 * 文章Service接口
 */
public interface ArticleService {
    /**
     * 查询文章
     *
     * @param id 文章主键
     * @return 文章
     */
    public TArticle selectTArticleById(Long id);

    /**
     * 查询文章列表
     *
     * @param tArticle 文章
     * @return 文章集合
     */
    public List<TArticle> selectTArticleList(TArticle tArticle);

    /**
     * 导出学校文章
     * @param tArticle
     * @return
     */
    public List<ExportSchoolArticle> exportSchoolArticle(Long[] idse);

    /**
     * 新增文章
     *
     * @param tArticle 文章
     * @return 结果
     */
    public int insertTArticle(TArticle tArticle);

    /**
     * 修改文章
     *
     * @param tArticle 文章
     * @return 结果
     */
    public int updateTArticle(TArticle tArticle);

    /**
     * 批量删除文章
     *
     * @param ids 需要删除的文章主键集合
     * @return 结果
     */
    public int deleteTArticleByIds(Long[] ids);

    /**
     * 删除文章信息
     *
     * @param id 文章主键
     * @return 结果
     */
    public int deleteTArticleById(Long id);

    /**
     * 查询学校咨询文章
     *
     * @return
     */
    public List<TArticle> getSchoolArticle(TArticle article);



    /**
     * 添加学校咨询文章
     *
     * @param tArticle
     * @return
     */
    String addSchoolArticle(TArticle tArticle);

    /**
     * 删除文章和关系表
     *
     * @param ids
     */
    String removeSchoolArticle(Long[] ids);

}
