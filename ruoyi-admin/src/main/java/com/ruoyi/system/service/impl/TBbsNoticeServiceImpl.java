package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TBbsNotice;
import com.ruoyi.system.mapper.TBbsNoticeMapper;
import com.ruoyi.system.service.ITBbsNoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 版块公告Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-28
 */
@Service
public class TBbsNoticeServiceImpl implements ITBbsNoticeService {
    @Autowired
    private TBbsNoticeMapper tBbsNoticeMapper;

    /**
     * 查询版块公告
     *
     * @param id 版块公告主键
     * @return 版块公告
     */
    @Override
    public TBbsNotice selectTBbsNoticeById(Long id) {
        return tBbsNoticeMapper.selectTBbsNoticeById(id);
    }

    /**
     * 查询版块公告列表
     *
     * @param tBbsNotice 版块公告
     * @return 版块公告
     */
    @Override
    public List<TBbsNotice> selectTBbsNoticeList(TBbsNotice tBbsNotice) {
        return tBbsNoticeMapper.selectTBbsNoticeList(tBbsNotice);
    }

    /**
     * 新增版块公告
     *
     * @param tBbsNotice 版块公告
     * @return 结果
     */
    @Override
    public int insertTBbsNotice(TBbsNotice tBbsNotice) {
        return tBbsNoticeMapper.insertTBbsNotice(tBbsNotice);
    }

    /**
     * 修改版块公告
     *
     * @param tBbsNotice 版块公告
     * @return 结果
     */
    @Override
    public int updateTBbsNotice(TBbsNotice tBbsNotice) {
        return tBbsNoticeMapper.updateTBbsNotice(tBbsNotice);
    }

    /**
     * 批量删除版块公告
     *
     * @param ids 需要删除的版块公告主键
     * @return 结果
     */
    @Override
    public int deleteTBbsNoticeByIds(Long[] ids) {
        return tBbsNoticeMapper.deleteTBbsNoticeByIds(ids);
    }

    /**
     * 删除版块公告信息
     *
     * @param id 版块公告主键
     * @return 结果
     */
    @Override
    public int deleteTBbsNoticeById(Long id) {
        return tBbsNoticeMapper.deleteTBbsNoticeById(id);
    }
}
