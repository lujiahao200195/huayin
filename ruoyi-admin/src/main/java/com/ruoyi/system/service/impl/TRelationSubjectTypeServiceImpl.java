package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TRelationSubjectType;
import com.ruoyi.system.mapper.TRelationSubjectTypeMapper;
import com.ruoyi.system.service.ITRelationSubjectTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 科目和分类的关系Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-17
 */
@Service
public class TRelationSubjectTypeServiceImpl implements ITRelationSubjectTypeService {
    @Autowired
    private TRelationSubjectTypeMapper tRelationSubjectTypeMapper;

    /**
     * 查询科目和分类的关系
     *
     * @param id 科目和分类的关系主键
     * @return 科目和分类的关系
     */
    @Override
    public TRelationSubjectType selectTRelationSubjectTypeById(Long id) {
        return tRelationSubjectTypeMapper.selectTRelationSubjectTypeById(id);
    }

    /**
     * 查询科目和分类的关系列表
     *
     * @param tRelationSubjectType 科目和分类的关系
     * @return 科目和分类的关系
     */
    @Override
    public List<TRelationSubjectType> selectTRelationSubjectTypeList(TRelationSubjectType tRelationSubjectType) {
        return tRelationSubjectTypeMapper.selectTRelationSubjectTypeList(tRelationSubjectType);
    }

    /**
     * 新增科目和分类的关系
     *
     * @param tRelationSubjectType 科目和分类的关系
     * @return 结果
     */
    @Override
    public int insertTRelationSubjectType(TRelationSubjectType tRelationSubjectType) {
        return tRelationSubjectTypeMapper.insertTRelationSubjectType(tRelationSubjectType);
    }

    /**
     * 修改科目和分类的关系
     *
     * @param tRelationSubjectType 科目和分类的关系
     * @return 结果
     */
    @Override
    public int updateTRelationSubjectType(TRelationSubjectType tRelationSubjectType) {
        return tRelationSubjectTypeMapper.updateTRelationSubjectType(tRelationSubjectType);
    }

    /**
     * 批量删除科目和分类的关系
     *
     * @param ids 需要删除的科目和分类的关系主键
     * @return 结果
     */
    @Override
    public int deleteTRelationSubjectTypeByIds(Long[] ids) {
        return tRelationSubjectTypeMapper.deleteTRelationSubjectTypeByIds(ids);
    }

    /**
     * 删除科目和分类的关系信息
     *
     * @param id 科目和分类的关系主键
     * @return 结果
     */
    @Override
    public int deleteTRelationSubjectTypeById(Long id) {
        return tRelationSubjectTypeMapper.deleteTRelationSubjectTypeById(id);
    }
}
