package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.TCourseSubjectRelation;

/**
 * 课程和科目关系Service接口
 * 
 * @author ruoyi
 * @date 2023-09-15
 */
public interface ITCourseSubjectRelationService 
{
    /**
     * 查询课程和科目关系
     * 
     * @param id 课程和科目关系主键
     * @return 课程和科目关系
     */
    public TCourseSubjectRelation selectTCourseSubjectRelationById(Long id);

    /**
     * 查询课程和科目关系列表
     * 
     * @param tCourseSubjectRelation 课程和科目关系
     * @return 课程和科目关系集合
     */
    public List<TCourseSubjectRelation> selectTCourseSubjectRelationList(TCourseSubjectRelation tCourseSubjectRelation);

    /**
     * 新增课程和科目关系
     * 
     * @param tCourseSubjectRelation 课程和科目关系
     * @return 结果
     */
    public int insertTCourseSubjectRelation(TCourseSubjectRelation tCourseSubjectRelation);

    /**
     * 修改课程和科目关系
     * 
     * @param tCourseSubjectRelation 课程和科目关系
     * @return 结果
     */
    public int updateTCourseSubjectRelation(TCourseSubjectRelation tCourseSubjectRelation);

    /**
     * 批量删除课程和科目关系
     * 
     * @param ids 需要删除的课程和科目关系主键集合
     * @return 结果
     */
    public int deleteTCourseSubjectRelationByIds(Long[] ids);

    /**
     * 删除课程和科目关系信息
     * 
     * @param id 课程和科目关系主键
     * @return 结果
     */
    public int deleteTCourseSubjectRelationById(Long id);
}
