package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.TCourse;

/**
 * 课程安排Service接口
 * 
 * @author ruoyi
 * @date 2023-09-15
 */
public interface ITCourseService 
{
    /**
     * 查询课程安排
     * 
     * @param id 课程安排主键
     * @return 课程安排
     */
    public TCourse selectTCourseById(Long id);

    /**
     * 查询课程安排列表
     * 
     * @param tCourse 课程安排
     * @return 课程安排集合
     */
    public List<TCourse> selectTCourseList(TCourse tCourse);

    /**
     * 新增课程安排
     * 
     * @param tCourse 课程安排
     * @return 结果
     */
    public int insertTCourse(TCourse tCourse);

    /**
     * 修改课程安排
     * 
     * @param tCourse 课程安排
     * @return 结果
     */
    public int updateTCourse(TCourse tCourse);

    /**
     * 批量删除课程安排
     * 
     * @param ids 需要删除的课程安排主键集合
     * @return 结果
     */
    public int deleteTCourseByIds(Long[] ids);

    /**
     * 删除课程安排信息
     * 
     * @param id 课程安排主键
     * @return 结果
     */
    public int deleteTCourseById(Long id);
}
