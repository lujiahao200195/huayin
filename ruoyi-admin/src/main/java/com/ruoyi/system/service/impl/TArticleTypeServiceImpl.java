package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TArticleType;
import com.ruoyi.system.mapper.TArticleTypeMapper;
import com.ruoyi.system.service.ITArticleTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 文章分类Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-15
 */
@Service
public class TArticleTypeServiceImpl implements ITArticleTypeService {
    @Autowired
    private TArticleTypeMapper tArticleTypeMapper;

    /**
     * 查询文章分类
     *
     * @param id 文章分类主键
     * @return 文章分类
     */
    @Override
    public TArticleType selectTArticleTypeById(Long id) {
        return tArticleTypeMapper.selectTArticleTypeById(id);
    }

    /**
     * 查询文章分类列表
     *
     * @param tArticleType 文章分类
     * @return 文章分类
     */
    @Override
    public List<TArticleType> selectTArticleTypeList(TArticleType tArticleType) {
        return tArticleTypeMapper.selectTArticleTypeList(tArticleType);
    }

    /**
     * 新增文章分类
     *
     * @param tArticleType 文章分类
     * @return 结果
     */
    @Override
    public int insertTArticleType(TArticleType tArticleType) {
        return tArticleTypeMapper.insertTArticleType(tArticleType);
    }

    /**
     * 修改文章分类
     *
     * @param tArticleType 文章分类
     * @return 结果
     */
    @Override
    public int updateTArticleType(TArticleType tArticleType) {
        return tArticleTypeMapper.updateTArticleType(tArticleType);
    }

    /**
     * 批量删除文章分类
     *
     * @param ids 需要删除的文章分类主键
     * @return 结果
     */
    @Override
    public int deleteTArticleTypeByIds(Long[] ids) {
        return tArticleTypeMapper.deleteTArticleTypeByIds(ids);
    }

    /**
     * 删除文章分类信息
     *
     * @param id 文章分类主键
     * @return 结果
     */
    @Override
    public int deleteTArticleTypeById(Long id) {
        return tArticleTypeMapper.deleteTArticleTypeById(id);
    }
}
