package com.ruoyi.system.service;

import com.ruoyi.system.domain.TBbsBoardTag;

import java.util.List;

/**
 * 版块标签Service接口
 *
 * @author ruoyi
 * @date 2023-08-29
 */
public interface ITBbsBoardTagService {
    /**
     * 查询版块标签
     *
     * @param id 版块标签主键
     * @return 版块标签
     */
    public TBbsBoardTag selectTBbsBoardTagById(Long id);

    /**
     * 查询版块标签列表
     *
     * @param tBbsBoardTag 版块标签
     * @return 版块标签集合
     */
    public List<TBbsBoardTag> selectTBbsBoardTagList(TBbsBoardTag tBbsBoardTag);

    /**
     * 新增版块标签
     *
     * @param tBbsBoardTag 版块标签
     * @return 结果
     */
    public int insertTBbsBoardTag(TBbsBoardTag tBbsBoardTag);

    /**
     * 修改版块标签
     *
     * @param tBbsBoardTag 版块标签
     * @return 结果
     */
    public int updateTBbsBoardTag(TBbsBoardTag tBbsBoardTag);

    /**
     * 批量删除版块标签
     *
     * @param ids 需要删除的版块标签主键集合
     * @return 结果
     */
    public int deleteTBbsBoardTagByIds(Long[] ids);

    /**
     * 删除版块标签信息
     *
     * @param id 版块标签主键
     * @return 结果
     */
    public int deleteTBbsBoardTagById(Long id);
}
