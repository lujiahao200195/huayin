package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TResourceRelation;
import com.ruoyi.system.mapper.TResourceRelationMapper;
import com.ruoyi.system.service.ITResourceRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 资源关系Service业务层处理
 *
 * @author ruoyi
 * @date 2023-09-11
 */
@Service
public class TResourceRelationServiceImpl implements ITResourceRelationService {
    @Autowired
    private TResourceRelationMapper tResourceRelationMapper;

    /**
     * 查询资源关系
     *
     * @param id 资源关系主键
     * @return 资源关系
     */
    @Override
    public TResourceRelation selectTResourceRelationById(Long id) {
        return tResourceRelationMapper.selectTResourceRelationById(id);
    }

    /**
     * 查询资源关系列表
     *
     * @param tResourceRelation 资源关系
     * @return 资源关系
     */
    @Override
    public List<TResourceRelation> selectTResourceRelationList(TResourceRelation tResourceRelation) {
        return tResourceRelationMapper.selectTResourceRelationList(tResourceRelation);
    }

    /**
     * 新增资源关系
     *
     * @param tResourceRelation 资源关系
     * @return 结果
     */
    @Override
    public int insertTResourceRelation(TResourceRelation tResourceRelation) {
        return tResourceRelationMapper.insertTResourceRelation(tResourceRelation);
    }

    /**
     * 修改资源关系
     *
     * @param tResourceRelation 资源关系
     * @return 结果
     */
    @Override
    public int updateTResourceRelation(TResourceRelation tResourceRelation) {
        return tResourceRelationMapper.updateTResourceRelation(tResourceRelation);
    }

    /**
     * 批量删除资源关系
     *
     * @param ids 需要删除的资源关系主键
     * @return 结果
     */
    @Override
    public int deleteTResourceRelationByIds(Long[] ids) {
        return tResourceRelationMapper.deleteTResourceRelationByIds(ids);
    }

    /**
     * 删除资源关系信息
     *
     * @param id 资源关系主键
     * @return 结果
     */
    @Override
    public int deleteTResourceRelationById(Long id) {
        return tResourceRelationMapper.deleteTResourceRelationById(id);
    }
}
