package com.ruoyi.system.service;

import com.ruoyi.system.domain.TResourceRelation;

import java.util.List;

/**
 * 资源关系Service接口
 *
 * @author ruoyi
 * @date 2023-09-11
 */
public interface ITResourceRelationService {
    /**
     * 查询资源关系
     *
     * @param id 资源关系主键
     * @return 资源关系
     */
    public TResourceRelation selectTResourceRelationById(Long id);

    /**
     * 查询资源关系列表
     *
     * @param tResourceRelation 资源关系
     * @return 资源关系集合
     */
    public List<TResourceRelation> selectTResourceRelationList(TResourceRelation tResourceRelation);

    /**
     * 新增资源关系
     *
     * @param tResourceRelation 资源关系
     * @return 结果
     */
    public int insertTResourceRelation(TResourceRelation tResourceRelation);

    /**
     * 修改资源关系
     *
     * @param tResourceRelation 资源关系
     * @return 结果
     */
    public int updateTResourceRelation(TResourceRelation tResourceRelation);

    /**
     * 批量删除资源关系
     *
     * @param ids 需要删除的资源关系主键集合
     * @return 结果
     */
    public int deleteTResourceRelationByIds(Long[] ids);

    /**
     * 删除资源关系信息
     *
     * @param id 资源关系主键
     * @return 结果
     */
    public int deleteTResourceRelationById(Long id);
}
