package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TWorkRoomMessage;
import com.ruoyi.system.mapper.TWorkRoomMessageMapper;
import com.ruoyi.system.service.ITWorkRoomMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 工作室消息Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-23
 */
@Service
public class TWorkRoomMessageServiceImpl implements ITWorkRoomMessageService {
    @Autowired
    private TWorkRoomMessageMapper tWorkRoomMessageMapper;

    /**
     * 查询工作室消息
     *
     * @param id 工作室消息主键
     * @return 工作室消息
     */
    @Override
    public TWorkRoomMessage selectTWorkRoomMessageById(Long id) {
        return tWorkRoomMessageMapper.selectTWorkRoomMessageById(id);
    }

    /**
     * 查询工作室消息列表
     *
     * @param tWorkRoomMessage 工作室消息
     * @return 工作室消息
     */
    @Override
    public List<TWorkRoomMessage> selectTWorkRoomMessageList(TWorkRoomMessage tWorkRoomMessage) {
        return tWorkRoomMessageMapper.selectTWorkRoomMessageList(tWorkRoomMessage);
    }

    /**
     * 新增工作室消息
     *
     * @param tWorkRoomMessage 工作室消息
     * @return 结果
     */
    @Override
    public int insertTWorkRoomMessage(TWorkRoomMessage tWorkRoomMessage) {
        return tWorkRoomMessageMapper.insertTWorkRoomMessage(tWorkRoomMessage);
    }

    /**
     * 修改工作室消息
     *
     * @param tWorkRoomMessage 工作室消息
     * @return 结果
     */
    @Override
    public int updateTWorkRoomMessage(TWorkRoomMessage tWorkRoomMessage) {
        return tWorkRoomMessageMapper.updateTWorkRoomMessage(tWorkRoomMessage);
    }

    /**
     * 批量删除工作室消息
     *
     * @param ids 需要删除的工作室消息主键
     * @return 结果
     */
    @Override
    public int deleteTWorkRoomMessageByIds(Long[] ids) {
        return tWorkRoomMessageMapper.deleteTWorkRoomMessageByIds(ids);
    }

    /**
     * 删除工作室消息信息
     *
     * @param id 工作室消息主键
     * @return 结果
     */
    @Override
    public int deleteTWorkRoomMessageById(Long id) {
        return tWorkRoomMessageMapper.deleteTWorkRoomMessageById(id);
    }
}
