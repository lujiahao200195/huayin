package com.ruoyi.system.service;

import com.ruoyi.system.domain.TPhaseOfStudying;

import java.util.List;

/**
 * 学段Service接口
 *
 * @author ruoyi
 * @date 2023-08-15
 */
public interface ITPhaseOfStudyingService {
    /**
     * 查询学段
     *
     * @param id 学段主键
     * @return 学段
     */
    public TPhaseOfStudying selectTPhaseOfStudyingById(Long id);

    /**
     * 查询学段列表
     *
     * @param tPhaseOfStudying 学段
     * @return 学段集合
     */
    public List<TPhaseOfStudying> selectTPhaseOfStudyingList(TPhaseOfStudying tPhaseOfStudying);

    /**
     * 新增学段
     *
     * @param tPhaseOfStudying 学段
     * @return 结果
     */
    public int insertTPhaseOfStudying(TPhaseOfStudying tPhaseOfStudying);

    /**
     * 修改学段
     *
     * @param tPhaseOfStudying 学段
     * @return 结果
     */
    public int updateTPhaseOfStudying(TPhaseOfStudying tPhaseOfStudying);

    /**
     * 批量删除学段
     *
     * @param ids 需要删除的学段主键集合
     * @return 结果
     */
    public int deleteTPhaseOfStudyingByIds(Long[] ids);

    /**
     * 删除学段信息
     *
     * @param id 学段主键
     * @return 结果
     */
    public int deleteTPhaseOfStudyingById(Long id);
}
