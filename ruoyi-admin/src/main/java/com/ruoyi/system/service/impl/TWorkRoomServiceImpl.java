package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TWorkRoom;
import com.ruoyi.system.mapper.TWorkRoomMapper;
import com.ruoyi.system.service.ITWorkRoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 工作室Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-22
 */
@Service
public class TWorkRoomServiceImpl implements ITWorkRoomService {
    @Autowired
    private TWorkRoomMapper tWorkRoomMapper;

    /**
     * 查询工作室
     *
     * @param id 工作室主键
     * @return 工作室
     */
    @Override
    public TWorkRoom selectTWorkRoomById(Long id) {
        return tWorkRoomMapper.selectTWorkRoomById(id);
    }

    /**
     * 查询工作室列表
     *
     * @param tWorkRoom 工作室
     * @return 工作室
     */
    @Override
    public List<TWorkRoom> selectTWorkRoomList(TWorkRoom tWorkRoom) {
        return tWorkRoomMapper.selectTWorkRoomList(tWorkRoom);
    }

    /**
     * 新增工作室
     *
     * @param tWorkRoom 工作室
     * @return 结果
     */
    @Override
    public int insertTWorkRoom(TWorkRoom tWorkRoom) {
        return tWorkRoomMapper.insertTWorkRoom(tWorkRoom);
    }

    /**
     * 修改工作室
     *
     * @param tWorkRoom 工作室
     * @return 结果
     */
    @Override
    public int updateTWorkRoom(TWorkRoom tWorkRoom) {
        return tWorkRoomMapper.updateTWorkRoom(tWorkRoom);
    }

    /**
     * 批量删除工作室
     *
     * @param ids 需要删除的工作室主键
     * @return 结果
     */
    @Override
    public int deleteTWorkRoomByIds(Long[] ids) {
        return tWorkRoomMapper.deleteTWorkRoomByIds(ids);
    }

    /**
     * 删除工作室信息
     *
     * @param id 工作室主键
     * @return 结果
     */
    @Override
    public int deleteTWorkRoomById(Long id) {
        return tWorkRoomMapper.deleteTWorkRoomById(id);
    }
}
