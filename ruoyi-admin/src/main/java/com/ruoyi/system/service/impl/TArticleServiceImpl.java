package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TArticle;
import com.ruoyi.system.mapper.TArticleMapper;
import com.ruoyi.system.service.ITArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 文章Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-17
 */
@Service
public class TArticleServiceImpl implements ITArticleService {
    @Autowired
    private TArticleMapper tArticleMapper;

    /**
     * 查询文章
     *
     * @param id 文章主键
     * @return 文章
     */
    @Override
    public TArticle selectTArticleById(Long id) {
        return tArticleMapper.selectTArticleById(id);
    }

    /**
     * 查询文章列表
     *
     * @param tArticle 文章
     * @return 文章
     */
    @Override
    public List<TArticle> selectTArticleList(TArticle tArticle) {
        return tArticleMapper.selectTArticleList(tArticle);
    }

    /**
     * 新增文章
     *
     * @param tArticle 文章
     * @return 结果
     */
    @Override
    public int insertTArticle(TArticle tArticle) {
        return tArticleMapper.insertTArticle(tArticle);
    }

    /**
     * 修改文章
     *
     * @param tArticle 文章
     * @return 结果
     */
    @Override
    public int updateTArticle(TArticle tArticle) {
        return tArticleMapper.updateTArticle(tArticle);
    }

    /**
     * 批量删除文章
     *
     * @param ids 需要删除的文章主键
     * @return 结果
     */
    @Override
    public int deleteTArticleByIds(Long[] ids) {
        return tArticleMapper.deleteTArticleByIds(ids);
    }

    /**
     * 删除文章信息
     *
     * @param id 文章主键
     * @return 结果
     */
    @Override
    public int deleteTArticleById(Long id) {
        return tArticleMapper.deleteTArticleById(id);
    }
}
