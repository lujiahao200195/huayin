package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.TQuestionType;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2024-04-19
 */
public interface ITQuestionTypeService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public TQuestionType selectTQuestionTypeById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param tQuestionType 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<TQuestionType> selectTQuestionTypeList(TQuestionType tQuestionType);

    /**
     * 新增【请填写功能名称】
     * 
     * @param tQuestionType 【请填写功能名称】
     * @return 结果
     */
    public int insertTQuestionType(TQuestionType tQuestionType);

    /**
     * 修改【请填写功能名称】
     * 
     * @param tQuestionType 【请填写功能名称】
     * @return 结果
     */
    public int updateTQuestionType(TQuestionType tQuestionType);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteTQuestionTypeByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteTQuestionTypeById(Long id);
}
