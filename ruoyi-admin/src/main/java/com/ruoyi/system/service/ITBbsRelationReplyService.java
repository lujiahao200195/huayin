package com.ruoyi.system.service;

import com.ruoyi.system.domain.TBbsRelationReply;

import java.util.List;

/**
 * 回复关系Service接口
 *
 * @author ruoyi
 * @date 2023-08-31
 */
public interface ITBbsRelationReplyService {
    /**
     * 查询回复关系
     *
     * @param id 回复关系主键
     * @return 回复关系
     */
    public TBbsRelationReply selectTBbsRelationReplyById(Long id);

    /**
     * 查询回复关系列表
     *
     * @param tBbsRelationReply 回复关系
     * @return 回复关系集合
     */
    public List<TBbsRelationReply> selectTBbsRelationReplyList(TBbsRelationReply tBbsRelationReply);

    /**
     * 新增回复关系
     *
     * @param tBbsRelationReply 回复关系
     * @return 结果
     */
    public int insertTBbsRelationReply(TBbsRelationReply tBbsRelationReply);

    /**
     * 修改回复关系
     *
     * @param tBbsRelationReply 回复关系
     * @return 结果
     */
    public int updateTBbsRelationReply(TBbsRelationReply tBbsRelationReply);

    /**
     * 批量删除回复关系
     *
     * @param ids 需要删除的回复关系主键集合
     * @return 结果
     */
    public int deleteTBbsRelationReplyByIds(Long[] ids);

    /**
     * 删除回复关系信息
     *
     * @param id 回复关系主键
     * @return 结果
     */
    public int deleteTBbsRelationReplyById(Long id);
}
