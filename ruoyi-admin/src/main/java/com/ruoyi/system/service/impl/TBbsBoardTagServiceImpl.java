package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TBbsBoardTag;
import com.ruoyi.system.mapper.TBbsBoardTagMapper;
import com.ruoyi.system.service.ITBbsBoardTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 版块标签Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-29
 */
@Service
public class TBbsBoardTagServiceImpl implements ITBbsBoardTagService {
    @Autowired
    private TBbsBoardTagMapper tBbsBoardTagMapper;

    /**
     * 查询版块标签
     *
     * @param id 版块标签主键
     * @return 版块标签
     */
    @Override
    public TBbsBoardTag selectTBbsBoardTagById(Long id) {
        return tBbsBoardTagMapper.selectTBbsBoardTagById(id);
    }

    /**
     * 查询版块标签列表
     *
     * @param tBbsBoardTag 版块标签
     * @return 版块标签
     */
    @Override
    public List<TBbsBoardTag> selectTBbsBoardTagList(TBbsBoardTag tBbsBoardTag) {
        return tBbsBoardTagMapper.selectTBbsBoardTagList(tBbsBoardTag);
    }

    /**
     * 新增版块标签
     *
     * @param tBbsBoardTag 版块标签
     * @return 结果
     */
    @Override
    public int insertTBbsBoardTag(TBbsBoardTag tBbsBoardTag) {
        return tBbsBoardTagMapper.insertTBbsBoardTag(tBbsBoardTag);
    }

    /**
     * 修改版块标签
     *
     * @param tBbsBoardTag 版块标签
     * @return 结果
     */
    @Override
    public int updateTBbsBoardTag(TBbsBoardTag tBbsBoardTag) {
        return tBbsBoardTagMapper.updateTBbsBoardTag(tBbsBoardTag);
    }

    /**
     * 批量删除版块标签
     *
     * @param ids 需要删除的版块标签主键
     * @return 结果
     */
    @Override
    public int deleteTBbsBoardTagByIds(Long[] ids) {
        return tBbsBoardTagMapper.deleteTBbsBoardTagByIds(ids);
    }

    /**
     * 删除版块标签信息
     *
     * @param id 版块标签主键
     * @return 结果
     */
    @Override
    public int deleteTBbsBoardTagById(Long id) {
        return tBbsBoardTagMapper.deleteTBbsBoardTagById(id);
    }
}
