package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TWechatMiniLog;
import com.ruoyi.system.mapper.TWechatMiniLogMapper;
import com.ruoyi.system.service.ITWechatMiniLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 微信小程序日志Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-18
 */
@Service
public class TWechatMiniLogServiceImpl implements ITWechatMiniLogService {
    @Autowired
    private TWechatMiniLogMapper tWechatMiniLogMapper;

    /**
     * 查询微信小程序日志
     *
     * @param id 微信小程序日志主键
     * @return 微信小程序日志
     */
    @Override
    public TWechatMiniLog selectTWechatMiniLogById(Long id) {
        return tWechatMiniLogMapper.selectTWechatMiniLogById(id);
    }

    /**
     * 查询微信小程序日志列表
     *
     * @param tWechatMiniLog 微信小程序日志
     * @return 微信小程序日志
     */
    @Override
    public List<TWechatMiniLog> selectTWechatMiniLogList(TWechatMiniLog tWechatMiniLog) {
        return tWechatMiniLogMapper.selectTWechatMiniLogList(tWechatMiniLog);
    }

    /**
     * 新增微信小程序日志
     *
     * @param tWechatMiniLog 微信小程序日志
     * @return 结果
     */
    @Override
    public int insertTWechatMiniLog(TWechatMiniLog tWechatMiniLog) {
        return tWechatMiniLogMapper.insertTWechatMiniLog(tWechatMiniLog);
    }

    /**
     * 修改微信小程序日志
     *
     * @param tWechatMiniLog 微信小程序日志
     * @return 结果
     */
    @Override
    public int updateTWechatMiniLog(TWechatMiniLog tWechatMiniLog) {
        return tWechatMiniLogMapper.updateTWechatMiniLog(tWechatMiniLog);
    }

    /**
     * 批量删除微信小程序日志
     *
     * @param ids 需要删除的微信小程序日志主键
     * @return 结果
     */
    @Override
    public int deleteTWechatMiniLogByIds(Long[] ids) {
        return tWechatMiniLogMapper.deleteTWechatMiniLogByIds(ids);
    }

    /**
     * 删除微信小程序日志信息
     *
     * @param id 微信小程序日志主键
     * @return 结果
     */
    @Override
    public int deleteTWechatMiniLogById(Long id) {
        return tWechatMiniLogMapper.deleteTWechatMiniLogById(id);
    }
}
