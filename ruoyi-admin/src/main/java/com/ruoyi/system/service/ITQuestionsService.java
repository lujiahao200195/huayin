package com.ruoyi.system.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.system.bo.wechat.TQuestionsVo;
import com.ruoyi.system.domain.TQuestions;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2024-04-19
 */
public interface ITQuestionsService 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public TQuestions selectTQuestionsById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param tQuestions 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<TQuestions> selectTQuestionsList(TQuestions tQuestions);

    /**
     * 新增【请填写功能名称】
     * 
     * @param tQuestions 【请填写功能名称】
     * @return 结果
     */
    public int insertTQuestions(TQuestions tQuestions);

    /**
     * 修改【请填写功能名称】
     * 
     * @param tQuestions 【请填写功能名称】
     * @return 结果
     */
    public int updateTQuestions(TQuestions tQuestions);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键集合
     * @return 结果
     */
    public int deleteTQuestionsByIds(Long[] ids);

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteTQuestionsById(Long id);

    List<TQuestionsVo> getQuestions();

    Map submitAnswer(Map<Long,String> answer);
}
