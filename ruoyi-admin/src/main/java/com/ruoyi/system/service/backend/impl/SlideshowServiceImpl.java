package com.ruoyi.system.service.backend.impl;

import com.ruoyi.system.domain.TSlideshow;
import com.ruoyi.system.mapper.backend.SlideshowMapper;
import com.ruoyi.system.service.backend.SlideshowService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 轮播图Service业务层处理
 */
@Service
public class SlideshowServiceImpl implements SlideshowService {
    @Resource
    private SlideshowMapper slideshowMapper;

    /**
     * 查询轮播图
     *
     * @param id 轮播图主键
     * @return 轮播图
     */
    @Override
    public TSlideshow selectTSlideshowById(Long id) {
        return slideshowMapper.selectTSlideshowById(id);
    }

    /**
     * 查询轮播图列表
     *
     * @param tSlideshow 轮播图
     * @return 轮播图
     */
    @Override
    public List<TSlideshow> selectTSlideshowList(TSlideshow tSlideshow) {
        return slideshowMapper.selectTSlideshowList(tSlideshow);
    }

    /**
     * 新增轮播图
     *
     * @param tSlideshow 轮播图
     * @return 结果
     */
    @Override
    public int insertTSlideshow(TSlideshow tSlideshow) {
        return slideshowMapper.insertTSlideshow(tSlideshow);
    }

    /**
     * 修改轮播图
     *
     * @param tSlideshow 轮播图
     * @return 结果
     */
    @Override
    public int updateTSlideshow(TSlideshow tSlideshow) {
        return slideshowMapper.updateTSlideshow(tSlideshow);
    }

    /**
     * 批量删除轮播图
     *
     * @param ids 需要删除的轮播图主键
     * @return 结果
     */
    @Override
    public int deleteTSlideshowByIds(Long[] ids) {
        return slideshowMapper.deleteTSlideshowByIds(ids);
    }

    /**
     * 删除轮播图信息
     *
     * @param id 轮播图主键
     * @return 结果
     */
    @Override
    public int deleteTSlideshowById(Long id) {
        return slideshowMapper.deleteTSlideshowById(id);
    }
}
