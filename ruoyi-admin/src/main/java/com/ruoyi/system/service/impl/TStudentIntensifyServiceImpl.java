package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TStudentIntensify;
import com.ruoyi.system.mapper.TStudentIntensifyMapper;
import com.ruoyi.system.service.ITStudentIntensifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 学生强化科目Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-30
 */
@Service
public class TStudentIntensifyServiceImpl implements ITStudentIntensifyService {
    @Autowired
    private TStudentIntensifyMapper tStudentIntensifyMapper;

    /**
     * 查询学生强化科目
     *
     * @param id 学生强化科目主键
     * @return 学生强化科目
     */
    @Override
    public TStudentIntensify selectTStudentIntensifyById(Long id) {
        return tStudentIntensifyMapper.selectTStudentIntensifyById(id);
    }

    /**
     * 查询学生强化科目列表
     *
     * @param tStudentIntensify 学生强化科目
     * @return 学生强化科目
     */
    @Override
    public List<TStudentIntensify> selectTStudentIntensifyList(TStudentIntensify tStudentIntensify) {
        return tStudentIntensifyMapper.selectTStudentIntensifyList(tStudentIntensify);
    }

    /**
     * 新增学生强化科目
     *
     * @param tStudentIntensify 学生强化科目
     * @return 结果
     */
    @Override
    public int insertTStudentIntensify(TStudentIntensify tStudentIntensify) {
        return tStudentIntensifyMapper.insertTStudentIntensify(tStudentIntensify);
    }

    /**
     * 修改学生强化科目
     *
     * @param tStudentIntensify 学生强化科目
     * @return 结果
     */
    @Override
    public int updateTStudentIntensify(TStudentIntensify tStudentIntensify) {
        return tStudentIntensifyMapper.updateTStudentIntensify(tStudentIntensify);
    }

    /**
     * 批量删除学生强化科目
     *
     * @param ids 需要删除的学生强化科目主键
     * @return 结果
     */
    @Override
    public int deleteTStudentIntensifyByIds(Long[] ids) {
        return tStudentIntensifyMapper.deleteTStudentIntensifyByIds(ids);
    }

    /**
     * 删除学生强化科目信息
     *
     * @param id 学生强化科目主键
     * @return 结果
     */
    @Override
    public int deleteTStudentIntensifyById(Long id) {
        return tStudentIntensifyMapper.deleteTStudentIntensifyById(id);
    }
}
