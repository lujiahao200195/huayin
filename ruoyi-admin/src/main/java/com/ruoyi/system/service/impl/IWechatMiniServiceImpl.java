package com.ruoyi.system.service.impl;

import com.alibaba.fastjson2.JSONObject;
import com.github.pagehelper.PageHelper;
import com.ruoyi.common.utils.uuid.UUID;
import com.ruoyi.system.bo.wechat.StudentSignUp;
import com.ruoyi.system.bo.wechat.*;
import com.ruoyi.system.common.WxReturnedValue;
import com.ruoyi.system.common.abs.wechat.ManagerStudentSignUpData;
import com.ruoyi.system.common.abs.wechat.ManagerTeacherSignUpData;
import com.ruoyi.system.common.abs.wechatmini.MiniAuditData;
import com.ruoyi.system.common.abs.wechatmini.MiniStudentSignUpData;
import com.ruoyi.system.common.abs.wechatmini.MiniTeacherSignUpData;
import com.ruoyi.system.common.config.Custom;
import com.ruoyi.system.common.tools.NTools;
import com.ruoyi.system.common.tools.OkHttp3Tools;
import com.ruoyi.system.common.tools.WxUtil;
import com.ruoyi.system.controller.socket.UserConsultServer;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.mapper.*;
import com.ruoyi.system.service.IWechatMiniService;
import com.ruoyi.system.vo.wechat.*;
import com.ruoyi.system.vo.wechat.SignUpTeacherInfo.Subject;
import okhttp3.Call;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.ibatis.annotations.Result;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Service
public class IWechatMiniServiceImpl implements IWechatMiniService {

    @Resource
    private Custom custom;

    @Resource
    private NTools nTools;

    @Resource
    private WxUtil wxUtil;

    @Resource
    private Custom customConfig;

    @Resource
    private TUserMapper tUserMapper;

    @Resource
    private TStudentMapper tStudentMapper;

    @Resource
    private TStudentIntensifyMapper tStudentIntensifyMapper;

    @Resource
    private WechatMiniMapper wechatMiniMapper;

    @Resource
    private TSubjectMapper tSubjectMapper;

    @Resource
    private TTeacherMapper tTeacherMapper;

    @Resource
    private TTeachingObjectsAndSubjectsMapper tTeachingObjectsAndSubjectsMapper;

    @Resource
    private TTeacherAdvantagesMapper tTeacherAdvantagesMapper;

    @Resource
    private TTeacherScheduleMapper tTeacherScheduleMapper;

    @Resource
    private TRelationTeacherCertificatesMapper tRelationTeacherCertificatesMapper;

    @Resource
    private TWorkRoomMapper tWorkRoomMapper;

    @Resource
    private TRelationWorkRoomSubjectMapper tRelationWorkRoomSubjectMapper;

    @Resource
    private TRelationWorkRoomUserMapper tRelationWorkRoomUserMapper;

    @Resource
    private TWorkRoomInviteMapper tWorkRoomInviteMapper;

    @Resource
    private TWorkRoomTeacherInfoMapper tWorkRoomTeacherInfoMapper;

    @Resource
    private RedisTemplate redisTemplate;

    @Resource
    private TArticleMapper tArticleMapper;

    @Resource
    private TCourseMapper tCourseMapper;

    @Resource
    private TCourseSubjectRelationMapper tCourseSubjectRelationMapper;

    @Resource
    private TConfigMapper configMapper;

    @Resource
    private TArticleTwoTypeMapper tArticleTwoTypeMapper;


    @Override
    public String selectTUserByWechatMiniOpenId(Map<String, String> jsCodeMap) {
        Request request = new Request.Builder()
                .url("https://api.weixin.qq.com/sns/jscode2session?appid=" + customConfig.WechatMiniAppID + "&secret=" + customConfig.WechatMiniSecret + "&js_code=" + jsCodeMap.get("jscode") + "&grant_type=authorization_code")//URL地址
                .get()
                .build();//构建
        Call call = OkHttp3Tools.client.newCall(request);
        //4.执行同步请求，获取响应体Response对象
        JSONObject jsonObject = null;
        try {
            Response response = call.execute();
            // {"session_key":"Vi29L0ytFE3rwoH1owv3Xg==","openid":"o2wmq5L6ExGms20q9oxZPmlPDgqE"}
            jsonObject = OkHttp3Tools.responseToMap(response);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        Object errcode = jsonObject.get("errcode");
        System.out.println(errcode);
        if (errcode != null) {
            throw new RuntimeException("小程序登入错误");
        }
        String token = (String) redisTemplate.opsForValue().get("wxmini:user-info:" + jsonObject.getString("openid"));
        if (token != null) {
            return token;
        }

        TUser tUser = new TUser();
        tUser.setUserWxMiniOpenid(jsonObject.getString("openid"));
        List<TUser> tUsers = tUserMapper.selectTUserList(tUser);
        if (!tUsers.isEmpty() && tUsers.get(0) != null) {
            TUser tUser1 = tUsers.get(0);
            tUser1.setLastLoginIp(nTools.getIp());
            tUser1.setLastLoginTime(new Date());
            tUserMapper.updateTUser(tUser1);
            tUser = tUser1;
//            System.out.println("用户 "+tUser);
        } else {
            // 微信用户第一次登入
            tUser = new TUser();
            tUser.setUserName("默认姓名");
            tUser.setUserWxMiniOpenid(jsonObject.getString("openid"));
            tUser.setUserWxMiniSessionkey(jsonObject.getString("session_key"));
            tUser.setRegisterIp(nTools.getIp());
            tUser.setLastLoginIp(nTools.getIp());
            tUser.setLastLoginTime(new Date());
            tUser.setUserIntro("该用户很懒, 什么都没有写.");
            tUser.setReadPrivacyPolicy("否");
            tUserMapper.insertTUser(tUser);
        }
        // 构建新的
        LoginVo loginVo = new LoginVo();
        loginVo.setId(tUser.getId());
        loginVo.setUserName(tUser.getUserName());
        loginVo.setUserGender(tUser.getUserGender());
        loginVo.setUserPhone(tUser.getUserPhone());
        loginVo.setUserEmail(tUser.getUserEmail());
        Date userBirth = tUser.getUserBirth();
        if (userBirth != null) {
            loginVo.setUserBirth(NTools.createBirthDay(userBirth));
        }
        loginVo.setUserPhoto(tUser.getUserPhoto());
        loginVo.setWechatMiniOpenId(tUser.getUserWxMiniOpenid());
        loginVo.setUserIntro(tUser.getUserIntro());
        loginVo.setReadPrivacyPolicy(tUser.getReadPrivacyPolicy());
        // 学生身份
        TStudent tStudent = new TStudent();
        tStudent.setUserId(tUser.getId());
        // 学生身份通过
        tStudent.setAuditStatus("通过");
        List<TStudent> tStudents = tStudentMapper.selectTStudentList(tStudent);
        if (!tStudents.isEmpty() && tStudents.get(0) != null) {
            tStudent = tStudents.get(0);
            LoginVo.StudentInfo studentInfo = new LoginVo.StudentInfo();
            studentInfo.setStudentId(tStudent.getId());
            studentInfo.setUserId(tUser.getId());
            studentInfo.setStudentCode(tStudent.getStudentCode());
            studentInfo.setStudentName(tStudent.getStudentName());
            if (tStudent.getStudentBirthday() != null){
                studentInfo.setStudentBirthday(NTools.createBirthDay(tStudent.getStudentBirthday()));
            }
            studentInfo.setStudentAge(tStudent.getStudentAge());
            studentInfo.setStudentPhone(tStudent.getStudentPhone());
            studentInfo.setStudentEmail(tStudent.getStudentEmail());
            studentInfo.setStudentTargetScore(tStudent.getStudentTargetScore());
            studentInfo.setStudentIntro(tStudent.getStudentIntro());
            loginVo.setStudentInfo(studentInfo);
        }
        // 教师身份
        TTeacher tTeacher = new TTeacher();
        tTeacher.setUserId(tUser.getId());
        // 教师身份通过
        tTeacher.setAuditStatus("通过");
        List<TTeacher> tTeachers = tTeacherMapper.selectTTeacherList(tTeacher);
        if (!tTeachers.isEmpty() && tTeachers.get(0) != null) {
            tTeacher = tTeachers.get(0);
            LoginVo.TeacherInfo teacherInfo = new LoginVo.TeacherInfo();
            teacherInfo.setTeacherId(tTeacher.getId());
            teacherInfo.setUserId(tTeacher.getUserId());
            teacherInfo.setTeacherName(tTeacher.getTeacherName());
            teacherInfo.setTeacherCode(tTeacher.getTeacherCode());
            teacherInfo.setTeacherPicture(tTeacher.getTeacherPicture());
            teacherInfo.setTeacherPhone(tTeacher.getTeacherPhone());
            teacherInfo.setTeacherEmail(tTeacher.getTeacherEmail());
            teacherInfo.setTeacherSchool(tTeacher.getTeacherSchool());
            teacherInfo.setTeacherMajor(tTeacher.getTeacherMajor());
            teacherInfo.setTeacherEducation(tTeacher.getTeacherEducation());
            teacherInfo.setTeacherCountryRegion(tTeacher.getTeahcerCountryregion());
            teacherInfo.setTeacherParty(tTeacher.getTeacherParty());
            teacherInfo.setTeacherNationality(tTeacher.getTeacherNationality());
            teacherInfo.setTeacherExperience(tTeacher.getTeacherExperience());
            teacherInfo.setTeacherIntro(tTeacher.getTeacherIntro());
            loginVo.setTeacherInfo(teacherInfo);
        }
        token = NTools.generateToken(JSONObject.toJSONString(loginVo));
        redisTemplate.opsForValue().set("wxmini:user-info:" + jsonObject.getString("openid"), token, 10, TimeUnit.MINUTES);
        return token;
    }

    @Override
    public List<LastStudentList> lastStudentList() {
        List<LastStudentList> tStudents = wechatMiniMapper.lastStudentList();
        List<LastStudentList> rs = new ArrayList<>();
        for (LastStudentList tStudent : tStudents) {
            LastStudentList lastStudentList = new LastStudentList();
            String studentName = tStudent.getStudentName();
            if (studentName != null && !studentName.isEmpty()) {
                studentName = studentName.charAt(0) + "**";
            } else {
                studentName = "***";
            }
            lastStudentList.setStudentName(studentName);
            lastStudentList.setSignUpTime(tStudent.getSignUpTime());
            String studentPhone = tStudent.getPhone();
            if (studentPhone != null && studentPhone.length() > 4) {
                studentPhone = "********" + studentPhone.substring(studentPhone.length() - 4);
            } else {
                studentPhone = "***********";
            }
            lastStudentList.setPhone(studentPhone);
            rs.add(lastStudentList);
        }
        return rs;
    }

    @Override
    public List<TSlideshow> selectTSlideshowList() {
        return wechatMiniMapper.selectTSlideshowList();
    }

    @Override
    public List<TArticle> indexArticleList() {
        return wechatMiniMapper.indexArticleList();
    }


    @Override
    public GetStudentInfo getStudentInfo(String openId) {

        GetStudentInfo getStudentInfo = new GetStudentInfo();
        // 通过openId学生信息
        TStudent tStudent = wechatMiniMapper.selectTStudentByWXMiniOpenId(openId);
        getStudentInfo
                .setStudentName(tStudent.getStudentName())
                .setSignupTime(tStudent.getCreatedTime())
                .setBirthDay(tStudent.getStudentBirthday())
                .setStudyAge(tStudent.getStudentAge())
                .setPhone(tStudent.getStudentPhone())
                .setEmail(tStudent.getStudentEmail())
                .setStudentIntro(tStudent.getStudentIntro())
                .setTargetScore(tStudent.getStudentTargetScore())
                .setNeedSubjectList(wechatMiniMapper.selectTSubjectByStudentId(tStudent.getId()));


        return getStudentInfo;
    }


    @Override
    @Transactional
    public com.ruoyi.system.vo.wechat.StudentSignUp studentSignUp(StudentSignUp studentSignUp) {
        //根据微信小程序openid取出用户的userid
        String openid = nTools.getMiniOpenId();
//       String openid = "o2wmq5JJtHMh910uESWEZFHPCKrw";
        Long userid = wechatMiniMapper.selectUserIdByOpenid(openid);
        if (userid == null || userid == 0) {
            throw new RuntimeException("报名失败, 用户信息不存在");
        }
        //插入用户性别
        wechatMiniMapper.updateGender(userid, studentSignUp.getGender());
        //判断学生是否已经报名
        TStudent tStudent = new TStudent();
        tStudent.setUserId(userid);
        tStudent.setAuditStatus("通过");
        if (!tStudentMapper.selectTStudentList(tStudent).isEmpty()) {
            throw new RuntimeException("禁止学生重复报名");
        }
        //组装学生报名pojo 插入数据库
        tStudent = new TStudent();
        tStudent.setUserId(userid);
        tStudent.setStudentName(studentSignUp.getName());
        tStudent.setStudentAge("0");
        tStudent.setStudentPhone(studentSignUp.getPhone());
        tStudent.setStudentEmail(studentSignUp.getEmail());
        tStudent.setStudentTargetScore(Long.valueOf(studentSignUp.getTargetScore()));
        tStudent.setStudentHandwrittenSignature(studentSignUp.getSign());
        tStudent.setStudentGender(studentSignUp.getGender());
        tStudent.setStudentBirthday(studentSignUp.getStudentBirthday());
        tStudent.setStudentInformedConsent(studentSignUp.getStudentInformedConsent());
        tStudent.setAuditStatus("通过");
        tStudentMapper.insertTStudent(tStudent);

        //插入学生需强化科目表
        //需强化的科目集合
        List<StudentSignUp.LearningDTO> learnings = studentSignUp.getLearning();
        //需要强化的科目名称数组
        String[] subjects = new String[learnings.size()];
        //遍历插入
        for (int i = 0; i < learnings.size(); i++) {
            TStudentIntensify tStudentIntensify = new TStudentIntensify();
            tStudentIntensify.setStudentId(tStudent.getId());
            tStudentIntensify.setSubjectId(Long.valueOf(learnings.get(i).getId()));
            tStudentIntensify.setStudentRate(learnings.get(i).getRate());
            StudentSignUp.LearningDTO learningDTO = learnings.get(i);
            Long score = null;
            if (learningDTO.getTargetScore() != null && !"".equals(learningDTO.getTargetScore())) {
                score = Long.valueOf(learningDTO.getTargetScore());
            }
            tStudentIntensify.setCurrentScore(learningDTO.getCurrentScore());
            tStudentIntensify.setTargetScore(score);
            tStudentIntensifyMapper.insertTStudentIntensify(tStudentIntensify);
            subjects[i] = learnings.get(i).getSubjectName();
        }

        //调用微信报名信息提交成功小程序消息
        MiniStudentSignUpData miniStudentSignUpData = new MiniStudentSignUpData();
        miniStudentSignUpData.setName3(studentSignUp.getName());
        miniStudentSignUpData.setThing1(NTools.arrayToStr(subjects));
        miniStudentSignUpData.setTime12(NTools.createTime());
        miniStudentSignUpData.setPhone_number5(studentSignUp.getPhone());
        miniStudentSignUpData.setPhone_number13(studentSignUp.getPhone());
        com.ruoyi.system.vo.wechat.StudentSignUp studentSignUp1 = new com.ruoyi.system.vo.wechat.StudentSignUp();
        List<String> stuSubscribeMsg = new ArrayList<>();
//        stuSubscribeMsg.add(WeChatMiniTemplateEnum.StudentAuditSuccess.getTemplateId());
        stuSubscribeMsg.add(custom.AuditSuccess);
        stuSubscribeMsg.add(custom.StudentSignUpSuccess);
        studentSignUp1.setStudentId(tStudent.getId());
        studentSignUp1.setTemplateId(stuSubscribeMsg);
        nTools.deleteLoginCache();
        return studentSignUp1;
    }

    @Override
    public String studentSignUpMsg(String studentId) {

        //openId
        String miniOpenId = nTools.getMiniOpenId();
        //学生对象
//        TStudent tStudent = tStudentMapper.selectTStudentById(Long.valueOf(studentId));
        TStudent tStudent = wechatMiniMapper.selectTStudentByOpenIdAndStudentId(miniOpenId, Long.valueOf(studentId));
        if (tStudent == null) {
            throw new RuntimeException("学生id与openId不匹配");
        }
        //学生科目
        String[] subjects = wechatMiniMapper.selectObjectsByStudentId(tStudent.getId());

        //调用微信报名信息提交成功小程序消息
        MiniStudentSignUpData miniStudentSignUpData = new MiniStudentSignUpData();
        miniStudentSignUpData.setName3(tStudent.getStudentName());
        miniStudentSignUpData.setThing1(NTools.arrayToStr(subjects));
        miniStudentSignUpData.setTime12(NTools.createTime());
        miniStudentSignUpData.setPhone_number5(tStudent.getStudentPhone());
        miniStudentSignUpData.setPhone_number13(tStudent.getStudentPhone());
        //通知管理员审核学生报名
        ManagerStudentSignUpData managerStudentSignUpData = new ManagerStudentSignUpData();
        managerStudentSignUpData.setThing5(tStudent.getStudentName());
        managerStudentSignUpData.setPhone_number8(tStudent.getStudentPhone());
        managerStudentSignUpData.setThing1(NTools.arrayToStr(subjects));


        //管理员员审核数据data数据
        tStudent.setDeleteTime(null);
        tStudent.setCreatedTime(null);
        tStudent.setUpdatedTime(null);
        tStudent.setCreatedBy(null);
        tStudent.setDeleteBy(null);
        tStudent.setUpdatedBy(null);
        tStudent.setNote(null);


//        String tStudentstr = JSONObject.toJSONString(tStudent);
//        redisTemplate.opsForValue().set("wxmini:username:"+openId,userName,10, TimeUnit.MINUTES);
        String uuid = UUID.fastUUID().toString();
        redisTemplate.opsForValue().set("wxmini:signUp:" + uuid, tStudent.getId(), 7, TimeUnit.DAYS);

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("appid", custom.WechatMiniAppID);
        hashMap.put("pagepath", "pages/record/record?type=学生&token=" + uuid);

        try {
            //通知学生报名成功
            WxReturnedValue wxReturnedValue = wxUtil.weChatMiniMsg(miniStudentSignUpData.msgTemplateData(miniOpenId, custom.StudentSignUpSuccess, custom.wechatMiniInfoLang,custom.wechatMiniPage,custom.miniprogramState));
            //通知管理员审核学生报名
            List<String> managerOpenIds = wxUtil.getManagerOpenIds();
            for (int i = 0; i < managerOpenIds.size(); i++) {
//                "ooL2_t76f_SvEAX5k7e3Cgy2RG-I"
                WxReturnedValue wxReturnedValue1 = wxUtil.weChatMsg(managerStudentSignUpData.msgTemplateData(managerOpenIds.get(i), custom.ManagerStudentSignUp, hashMap));
                System.out.println(wxReturnedValue1);
            }

            StringBuilder stringBuilder = new StringBuilder();
            if (wxReturnedValue.getErrcode() != 0) {
                stringBuilder.append("[通知学生失败，错误码:" + wxReturnedValue.getErrcode() + ";错误消息:" + wxReturnedValue.getErrmsg() + "]");
            }
//            if (wxReturnedValue1.getErrcode() != 0) {
//                stringBuilder.append("[通知管理员失败，错误码:" + wxReturnedValue1.getErrcode() + ";错误消息:" + wxReturnedValue1.getErrmsg() + "]");
//            }
            return stringBuilder.toString();

        } catch (IOException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public String teacherSignUpMsg(String teacherId) {

//        String miniOpenId = "o2wmq5JJtHMh910uESWEZFHPCKrw";
        //openId
        String miniOpenId = nTools.getMiniOpenId();

        //教师对象
//        TTeacher tTeacher = tTeacherMapper.selectTTeacherById(Long.valueOf(teacherId));

        TTeacher tTeacher = wechatMiniMapper.selectTTeacherByOpenIdAndTeacherId(miniOpenId, Long.valueOf(teacherId));
        if (tTeacher == null) {
            throw new RuntimeException("教师id与openId不匹配");
        }

        //教师科目 根据 查询
        String[] subjects = wechatMiniMapper.selectObjectsByTeacherId(tTeacher.getId());
        //教师报名模板通知
        MiniTeacherSignUpData miniTeacherSignUpData = new MiniTeacherSignUpData();
        miniTeacherSignUpData.setName9(tTeacher.getTeacherName());
        miniTeacherSignUpData.setThing1(NTools.arrayToStr(subjects));
        miniTeacherSignUpData.setThing11("无");
        miniTeacherSignUpData.setTime28(NTools.createTime());
        //通知管理员教师报名模板数据
        ManagerTeacherSignUpData managerTeacherSignUpData = new ManagerTeacherSignUpData();
        managerTeacherSignUpData.setThing1(tTeacher.getTeacherName());
        managerTeacherSignUpData.setTime2(NTools.createTime());

        //管理员员审核数据
        tTeacher.setDeleteTime(null);
        tTeacher.setCreatedTime(null);
        tTeacher.setUpdatedTime(null);
        tTeacher.setCreatedBy(null);
        tTeacher.setDeleteBy(null);
        tTeacher.setUpdatedBy(null);
        tTeacher.setNote(null);

//        String teacher = JSONObject.toJSONString(tTeacher);
//        redisTemplate.opsForValue().set("wxmini:username:"+openId,userName,10, TimeUnit.MINUTES);
        String uuid = UUID.fastUUID().toString();
        redisTemplate.opsForValue().set("wxmini:signUp:" + uuid, tTeacher.getId(), 7, TimeUnit.DAYS);

        //data数据
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("appid", custom.WechatMiniAppID);
        hashMap.put("pagepath", "pages/record/record?type=教师&token=" + uuid);

        try {
            //通知教师报名成功
            WxReturnedValue wxReturnedValue = wxUtil.weChatMiniMsg(miniTeacherSignUpData.msgTemplateData(miniOpenId, custom.TeacherSignUpSuccess, custom.wechatMiniInfoLang,custom.wechatMiniPage,custom.miniprogramState));
            //通知管理员审核教师
            List<String> managerOpenIds = wxUtil.getManagerOpenIds();
            for (int i = 0; i < managerOpenIds.size(); i++) {
//                "ooL2_t76f_SvEAX5k7e3Cgy2RG-I"
                WxReturnedValue wxReturnedValue1 = wxUtil.weChatMsg(managerTeacherSignUpData.msgTemplateData(managerOpenIds.get(i), custom.ManagerTeacherSignUp, hashMap));
            }


            StringBuilder stringBuilder = new StringBuilder();
            if (wxReturnedValue.getErrcode() != 0) {
                stringBuilder.append("[通知教师失败，错误码:" + wxReturnedValue.getErrcode() + ";错误消息:" + wxReturnedValue.getErrmsg() + "]");
            }
//            if (wxReturnedValue1.getErrcode() != 0) {
//                stringBuilder.append("[通知管理员失败，错误码:" + wxReturnedValue1.getErrcode() + ";错误消息:" + wxReturnedValue1.getErrmsg() + "]");
//            }
            return stringBuilder.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public GetUserInfo getUserInfo() {
        TUser tUser = getTUserByMiniOpenId(nTools.getMiniOpenId());
        GetUserInfo getUserInfo = new GetUserInfo();
        getUserInfo
                .setPhoto(tUser.getUserPhoto())
                .setGender(tUser.getUserGender())
                .setUsername(tUser.getUserName())
                .setEmail(tUser.getUserEmail())
                .setBirthDay(tUser.getUserBirth())
                .setPhone(tUser.getUserPhone())
                .setIntro(tUser.getUserIntro());
        return getUserInfo;
    }

    @Override
    public TUser getTUserByMiniOpenId(String openId) {
        TUser tUser = wechatMiniMapper.selectTUserByWechatMiniOpenId(openId);
        if (tUser == null) {
            throw new RuntimeException("用户信息异常");
        }
        return tUser;
    }

    @Override
    @Transactional
    public TeacherSignUpVo teacherSignUp(TeacherSignUp teacherSignUp) {

        // 获取用户数据
        TUser tUser = new TUser();
        tUser.setUserWxMiniOpenid(nTools.getMiniOpenId());
        List<TUser> tUsers = tUserMapper.selectTUserList(tUser);
        if (tUsers.isEmpty() || tUsers.get(0) == null) {
            throw new RuntimeException("报名失败, 用户信息不存在");
        }

        tUser = tUsers.get(0);
        TTeacher tTeacher = new TTeacher();
        tTeacher.setUserId(tUser.getId());
        tTeacher.setAuditStatus("待处理");
        //判断教师是否已经报名
        if (!tTeacherMapper.selectTTeacherList(tTeacher).isEmpty()) {
            throw new RuntimeException("禁止教师重复报名");
        }
        // 录入教师数据
        tTeacher = new TTeacher();
        tTeacher.setUserId(tUser.getId());
        tTeacher.setTeacherName(teacherSignUp.getUsername());
        tTeacher.setTeacherScore(60L);
        tTeacher.setTeacherPicture(teacherSignUp.getPicture());
        tTeacher.setTeacherEmail(teacherSignUp.getEmail());
        tTeacher.setTeacherPhone(teacherSignUp.getTeacherPhone());
        tTeacher.setTeacherSchool(teacherSignUp.getSchool());
        tTeacher.setTeacherMajor(teacherSignUp.getMajor());
        tTeacher.setTeacherEducation(teacherSignUp.getCurrentEducation());
        tTeacher.setTeahcerCountryregion(teacherSignUp.getAddress());
        tTeacher.setTeacherInformedbrief(teacherSignUp.getConfirmation());
        tTeacher.setTeacherSignature(teacherSignUp.getSignature());
        tTeacher.setTeacherParty(teacherSignUp.getParty());
        tTeacher.setTeacherNationality(teacherSignUp.getNationality());
        tTeacher.setTeacherExperience(teacherSignUp.getExperience());
        tTeacher.setTeacherGender(teacherSignUp.getGender());
        tTeacher.setTeacherBirthday(teacherSignUp.getBirthday());
        tTeacher.setTeahcerStatus("正常");
        tTeacher.setAuditStatus("待处理");

        tTeacherMapper.insertTTeacher(tTeacher);

        // 录入教师意向数据 课程
        for (int i = 0; i < teacherSignUp.getSubject().size(); i++) {
            TeacherSignUp.Subject subject = teacherSignUp.getSubject().get(i);
            TTeachingObjectsAndSubjects tTeachingObjectsAndSubjects = new TTeachingObjectsAndSubjects();
            tTeachingObjectsAndSubjects.setTeacherId(tTeacher.getId());
            tTeachingObjectsAndSubjects.setTeachingYearId(subject.getTeachingYearId());
            tTeachingObjectsAndSubjects.setSubjectId(subject.getSubjectId());
            tTeachingObjectsAndSubjects.setTeachingRemark(subject.getTeachingRemark());
            int i1 = tTeachingObjectsAndSubjectsMapper.insertTTeachingObjectsAndSubjects(tTeachingObjectsAndSubjects);
            if (i1 == 0) {
                throw new RuntimeException("新增教师意向课程失败");
            }
        }


        // 录入教师个人优势数据
        if (teacherSignUp.getTeacherAdvantageList() != null && !teacherSignUp.getTeacherAdvantageList().isEmpty()){
            for (TeacherSignUp.TeacherAdvantage teacherAdvantage : teacherSignUp.getTeacherAdvantageList()) {
                TTeacherAdvantages tTeacherAdvantages = new TTeacherAdvantages();
                tTeacherAdvantages.setTeacherId(tTeacher.getId());
                tTeacherAdvantages.setPersonalAdvantagesTypeId(teacherAdvantage.getTeacherAdvantageTypeId());
                tTeacherAdvantages.setAdvantagesRemark(teacherAdvantage.getAdvantageRemark());
                tTeacherAdvantagesMapper.insertTTeacherAdvantages(tTeacherAdvantages);
            }
        }


        // 教师可安排时间
        if (teacherSignUp.getTeacherScheduleList() != null && !teacherSignUp.getTeacherScheduleList().isEmpty()){
            for (TeacherSignUp.teacherSchedule teacherSchedule : teacherSignUp.getTeacherScheduleList()) {
                TTeacherSchedule tTeacherSchedule = new TTeacherSchedule();
                tTeacherSchedule.setTeacherId(tTeacher.getId());
                tTeacherSchedule.setScheduleMonth(teacherSchedule.getScheduleMonth());
                tTeacherSchedule.setScheduleWeek(teacherSchedule.getScheduleWeek());
                tTeacherSchedule.setScheduleStartTime(teacherSchedule.getStartTime());
                tTeacherSchedule.setScheduleEndTime(teacherSchedule.getEndTime());
                tTeacherSchedule.setPlanNote(teacherSchedule.getPlanNote());
                tTeacherScheduleMapper.insertTTeacherSchedule(tTeacherSchedule);
            }
        }

        //todo 取得的证书  引荐人邀请码
        //通知模板id集合
        List<String> templateIdList = new ArrayList<>();

        templateIdList.add(custom.TeacherSignUpSuccess);
//        templateIdList.add(WeChatMiniTemplateEnum.TeacherAuditSuccess.getTemplateId());
        templateIdList.add(custom.AuditSuccess);

        //返回对象
        TeacherSignUpVo teacherSignUpVo = new TeacherSignUpVo();
        teacherSignUpVo.setTeacherId(tTeacher.getId());
        teacherSignUpVo.setTemplateIds(templateIdList);
        // 清除用户数据缓存
        nTools.deleteLoginCache();
        return teacherSignUpVo;
    }

    @Override
    public String wechatMiniAddTeacherCertificate(AddTeacherCertificate addTeacherCertificate) {
        TRelationTeacherCertificates relationTeacherCertificates = new TRelationTeacherCertificates();
        relationTeacherCertificates.setTeacherCertificates(addTeacherCertificate.getCertificatesId());
        relationTeacherCertificates.setTeacherId(addTeacherCertificate.getTeacherId());
        relationTeacherCertificates.setCertificatesPhoto1(addTeacherCertificate.getCertificatesPhoto1());
        relationTeacherCertificates.setCertificatesPhoto2(addTeacherCertificate.getCertificatesPhoto2());
        relationTeacherCertificates.setCertificatesPhoto3(addTeacherCertificate.getCertificatesPhoto3());
        relationTeacherCertificates.setCertificatesPhoto4(addTeacherCertificate.getCertificatesPhoto4());
        relationTeacherCertificates.setCertificatesPhoto5(addTeacherCertificate.getCertificatesPhoto5());
        relationTeacherCertificates.setAuditStatus("待处理");
        tRelationTeacherCertificatesMapper.insertTRelationTeacherCertificates(relationTeacherCertificates);
        return "提交成功";
    }

    @Override
    public List<TTeacherCertificates> wechatMiniTeacherOwnCertificateList(String openId, Long teacherId) {
        return wechatMiniMapper.wechatMiniTeacherOwnCertificateList(openId, teacherId);
    }

    @Override
    public List<TTeacherCertificates> wechatMiniTeacherAllCertificateList(String openId, Long teacherId) {
        return wechatMiniMapper.wechatMiniTeacherAllCertificateList(openId, teacherId);
    }

    @Override
    public Long wechatMiniIndexSignUpNumber() {


        return wechatMiniMapper.wechatMiniIndexSignUpNumber();
    }

    @Override
    public List<TArticle> wechatMiniSubjectArticleList(Long subjectId) {
        List<TArticle> tArticles = wechatMiniMapper.wechatMiniSubjectArticleList(subjectId);
        return tArticles;
    }

    @Override
    public List<TArticle> wechatMiniSubjectArticleList1(long subjectId, long articleTwoTypeId) {
        if (articleTwoTypeId == 0){
            return wechatMiniSubjectArticleList(subjectId);
        }
        List<TArticle> tArticles = wechatMiniMapper.wechatMiniSubjectArticleList1(subjectId,articleTwoTypeId);
        return tArticles;
    }

    @Override
    public List<TArticle> wechatMiniIndexSubjectList() {
        List<TArticle> tArticles = wechatMiniMapper.wechatMiniIndexSubjectList();
        return tArticles;
    }



    @Override
    public List<TTeacherCertificates> wechatMiniCertificateList() {
        List<TTeacherCertificates> tTeacherCertificates = wechatMiniMapper.wechatMiniCertificateList();
        return tTeacherCertificates;
    }

    @Override
    public List<OwnWorkRoomSearchVo> wechatMiniSelfJoinWorkRoomList() {
        // todo 我加入的工作室
        //小程序openid
        String miniOpenId = nTools.getMiniOpenId();
        //根据openid查询用户加入的工作室
        List<TWorkRoom> tWorkRooms = wechatMiniMapper.selectWorkRoomByOpenId(miniOpenId);


        List<OwnWorkRoomSearchVo> workRoomSearchVoList = new ArrayList<>();
        for (TWorkRoom tWorkRoom : tWorkRooms) {
            OwnWorkRoomSearchVo workRoomSearchVo = new OwnWorkRoomSearchVo();
            workRoomSearchVo.setOnlineStatus(UserConsultServer.getWorkRoomOnlineStatus(tWorkRoom.getId()));
            workRoomSearchVo.setMsgNum(0l);
            workRoomSearchVo.setWorkRoomName(tWorkRoom.getWorkRoomName());
            workRoomSearchVo.setWorkRoomId(tWorkRoom.getId());
            workRoomSearchVo.setPrincipalTeacherId(tWorkRoom.getPrincipalTeacherId());
            workRoomSearchVo.setConsultTeacherId(tWorkRoom.getConsultTeacherId());//咨询教师
            workRoomSearchVo.setSubjectList(wechatMiniMapper.selectTSubjectByWorkRoomId(tWorkRoom.getId()));
            workRoomSearchVo.setIntro(tWorkRoom.getWorkRoomIntro());
            workRoomSearchVo.setPhone(tWorkRoom.getWorkRoomPhone());
            workRoomSearchVo.setEmail(tWorkRoom.getWorkRoomEmail());
            workRoomSearchVo.setWorkRoomId(tWorkRoom.getId());
            workRoomSearchVo.setArticleId(tWorkRoom.getShowArticleId());
            workRoomSearchVo.setWorkRoomIcon(tWorkRoom.getWorkRoomIcon());
            workRoomSearchVo.setWorkRoomImage(tWorkRoom.getWorkRoomImage());
            workRoomSearchVo.setWorkRoomLink(tWorkRoom.getWorkRoomLink());
            TArticle tArticle = tArticleMapper.selectTArticleById(tWorkRoom.getShowArticleId());
            workRoomSearchVo.setArticleTitle(tArticle.getArticleTitle());
            workRoomSearchVo.setArticleContent(tArticle.getArticleContent());

            workRoomSearchVoList.add(workRoomSearchVo);

        }


        return workRoomSearchVoList;
    }

    @Override
    public List<TWorkRoom> wechatMiniPublicWorkRoomList() {
        TWorkRoom tWorkRoom = new TWorkRoom();
        tWorkRoom.setAuditStatus("通过");
        return tWorkRoomMapper.selectTWorkRoomList(tWorkRoom);
    }

    @Override
    @Transactional
    public String wechatMiniCreateWorkRoom(CreateWorkRoom createWorkRoom) {
        LoginVo loginInfo = nTools.getLoginInfo();
        LoginVo.TeacherInfo teacherInfo = loginInfo.getTeacherInfo();
        if (teacherInfo == null) {
            throw new RuntimeException("教师信息错误");
        }
        TWorkRoom tWorkRoom = new TWorkRoom();
        tWorkRoom.setPrincipalTeacherId(teacherInfo.getTeacherId());
        tWorkRoom.setWorkRoomName(createWorkRoom.getWorkRoomName());
        tWorkRoom.setWorkRoomLink(createWorkRoom.getWorkRoomLink());
        tWorkRoom.setWorkRoomDetails(createWorkRoom.getWorkRoomDetails());
        tWorkRoom.setWorkRoomIcon(createWorkRoom.getWorkRoomIcon());
        tWorkRoom.setWorkRoomImage(createWorkRoom.getWorkRoomImage());
        tWorkRoom.setWorkRoomIntro(createWorkRoom.getWorkRoomIntro());
        tWorkRoom.setWorkRoomEmail(createWorkRoom.getWorkRoomEmail());
        tWorkRoom.setWorkRoomPhone(createWorkRoom.getWorkRoomPhone());
        tWorkRoom.setAuditStatus("待处理");
        tWorkRoom.setShowArticleId(createWorkRoom.getShowArticleId());
        int i = tWorkRoomMapper.insertTWorkRoom(tWorkRoom);
        if (createWorkRoom.getSubjectIdList() != null) {
            for (Long subjectId : createWorkRoom.getSubjectIdList()) {
                TRelationWorkRoomSubject tRelationWorkRoomSubject = new TRelationWorkRoomSubject();
                tRelationWorkRoomSubject.setWorkRoomId(tWorkRoom.getId());
                tRelationWorkRoomSubject.setSubjectId(subjectId);
                int i1 = tRelationWorkRoomSubjectMapper.insertTRelationWorkRoomSubject(tRelationWorkRoomSubject);
                if (i1 == 0) {
                    throw new RuntimeException("新增科目信息异常");
                }
            }
        }

        if (i == 0) {
            throw new RuntimeException("工作室创建失败");
        }
        //创建工作室教师加入工作室
        TRelationWorkRoomUser tRelationWorkRoomUser = new TRelationWorkRoomUser();
        tRelationWorkRoomUser.setUserRoomName(loginInfo.getTeacherInfo().getTeacherName());
        tRelationWorkRoomUser.setTargetId(loginInfo.getTeacherInfo().getTeacherId());
        tRelationWorkRoomUser.setWorkRoomId(tWorkRoom.getId());
        tRelationWorkRoomUser.setJoinType("教师");
        tRelationWorkRoomUser.setAuditStatus("通过");
        tRelationWorkRoomUserMapper.insertTRelationWorkRoomUser(tRelationWorkRoomUser);

        // 创建工作室默认文章
        TArticle tArticle = new TArticle();
        tArticle.setWorkRoomId(tWorkRoom.getId());
        tArticle.setUserId(nTools.getLoginInfo().getId());
        tArticle.setArticleReadNum(0l);
        tArticle.setArticleDownNum(0l);
        tArticle.setArticleUpNum(0l);
        tArticle.setArticleTitle("工作室介绍");
        tArticle.setArticleContent("未设置工作室介绍");
        tArticle.setAuditStatus("通过");
        int i1 = tArticleMapper.insertTArticle(tArticle);
        if (i1 == 0) {
            throw new RuntimeException("工作室默认文章创建失败");
        }
        tWorkRoom.setShowArticleId(tArticle.getId());
        int i2 = tWorkRoomMapper.updateTWorkRoom(tWorkRoom);
        if (i2 == 0) {
            throw new RuntimeException("工作室默认文章创建异常");
        }
        return "工作室创建失败";
    }

    @Override
    public String wechatMiniJoinWorkRoom(JoinWorkRoom joinWorkRoom) {
        String joinType = joinWorkRoom.getJoinType();
        LoginVo loginInfo = nTools.getLoginInfo();
        if (joinType == null || "".equals(joinType)) {
            throw new RuntimeException("加入类型错误");
        }
        if (joinWorkRoom.getWorkRoomId() == null || joinWorkRoom.getWorkRoomId() == 0) {
            throw new RuntimeException("不正确的工作室Id");
        }
        String s = wechatMiniCheckJoinOrNotWorkRoom(joinWorkRoom.getWorkRoomId(), joinWorkRoom.getJoinType());
        if ("通过".equals(s)) {
            throw new RuntimeException("您已加入工作室");
        }
        if ("待处理".equals(s)) {
            throw new RuntimeException("您已申请加入工作室,等待审核");
        }

        if ("教师".equals(joinType)) {
            LoginVo.TeacherInfo teacherInfo = loginInfo.getTeacherInfo();
            if (teacherInfo == null) {
                throw new RuntimeException("教师信息错误");
            }
            TRelationWorkRoomUser tRelationWorkRoomUser = new TRelationWorkRoomUser();
            tRelationWorkRoomUser.setWorkRoomId(joinWorkRoom.getWorkRoomId());
            tRelationWorkRoomUser.setAuditStatus("待处理");
            tRelationWorkRoomUser.setTargetId(teacherInfo.getTeacherId());
            tRelationWorkRoomUser.setUserRoomName(joinWorkRoom.getWorkRoomSideName());
            tRelationWorkRoomUser.setJoinType("教师");
            int i = tRelationWorkRoomUserMapper.insertTRelationWorkRoomUser(tRelationWorkRoomUser);
            if (i == 0) {
                throw new RuntimeException("申请加入工作室失败");
            }
        }
        if ("学生".equals(joinType)) {
            LoginVo.StudentInfo studentInfo = loginInfo.getStudentInfo();
            if (studentInfo == null) {
                throw new RuntimeException("学生信息错误");
            }
            TRelationWorkRoomUser tRelationWorkRoomUser = new TRelationWorkRoomUser();
            tRelationWorkRoomUser.setWorkRoomId(joinWorkRoom.getWorkRoomId());
            tRelationWorkRoomUser.setAuditStatus("待处理");
            tRelationWorkRoomUser.setTargetId(studentInfo.getStudentId());
            tRelationWorkRoomUser.setUserRoomName(joinWorkRoom.getWorkRoomSideName());
            tRelationWorkRoomUser.setJoinType("学生");
            int i = tRelationWorkRoomUserMapper.insertTRelationWorkRoomUser(tRelationWorkRoomUser);
            if (i == 0) {
                throw new RuntimeException("申请加入工作室失败");
            }
        }
        if ("用户".equals(joinType)) {
            TRelationWorkRoomUser tRelationWorkRoomUser = new TRelationWorkRoomUser();
            tRelationWorkRoomUser.setWorkRoomId(joinWorkRoom.getWorkRoomId());
            tRelationWorkRoomUser.setAuditStatus("待处理");
            tRelationWorkRoomUser.setTargetId(loginInfo.getId());
            tRelationWorkRoomUser.setUserRoomName(joinWorkRoom.getWorkRoomSideName());
            tRelationWorkRoomUser.setJoinType("用户");
            int i = tRelationWorkRoomUserMapper.insertTRelationWorkRoomUser(tRelationWorkRoomUser);
            if (i == 0) {
                throw new RuntimeException("申请加入工作室失败");
            }
        }
        return "申请加入工作室成功，等待审核";
    }

    @Override
    public List<TSubject> wechatMiniSubjectListAndOwnCreate() {
        TUser tUser = wechatMiniMapper.selectTUserByWechatMiniOpenId(nTools.getMiniOpenId());
        if (tUser == null) {
            throw new RuntimeException("用户信息错误");
        }
        return wechatMiniMapper.wechatMiniSubjectListAndOwnCreate(tUser.getId());
    }

    @Override
    public List<WorkRoomSearchVo> wechatMiniWorkRoomSearch(WorkRoomSearch workRoomSearch) {

        List<TWorkRoom> tWorkRooms = wechatMiniMapper.wechatMiniWorkRoomSearch(workRoomSearch);
        List<WorkRoomSearchVo> workRoomSearchVoList = new ArrayList<>();
        for (TWorkRoom tWorkRoom : tWorkRooms) {
            WorkRoomSearchVo workRoomSearchVo = new WorkRoomSearchVo();
            workRoomSearchVo.setOnlineStatus(UserConsultServer.getWorkRoomOnlineStatus(tWorkRoom.getId()));
            workRoomSearchVo.setMsgNum(0l);
            workRoomSearchVo.setWorkRoomName(tWorkRoom.getWorkRoomName());
            workRoomSearchVo.setWorkRoomId(tWorkRoom.getId());
            workRoomSearchVo.setPrincipalTeacherId(tWorkRoom.getPrincipalTeacherId());
            workRoomSearchVo.setConsultTeacherId(tWorkRoom.getConsultTeacherId());//咨询教师
            workRoomSearchVo.setSubjectList(wechatMiniMapper.selectTSubjectByWorkRoomId(tWorkRoom.getId()));
            workRoomSearchVo.setIntro(tWorkRoom.getWorkRoomIntro());
            workRoomSearchVo.setPhone(tWorkRoom.getWorkRoomPhone());
            workRoomSearchVo.setEmail(tWorkRoom.getWorkRoomEmail());
            workRoomSearchVo.setWorkRoomId(tWorkRoom.getId());
            workRoomSearchVo.setArticleId(tWorkRoom.getShowArticleId());
            workRoomSearchVo.setWorkRoomIcon(tWorkRoom.getWorkRoomIcon());
            workRoomSearchVo.setWorkRoomImage(tWorkRoom.getWorkRoomImage());
            workRoomSearchVo.setWorkRoomLink(tWorkRoom.getWorkRoomLink());
            workRoomSearchVoList.add(workRoomSearchVo);
        }
        return workRoomSearchVoList;
    }

    @Override
    public List<WorkRoomMsgShowVo> wechatMiniWorkRoomMsg() {
        String miniOpenId = nTools.getMiniOpenId();
        List<WorkRoomMsgShowVo> workRoomMsgShowVos = wechatMiniMapper.wechatMiniWorkRoomMsg(miniOpenId);
        for (int i = 0; i < workRoomMsgShowVos.size(); i++) {
            if ("咨询教师".equals(workRoomMsgShowVos.get(i).getMessageType())) {
                workRoomMsgShowVos.get(i).setOnlineStatus(UserConsultServer.getWorkRoomOnlineStatus(workRoomMsgShowVos.get(i).getWorkRoomId()));
            } else {
                workRoomMsgShowVos.get(i).setOnlineStatus(UserConsultServer.getUserOnlineStatus(wechatMiniMapper.selectOpenIdByUserId(workRoomMsgShowVos.get(i).getTargetId())));
            }
        }
        return workRoomMsgShowVos;
    }

    @Override
    public List<TArticle> wechatMiniArticleListBySubjectId(Long subjectId) {
        List<TArticle> tArticles = wechatMiniMapper.wechatMiniArticleListBySubjectId(subjectId);
        return tArticles;
    }

    @Override
    public List<TWorkRoomTeacherInfo> wechatMiniWorkRoomTeacherInfo(String workRoomId) {
        List<TWorkRoomTeacherInfo> tWorkRoomTeacherInfos = wechatMiniMapper.selectWorkRoomTeacherInfo(Integer.valueOf(workRoomId));
        if (tWorkRoomTeacherInfos == null) {
            throw new RuntimeException("教师个人信息表空");
        }


        return tWorkRoomTeacherInfos;
    }

    @Override
    public String wechatMiniDeleteWorkRoomTeacherInfo(String id, String workRoomId) {
        TWorkRoomTeacherInfo tWorkRoomTeacherInfo = new TWorkRoomTeacherInfo();
        tWorkRoomTeacherInfo.setId(Long.valueOf(id));
        tWorkRoomTeacherInfo.setWorkRoomId(Long.valueOf(workRoomId));
        tWorkRoomTeacherInfo.setDeleteTime(new Date());
        int i = tWorkRoomTeacherInfoMapper.updateTWorkRoomTeacherInfo(tWorkRoomTeacherInfo);
        if (i == 0) {
            return "失败";
        }
        return "成功";
    }

    @Override
    public String wechatMiniUpdateWorkRoomTeacherInfo(WorkRoomTeacherInfo workRoomTeacherInfo) {
        Long workRoomId = workRoomTeacherInfo.getWorkRoomId();
        String miniOpenId = nTools.getMiniOpenId();
        TWorkRoom tWorkRoom = wechatMiniMapper.judgmentOpenIdAndWorkRoomId(miniOpenId, workRoomId);
        if (tWorkRoom == null) {
            throw new RuntimeException("不是负责教师");
        }
        TWorkRoomTeacherInfo tWorkRoomTeacherInfo = new TWorkRoomTeacherInfo();
        tWorkRoomTeacherInfo.setId(workRoomTeacherInfo.getId());
        tWorkRoomTeacherInfo.setWorkRoomId(workRoomId);
//        List<TWorkRoomTeacherInfo> tWorkRoomTeacherInfos = tWorkRoomTeacherInfoMapper.selectTWorkRoomTeacherInfoList(tWorkRoomTeacherInfo);
//        tWorkRoomTeacherInfo = tWorkRoomTeacherInfos.get(0);
        tWorkRoomTeacherInfo.setTeacherEmail(workRoomTeacherInfo.getTeacherEmail());
        tWorkRoomTeacherInfo.setTeacherDetails(workRoomTeacherInfo.getTeacherDetails());
        tWorkRoomTeacherInfo.setTeacherName(workRoomTeacherInfo.getTeacherName());
        tWorkRoomTeacherInfo.setTeacherIntro(workRoomTeacherInfo.getTeacherIntro());
        tWorkRoomTeacherInfo.setTeacherPhone(workRoomTeacherInfo.getTeacherPhone());
        tWorkRoomTeacherInfo.setTeacherPhoto1(workRoomTeacherInfo.getTeacherPhoto1());
        tWorkRoomTeacherInfo.setTeacherPhoto2(workRoomTeacherInfo.getTeacherPhoto2());
        tWorkRoomTeacherInfo.setTeacherPhoto3(workRoomTeacherInfo.getTeacherPhoto3());
        int i = tWorkRoomTeacherInfoMapper.updateTWorkRoomTeacherInfo(tWorkRoomTeacherInfo);
        if (i == 0) {
            throw new RuntimeException("修改失败");
        }
        return "成功";
    }

    @Override
    public String wechatMiniInsertWorkRoomTeacherInfo(WorkRoomTeacherInfo workRoomTeacherInfo) {
        TWorkRoomTeacherInfo tWorkRoomTeacherInfo = new TWorkRoomTeacherInfo();
        tWorkRoomTeacherInfo.setWorkRoomId(workRoomTeacherInfo.getWorkRoomId());
        tWorkRoomTeacherInfo.setTeacherIntro(workRoomTeacherInfo.getTeacherIntro());
        tWorkRoomTeacherInfo.setTeacherPhoto1(workRoomTeacherInfo.getTeacherPhoto1());
        tWorkRoomTeacherInfo.setTeacherPhoto2(workRoomTeacherInfo.getTeacherPhoto2());
        tWorkRoomTeacherInfo.setTeacherPhoto3(workRoomTeacherInfo.getTeacherPhoto3());
        tWorkRoomTeacherInfo.setTeacherDetails(workRoomTeacherInfo.getTeacherDetails());
        tWorkRoomTeacherInfo.setTeacherPhone(workRoomTeacherInfo.getTeacherPhone());
        tWorkRoomTeacherInfo.setTeacherEmail(workRoomTeacherInfo.getTeacherEmail());
        tWorkRoomTeacherInfo.setTeacherName(workRoomTeacherInfo.getTeacherName());
        tWorkRoomTeacherInfo.setAuditStatus("通过");
        int i = tWorkRoomTeacherInfoMapper.insertTWorkRoomTeacherInfo(tWorkRoomTeacherInfo);
        if (i == 0) {
            throw new RuntimeException("新增失败");
        }
        return "成功";
    }

//    @Override
//    public String wechatMiniUpdateWorkRoomInfo(WorkRoomInfo workRoomInfo) {
//        TWorkRoom tWorkRoom = new TWorkRoom();
//        tWorkRoom.setId(workRoomInfo.getId());
//        tWorkRoom.setWorkRoomName(workRoomInfo.getWorkRoomName());
//        tWorkRoom.setWorkRoomIntro(workRoomInfo.getWorkRoomIntro());
//        tWorkRoom.setWorkRoomPhone(workRoomInfo.getWorkRoomPhone());
//        tWorkRoom.setWorkRoomEmail(workRoomInfo.getWorkRoomEmail());
//        tWorkRoom.setWorkRoomIcon(workRoomInfo.getWorkRoomIcon());
//        tWorkRoom.setWorkRoomImage(workRoomInfo.getWorkRoomImage());
//        int i = tWorkRoomMapper.updateTWorkRoom(tWorkRoom);
//        if (i == 0) {
//            throw new RuntimeException("失败");
//        }
//        return "成功";
//    }

    @Override
    public List<GetWorkRoomAuditList> wechatMiniAuditJoinWorkRoomList(String workRoomId) {
        TRelationWorkRoomUser tRelationWorkRoomUser = new TRelationWorkRoomUser();
        tRelationWorkRoomUser.setWorkRoomId(Long.valueOf(workRoomId));
        tRelationWorkRoomUser.setAuditStatus("待处理");

        List<GetWorkRoomAuditList> getWorkRoomAuditLists = wechatMiniMapper.wechatMiniGetWorkRoomAuditListByWorkRoomId(Long.valueOf(workRoomId));

//        List<TRelationWorkRoomUser> tRelationWorkRoomUsers = tRelationWorkRoomUserMapper.selectTRelationWorkRoomUserList(tRelationWorkRoomUser);

        if (getWorkRoomAuditLists.size() == 0) {
            throw new RuntimeException("无审核列表");
        }
        return getWorkRoomAuditLists;
    }

    @Override
    public String wechatMiniAuditJoinWorkRoom(String id, String auditStatus, String auditResult) {
        TRelationWorkRoomUser tRelationWorkRoomUser = new TRelationWorkRoomUser();
        tRelationWorkRoomUser.setId(Long.valueOf(id));
        tRelationWorkRoomUser.setAuditStatus(auditStatus);
        tRelationWorkRoomUser.setAuditResult(auditResult);
        int i = tRelationWorkRoomUserMapper.updateTRelationWorkRoomUser(tRelationWorkRoomUser);
        if (i == 0) {
            throw new RuntimeException("修改审核状态失败");
        }
        return "成功";
    }

    @Override
    public String wechatMiniWorkRoomOnline(String workRoomId) {
        return UserConsultServer.getWorkRoomOnlineStatus(Long.valueOf(workRoomId));
    }


    @Override
    public String wechatMiniChangeUserInfo(ChangeUserInfo changeUserInfo) {
        //通过
        TUser tUser = new TUser();
        tUser.setUserWxMiniOpenid(nTools.getMiniOpenId());
        List<TUser> tUsers = tUserMapper.selectTUserList(tUser);
        tUser = tUsers.get(0);
        tUser.setUserName(changeUserInfo.getUserName());
        tUser.setUserGender(changeUserInfo.getUserGender());
        tUser.setUserPhone(changeUserInfo.getUserPhone());
        tUser.setUserEmail(changeUserInfo.getUserEmail());
        tUser.setUserBirth(changeUserInfo.getUserBirth());
        tUser.setUserPhoto(changeUserInfo.getUserPhoto());
        tUser.setUserIntro(changeUserInfo.getUserIntro());
        int i = tUserMapper.updateTUser(tUser);
        if (i == 0) {
            throw new RuntimeException("修改失败");
        }
        // 清除用户数据缓存
        nTools.deleteLoginCache();
        return "成功";
    }

    @Override
    public List<TWorkRoomMessage> wechatMiniMessageHistoryListByWorkRoom(Long workRoomId) {
        String miniOpenId = nTools.getMiniOpenId();
        return wechatMiniMapper.selectTWorkRoomMessageByMiniOpenIdAndWorkRoomId(miniOpenId, workRoomId);
    }

    @Override
    public List<WechatMiniArticleVo> wechatMiniOneselfArticle() {
        String miniOpenId = nTools.getMiniOpenId();
        return wechatMiniMapper.wechatMiniOneselfArticle(miniOpenId);
    }

    @Override
    public WechatMiniArticleVo wechatMiniWorkRoomShowArticle(Long workRoomId) {
        WechatMiniArticleVo wechatMiniArticleVo = wechatMiniMapper.wechatMiniWorkRoomShowArticle(workRoomId);
        if (wechatMiniArticleVo == null) {
            throw new RuntimeException("无文章");
        }
        return wechatMiniArticleVo;
    }

    @Override
    public List<WechatMiniArticleVo> selectOwnArticleList(Long workRoomId) {
        String miniOpenId = nTools.getMiniOpenId();
        return wechatMiniMapper.selectOwnArticleList(workRoomId, miniOpenId);
    }

    @Override
    public List<WorkRoomGetMember> selectWorkRoomGetMemberList(Long workRoomId) {
        List<WorkRoomGetMember> workRoomGetMembers = wechatMiniMapper.selectWorkRoomGetMemberList(workRoomId);
        for (int i = 0; i < workRoomGetMembers.size(); i++) {
            if ("学生".equals(workRoomGetMembers.get(i).getJoinType())) {
                String[] tSubjects = wechatMiniMapper.selectObjectsByStudentId(workRoomGetMembers.get(i).getTargetId());
                workRoomGetMembers.get(i).setSubject(NTools.arrayToStr(tSubjects));
            }
            if ("教师".equals(workRoomGetMembers.get(i).getJoinType())) {
                String[] tSubjects = wechatMiniMapper.selectObjectsByTeacherId(workRoomGetMembers.get(i).getTargetId());
                workRoomGetMembers.get(i).setSubject(NTools.arrayToStr(tSubjects));
            }
        }
        return workRoomGetMembers;
    }

    @Override
    public String wechatMiniCheckJoinOrNotWorkRoom(Long workRoomId, String joinType) {
        LoginVo loginInfo = nTools.getLoginInfo();
        Long id = null;
        if ("用户".equals(joinType)) {
            id = loginInfo.getId();
        }
        if ("教师".equals(joinType)) {
            id = loginInfo.getTeacherInfo().getTeacherId();
        }
        if ("学生".equals(joinType)) {
            id = loginInfo.getStudentInfo().getStudentId();
        }
        String s = wechatMiniMapper.wechatMiniCheckJoinOrNotWorkRoom(id, workRoomId, joinType);
        if (s == null) {
            return "未加入";
        }
        return s;
    }

    @Override
    public WorkRoomMemberDetails wechatMiniWorkRoomMemberDetails(String id, String type) {
        WorkRoomMemberDetails workRoomMemberDetails = new WorkRoomMemberDetails();

        workRoomMemberDetails.setType(type);
        if ("用户".equals(type)) {
            TUser tUser = tUserMapper.selectTUserById(Long.valueOf(id));
            GetUserInfo getUserInfo = new GetUserInfo();
            getUserInfo
                    .setPhoto(tUser.getUserPhoto())
                    .setGender(tUser.getUserGender())
                    .setUsername(tUser.getUserName())
                    .setEmail(tUser.getUserEmail())
                    .setBirthDay(tUser.getUserBirth())
                    .setPhone(tUser.getUserPhone())
                    .setIntro(tUser.getUserIntro());
            workRoomMemberDetails.setData(getUserInfo);
        }
        if ("学生".equals(type)) {
            GetStudentInfo getStudentInfo = new GetStudentInfo();
            TStudent tStudent = tStudentMapper.selectTStudentById(Long.valueOf(id));
            getStudentInfo
                    .setStudentName(tStudent.getStudentName())
                    .setSignupTime(tStudent.getCreatedTime())
                    .setBirthDay(tStudent.getStudentBirthday())
                    .setStudyAge(tStudent.getStudentAge())
                    .setPhone(tStudent.getStudentPhone())
                    .setEmail(tStudent.getStudentEmail())
                    .setTargetScore(tStudent.getStudentTargetScore())
                    .setStudentIntro(tStudent.getStudentIntro())
                    .setNeedSubjectList(wechatMiniMapper.selectTSubjectByStudentId(tStudent.getId()));
            workRoomMemberDetails.setData(getStudentInfo);
        }
        if ("教师".equals(type)) {
            GetTeacherInfo getTeacherInfo = new GetTeacherInfo();
            TTeacher tTeacher = tTeacherMapper.selectTTeacherById(Long.valueOf(id));
            getTeacherInfo
                    .setTeacherMajor(tTeacher.getTeacherMajor())
                    .setTeacherIntro(tTeacher.getTeacherIntro())
                    .setTeacherEducation(tTeacher.getTeacherEducation())
                    .setTeacherExperience(tTeacher.getTeacherExperience())
                    .setTeacherParty(tTeacher.getTeacherParty())
                    .setTeacherName(tTeacher.getTeacherName())
                    .setTeacherPicture(tTeacher.getTeacherPicture())
                    .setTeacherNationality(tTeacher.getTeacherNationality())
                    .setTeacherSchool(tTeacher.getTeacherSchool())
                    .setTeacherEmail(tTeacher.getTeacherEmail())
                    .setTeacherPhone(tTeacher.getTeacherPhone())
                    .setTeahcerCountryregion(tTeacher.getTeahcerCountryregion())
                    .setSubjectList(wechatMiniMapper.selectTSubjectByTeacherId(tTeacher.getId()));
            workRoomMemberDetails.setData(getTeacherInfo);
        }
        return workRoomMemberDetails;
    }

    @Override
    public String wechatMiniQuitWorkRoom(String id, String workRoomId, String joinType) {
        Integer integer = wechatMiniMapper.wechatMiniQuitWorkRoom(nTools.getTeacherInfo().getTeacherId(), workRoomId, "教师");
        if (integer == 0) {
            throw new RuntimeException("失败");
        }
        return "成功";
    }

    @Override
    public List<OwnWorkRoomSearchVo> wechatMiniOwnCreateWorkRoom() {
        //todo 我创建的工作室列表
        Long teacherId = nTools.getLoginInfo().getTeacherInfo().getTeacherId();
        //teacherId
        TWorkRoom tWorkRoom1 = new TWorkRoom();
        tWorkRoom1.setPrincipalTeacherId(teacherId);

        List<TWorkRoom> tWorkRooms = wechatMiniMapper.selectTWorkRoomList(teacherId);

        List<OwnWorkRoomSearchVo> workRoomSearchVoList = new ArrayList<>();
        for (TWorkRoom tWorkRoom : tWorkRooms) {
            OwnWorkRoomSearchVo workRoomSearchVo = new OwnWorkRoomSearchVo();
            workRoomSearchVo.setOnlineStatus(UserConsultServer.getWorkRoomOnlineStatus(tWorkRoom.getId()));
            workRoomSearchVo.setMsgNum(0l);
            workRoomSearchVo.setWorkRoomName(tWorkRoom.getWorkRoomName());
            workRoomSearchVo.setWorkRoomId(tWorkRoom.getId());
            workRoomSearchVo.setPrincipalTeacherId(tWorkRoom.getPrincipalTeacherId());
            workRoomSearchVo.setConsultTeacherId(tWorkRoom.getConsultTeacherId());//咨询教师
            workRoomSearchVo.setSubjectList(wechatMiniMapper.selectTSubjectByWorkRoomId(tWorkRoom.getId()));
            workRoomSearchVo.setIntro(tWorkRoom.getWorkRoomIntro());
            workRoomSearchVo.setPhone(tWorkRoom.getWorkRoomPhone());
            workRoomSearchVo.setEmail(tWorkRoom.getWorkRoomEmail());
            workRoomSearchVo.setWorkRoomId(tWorkRoom.getId());
            workRoomSearchVo.setArticleId(tWorkRoom.getShowArticleId());
            workRoomSearchVo.setWorkRoomIcon(tWorkRoom.getWorkRoomIcon());
            workRoomSearchVo.setWorkRoomImage(tWorkRoom.getWorkRoomImage());
            workRoomSearchVo.setWorkRoomLink(tWorkRoom.getWorkRoomLink());
            TArticle tArticle = tArticleMapper.selectTArticleById(tWorkRoom.getShowArticleId());
            workRoomSearchVo.setArticleTitle(tArticle.getArticleTitle());
            workRoomSearchVo.setArticleContent(tArticle.getArticleContent());

            workRoomSearchVoList.add(workRoomSearchVo);

        }
        return workRoomSearchVoList;
    }

    @Override
    public SignUpRecordVo wechatMiniSignUpRecord() {

        LoginVo loginInfo = nTools.getLoginInfo();
        // 查询学生报名记录
        List<TStudent> tStudents = wechatMiniMapper.selectTStudentSByWXMiniOpenId(loginInfo.getWechatMiniOpenId());
        // 查询教师报名记录
        List<TTeacher> tTeachers = wechatMiniMapper.selectTTeachersByOpenId(loginInfo.getWechatMiniOpenId());
        SignUpRecordVo signUpRecordVo = new SignUpRecordVo();
        signUpRecordVo.setStudents(tStudents);
        signUpRecordVo.setTeachers(tTeachers);

        return signUpRecordVo;
    }

    @Override
    public WechatMiniArticleVo wechatMiniStudentLookOut() {
        List<WechatMiniArticleVo> wechatMiniArticleVos = wechatMiniMapper.selectArticleByTypeName("学生注意事项");
        if (wechatMiniArticleVos.size() == 0) {
            throw new RuntimeException("该分类下无文章");
        }
        return wechatMiniArticleVos.get(0);
    }

    @Override
    public WechatMiniArticleVo wechatMiniTeacherLookOut() {
        List<WechatMiniArticleVo> wechatMiniArticleVos = wechatMiniMapper.selectArticleByTypeName("教师注意事项");
        if (wechatMiniArticleVos.size() == 0) {
            throw new RuntimeException("该分类下无文章");
        }
        return wechatMiniArticleVos.get(0);
    }

    @Override
    public String wechatMiniSelfUpdateTeacherInfo(SelfUpdateTeacherInfoDo selfUpdateTeacherInfoDo) {
        LoginVo.TeacherInfo teacherInfo = nTools.getTeacherInfo();
        if (teacherInfo == null) {
            throw new RuntimeException("账号教师信息错误");
        }
        TTeacher tTeacher = new TTeacher();
        tTeacher.setId(teacherInfo.getTeacherId());
        tTeacher.setTeacherName(selfUpdateTeacherInfoDo.getTeacherName());
        tTeacher.setTeacherIntro(selfUpdateTeacherInfoDo.getTeacherIntro());
        tTeacher.setTeacherPicture(selfUpdateTeacherInfoDo.getTeacherPicture());
        tTeacher.setTeacherPhone(selfUpdateTeacherInfoDo.getTeacherPhone());
        tTeacher.setTeacherEmail(selfUpdateTeacherInfoDo.getTeacherEmail());
        tTeacher.setTeacherSchool(selfUpdateTeacherInfoDo.getTeacherSchool());
        tTeacher.setTeacherMajor(selfUpdateTeacherInfoDo.getTeacherMajor());
        tTeacher.setTeacherEducation(selfUpdateTeacherInfoDo.getTeacherEducation());
        tTeacher.setTeahcerCountryregion(selfUpdateTeacherInfoDo.getTeahcerCountryregion());
        tTeacher.setTeacherParty(selfUpdateTeacherInfoDo.getTeacherParty());
        tTeacher.setTeacherNationality(selfUpdateTeacherInfoDo.getTeacherNationality());
        tTeacher.setTeacherExperience(selfUpdateTeacherInfoDo.getTeacherExperience());
        tTeacher.setReferrerCode(selfUpdateTeacherInfoDo.getReferrerCode());
        int i = tTeacherMapper.updateTTeacher(tTeacher);
        if (i > 0) {
            // 清除用户数据缓存
            nTools.deleteLoginCache();
            return "修改成功";
        }
        return "修改失败";
    }

    @Override
    public String wechatMiniSelfUpdateStudentInfo(SelfUpdateStudentInfoDo selfUpdateStudentInfoDo) {
        LoginVo.StudentInfo studentInfo = nTools.getStudentInfo();
        if (studentInfo == null) {
            throw new RuntimeException("账号学生信息错误");
        }
        TStudent tStudent = new TStudent();
        tStudent.setId(studentInfo.getStudentId());
        tStudent.setStudentIntro(selfUpdateStudentInfoDo.getStudentIntro());
        tStudent.setStudentName(selfUpdateStudentInfoDo.getStudentName());
        tStudent.setStudentBirthday(selfUpdateStudentInfoDo.getStudentBirthday());
        tStudent.setStudentAge(selfUpdateStudentInfoDo.getStudentAge());
        tStudent.setStudentPhone(selfUpdateStudentInfoDo.getStudentPhone());
        tStudent.setStudentEmail(selfUpdateStudentInfoDo.getStudentEmail());
        tStudent.setStudentTargetScore(selfUpdateStudentInfoDo.getStudentTargetScore());
        int i = tStudentMapper.updateTStudent(tStudent);
        if (i > 0) {
            // 清除用户数据缓存
            nTools.deleteLoginCache();
            return "修改成功";
        }
        return "修改失败";
    }

    @Override
    public List<GetArticleList> bbsGetArticleList(Integer page, Integer size, Long typeId) {
        // todo
        PageHelper.startPage(page, size);
        List<GetArticleList> getArticleLists = wechatMiniMapper.selectGetArticleListById(typeId);

//        PageInfo<GetArticleList> pageInfo = new PageInfo<>(getArticleLists);

        return getArticleLists;
    }

    @Override
    public List<GetArticleList> bbsGetNewArticleList(String page, String size) {
        // todo
        // todo 从缓存
        return null;
    }

    @Override
    public SignUpTeacherInfoVo ShowTeacherSignUpInfo(String token) {
        // todo 从缓存中获取数据
        Long teacherId = (Long) redisTemplate.opsForValue().get("wxmini:signUp:" + token);
        if (teacherId == null) {
            throw new RuntimeException("令牌失效");
        }
        SignUpTeacherInfoVo signUpTeacherInfoVo = new SignUpTeacherInfoVo();
        TTeacher tTeacher = tTeacherMapper.selectTTeacherById(teacherId);
        if (tTeacher == null) {
            throw new RuntimeException("教师信息为空");
        }

        // 填充教师数据
        signUpTeacherInfoVo.setId(teacherId);
        signUpTeacherInfoVo.setUserId(tTeacher.getUserId());
        signUpTeacherInfoVo.setTeacherName(tTeacher.getTeacherName());
        signUpTeacherInfoVo.setTeacherCode(tTeacher.getTeacherCode());
        signUpTeacherInfoVo.setTeacherScore(tTeacher.getTeacherScore());
        signUpTeacherInfoVo.setTeacherGender(tTeacher.getTeacherGender());
        signUpTeacherInfoVo.setTeacherBirthday(tTeacher.getTeacherBirthday());
        signUpTeacherInfoVo.setTeacherIntro(tTeacher.getTeacherIntro());
        signUpTeacherInfoVo.setTeacherPicture(tTeacher.getTeacherPicture());
        signUpTeacherInfoVo.setTeacherPhone(tTeacher.getTeacherPhone());
        signUpTeacherInfoVo.setTeacherEmail(tTeacher.getTeacherEmail());
        signUpTeacherInfoVo.setTeacherSchool(tTeacher.getTeacherSchool());
        signUpTeacherInfoVo.setTeacherMajor(tTeacher.getTeacherMajor());
        signUpTeacherInfoVo.setTeacherEducation(tTeacher.getTeacherEducation());
        signUpTeacherInfoVo.setTeacherInformedBrief(tTeacher.getTeacherInformedbrief());
        signUpTeacherInfoVo.setTeacherSignature(tTeacher.getTeacherSignature());
        signUpTeacherInfoVo.setTeacherParty(tTeacher.getTeacherParty());
        signUpTeacherInfoVo.setTeacherNationality(tTeacher.getTeacherNationality());
        signUpTeacherInfoVo.setTeacherExperience(tTeacher.getTeacherExperience());
        signUpTeacherInfoVo.setTeacherRemark(tTeacher.getTeacherRemark());
        signUpTeacherInfoVo.setTeacherStatus(tTeacher.getTeahcerStatus());
        signUpTeacherInfoVo.setAuditStatus(tTeacher.getAuditStatus());
        signUpTeacherInfoVo.setCreatedTime(tTeacher.getCreatedTime());
        signUpTeacherInfoVo.setTeacherCountryRegion(tTeacher.getTeahcerCountryregion());
        // 引荐用户数据
        if (tTeacher.getReferrerUserId() != null) {
            TUser tUser = tUserMapper.selectTUserById(tTeacher.getReferrerUserId());
            signUpTeacherInfoVo.setReferrerUserName(tUser.getUserName());
        }
        // 教师证书数据
        TRelationTeacherCertificates relationTeacherCertificates = new TRelationTeacherCertificates();
        relationTeacherCertificates.setTeacherId(teacherId);
        List<TTeacherCertificates> teacherCertificates = wechatMiniMapper.selectTTeacherCertificatesListByTeacherId(teacherId);
        List<String> certificateNameList = new ArrayList<>();
        if (!teacherCertificates.isEmpty()) {
            for (TTeacherCertificates teacherCertificate : teacherCertificates) {
                certificateNameList.add(teacherCertificate.getCertificatesName());
            }
        }
        signUpTeacherInfoVo.setTeacherCertificate(certificateNameList);
        // 教师教学科目
        List<Subject> subjectList = wechatMiniMapper.selectSubjectByTeacherId(teacherId);
        for (int i = 0; i < subjectList.size(); i++) {
            if (subjectList.get(i).getTeachingYear() == null) {
                subjectList.get(i).setTeachingYear("");
            }
            if (subjectList.get(i).getTeachingRemark() == null) {
                subjectList.get(i).setTeachingRemark("");
            }
        }
        signUpTeacherInfoVo.setSubjectList(subjectList);
        // 教学时段数据
        TTeacherSchedule tTeacherSchedule = new TTeacherSchedule();
        tTeacherSchedule.setTeacherId(teacherId);
        List<TTeacherSchedule> tTeacherSchedules = tTeacherScheduleMapper.selectTTeacherScheduleList(tTeacherSchedule);
        if (!tTeacherSchedules.isEmpty()) {
            List<SignUpTeacherInfoVo.TeacherSchedule> teacherScheduleList = new ArrayList<>();
            for (TTeacherSchedule teacherSchedule : tTeacherSchedules) {
                SignUpTeacherInfoVo.TeacherSchedule teacherScheduleVo = new SignUpTeacherInfoVo.TeacherSchedule();
                teacherScheduleVo.setStartTime(teacherSchedule.getScheduleStartTime());
                teacherScheduleVo.setEndTime(teacherSchedule.getScheduleEndTime());
                teacherScheduleVo.setScheduleMonth(teacherSchedule.getScheduleMonth());
                teacherScheduleVo.setScheduleWeek(teacherSchedule.getScheduleWeek());
                teacherScheduleVo.setPlanNote(teacherSchedule.getPlanNote());
                teacherScheduleList.add(teacherScheduleVo);
            }
            signUpTeacherInfoVo.setTeacherScheduleList(teacherScheduleList);
        }

        //查询个人优势
        List<TTeacherAdvantagesType> tTeacherAdvantagesTypes = wechatMiniMapper.SelectTeacherAdvantagesTypeByTeacherId(teacherId);
        if (!tTeacherAdvantagesTypes.isEmpty()) {
            List<SignUpTeacherInfoVo.TeacherAdvantage> teacherAdvantage = new ArrayList<>();
            for (TTeacherAdvantagesType TeacherAdvantagesType : tTeacherAdvantagesTypes) {
                SignUpTeacherInfoVo.TeacherAdvantage teacherAdvantages = new SignUpTeacherInfoVo.TeacherAdvantage();
                teacherAdvantages.setTeacherAdvantage(TeacherAdvantagesType.getPersonalAdvantages());
                teacherAdvantages.setAdvantageRemark(TeacherAdvantagesType.getAdvantagesRemark());
                teacherAdvantage.add(teacherAdvantages);
            }
            signUpTeacherInfoVo.setTeacherAdvantageList(teacherAdvantage);
        }


        return signUpTeacherInfoVo;
    }

    @Override
    public SignUpStudentInfoVo ShowStudentSignUpInfo(String token) {
        // todo 从缓存中获取数据
        Long studentId = (Long) redisTemplate.opsForValue().get("wxmini:signUp:" + token);
        if (studentId == null) {
            throw new RuntimeException("令牌失效");
        }
        SignUpStudentInfoVo signUpStudentInfoVo = new SignUpStudentInfoVo();
        TStudent tStudent = tStudentMapper.selectTStudentById(studentId);
        if (tStudent == null) {
            throw new RuntimeException("学生信息为空");
        }
        // 填充学生数据
        signUpStudentInfoVo.setId(tStudent.getId());
        signUpStudentInfoVo.setUserId(tStudent.getUserId());
        signUpStudentInfoVo.setStudentIntro(tStudent.getStudentIntro());
        signUpStudentInfoVo.setStudentCode(tStudent.getStudentCode());
        signUpStudentInfoVo.setStudentName(tStudent.getStudentName());
        signUpStudentInfoVo.setStudentBirthday(tStudent.getStudentBirthday());
        signUpStudentInfoVo.setStudentGender(tStudent.getStudentGender());
        signUpStudentInfoVo.setStudentAge(tStudent.getStudentAge());
        signUpStudentInfoVo.setStudentPhone(tStudent.getStudentPhone());
        signUpStudentInfoVo.setStudentEmail(tStudent.getStudentEmail());
        signUpStudentInfoVo.setStudentTargetScore(tStudent.getStudentTargetScore());
        signUpStudentInfoVo.setStudentInformedConsent(tStudent.getStudentInformedConsent());
        signUpStudentInfoVo.setStudentHandwrittenSignature(tStudent.getStudentHandwrittenSignature());
        signUpStudentInfoVo.setAuditStatus(tStudent.getAuditStatus());
        signUpStudentInfoVo.setCreatedTime(tStudent.getCreatedTime());
        // 学生需要强化科目
        List<SignUpStudentInfoVo.StudentNeedSubject> studentNeedSubjectList = new ArrayList<>();
        TStudentIntensify tStudentIntensify = new TStudentIntensify();
        tStudentIntensify.setStudentId(studentId);
        List<TStudentIntensify> tStudentIntensifies = tStudentIntensifyMapper.selectTStudentIntensifyList(tStudentIntensify);
        if (tStudentIntensifies != null && !tStudentIntensifies.isEmpty()) {
            for (TStudentIntensify studentIntensify : tStudentIntensifies) {
                try {
                    SignUpStudentInfoVo.StudentNeedSubject studentNeedSubject = new SignUpStudentInfoVo.StudentNeedSubject();
                    TSubject tSubject = tSubjectMapper.selectTSubjectById(studentIntensify.getSubjectId());
                    studentNeedSubject.setSubjectId(tSubject.getId());
                    studentNeedSubject.setSubjectName(tSubject.getSubjectName());
                    studentNeedSubject.setLearningProgress("null");
                    studentNeedSubject.setCurrentScore((long) -1);
                    studentNeedSubject.setTargetScore((long) -1);
                    if (studentIntensify.getStudentRate() != null) {
                        studentNeedSubject.setLearningProgress(studentIntensify.getStudentRate());
                    }
                    if (studentIntensify.getCurrentScore() != null) {
                        studentNeedSubject.setCurrentScore(studentIntensify.getCurrentScore());
                    }
                    if (studentIntensify.getTargetScore() != null) {
                        studentNeedSubject.setTargetScore(studentIntensify.getTargetScore());
                    }
                    studentNeedSubjectList.add(studentNeedSubject);
                } catch (Exception ignored) {
                }
            }
        }
        signUpStudentInfoVo.setStudentNeedSubject(studentNeedSubjectList);
        return signUpStudentInfoVo;
    }

    //todo
    @Override
    public String wechatMiniAuditResult(WechatMiniAuditResult wechatMiniAuditResult) {
        if ("学生".equals(wechatMiniAuditResult.getType())) {
            TStudent tStudent = new TStudent();
            tStudent.setAuditStatus(wechatMiniAuditResult.getResult());
            tStudent.setId(wechatMiniAuditResult.getId());
            tStudent.setAuditResult(wechatMiniAuditResult.getResultStr());
            int i = tStudentMapper.updateTStudent(tStudent);
            if (i == 0) {
                throw new RuntimeException("更新失败");
            }
            MiniAuditData miniAuditData = new MiniAuditData();
            miniAuditData.setPhrase1(wechatMiniAuditResult.getName());
            miniAuditData.setPhrase1(wechatMiniAuditResult.getResult());
            miniAuditData.setThing3(wechatMiniAuditResult.getResultStr());
            miniAuditData.setTime2(NTools.createTime());
            String openId = wechatMiniMapper.selectOpenIdByTableId("t_student", wechatMiniAuditResult.getId());

            try {
                wxUtil.weChatMiniMsg(miniAuditData.msgTemplateData(openId, custom.AuditSuccess, custom.wechatMiniInfoLang,custom.wechatMiniPage,custom.miniprogramState));
            } catch (Exception e) {
                throw new RuntimeException("微信通知失败");
            }

        }
        if ("教师".equals(wechatMiniAuditResult.getType())) {
            TTeacher tTeacher = new TTeacher();
            tTeacher.setAuditStatus(wechatMiniAuditResult.getResult());
            tTeacher.setId(Long.valueOf(wechatMiniAuditResult.getId()));
            tTeacher.setAuditResult(wechatMiniAuditResult.getResultStr());
            int i = tTeacherMapper.updateTTeacher(tTeacher);
            if (i == 0) {
                throw new RuntimeException("更新失败");
            }
            MiniAuditData miniAuditData = new MiniAuditData();
            miniAuditData.setPhrase1(wechatMiniAuditResult.getName());
            miniAuditData.setPhrase1(wechatMiniAuditResult.getResult());
            miniAuditData.setThing3(wechatMiniAuditResult.getResultStr());
            miniAuditData.setTime2(NTools.createTime());
            String openId = wechatMiniMapper.selectOpenIdByTableId("t_teacher", wechatMiniAuditResult.getId());
            try {
                wxUtil.weChatMiniMsg(miniAuditData.msgTemplateData(openId, custom.AuditSuccess, custom.wechatMiniInfoLang,custom.wechatMiniPage,custom.miniprogramState));
            } catch (Exception e) {
                throw new RuntimeException("微信通知失败");
            }

        }
        return "参数不正确";
    }

    @Override
    @Transactional
    public String wechatMiniDeleteOwnWorkRoom(Long workRoomId) {
        TWorkRoom tWorkRoom = tWorkRoomMapper.selectTWorkRoomById(workRoomId);
        if (tWorkRoom == null) {
            throw new RuntimeException("工作室信息异常");
        }
        Long teacherId = nTools.getTeacherInfo().getTeacherId();
        if (Objects.equals(tWorkRoom.getPrincipalTeacherId(), teacherId)) {
            tWorkRoom.setDeleteTime(new Date());
            int i = tWorkRoomMapper.updateTWorkRoom(tWorkRoom);
            //删除关联的文章
            Long showArticleId = tWorkRoom.getShowArticleId();
            TArticle tArticle = new TArticle();
            tArticle.setId(showArticleId);
            tArticle.setDeleteTime(new Date());
            tArticleMapper.updateTArticle(tArticle);
            //删除教师信息卡
            TWorkRoomTeacherInfo tWorkRoomTeacherInfo = new TWorkRoomTeacherInfo();
            tWorkRoomTeacherInfo.setWorkRoomId(tWorkRoom.getId());
            List<TWorkRoomTeacherInfo> tWorkRoomTeacherInfos = tWorkRoomTeacherInfoMapper.selectTWorkRoomTeacherInfoList(tWorkRoomTeacherInfo);
            if (tWorkRoomTeacherInfos.size() != 0) {
                for (int i1 = 0; i1 < tWorkRoomTeacherInfos.size(); i1++) {
                    tWorkRoomTeacherInfo = new TWorkRoomTeacherInfo();
                    tWorkRoomTeacherInfo.setId(tWorkRoomTeacherInfos.get(i).getId());
                    tWorkRoomTeacherInfo.setDeleteTime(new Date());
                    tWorkRoomTeacherInfoMapper.updateTWorkRoomTeacherInfo(tWorkRoomTeacherInfo);
                }
            }
            //删除工作室用户关系表
            TRelationWorkRoomUser tRelationWorkRoomUser = new TRelationWorkRoomUser();
            tRelationWorkRoomUser.setWorkRoomId(tWorkRoom.getId());
            List<TRelationWorkRoomUser> tRelationWorkRoomUsers = tRelationWorkRoomUserMapper.selectTRelationWorkRoomUserList(tRelationWorkRoomUser);
            if (tRelationWorkRoomUsers.size() != 0) {
                for (int j = 0; j < tRelationWorkRoomUsers.size(); j++) {
                    tRelationWorkRoomUser = new TRelationWorkRoomUser();
                    tRelationWorkRoomUser.setId(tRelationWorkRoomUsers.get(j).getId());
                    tRelationWorkRoomUser.setDeleteTime(new Date());
                    tRelationWorkRoomUserMapper.updateTRelationWorkRoomUser(tRelationWorkRoomUser);
                }
            }

            if (i > 0) {
                return "删除成功";
            }
        }
        return "删除失败";
    }

    @Override
    @Transactional
    public String wechatMiniUpdateWorkRoom(UpdateWorkRoomDo updateWorkRoomDo) {
        if (updateWorkRoomDo.getWorkRoomId() == null) {
            throw new RuntimeException("工作室信息错误");
        }
        TWorkRoom tWorkRoom = tWorkRoomMapper.selectTWorkRoomById(updateWorkRoomDo.getWorkRoomId());
        if (tWorkRoom == null) {
            throw new RuntimeException("工作室信息错误");
        }
        tWorkRoom.setWorkRoomName(updateWorkRoomDo.getWorkRoomName());
        tWorkRoom.setWorkRoomLink(updateWorkRoomDo.getWorkRoomLink());
        tWorkRoom.setWorkRoomPhone(updateWorkRoomDo.getWorkRoomPhone());
        tWorkRoom.setWorkRoomEmail(updateWorkRoomDo.getWorkRoomEmail());
        tWorkRoom.setWorkRoomDetails(updateWorkRoomDo.getWorkRoomDetails());
        tWorkRoom.setWorkRoomIcon(updateWorkRoomDo.getWorkRoomIcon());
        tWorkRoom.setWorkRoomImage(updateWorkRoomDo.getWorkRoomImage());
        tWorkRoom.setWorkRoomIntro(updateWorkRoomDo.getWorkRoomIntro());
        int i = tWorkRoomMapper.updateTWorkRoom(tWorkRoom);
        if (i == 0) {
            throw new RuntimeException("工作室信息修改失败");
        }
        // 修改文章数据
        Long showArticleId = tWorkRoom.getShowArticleId();
        if (showArticleId == null) {
            throw new RuntimeException("文章数据异常");
        }
        TArticle tArticle = tArticleMapper.selectTArticleById(showArticleId);
        tArticle.setArticleTitle(updateWorkRoomDo.getArticleTitle());
        tArticle.setArticleContent(updateWorkRoomDo.getArticleContent());
        int i1 = tArticleMapper.updateTArticle(tArticle);
        //todo 修改工作室科目

        //传上来的记录
        List<Long> subjectIdList = updateWorkRoomDo.getSubjectIdList();
        if (updateWorkRoomDo.getSubjectIdList() != null && !updateWorkRoomDo.getSubjectIdList().isEmpty()) {
            TRelationWorkRoomSubject tRelationWorkRoomSubject = new TRelationWorkRoomSubject();
            tRelationWorkRoomSubject.setWorkRoomId(tWorkRoom.getId());
            //原记录
            List<TRelationWorkRoomSubject> tRelationWorkRoomSubjects = tRelationWorkRoomSubjectMapper.selectTRelationWorkRoomSubjectList(tRelationWorkRoomSubject);
            //判断原记录是否为空
            if (tRelationWorkRoomSubjects != null && !tRelationWorkRoomSubjects.isEmpty()) {
                //相同的科目id
                for (int j = 0; j < subjectIdList.size(); j++) {
                    for (int i2 = 0; i2 < tRelationWorkRoomSubjects.size(); i2++) {
                        if (subjectIdList.get(j).equals(tRelationWorkRoomSubjects.get(i2).getSubjectId())) {
                            subjectIdList.remove(subjectIdList.get(j));
                            tRelationWorkRoomSubjects.remove(tRelationWorkRoomSubjects.get(i2));
                        }
                    }
                }
                //删除原表的关系
                for (int i2 = 0; i2 < tRelationWorkRoomSubjects.size(); i2++) {
                    tRelationWorkRoomSubjects.get(i2).setDeleteTime(new Date());
                    tRelationWorkRoomSubjectMapper.updateTRelationWorkRoomSubject(tRelationWorkRoomSubjects.get(i2));
                }

            }

            //新增新的关系
            for (int i2 = 0; i2 < subjectIdList.size(); i2++) {
                tRelationWorkRoomSubject.setSubjectId(subjectIdList.get(i2));
                tRelationWorkRoomSubjectMapper.insertTRelationWorkRoomSubject(tRelationWorkRoomSubject);
            }

        }

        if (i1 > 0 && i > 0) {
            return "工作室信息修改成功";
        }
        return "修改失败";
    }

    @Override
    public List<OwnJoinWorkRoomVo> wechatMiniOwnJoinWorkRoom() {
        //教师id
        Long teacherId = nTools.getTeacherInfo().getTeacherId();
        //查询记录
        List<OwnJoinWorkRoomVo> ownJoinWorkRoomVos = wechatMiniMapper.wechatMiniOwnJoinWorkRoom(teacherId);
        return ownJoinWorkRoomVos;
    }


    @Override
    public String wechatMiniUpdateWorkRoomTeacherName(String workRoomId, String teacherName) {
        //教师id
        Long teacherId = nTools.getTeacherInfo().getTeacherId();
        Integer integer = wechatMiniMapper.wechatMiniUpdateWorkRoomTeacherName(teacherName, workRoomId, String.valueOf(teacherId));
        if (integer == 0) {
            throw new RuntimeException("修改失败");
        }
        return "修改成功";
    }

    @Override
    public TWorkRoomTeacherInfo wechatMiniWorkRoomTeaCardDetails(Long id) {
        TWorkRoomTeacherInfo tWorkRoomTeacherInfo = tWorkRoomTeacherInfoMapper.selectTWorkRoomTeacherInfoById(id);
        tWorkRoomTeacherInfo.setDeleteTime(null);
        tWorkRoomTeacherInfo.setCreatedTime(null);
        tWorkRoomTeacherInfo.setUpdatedTime(null);
        tWorkRoomTeacherInfo.setAuditStatus(null);
        tWorkRoomTeacherInfo.setHiddenStatus(null);
        return tWorkRoomTeacherInfo;
    }

    @Override
    public String wechatMiniSendCustomerService(String workRoomName) {
        LoginVo loginInfo = nTools.getLoginInfo();
        try {
            nTools.sendCustomerService(loginInfo.getWechatMiniOpenId(), "来自工作室[" + workRoomName + "]的咨询消息");
        } catch (IOException e) {
            throw new RuntimeException("发送失败");
        }
        return "发送成功";
    }

    @Override
    public String wechatMiniUserReadPrivacyPolicy() {
        LoginVo loginInfo = nTools.getLoginInfo();
        // 用户数据更新
        TUser tUser = tUserMapper.selectTUserById(loginInfo.getId());
        tUser.setReadPrivacyPolicy("是");
        int i = tUserMapper.updateTUser(tUser);
        if (i == 0) {
            throw new RuntimeException("阅读失败, 用户数据更新异常");
        }
        // 清除用户登入数据
        nTools.deleteLoginCache();
        return "阅读成功";
    }

    @Override
    public List<GetCourse> wechatMiniStudentCourse(String date) {
//        Long studentId = nTools.getStudentInfo().getStudentId();
//        if(studentId==null){
//            throw new RuntimeException("学生id为空");
//        }
        Long studentId = 1l;
        List<String> strings = null;
        try {
            strings = NTools.printWeekdays(date);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        List<GetCourse> getCourses = new ArrayList<>();
        for (int i = 0; i < strings.size(); i++) {
            GetCourse getCourse = new GetCourse();
            getCourse.setDateTime(strings.get(i));
            List<TCourse> tCourses = wechatMiniMapper.wechatMiniStudentCourse(studentId, strings.get(i));
            if (tCourses.size() == 0) {
                getCourses.add(getCourse);
                continue;
            }

            List<GetCourse.CourseDTO> course = new ArrayList<>();
            for (int i1 = 0; i1 < tCourses.size(); i1++) {
                GetCourse.CourseDTO courseDTOs = new GetCourse.CourseDTO();
                courseDTOs.setStartTime(tCourses.get(i1).getCourseBegin());
                courseDTOs.setEndTime(tCourses.get(i1).getCourseFinish());
                List<String> subjectNameByIds = wechatMiniMapper.getSubjectNameByIds(tCourses.get(i1).getId());
                courseDTOs.setSubjectStrs(subjectNameByIds);
                course.add(courseDTOs);
            }
            getCourse.setCourse(course);
            getCourses.add(getCourse);
        }
        return getCourses;
    }

    @Override
    public List<GetCourse> wechatMiniTeacherCourse(String date) {
//        Long teacherId = nTools.getTeacherInfo().getTeacherId();
//        if (teacherId == null) {
//            throw new RuntimeException("教师id为空");
//        }
        Long teacherId = 1l;
        List<String> strings = null;
        try {
            strings = NTools.printWeekdays(date);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
        List<GetCourse> getCourses = new ArrayList<>();
        for (int i = 0; i < strings.size(); i++) {
            GetCourse getCourse = new GetCourse();
            getCourse.setDateTime(strings.get(i));
            List<TCourse> tCourses = wechatMiniMapper.wechatMiniTeacherCourse(teacherId, strings.get(i));
            if (tCourses.size() == 0) {
                getCourses.add(getCourse);
                continue;
            }

            List<GetCourse.CourseDTO> course = new ArrayList<>();
            for (int i1 = 0; i1 < tCourses.size(); i1++) {
                GetCourse.CourseDTO courseDTOs = new GetCourse.CourseDTO();
                courseDTOs.setStartTime(tCourses.get(i1).getCourseBegin());
                courseDTOs.setEndTime(tCourses.get(i1).getCourseFinish());
                List<String> subjectNameByIds = wechatMiniMapper.getSubjectNameByIds(tCourses.get(i1).getId());
                courseDTOs.setSubjectStrs(subjectNameByIds);
                course.add(courseDTOs);
            }
            getCourse.setCourse(course);
            getCourses.add(getCourse);
        }
        return getCourses;

    }

    @Override
    public String wechatMiniGetVisitNum() {
        TConfig tConfig = new TConfig();
        tConfig.setConfigKey("访问数");
        List<TConfig> tConfigs = configMapper.selectTConfigList(tConfig);
        if (!tConfigs.isEmpty() && tConfigs.get(0) != null){
            return tConfigs.get(0).getConfigValue();
        }else {
            throw new RuntimeException("系统错误");
        }
    }

    @Override
    public String wechatMiniUserVisit() {
        TConfig tConfig = new TConfig();
        tConfig.setConfigKey("访问数");
        List<TConfig> tConfigs = configMapper.selectTConfigList(tConfig);
        if (!tConfigs.isEmpty() && tConfigs.get(0) != null){
            TConfig tConfig1 = tConfigs.get(0);
            long l = Long.parseLong(tConfig1.getConfigValue()) + 1;
            tConfig1.setConfigValue(l+"");
            int i = configMapper.updateTConfig(tConfig1);
            if (i == 0){
                throw new RuntimeException("系统错误");
            }
            return "访问成功";
        }else {
            throw new RuntimeException("系统错误");
        }
    }

    @Override
    public List<GlobalSearchVo> wechatMiniGlobalSearch(String search, Long startCount, Long endCount) {
        LoginVo loginInfo = nTools.getLoginInfo();
        Long id = 0l;
        if (loginInfo != null){
            id = loginInfo.getId();
        }
        if (startCount == null){
            startCount = 0l;
        }
        if (endCount == null){
            endCount = 0l;
        }
        return wechatMiniMapper.globalSearch(search,id, startCount, endCount);
    }

    @Override
    public String wechatMiniUserArticleRead(Long articleId) {
        LoginVo loginInfo = nTools.getLoginInfo();
        // 阅读数据做缓存
        String b = (String) redisTemplate.opsForValue().get("wxmini:user-info:" + loginInfo.getId() + ":" + articleId);
        if (b != null) {
            return "成功";
        }
        redisTemplate.opsForValue().set("wxmini:user-info:" + loginInfo.getId() + ":" + articleId, "true", 2, TimeUnit.DAYS);
        // 更新文章数据
        TArticle tArticle = tArticleMapper.selectTArticleById(articleId);
        Long articleReadNum = tArticle.getArticleReadNum();
        if (articleReadNum == null) {
            articleReadNum = 0l;
        }
        tArticle.setArticleReadNum(articleReadNum + 1);
        int i = tArticleMapper.updateTArticle(tArticle);
        if (i > 0) {
            return "成功";
        }
        return "失败";
    }


    @Override
    public String wechatMiniCreateWorkInviteCode(String workRoomId, int time) {

        //判断teacherId和openid是否为同一用户
//        String miniOpenId = nTools.getMiniOpenId();
////       String miniOpenId = "o2wmq5JJtHMh910uESWEZFHPCKrw";
//        TTeacher tTeacher = wechatMiniMapper.selectTTeacherByOpenIdAndTeacherId(miniOpenId, Long.valueOf(teacherId));
//        if (tTeacher == null) {
//            throw new RuntimeException("teacherId与openid不匹配");
//        }


        Long teacherId1 = nTools.getTeacherInfo().getTeacherId();

        //判断教师id是否为工作室测创建教师
        String roomName = wechatMiniMapper.selectWorkRoomNameByIdAndTeacherId(teacherId1, Long.valueOf(workRoomId));
        if (roomName == null) {
            throw new RuntimeException("教师与工作室负责教师不匹配");
        }
        //创建邀请码插入数据库
        //邀请码
        String inviteCode = NTools.getRandomMath();
        //插入数据库
        TWorkRoomInvite tWorkRoomInvite = new TWorkRoomInvite();
        tWorkRoomInvite.setInviteCode(inviteCode);
        tWorkRoomInvite.setWorkRoomId(Long.valueOf(workRoomId));
        tWorkRoomInvite.setCreateTeacherId(teacherId1);
        tWorkRoomInvite.setValidTime(NTools.createExpirationTime(time));
        tWorkRoomInviteMapper.insertTWorkRoomInvite(tWorkRoomInvite);
        return "邀请码创建成功";
    }

    @Override
    public String wechatMiniDeleteWorkInviteCode(String workRoomInviteId) {
        //判断teacherId和openid是否为同一用户
//        String miniOpenId = nTools.getMiniOpenId();
////        String miniOpenId = "o2wmq5JJtHMh910uESWEZFHPCKrw";
//        TTeacher tTeacher = wechatMiniMapper.selectTTeacherByOpenIdAndTeacherId(miniOpenId, Long.valueOf(teacherId));
//        if (tTeacher == null) {
//            throw new RuntimeException("teacherId与openid不匹配");
//        }

        Long teacherId1 = nTools.getTeacherInfo().getTeacherId();
        //通过邀请码id查工作室id
        Long workRoomId = wechatMiniMapper.selectWorkRoomIdByInviteId(Long.valueOf(workRoomInviteId));
        //判断教师id是否为工作室测创建教师
        String roomName = wechatMiniMapper.selectWorkRoomNameByIdAndTeacherId(teacherId1, Long.valueOf(workRoomId));
        if (roomName == null) {
            throw new RuntimeException("教师与工作室负责教师不匹配");
        }
        //删除邀请码
        TWorkRoomInvite tWorkRoomInvite = new TWorkRoomInvite();
        tWorkRoomInvite.setId(Long.valueOf(workRoomInviteId));
//        TWorkRoomInvite tWorkRoomInvite1 = tWorkRoomInviteMapper.selectTWorkRoomInviteById(Long.valueOf(workRoomInviteId));
//        if(tWorkRoomInvite1.getCreateTeacherId().equals(teacherId)){
//            throw new RuntimeException("教师id与邀请码id不匹配");
//        }
        tWorkRoomInvite.setDeleteTime(new Date());
        tWorkRoomInviteMapper.updateTWorkRoomInvite(tWorkRoomInvite);
        return "删除邀请码成功";
    }

    @Override
    public List<TWorkRoomInvite> wechatMiniWorkInviteCodeList(String wordRoomId) {
        return wechatMiniMapper.selectTWorkRoomInviteList(Long.valueOf(wordRoomId));
    }

    @Override
    public String wechatMiniUseInviteCode(String type, String inviteCode) {
        //判断邀请码是否过期
        Long roomId = wechatMiniMapper.selectRoomIdByInviteCode(inviteCode);
        if (roomId == null) {
            throw new RuntimeException("邀请码已过期或不存在");
        }
        //判断用户是否已经加入工作室
        //用户加入工作室
//        if ("用户".equals(type)) {
//            Long id = nTools.getLoginInfo().getId();
//            Long aLong = wechatMiniMapper.selectId(roomId, type, id);
//            if (aLong != null) {
//                throw new RuntimeException("当前身份已存在申请记录或已加入工作室");
//            }
//            TRelationWorkRoomUser tRelationWorkRoomUser = new TRelationWorkRoomUser();
//            tRelationWorkRoomUser.setAuditStatus("待处理");
//            tRelationWorkRoomUser.setWorkRoomId(roomId);
//            tRelationWorkRoomUser.setJoinType(type);
//            tRelationWorkRoomUser.setTargetId(id);
//            tRelationWorkRoomUser.setUserRoomName("默认昵称");
//            int i = tRelationWorkRoomUserMapper.insertTRelationWorkRoomUser(tRelationWorkRoomUser);
//            if (i > 0) {
//                return "加入工作室成功";
//            }
//
//        }
//        if ("学生".equals(type)) {
//            Long id = nTools.getStudentInfo().getStudentId();
//            Long aLong = wechatMiniMapper.selectId(roomId, type, id);
//            if (aLong != null) {
//                throw new RuntimeException("当前身份已存在申请记录或已加入工作室");
//            }
//            TRelationWorkRoomUser tRelationWorkRoomUser = new TRelationWorkRoomUser();
//            tRelationWorkRoomUser.setAuditStatus("待处理");
//            tRelationWorkRoomUser.setWorkRoomId(roomId);
//            tRelationWorkRoomUser.setJoinType(type);
//            tRelationWorkRoomUser.setTargetId(id);
//            tRelationWorkRoomUser.setUserRoomName("默认昵称");
//            int i = tRelationWorkRoomUserMapper.insertTRelationWorkRoomUser(tRelationWorkRoomUser);
//            if (i > 0) {
//                return "加入工作室成功";
//            }
//        }
//        if ("教师".equals(type)) {
        Long id = nTools.getTeacherInfo().getTeacherId();
        Long aLong = wechatMiniMapper.selectId(roomId, type, id);
        if (aLong != null) {
            throw new RuntimeException("当前身份已存在申请记录或已加入工作室");
        }
        TRelationWorkRoomUser tRelationWorkRoomUser = new TRelationWorkRoomUser();
        tRelationWorkRoomUser.setAuditStatus("待处理");
        tRelationWorkRoomUser.setWorkRoomId(roomId);
        tRelationWorkRoomUser.setJoinType(type);
        tRelationWorkRoomUser.setTargetId(id);
        tRelationWorkRoomUser.setUserRoomName("默认昵称");
        int i = tRelationWorkRoomUserMapper.insertTRelationWorkRoomUser(tRelationWorkRoomUser);
        if (i > 0) {
            return "加入工作室成功";
        }
//        }
        return "加入工作室失败";
    }

    @Override
    public List<GetWorkRoomAuditList> wechatMiniGetWorkRoomAuditList() {
        //根据教师id 查对应的审核列表
        Long teacherId = nTools.getTeacherInfo().getTeacherId();
        List<GetWorkRoomAuditList> getWorkRoomAuditLists = wechatMiniMapper.wechatMiniGetWorkRoomAuditList(teacherId);
        return getWorkRoomAuditLists;
    }

    @Override
    public void logout() {
        String openId = nTools.getMiniOpenId();
        TUser tUser = wechatMiniMapper.selectTUserByWechatMiniOpenId(openId);
        int count = tUserMapper.deleteTUserById(tUser.getId());
        if (count > 0){
            tStudentMapper.deleteTStudentByUserId(tUser.getId());
            tTeacherMapper.deleteTTeacherByUserId(tUser.getId());
            return;
        }
        throw new RuntimeException("注销失败，没有该用户");
    }



    @Override
    public List<TArticleTwoType> wechatMiniArticleTypeList() {
        return tArticleTwoTypeMapper.selectTwoTypeList();
    }

    @Override
    public List<TArticleSlideshow> selectArticleTSlideshowList() {
        return wechatMiniMapper.selectTArticleSlideshowList();
    }
}
