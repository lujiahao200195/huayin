package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TTeacherAdvantages;
import com.ruoyi.system.mapper.TTeacherAdvantagesMapper;
import com.ruoyi.system.service.ITTeacherAdvantagesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 教师个人优势Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-16
 */
@Service
public class TTeacherAdvantagesServiceImpl implements ITTeacherAdvantagesService {
    @Autowired
    private TTeacherAdvantagesMapper tTeacherAdvantagesMapper;

    /**
     * 查询教师个人优势
     *
     * @param id 教师个人优势主键
     * @return 教师个人优势
     */
    @Override
    public TTeacherAdvantages selectTTeacherAdvantagesById(Long id) {
        return tTeacherAdvantagesMapper.selectTTeacherAdvantagesById(id);
    }

    /**
     * 查询教师个人优势列表
     *
     * @param tTeacherAdvantages 教师个人优势
     * @return 教师个人优势
     */
    @Override
    public List<TTeacherAdvantages> selectTTeacherAdvantagesList(TTeacherAdvantages tTeacherAdvantages) {
        return tTeacherAdvantagesMapper.selectTTeacherAdvantagesList(tTeacherAdvantages);
    }

    /**
     * 新增教师个人优势
     *
     * @param tTeacherAdvantages 教师个人优势
     * @return 结果
     */
    @Override
    public int insertTTeacherAdvantages(TTeacherAdvantages tTeacherAdvantages) {
        return tTeacherAdvantagesMapper.insertTTeacherAdvantages(tTeacherAdvantages);
    }

    /**
     * 修改教师个人优势
     *
     * @param tTeacherAdvantages 教师个人优势
     * @return 结果
     */
    @Override
    public int updateTTeacherAdvantages(TTeacherAdvantages tTeacherAdvantages) {
        return tTeacherAdvantagesMapper.updateTTeacherAdvantages(tTeacherAdvantages);
    }

    /**
     * 批量删除教师个人优势
     *
     * @param ids 需要删除的教师个人优势主键
     * @return 结果
     */
    @Override
    public int deleteTTeacherAdvantagesByIds(Long[] ids) {
        return tTeacherAdvantagesMapper.deleteTTeacherAdvantagesByIds(ids);
    }

    /**
     * 删除教师个人优势信息
     *
     * @param id 教师个人优势主键
     * @return 结果
     */
    @Override
    public int deleteTTeacherAdvantagesById(Long id) {
        return tTeacherAdvantagesMapper.deleteTTeacherAdvantagesById(id);
    }
}
