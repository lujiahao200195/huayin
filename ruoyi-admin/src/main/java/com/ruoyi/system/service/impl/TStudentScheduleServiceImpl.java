package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TStudentSchedule;
import com.ruoyi.system.mapper.TStudentScheduleMapper;
import com.ruoyi.system.service.ITStudentScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 学生可安排时间Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-17
 */
@Service
public class TStudentScheduleServiceImpl implements ITStudentScheduleService {
    @Autowired
    private TStudentScheduleMapper tStudentScheduleMapper;

    /**
     * 查询学生可安排时间
     *
     * @param id 学生可安排时间主键
     * @return 学生可安排时间
     */
    @Override
    public TStudentSchedule selectTStudentScheduleById(Long id) {
        return tStudentScheduleMapper.selectTStudentScheduleById(id);
    }

    /**
     * 查询学生可安排时间列表
     *
     * @param tStudentSchedule 学生可安排时间
     * @return 学生可安排时间
     */
    @Override
    public List<TStudentSchedule> selectTStudentScheduleList(TStudentSchedule tStudentSchedule) {
        return tStudentScheduleMapper.selectTStudentScheduleList(tStudentSchedule);
    }

    /**
     * 新增学生可安排时间
     *
     * @param tStudentSchedule 学生可安排时间
     * @return 结果
     */
    @Override
    public int insertTStudentSchedule(TStudentSchedule tStudentSchedule) {
        return tStudentScheduleMapper.insertTStudentSchedule(tStudentSchedule);
    }

    /**
     * 修改学生可安排时间
     *
     * @param tStudentSchedule 学生可安排时间
     * @return 结果
     */
    @Override
    public int updateTStudentSchedule(TStudentSchedule tStudentSchedule) {
        return tStudentScheduleMapper.updateTStudentSchedule(tStudentSchedule);
    }

    /**
     * 批量删除学生可安排时间
     *
     * @param ids 需要删除的学生可安排时间主键
     * @return 结果
     */
    @Override
    public int deleteTStudentScheduleByIds(Long[] ids) {
        return tStudentScheduleMapper.deleteTStudentScheduleByIds(ids);
    }

    /**
     * 删除学生可安排时间信息
     *
     * @param id 学生可安排时间主键
     * @return 结果
     */
    @Override
    public int deleteTStudentScheduleById(Long id) {
        return tStudentScheduleMapper.deleteTStudentScheduleById(id);
    }
}
