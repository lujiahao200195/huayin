package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TBbsPost;
import com.ruoyi.system.mapper.TBbsPostMapper;
import com.ruoyi.system.service.ITBbsPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 论坛帖Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-30
 */
@Service
public class TBbsPostServiceImpl implements ITBbsPostService {
    @Autowired
    private TBbsPostMapper tBbsPostMapper;

    /**
     * 查询论坛帖
     *
     * @param id 论坛帖主键
     * @return 论坛帖
     */
    @Override
    public TBbsPost selectTBbsPostById(Long id) {
        return tBbsPostMapper.selectTBbsPostById(id);
    }

    /**
     * 查询论坛帖列表
     *
     * @param tBbsPost 论坛帖
     * @return 论坛帖
     */
    @Override
    public List<TBbsPost> selectTBbsPostList(TBbsPost tBbsPost) {
        return tBbsPostMapper.selectTBbsPostList(tBbsPost);
    }

    /**
     * 新增论坛帖
     *
     * @param tBbsPost 论坛帖
     * @return 结果
     */
    @Override
    public int insertTBbsPost(TBbsPost tBbsPost) {
        return tBbsPostMapper.insertTBbsPost(tBbsPost);
    }

    /**
     * 修改论坛帖
     *
     * @param tBbsPost 论坛帖
     * @return 结果
     */
    @Override
    public int updateTBbsPost(TBbsPost tBbsPost) {
        return tBbsPostMapper.updateTBbsPost(tBbsPost);
    }

    /**
     * 批量删除论坛帖
     *
     * @param ids 需要删除的论坛帖主键
     * @return 结果
     */
    @Override
    public int deleteTBbsPostByIds(Long[] ids) {
        return tBbsPostMapper.deleteTBbsPostByIds(ids);
    }

    /**
     * 删除论坛帖信息
     *
     * @param id 论坛帖主键
     * @return 结果
     */
    @Override
    public int deleteTBbsPostById(Long id) {
        return tBbsPostMapper.deleteTBbsPostById(id);
    }
}
