package com.ruoyi.system.service;

import com.ruoyi.system.domain.TBbsPost;

import java.util.List;

/**
 * 论坛帖Service接口
 *
 * @author ruoyi
 * @date 2023-08-30
 */
public interface ITBbsPostService {
    /**
     * 查询论坛帖
     *
     * @param id 论坛帖主键
     * @return 论坛帖
     */
    public TBbsPost selectTBbsPostById(Long id);

    /**
     * 查询论坛帖列表
     *
     * @param tBbsPost 论坛帖
     * @return 论坛帖集合
     */
    public List<TBbsPost> selectTBbsPostList(TBbsPost tBbsPost);

    /**
     * 新增论坛帖
     *
     * @param tBbsPost 论坛帖
     * @return 结果
     */
    public int insertTBbsPost(TBbsPost tBbsPost);

    /**
     * 修改论坛帖
     *
     * @param tBbsPost 论坛帖
     * @return 结果
     */
    public int updateTBbsPost(TBbsPost tBbsPost);

    /**
     * 批量删除论坛帖
     *
     * @param ids 需要删除的论坛帖主键集合
     * @return 结果
     */
    public int deleteTBbsPostByIds(Long[] ids);

    /**
     * 删除论坛帖信息
     *
     * @param id 论坛帖主键
     * @return 结果
     */
    public int deleteTBbsPostById(Long id);
}
