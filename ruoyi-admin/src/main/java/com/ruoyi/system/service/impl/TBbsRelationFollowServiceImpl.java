package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TBbsRelationFollow;
import com.ruoyi.system.mapper.TBbsRelationFollowMapper;
import com.ruoyi.system.service.ITBbsRelationFollowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 关注关系Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-29
 */
@Service
public class TBbsRelationFollowServiceImpl implements ITBbsRelationFollowService {
    @Autowired
    private TBbsRelationFollowMapper tBbsRelationFollowMapper;

    /**
     * 查询关注关系
     *
     * @param id 关注关系主键
     * @return 关注关系
     */
    @Override
    public TBbsRelationFollow selectTBbsRelationFollowById(Long id) {
        return tBbsRelationFollowMapper.selectTBbsRelationFollowById(id);
    }

    /**
     * 查询关注关系列表
     *
     * @param tBbsRelationFollow 关注关系
     * @return 关注关系
     */
    @Override
    public List<TBbsRelationFollow> selectTBbsRelationFollowList(TBbsRelationFollow tBbsRelationFollow) {
        return tBbsRelationFollowMapper.selectTBbsRelationFollowList(tBbsRelationFollow);
    }

    /**
     * 新增关注关系
     *
     * @param tBbsRelationFollow 关注关系
     * @return 结果
     */
    @Override
    public int insertTBbsRelationFollow(TBbsRelationFollow tBbsRelationFollow) {
        return tBbsRelationFollowMapper.insertTBbsRelationFollow(tBbsRelationFollow);
    }

    /**
     * 修改关注关系
     *
     * @param tBbsRelationFollow 关注关系
     * @return 结果
     */
    @Override
    public int updateTBbsRelationFollow(TBbsRelationFollow tBbsRelationFollow) {
        return tBbsRelationFollowMapper.updateTBbsRelationFollow(tBbsRelationFollow);
    }

    /**
     * 批量删除关注关系
     *
     * @param ids 需要删除的关注关系主键
     * @return 结果
     */
    @Override
    public int deleteTBbsRelationFollowByIds(Long[] ids) {
        return tBbsRelationFollowMapper.deleteTBbsRelationFollowByIds(ids);
    }

    /**
     * 删除关注关系信息
     *
     * @param id 关注关系主键
     * @return 结果
     */
    @Override
    public int deleteTBbsRelationFollowById(Long id) {
        return tBbsRelationFollowMapper.deleteTBbsRelationFollowById(id);
    }
}
