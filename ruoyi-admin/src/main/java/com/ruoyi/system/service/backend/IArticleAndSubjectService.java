package com.ruoyi.system.service.backend;

import com.ruoyi.system.vo.wechat.backend.ArticleAndSubjectVo;

import java.util.List;

public interface IArticleAndSubjectService {

    /*查询文章*/
    public List<ArticleAndSubjectVo> selectArticleAndSubject(ArticleAndSubjectVo articleAndSubjectVo);

    /*添加文章方法，同步添加文章关系*/
    public int insertArticle(ArticleAndSubjectVo articleAndSubjectVo);

    /*查询文章根据ID（详情）*/
    public ArticleAndSubjectVo selectArticleById(Long id);

    public int deleteArticle(Long[] ids);
}
