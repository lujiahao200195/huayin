package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TOptionMapper;
import com.ruoyi.system.domain.TOption;
import com.ruoyi.system.service.ITOptionService;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-19
 */
@Service
public class TOptionServiceImpl implements ITOptionService 
{
    @Autowired
    private TOptionMapper tOptionMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public TOption selectTOptionById(Long id)
    {
        return tOptionMapper.selectTOptionById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param tOption 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<TOption> selectTOptionList(TOption tOption)
    {
        return tOptionMapper.selectTOptionList(tOption);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param tOption 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertTOption(TOption tOption)
    {
        return tOptionMapper.insertTOption(tOption);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param tOption 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateTOption(TOption tOption)
    {
        return tOptionMapper.updateTOption(tOption);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteTOptionByIds(Long[] ids)
    {
        return tOptionMapper.deleteTOptionByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteTOptionById(Long id)
    {
        return tOptionMapper.deleteTOptionById(id);
    }
}
