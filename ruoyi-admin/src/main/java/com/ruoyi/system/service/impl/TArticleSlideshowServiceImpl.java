package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TArticleSlideshowMapper;
import com.ruoyi.system.domain.TArticleSlideshow;
import com.ruoyi.system.service.ITArticleSlideshowService;

/**
 * 轮播图Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-05-11
 */
@Service
public class TArticleSlideshowServiceImpl implements ITArticleSlideshowService 
{
    @Autowired
    private TArticleSlideshowMapper tArticleSlideshowMapper;

    /**
     * 查询轮播图
     * 
     * @param id 轮播图主键
     * @return 轮播图
     */
    @Override
    public TArticleSlideshow selectTArticleSlideshowById(Long id)
    {
        return tArticleSlideshowMapper.selectTArticleSlideshowById(id);
    }

    /**
     * 查询轮播图列表
     * 
     * @param tArticleSlideshow 轮播图
     * @return 轮播图
     */
    @Override
    public List<TArticleSlideshow> selectTArticleSlideshowList(TArticleSlideshow tArticleSlideshow)
    {
        return tArticleSlideshowMapper.selectTArticleSlideshowList(tArticleSlideshow);
    }

    /**
     * 新增轮播图
     * 
     * @param tArticleSlideshow 轮播图
     * @return 结果
     */
    @Override
    public int insertTArticleSlideshow(TArticleSlideshow tArticleSlideshow)
    {
        return tArticleSlideshowMapper.insertTArticleSlideshow(tArticleSlideshow);
    }

    /**
     * 修改轮播图
     * 
     * @param tArticleSlideshow 轮播图
     * @return 结果
     */
    @Override
    public int updateTArticleSlideshow(TArticleSlideshow tArticleSlideshow)
    {
        return tArticleSlideshowMapper.updateTArticleSlideshow(tArticleSlideshow);
    }

    /**
     * 批量删除轮播图
     * 
     * @param ids 需要删除的轮播图主键
     * @return 结果
     */
    @Override
    public int deleteTArticleSlideshowByIds(Long[] ids)
    {
        return tArticleSlideshowMapper.deleteTArticleSlideshowByIds(ids);
    }

    /**
     * 删除轮播图信息
     * 
     * @param id 轮播图主键
     * @return 结果
     */
    @Override
    public int deleteTArticleSlideshowById(Long id)
    {
        return tArticleSlideshowMapper.deleteTArticleSlideshowById(id);
    }
}
