package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TDifficultyLevelMapper;
import com.ruoyi.system.domain.TDifficultyLevel;
import com.ruoyi.system.service.ITDifficultyLevelService;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-04-19
 */
@Service
public class TDifficultyLevelServiceImpl implements ITDifficultyLevelService 
{
    @Autowired
    private TDifficultyLevelMapper tDifficultyLevelMapper;

    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    @Override
    public TDifficultyLevel selectTDifficultyLevelById(Long id)
    {
        return tDifficultyLevelMapper.selectTDifficultyLevelById(id);
    }

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param tDifficultyLevel 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<TDifficultyLevel> selectTDifficultyLevelList(TDifficultyLevel tDifficultyLevel)
    {
        return tDifficultyLevelMapper.selectTDifficultyLevelList(tDifficultyLevel);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param tDifficultyLevel 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int insertTDifficultyLevel(TDifficultyLevel tDifficultyLevel)
    {
        return tDifficultyLevelMapper.insertTDifficultyLevel(tDifficultyLevel);
    }

    /**
     * 修改【请填写功能名称】
     * 
     * @param tDifficultyLevel 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int updateTDifficultyLevel(TDifficultyLevel tDifficultyLevel)
    {
        return tDifficultyLevelMapper.updateTDifficultyLevel(tDifficultyLevel);
    }

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteTDifficultyLevelByIds(Long[] ids)
    {
        return tDifficultyLevelMapper.deleteTDifficultyLevelByIds(ids);
    }

    /**
     * 删除【请填写功能名称】信息
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    @Override
    public int deleteTDifficultyLevelById(Long id)
    {
        return tDifficultyLevelMapper.deleteTDifficultyLevelById(id);
    }
}
