package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TBbsBoardSuggest;
import com.ruoyi.system.mapper.TBbsBoardSuggestMapper;
import com.ruoyi.system.service.ITBbsBoardSuggestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 版主推荐Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-29
 */
@Service
public class TBbsBoardSuggestServiceImpl implements ITBbsBoardSuggestService {
    @Autowired
    private TBbsBoardSuggestMapper tBbsBoardSuggestMapper;

    /**
     * 查询版主推荐
     *
     * @param id 版主推荐主键
     * @return 版主推荐
     */
    @Override
    public TBbsBoardSuggest selectTBbsBoardSuggestById(Long id) {
        return tBbsBoardSuggestMapper.selectTBbsBoardSuggestById(id);
    }

    /**
     * 查询版主推荐列表
     *
     * @param tBbsBoardSuggest 版主推荐
     * @return 版主推荐
     */
    @Override
    public List<TBbsBoardSuggest> selectTBbsBoardSuggestList(TBbsBoardSuggest tBbsBoardSuggest) {
        return tBbsBoardSuggestMapper.selectTBbsBoardSuggestList(tBbsBoardSuggest);
    }

    /**
     * 新增版主推荐
     *
     * @param tBbsBoardSuggest 版主推荐
     * @return 结果
     */
    @Override
    public int insertTBbsBoardSuggest(TBbsBoardSuggest tBbsBoardSuggest) {
        return tBbsBoardSuggestMapper.insertTBbsBoardSuggest(tBbsBoardSuggest);
    }

    /**
     * 修改版主推荐
     *
     * @param tBbsBoardSuggest 版主推荐
     * @return 结果
     */
    @Override
    public int updateTBbsBoardSuggest(TBbsBoardSuggest tBbsBoardSuggest) {
        return tBbsBoardSuggestMapper.updateTBbsBoardSuggest(tBbsBoardSuggest);
    }

    /**
     * 批量删除版主推荐
     *
     * @param ids 需要删除的版主推荐主键
     * @return 结果
     */
    @Override
    public int deleteTBbsBoardSuggestByIds(Long[] ids) {
        return tBbsBoardSuggestMapper.deleteTBbsBoardSuggestByIds(ids);
    }

    /**
     * 删除版主推荐信息
     *
     * @param id 版主推荐主键
     * @return 结果
     */
    @Override
    public int deleteTBbsBoardSuggestById(Long id) {
        return tBbsBoardSuggestMapper.deleteTBbsBoardSuggestById(id);
    }
}
