package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TStudent;
import com.ruoyi.system.mapper.TStudentMapper;
import com.ruoyi.system.service.ITStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 学生Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-31
 */
@Service
public class TStudentServiceImpl implements ITStudentService {
    @Autowired
    private TStudentMapper tStudentMapper;

    /**
     * 查询学生
     *
     * @param id 学生主键
     * @return 学生
     */
    @Override
    public TStudent selectTStudentById(Long id) {
        return tStudentMapper.selectTStudentById(id);
    }

    /**
     * 查询学生列表
     *
     * @param tStudent 学生
     * @return 学生
     */
    @Override
    public List<TStudent> selectTStudentList(TStudent tStudent) {
        return tStudentMapper.selectTStudentList(tStudent);
    }

    /**
     * 新增学生
     *
     * @param tStudent 学生
     * @return 结果
     */
    @Override
    public int insertTStudent(TStudent tStudent) {
        return tStudentMapper.insertTStudent(tStudent);
    }

    /**
     * 修改学生
     *
     * @param tStudent 学生
     * @return 结果
     */
    @Override
    public int updateTStudent(TStudent tStudent) {
        return tStudentMapper.updateTStudent(tStudent);
    }

    /**
     * 批量删除学生
     *
     * @param ids 需要删除的学生主键
     * @return 结果
     */
    @Override
    public int deleteTStudentByIds(Long[] ids) {
        return tStudentMapper.deleteTStudentByIds(ids);
    }

    /**
     * 删除学生信息
     *
     * @param id 学生主键
     * @return 结果
     */
    @Override
    public int deleteTStudentById(Long id) {
        return tStudentMapper.deleteTStudentById(id);
    }
}
