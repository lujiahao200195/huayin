package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TBbsBoard;
import com.ruoyi.system.mapper.TBbsBoardMapper;
import com.ruoyi.system.service.ITBbsBoardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 论坛版块Service业务层处理
 *
 * @author ruoyi
 * @date 2023-09-08
 */
@Service
public class TBbsBoardServiceImpl implements ITBbsBoardService {
    @Autowired
    private TBbsBoardMapper tBbsBoardMapper;

    /**
     * 查询论坛版块
     *
     * @param id 论坛版块主键
     * @return 论坛版块
     */
    @Override
    public TBbsBoard selectTBbsBoardById(Long id) {
        return tBbsBoardMapper.selectTBbsBoardById(id);
    }

    /**
     * 查询论坛版块列表
     *
     * @param tBbsBoard 论坛版块
     * @return 论坛版块
     */
    @Override
    public List<TBbsBoard> selectTBbsBoardList(TBbsBoard tBbsBoard) {
        return tBbsBoardMapper.selectTBbsBoardList(tBbsBoard);
    }

    /**
     * 新增论坛版块
     *
     * @param tBbsBoard 论坛版块
     * @return 结果
     */
    @Override
    public int insertTBbsBoard(TBbsBoard tBbsBoard) {
        return tBbsBoardMapper.insertTBbsBoard(tBbsBoard);
    }

    /**
     * 修改论坛版块
     *
     * @param tBbsBoard 论坛版块
     * @return 结果
     */
    @Override
    public int updateTBbsBoard(TBbsBoard tBbsBoard) {
        return tBbsBoardMapper.updateTBbsBoard(tBbsBoard);
    }

    /**
     * 批量删除论坛版块
     *
     * @param ids 需要删除的论坛版块主键
     * @return 结果
     */
    @Override
    public int deleteTBbsBoardByIds(Long[] ids) {
        return tBbsBoardMapper.deleteTBbsBoardByIds(ids);
    }

    /**
     * 删除论坛版块信息
     *
     * @param id 论坛版块主键
     * @return 结果
     */
    @Override
    public int deleteTBbsBoardById(Long id) {
        return tBbsBoardMapper.deleteTBbsBoardById(id);
    }
}
