package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TSubject;
import com.ruoyi.system.mapper.TSubjectMapper;
import com.ruoyi.system.service.ITSubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 教学科目Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-18
 */
@Service
public class TSubjectServiceImpl implements ITSubjectService {
    @Autowired
    private TSubjectMapper tSubjectMapper;

    /**
     * 查询教学科目
     *
     * @param id 教学科目主键
     * @return 教学科目
     */
    @Override
    public TSubject selectTSubjectById(Long id) {
        return tSubjectMapper.selectTSubjectById(id);
    }

    /**
     * 查询教学科目列表
     *
     * @param tSubject 教学科目
     * @return 教学科目
     */
    @Override
    public List<TSubject> selectTSubjectList(TSubject tSubject) {
        return tSubjectMapper.selectTSubjectList(tSubject);
    }

    /**
     * 新增教学科目
     *
     * @param tSubject 教学科目
     * @return 结果
     */
    @Override
    public int insertTSubject(TSubject tSubject) {
        return tSubjectMapper.insertTSubject(tSubject);
    }

    /**
     * 修改教学科目
     *
     * @param tSubject 教学科目
     * @return 结果
     */
    @Override
    public int updateTSubject(TSubject tSubject) {
        return tSubjectMapper.updateTSubject(tSubject);
    }

    /**
     * 批量删除教学科目
     *
     * @param ids 需要删除的教学科目主键
     * @return 结果
     */
    @Override
    public int deleteTSubjectByIds(Long[] ids) {
        return tSubjectMapper.deleteTSubjectByIds(ids);
    }

    /**
     * 删除教学科目信息
     *
     * @param id 教学科目主键
     * @return 结果
     */
    @Override
    public int deleteTSubjectById(Long id) {
        return tSubjectMapper.deleteTSubjectById(id);
    }
}
