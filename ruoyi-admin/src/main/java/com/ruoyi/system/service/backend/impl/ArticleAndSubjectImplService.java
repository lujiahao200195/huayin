package com.ruoyi.system.service.backend.impl;

import com.ruoyi.system.mapper.backend.ArticleAndSubjectMapper;
import com.ruoyi.system.service.backend.IArticleAndSubjectService;
import com.ruoyi.system.vo.wechat.backend.ArticleAndSubjectVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ArticleAndSubjectImplService implements IArticleAndSubjectService {


    @Resource
    private ArticleAndSubjectMapper articleAndSubjectMapper;


    /*查询文章*/
    @Override
    public List<ArticleAndSubjectVo> selectArticleAndSubject(ArticleAndSubjectVo articleAndSubjectVo) {
        return articleAndSubjectMapper.queryArticleAndSubject(articleAndSubjectVo);
    }


    /*查询文章根据Id（详情）*/

    @Override
    public ArticleAndSubjectVo selectArticleById(Long id) {
        return articleAndSubjectMapper.selectArticleById(id);
    }


    /**/
    @Override
    public int insertArticle(ArticleAndSubjectVo articleAndSubjectVo) {
        return articleAndSubjectMapper.insertTArticle(articleAndSubjectVo);
    }


    /*删除文章*/

    @Override
    public int deleteArticle(Long[] ids) {
        return articleAndSubjectMapper.deleteArticle(ids);
    }
}
