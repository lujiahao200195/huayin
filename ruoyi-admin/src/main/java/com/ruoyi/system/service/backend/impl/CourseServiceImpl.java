package com.ruoyi.system.service.backend.impl;

import com.ruoyi.system.bo.wechat.AddTCourse;
import com.ruoyi.system.common.tools.NTools;
import com.ruoyi.system.domain.TCourse;
import com.ruoyi.system.domain.TCourseSubjectRelation;
import com.ruoyi.system.domain.TStudent;
import com.ruoyi.system.domain.TTeacher;
import com.ruoyi.system.mapper.TCourseMapper;
import com.ruoyi.system.mapper.TCourseSubjectRelationMapper;
import com.ruoyi.system.mapper.WechatMiniMapper;
import com.ruoyi.system.mapper.backend.CourseMapper;
import com.ruoyi.system.service.ITCourseService;
import com.ruoyi.system.service.backend.CourseService;
import com.ruoyi.system.vo.wechat.GetCourse;
import com.ruoyi.system.vo.wechat.backend.CourseTeacherAndStudent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 课程安排Service业务层处理
 *
 * @author ruoyi
 * @date 2023-09-14
 */
@Service
public class CourseServiceImpl implements CourseService {
    @Resource
    private CourseMapper courseMapper;

    @Resource
    private TCourseMapper tCourseMapper;

    @Resource
    private WechatMiniMapper wechatMiniMapper;

    @Resource
    private TCourseSubjectRelationMapper tCourseSubjectRelationMapper;

    /**
     * 查询课程安排
     *
     * @param id 课程安排主键
     * @return 课程安排
     */
    @Override
    public TCourse selectTCourseById(Long id) {
        return courseMapper.selectTCourseById(id);
    }

    /**
     * 查询课程安排列表
     *
     * @param tCourse 课程安排
     * @return 课程安排
     */
    @Override
    public List<TCourse> selectTCourseList(TCourse tCourse) {
        return courseMapper.selectTCourseList(tCourse);
    }

    /**
     * 新增课程安排
     *
     * @return 结果
     */
    @Override
    public int insertTCourse(AddTCourse addTCourse) {
        TCourse tCourse = new TCourse();
        tCourse.setCourseDate(addTCourse.getDateTime());
        tCourse.setStudentId(addTCourse.getStudentId());
        tCourse.setStudentName(addTCourse.getStudentName());
        tCourse.setTeacherId(addTCourse.getTeacherId());
        tCourse.setTeacherName(addTCourse.getTeacherName());
        List<AddTCourse.CourseDTO> course = addTCourse.getCourse();
        //时段
        for (int i = 0; i < course.size(); i++) {
            tCourse.setCourseBegin(course.get(i).getStartTime());
            tCourse.setCourseFinish(course.get(i).getEndTime());
            courseMapper.insertTCourse(tCourse);
            List<Long> subjectIds = course.get(i).getSubjectIds();
            //课程关系表
            for (int j = 0; j < subjectIds.size(); j++) {
                TCourseSubjectRelation tCourseSubjectRelation = new TCourseSubjectRelation();
                tCourseSubjectRelation.setCourseId(tCourse.getId());
                tCourseSubjectRelation.setSubjectId(subjectIds.get(j));
                tCourseSubjectRelationMapper.insertTCourseSubjectRelation(tCourseSubjectRelation);
            }

        }

        return 1;
    }

    /**
     * 修改课程安排
     *
     * @return 结果
     */
    @Override
    @Transactional
    public int updateTCourse(AddTCourse addTCourse) {

        TCourse tCourse = new TCourse();
        tCourse.setId(addTCourse.getId());
        tCourse.setStudentId(addTCourse.getStudentId());
        tCourse.setStudentName(addTCourse.getStudentName());
        tCourse.setTeacherName(addTCourse.getTeacherName());
        tCourse.setTeacherId(addTCourse.getTeacherId());
        tCourse.setCourseDate(addTCourse.getDateTime());

        //课程列表
        List<AddTCourse.CourseDTO> course = addTCourse.getCourse();
        for (int i = 0; i < course.size(); i++) {
            tCourse.setCourseBegin(course.get(i).getStartTime());
            tCourse.setCourseFinish(course.get(i).getEndTime());
            courseMapper.updateTCourse(tCourse);
            //关系表课程和科目
            List<Long> subjectIds = course.get(i).getSubjectIds();
            TCourseSubjectRelation tCourseSubjectRelation1 = new TCourseSubjectRelation();
            tCourseSubjectRelation1.setCourseId(addTCourse.getId());
            List<TCourseSubjectRelation> tCourseSubjectRelations = courseMapper.selectTCourseSubjectRelationList(tCourseSubjectRelation1);
            for (int i1 = 0; i1 < subjectIds.size(); i1++) {
                for (int i2 = 0; i2 < tCourseSubjectRelations.size(); i2++) {
                    if (subjectIds.get(i1) == tCourseSubjectRelations.get(i2).getSubjectId()) {
                        subjectIds.remove(subjectIds.get(i1));
                        tCourseSubjectRelations.remove(tCourseSubjectRelations.get(i2));
                    }
                }
            }
            //删除原来的课程科目关系表
            Long[] idArray = tCourseSubjectRelations.stream()
                    .map(TCourseSubjectRelation::getId)
                    .toArray(Long[]::new);
            courseMapper.deleteTCourseSubjectRelationByIds(idArray);

            //插入新的关系表
            for (int i1 = 0; i1 < subjectIds.size(); i1++) {
                TCourseSubjectRelation tCourseSubjectRelation = new TCourseSubjectRelation();
                tCourseSubjectRelation.setCourseId(addTCourse.getId());
                tCourseSubjectRelation.setSubjectId(subjectIds.get(i1));
                tCourseSubjectRelationMapper.insertTCourseSubjectRelation(tCourseSubjectRelation);
            }


        }

        return 1;
    }

    /**
     * 批量删除课程安排
     *
     * @param ids 需要删除的课程安排主键
     * @return 结果
     */
    @Override
    @Transactional
    public int deleteTCourseByIds(Long[] ids) {
        int i1 = courseMapper.deleteTCourseByIds(ids);
        int i = courseMapper.deleteTCourseSubjectRelationByIds(ids);
        if (i1 == 0 || i == 0) {
            throw new RuntimeException("删除失败");
        }
        return 1;
    }

    /**
     * 删除课程安排信息
     *
     * @param id 课程安排主键
     * @return 结果
     */
    @Override
    public int deleteTCourseById(Long id) {
        return courseMapper.deleteTCourseById(id);
    }

    @Override
    public List<GetCourse> getCourse(String studentId, String date,String type) {
        List<GetCourse> getCourses = new ArrayList<>();
        if(type==null||"".equals(type)){
            List<String> strings = null;
            try {
                strings = NTools.printWeekdays(date);
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
            for (int i = 0; i < strings.size(); i++) {
                GetCourse getCourse = new GetCourse();
                getCourse.setDateTime(strings.get(i));
                List<TCourse> tCourses = wechatMiniMapper.getCourse( strings.get(i));
                if (tCourses.size() == 0) {
                    getCourses.add(getCourse);
                    continue;
                }

                List<GetCourse.CourseDTO> course = new ArrayList<>();
                for (int i1 = 0; i1 < tCourses.size(); i1++) {
                    getCourse.setId(tCourses.get(i1).getId());
                    GetCourse.CourseDTO courseDTOs = new GetCourse.CourseDTO();
                    courseDTOs.setStartTime(tCourses.get(i1).getCourseBegin());
                    courseDTOs.setEndTime(tCourses.get(i1).getCourseFinish());
                    List<String> subjectNameByIds = wechatMiniMapper.getSubjectNameByIds(tCourses.get(i1).getId());
                    courseDTOs.setSubjectStrs(subjectNameByIds);
                    course.add(courseDTOs);
                }
                getCourse.setCourse(course);
                getCourses.add(getCourse);
            }


        }

        if("学生".equals(type)){
            List<String> strings = null;
            try {
                strings = NTools.printWeekdays(date);
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
            for (int i = 0; i < strings.size(); i++) {
                GetCourse getCourse = new GetCourse();
                getCourse.setDateTime(strings.get(i));
                List<TCourse> tCourses = wechatMiniMapper.wechatMiniStudentCourse(Long.valueOf(studentId), strings.get(i));
                if (tCourses.size() == 0) {
                    getCourses.add(getCourse);
                    continue;
                }

                List<GetCourse.CourseDTO> course = new ArrayList<>();
                for (int i1 = 0; i1 < tCourses.size(); i1++) {
                    getCourse.setId(tCourses.get(i1).getId());
                    GetCourse.CourseDTO courseDTOs = new GetCourse.CourseDTO();
                    courseDTOs.setStartTime(tCourses.get(i1).getCourseBegin());
                    courseDTOs.setEndTime(tCourses.get(i1).getCourseFinish());
                    List<String> subjectNameByIds = wechatMiniMapper.getSubjectNameByIds(tCourses.get(i1).getId());
                    courseDTOs.setSubjectStrs(subjectNameByIds);
                    course.add(courseDTOs);
                }
                getCourse.setCourse(course);
                getCourses.add(getCourse);
            }

        }

        if("教师".equals(type)){
            List<String> strings = null;
            try {
                strings = NTools.printWeekdays(date);
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
            for (int i = 0; i < strings.size(); i++) {
                GetCourse getCourse = new GetCourse();
                getCourse.setDateTime(strings.get(i));
                List<TCourse> tCourses = wechatMiniMapper.wechatMiniStudentCourse(Long.valueOf(studentId), strings.get(i));
                if (tCourses.size() == 0) {
                    getCourses.add(getCourse);
                    continue;
                }

                List<GetCourse.CourseDTO> course = new ArrayList<>();
                for (int i1 = 0; i1 < tCourses.size(); i1++) {
                    getCourse.setId(tCourses.get(i1).getId());
                    GetCourse.CourseDTO courseDTOs = new GetCourse.CourseDTO();
                    courseDTOs.setStartTime(tCourses.get(i1).getCourseBegin());
                    courseDTOs.setEndTime(tCourses.get(i1).getCourseFinish());
                    List<String> subjectNameByIds = wechatMiniMapper.getSubjectNameByIds(tCourses.get(i1).getId());
                    courseDTOs.setSubjectStrs(subjectNameByIds);
                    course.add(courseDTOs);
                }
                getCourse.setCourse(course);
                getCourses.add(getCourse);
            }

        }

        return getCourses;
    }

    @Override
    public List<CourseTeacherAndStudent> getCourseTeacherAndStudent(String type) {
        List<CourseTeacherAndStudent> courseTeacherAndStudents = new ArrayList<>();

        if(type==null||"".equals(type)){
            List<CourseTeacherAndStudent> studentAndTeacher = courseMapper.getStudentAndTeacher();
            return studentAndTeacher;
        }

        if("学生".equals(type)){
            List<CourseTeacherAndStudent> studentInfo = courseMapper.getStudentInfo();
            return studentInfo;
//            for (int i = 0; i < studentInfo.size(); i++) {
//                CourseTeacherAndStudent courseTeacherAndStudent = new CourseTeacherAndStudent();
//                courseTeacherAndStudent.setName(studentInfo.get(i).getStudentName());
//                courseTeacherAndStudent.setId(studentInfo.get(i).getId());
//                courseTeacherAndStudent.setType("学生");
//                courseTeacherAndStudents.add(courseTeacherAndStudent);
//            }
        }

        if("教师".equals(type)){
            List<CourseTeacherAndStudent> teacherInfo = courseMapper.getTeacherInfo();
            return teacherInfo;
//            for (int i = 0; i < teacherInfo.size(); i++) {
//                CourseTeacherAndStudent courseTeacherAndStudent = new CourseTeacherAndStudent();
//                courseTeacherAndStudent.setName(teacherInfo.get(i).getTeacherName());
//                courseTeacherAndStudent.setId(teacherInfo.get(i).getId());
//                courseTeacherAndStudent.setType("教师");
//                courseTeacherAndStudents.add(courseTeacherAndStudent);
//            }
        }
        return courseTeacherAndStudents;
    }

//    @Override
//    public List<GetCourse> wechatMiniStudentCourse(String studentId, String date) {
//
//        List<String> strings = null;
//        try {
//            strings = NTools.printWeekdays(date);
//        } catch (ParseException e) {
//            throw new RuntimeException(e);
//        }
//        List<GetCourse> getCourses = new ArrayList<>();
//        for (int i = 0; i < strings.size(); i++) {
//            GetCourse getCourse = new GetCourse();
//            getCourse.setDateTime(strings.get(i));
//            List<TCourse> tCourses = wechatMiniMapper.wechatMiniStudentCourse(Long.valueOf(studentId), strings.get(i));
//            if (tCourses.size() == 0) {
//                getCourses.add(getCourse);
//                continue;
//            }
//
//            List<GetCourse.CourseDTO> course = new ArrayList<>();
//            for (int i1 = 0; i1 < tCourses.size(); i1++) {
//                getCourse.setId(tCourses.get(i).getId());
//                GetCourse.CourseDTO courseDTOs = new GetCourse.CourseDTO();
//                courseDTOs.setStartTime(tCourses.get(i1).getCourseBegin());
//                courseDTOs.setEndTime(tCourses.get(i1).getCourseFinish());
//                List<String> subjectNameByIds = wechatMiniMapper.getSubjectNameByIds(tCourses.get(i1).getId());
//                courseDTOs.setSubjectStrs(subjectNameByIds);
//                course.add(courseDTOs);
//            }
//            getCourse.setCourse(course);
//            getCourses.add(getCourse);
//        }
//        return getCourses;
//    }
//
//    @Override
//    public List<GetCourse> wechatMiniTeacherCourse(String teacherId, String date) {
//        List<String> strings = null;
//        try {
//            strings = NTools.printWeekdays(date);
//        } catch (ParseException e) {
//            throw new RuntimeException(e);
//        }
//        List<GetCourse> getCourses = new ArrayList<>();
//        for (int i = 0; i < strings.size(); i++) {
//            GetCourse getCourse = new GetCourse();
//            getCourse.setDateTime(strings.get(i));
//            List<TCourse> tCourses = wechatMiniMapper.wechatMiniTeacherCourse(Long.valueOf(teacherId), strings.get(i));
//            if (tCourses.size() == 0) {
//                getCourses.add(getCourse);
//                continue;
//            }
//
//            List<GetCourse.CourseDTO> course = new ArrayList<>();
//            for (int i1 = 0; i1 < tCourses.size(); i1++) {
//                getCourse.setId(tCourses.get(i).getId());
//                GetCourse.CourseDTO courseDTOs = new GetCourse.CourseDTO();
//                courseDTOs.setStartTime(tCourses.get(i1).getCourseBegin());
//                courseDTOs.setEndTime(tCourses.get(i1).getCourseFinish());
//                List<String> subjectNameByIds = wechatMiniMapper.getSubjectNameByIds(tCourses.get(i1).getId());
//                courseDTOs.setSubjectStrs(subjectNameByIds);
//                course.add(courseDTOs);
//            }
//            getCourse.setCourse(course);
//            getCourses.add(getCourse);
//        }
//        return getCourses;
//    }
}
