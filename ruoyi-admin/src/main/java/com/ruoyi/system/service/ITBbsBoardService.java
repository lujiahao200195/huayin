package com.ruoyi.system.service;

import com.ruoyi.system.domain.TBbsBoard;

import java.util.List;

/**
 * 论坛版块Service接口
 *
 * @author ruoyi
 * @date 2023-09-08
 */
public interface ITBbsBoardService {
    /**
     * 查询论坛版块
     *
     * @param id 论坛版块主键
     * @return 论坛版块
     */
    public TBbsBoard selectTBbsBoardById(Long id);

    /**
     * 查询论坛版块列表
     *
     * @param tBbsBoard 论坛版块
     * @return 论坛版块集合
     */
    public List<TBbsBoard> selectTBbsBoardList(TBbsBoard tBbsBoard);

    /**
     * 新增论坛版块
     *
     * @param tBbsBoard 论坛版块
     * @return 结果
     */
    public int insertTBbsBoard(TBbsBoard tBbsBoard);

    /**
     * 修改论坛版块
     *
     * @param tBbsBoard 论坛版块
     * @return 结果
     */
    public int updateTBbsBoard(TBbsBoard tBbsBoard);

    /**
     * 批量删除论坛版块
     *
     * @param ids 需要删除的论坛版块主键集合
     * @return 结果
     */
    public int deleteTBbsBoardByIds(Long[] ids);

    /**
     * 删除论坛版块信息
     *
     * @param id 论坛版块主键
     * @return 结果
     */
    public int deleteTBbsBoardById(Long id);
}
