package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.TBbsBoardMember;
import com.ruoyi.system.mapper.TBbsBoardMemberMapper;
import com.ruoyi.system.service.ITBbsBoardMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 版块成员Service业务层处理
 *
 * @author ruoyi
 * @date 2023-08-28
 */
@Service
public class TBbsBoardMemberServiceImpl implements ITBbsBoardMemberService {
    @Autowired
    private TBbsBoardMemberMapper tBbsBoardMemberMapper;

    /**
     * 查询版块成员
     *
     * @param id 版块成员主键
     * @return 版块成员
     */
    @Override
    public TBbsBoardMember selectTBbsBoardMemberById(Long id) {
        return tBbsBoardMemberMapper.selectTBbsBoardMemberById(id);
    }

    /**
     * 查询版块成员列表
     *
     * @param tBbsBoardMember 版块成员
     * @return 版块成员
     */
    @Override
    public List<TBbsBoardMember> selectTBbsBoardMemberList(TBbsBoardMember tBbsBoardMember) {
        return tBbsBoardMemberMapper.selectTBbsBoardMemberList(tBbsBoardMember);
    }

    /**
     * 新增版块成员
     *
     * @param tBbsBoardMember 版块成员
     * @return 结果
     */
    @Override
    public int insertTBbsBoardMember(TBbsBoardMember tBbsBoardMember) {
        return tBbsBoardMemberMapper.insertTBbsBoardMember(tBbsBoardMember);
    }

    /**
     * 修改版块成员
     *
     * @param tBbsBoardMember 版块成员
     * @return 结果
     */
    @Override
    public int updateTBbsBoardMember(TBbsBoardMember tBbsBoardMember) {
        return tBbsBoardMemberMapper.updateTBbsBoardMember(tBbsBoardMember);
    }

    /**
     * 批量删除版块成员
     *
     * @param ids 需要删除的版块成员主键
     * @return 结果
     */
    @Override
    public int deleteTBbsBoardMemberByIds(Long[] ids) {
        return tBbsBoardMemberMapper.deleteTBbsBoardMemberByIds(ids);
    }

    /**
     * 删除版块成员信息
     *
     * @param id 版块成员主键
     * @return 结果
     */
    @Override
    public int deleteTBbsBoardMemberById(Long id) {
        return tBbsBoardMemberMapper.deleteTBbsBoardMemberById(id);
    }
}
