package com.ruoyi.system.vo.wechat;

import com.ruoyi.system.domain.TStudent;
import com.ruoyi.system.domain.TTeacher;
import lombok.Data;

import java.util.List;

/**
 * 报名记录
 */
@Data
public class SignUpRecordVo {
//
//    /**
//     * 学生Id
//     */
//    private Long studentId;
//
//    /**
//     * 教师Id
//     */
//    private Long teacherId;
//
//    /**
//     * 报名类型
//     */
//    private String signUpType;
//
//    /**
//     * 审核状态
//     */
//    private String auditStatus;
//
//    /**
//     * 审核结果原因
//     */
//    private String auditResult;
    /**
     * 学生
     */
    private List<TStudent> students;

    /**
     * 教师数据
     */
    private List<TTeacher> teachers;

}
