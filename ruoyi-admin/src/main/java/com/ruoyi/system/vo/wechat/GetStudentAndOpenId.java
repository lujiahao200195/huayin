package com.ruoyi.system.vo.wechat;

import lombok.Data;

/**
 * 查询学生名 学生电话与openid 学生id
 */
@Data
public class GetStudentAndOpenId {

    private Integer studentId;
    private String studentName;
    private String studentPhone;
    private String userWxMiniOpenid;

}
