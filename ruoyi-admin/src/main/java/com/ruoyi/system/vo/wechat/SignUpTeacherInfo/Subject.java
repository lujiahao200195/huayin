package com.ruoyi.system.vo.wechat.SignUpTeacherInfo;

import lombok.Data;

/**
 * 教师教学课程
 */
@Data
public class Subject {
    /**
     * 课程Id
     */
    private Long subjectId;

    /**
     * 科目名字
     */
    private String subjectName;

    /**
     * 教学年段
     */
    private String teachingYear;

    /**
     * 教学备注
     */
    private String teachingRemark;
}
