package com.ruoyi.system.vo.wechat.rs;


import lombok.Data;
import lombok.experimental.Accessors;


/**
 * 资源标签
 */
@Accessors(chain = true)
@Data
public class ResourceTagVo {


    private Long tagId;

    private String tagName;
}
