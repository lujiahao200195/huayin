package com.ruoyi.system.vo.wechat.rs;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 搜索到的学生数据
 */
@Data
@Accessors(chain = true)
public class SearchStudentVo {

    /**
     * 学生Id
     */
    private Long studentId;

    /**
     * 学生姓名
     */
    private String studentName;
}
