package com.ruoyi.system.vo.wechat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.system.domain.TSubject;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

@Data
@Accessors(chain = true)
public class GetStudentInfo {

    /**
     * 学生名
     */
    private String studentName;

    /**
     * 报名时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date signupTime;

    /**
     * 出生日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthDay;

    /**
     * 学龄
     */
    private String studyAge;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 学生简介
     */
    private String studentIntro;

    /**
     * 学生邮箱
     */
    private String email;

    /**
     * 目表分数
     */
    private Long targetScore;


    /**
     * 需要的科目
     */
    private List<TSubject> needSubjectList;

}
