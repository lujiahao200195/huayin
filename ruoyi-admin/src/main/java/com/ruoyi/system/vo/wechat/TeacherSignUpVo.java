package com.ruoyi.system.vo.wechat;

import lombok.Data;

import java.util.List;

@Data
public class TeacherSignUpVo {
    /**
     * 教师id
     */
    private Long teacherId;
    /**
     * 模板集合
     */
    private List<String> templateIds;
}
