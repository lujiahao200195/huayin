package com.ruoyi.system.vo.wechat;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.system.domain.TSubject;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class GetTeacherInfo {


    /**
     * 作为教师时的教师名
     */
    @Excel(name = "作为教师时的教师名")
    private String teacherName;

    /**
     * 教师简介
     */
    @Excel(name = "教师简介")
    private String teacherIntro;

    /**
     * 教师老师照片链接
     */
    @Excel(name = "教师老师照片链接")
    private String teacherPicture;

    /**
     * 教师联系电话
     */
    @Excel(name = "教师联系电话")
    private String teacherPhone;

    /**
     * 教师电子邮箱
     */
    @Excel(name = "教师电子邮箱")
    private String teacherEmail;

    /**
     * 教师院校名称
     */
    @Excel(name = "教师院校名称")
    private String teacherSchool;

    /**
     * 所学专业
     */
    @Excel(name = "所学专业")
    private String teacherMajor;

    /**
     * 当前学历
     */
    @Excel(name = "当前学历")
    private String teacherEducation;

    /**
     * 所在国家或地区
     */
    @Excel(name = "所在国家或地区")
    private String teahcerCountryregion;


    /**
     * 教师政治面貌
     */
    @Excel(name = "教师政治面貌")
    private String teacherParty;

    /**
     * 教师民族
     */
    @Excel(name = "教师民族")
    private String teacherNationality;

    /**
     * 教学或工作经历
     */
    @Excel(name = "教学或工作经历")
    private String teacherExperience;

    /**
     * 需要的科目
     */
    private List<TSubject> SubjectList;
}
