package com.ruoyi.system.vo.wechat;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;


@Data
@Accessors(chain = true)
public class LastStudentList {

    /**
     * 打码后的学生名字
     */
    private String studentName;

    /**
     * 学生手机号, 打码数据
     */
    private String phone;

    /**
     * 报名时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date signUpTime;
}
