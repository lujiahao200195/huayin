package com.ruoyi.system.vo.wechat;


import com.ruoyi.system.domain.TSubject;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class WorkRoomSearchVo {

    /**
     * 工作室Id
     */
    private Long workRoomId;

    /**
     * 工作室名字
     */
    private String workRoomName;

    /**
     * 工作室负责教师Id
     */
    private Long principalTeacherId;

    /**
     * 工作室咨询教师Id
     */
    private Long consultTeacherId;

    /**
     * 工作室图标
     */
    private String workRoomIcon;

    /**
     * 工作室照片
     */
    private String workRoomImage;

    /**
     * 工作室链接
     */
    private String workRoomLink;

    /**
     * 未读消息数
     */
    private Long msgNum;

    /**
     * 当前在线状态
     */
    private String onlineStatus;

    /**
     * 科目列表
     */
    private List<TSubject> subjectList;

    /**
     * 工作室简介
     */
    private String intro;

    /**
     * 工作室电话
     */
    private String phone;

    /**
     * 工作室邮箱
     */
    private String email;

    /**
     * 工作室文章ID
     */
    private Long articleId;


}
