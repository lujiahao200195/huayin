package com.ruoyi.system.vo.wechat.backend;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;

import java.util.Date;

@Data
public class ExportSchoolArticle {
    /**
     * 主键ID
     */
    private Long id;


    /**
     * 文章标题
     */
    @Excel(name = "文章标题")
    private String articleTitle;

    /**
     * 文章简介
     */
    @Excel(name = "文章简介")
    private String articleNote;

    /**
     * 文章图片
     */
    @Excel(name = "文章图片")
    private String articleImage;

    /**
     * 文章链接
     */
    @Excel(name = "文章链接")
    private String articleLink;

    /**
     * 文章内容
     */
    @Excel(name = "文章内容")
    private String articleContent;


    /**
     * 审核状态
     */
    @Excel(name = "审核状态")
    private String auditStatus;


    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdTime;


    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedTime;

//
//    /**
//     * 删除时间
//     */
//    @JsonFormat(pattern = "yyyy-MM-dd")
//    @Excel(name = "删除时间", width = 30, dateFormat = "yyyy-MM-dd")
//    private Date deleteTime;

}
