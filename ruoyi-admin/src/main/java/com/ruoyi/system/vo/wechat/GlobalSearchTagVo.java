package com.ruoyi.system.vo.wechat;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class GlobalSearchTagVo {

    @JsonProperty("tagId")
    private Integer tagId;
    @JsonProperty("tagName")
    private String tagName;
}
