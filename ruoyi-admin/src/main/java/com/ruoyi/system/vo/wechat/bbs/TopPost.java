package com.ruoyi.system.vo.wechat.bbs;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class TopPost {

    /**
     * 置顶文章Id
     */
    private Long postId;

    /**
     * 文章标题
     */
    private String postTitle;
}
