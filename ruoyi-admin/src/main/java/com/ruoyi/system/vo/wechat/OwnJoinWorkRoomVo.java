package com.ruoyi.system.vo.wechat;

import lombok.Data;

/**
 * 用户查询自己申请加入工作室的记录
 */
@Data
public class OwnJoinWorkRoomVo {
    /**
     * 申请表id
     */
    public Long id;
    /**
     * 工作室id
     */
    public Long workRoomId;
    /**
     * 工作室名
     */
    public String workRoomName;
    /**
     * 工作室图标
     */
    public String workRoomIcon;
    /**
     * 审核结果
     */
    public String auditStatus;
    /**
     * 审核结果原因
     */
    public String auditResult;

}
