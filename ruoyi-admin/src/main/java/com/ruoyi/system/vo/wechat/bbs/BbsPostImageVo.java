package com.ruoyi.system.vo.wechat.bbs;


import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 文章图片信息
 */
@Data
@Accessors(chain = true)
public class BbsPostImageVo {

    /**
     * 图片Id
     */
    private Long imageId;

    /**
     * 图片连接
     */
    private String imageUrl;
}
