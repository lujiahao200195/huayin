package com.ruoyi.system.vo.wechat;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class GetArticleList {
    /**
     * 主键ID
     */
    private Long id;

    /**
     * 发布文章的用户ID
     */
    private Long userId;

    /**
     * 发布文章的用户名
     */
    private String userName;

    /**
     * 文章分类
     */
    private String articleType;

    /**
     * 文章标题
     */
    private String articleTitle;

    /**
     * 文章简介
     */
    private String articleNote;

    /**
     * 文章图片
     */
    private String articleImage;

    /**
     * 文章链接
     */
    private String articleLink;

    /**
     * 文章内容
     */
    private String articleContent;

    /**
     * 文章阅读数量
     */
    private Long articleReadNum;

    /**
     * 文章点赞数量
     */
    private Long articleUpNum;

    /**
     * 文章点踩数量
     */
    private Long articleDownNum;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createdTime;

}
