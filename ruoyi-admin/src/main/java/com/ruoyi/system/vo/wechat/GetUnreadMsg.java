package com.ruoyi.system.vo.wechat;

import lombok.Data;

/**
 * 未读消息
 */
@Data
public class GetUnreadMsg {
    private Integer roomId;

    private Long sum;

}
