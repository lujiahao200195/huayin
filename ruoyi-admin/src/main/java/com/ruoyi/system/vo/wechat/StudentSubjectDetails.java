package com.ruoyi.system.vo.wechat;

import lombok.Data;

import java.util.List;

/**
 * 后台管理 学生科目详情
 */
@Data
public class StudentSubjectDetails {

    /**
     * 学生优势科目
     */
    private String advantageSubject;

    /**
     * 需要强化科目集合
     */
    private List<SubjectDto> needSubject;


}
