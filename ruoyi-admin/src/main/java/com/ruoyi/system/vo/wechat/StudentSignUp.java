package com.ruoyi.system.vo.wechat;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class StudentSignUp {

    private Long studentId;

    private List<String> templateId;
}
