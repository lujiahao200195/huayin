package com.ruoyi.system.vo.wechat.bbs;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 版块标签对象
 */
@Data
@Accessors(chain = true)
public class BoardTagVo {

    /**
     * 标签Id
     */
    private Long tagId;

    /**
     * 标签名
     */
    private String tagName;
}
