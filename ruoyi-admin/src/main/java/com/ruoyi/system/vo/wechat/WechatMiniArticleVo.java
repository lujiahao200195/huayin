package com.ruoyi.system.vo.wechat;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

@Data
public class WechatMiniArticleVo {

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 发布文章的用户ID
     */
    @Excel(name = "发布文章的用户ID")
    private Long userId;

    /**
     * 工作室ID
     */
    @Excel(name = "工作室ID")
    private Long workRoomId;

    /**
     * 文章标题
     */
    @Excel(name = "文章标题")
    private String articleTitle;

    /**
     * 文章简介
     */
    @Excel(name = "文章简介")
    private String articleNote;

    /**
     * 文章图片
     */
    @Excel(name = "文章图片")
    private String articleImage;

    /**
     * 文章链接
     */
    @Excel(name = "文章链接")
    private String articleLink;

    /**
     * 文章内容
     */
    @Excel(name = "文章内容")
    private String articleContent;

    /**
     * 文章阅读数量
     */
    @Excel(name = "文章阅读数量")
    private Long articleReadNum;

    /**
     * 文章点赞数量
     */
    @Excel(name = "文章点赞数量")
    private Long articleUpNum;

    /**
     * 文章点踩数量
     */
    @Excel(name = "文章点踩数量")
    private Long articleDownNum;

    @Excel(name = "审核状态")
    private String auditStatus;
}
