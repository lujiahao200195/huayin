package com.ruoyi.system.vo.wechat.backend;

import lombok.Data;

@Data
public class ArticleIdsVo {
    private Long articleId;
}
