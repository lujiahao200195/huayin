package com.ruoyi.system.vo.wechat.bbs;


import lombok.Data;
import lombok.experimental.Accessors;


/**
 * 版块成员加入列表
 */
@Data
@Accessors(chain = true)
public class BoardMemberJoinVo {

    /**
     * 加入的用户ID
     */
    private Long joinUserId;

    /**
     * 用户名称
     */
    private String joinUserName;

    /**
     * 用户头像
     */
    private String joinUserPhoto;

    /**
     * 审核状态
     */
    private String auditStatus;

    /**
     * 版块ID
     */
    private Long boardId;

}
