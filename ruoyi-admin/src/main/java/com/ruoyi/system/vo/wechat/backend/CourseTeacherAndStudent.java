package com.ruoyi.system.vo.wechat.backend;

import lombok.Data;

@Data
public class CourseTeacherAndStudent {

    private Long id;

    private String name;

    private String photo;

    private String type;


}
