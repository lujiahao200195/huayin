package com.ruoyi.system.vo.wechat;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class GlobalSearchVo {

    @JsonProperty("type")
    private String type;
    @JsonProperty("targetId")
    private String targetId;
    @JsonProperty("targetIcon")
    private String targetIcon;
    @JsonProperty("targetName")
    private String targetName;
    @JsonProperty("targetIntro")
    private String targetIntro;
    @JsonProperty("targetExtData")
    private String targetExtData;
    @JsonProperty("targetTagList")
    private List<GlobalSearchTagVo> targetTagList;


}
