package com.ruoyi.system.vo.wechat;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

@Data
public class WorkRoomGetMember {


    /**
     * 表id
     */
    @Excel(name = "表id")
    private Long id;

    /**
     * 工作室ID
     */
    @Excel(name = "工作室ID")
    private Long workRoomId;

    /**
     * 工作室加入类型(用户, 教师, 学生)
     */
    @Excel(name = "工作室加入类型(用户, 教师, 学生)")
    private String joinType;

    /**
     * 目标ID
     */
    @Excel(name = "目标ID")
    private Long targetId;

    /**
     * 头像
     */
    private String photo;

    /**
     * 工作室内用户名称
     */
    @Excel(name = "工作室内用户名称")
    private String userRoomName;

    /**
     * 科目
     */
    private String subject;

    /**
     * 用户简介
     */
    private String userIntro;

}
