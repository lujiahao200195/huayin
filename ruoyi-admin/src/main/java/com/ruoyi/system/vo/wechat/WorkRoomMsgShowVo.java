package com.ruoyi.system.vo.wechat;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

/**
 * 展示用户消息列表
 */
@Data
public class WorkRoomMsgShowVo {

    /**
     * 工作室Id
     */
    private Long workRoomId;

    /**
     * 工作室名字
     */
    private String workRoomName;

    /**
     * 消息类别
     */
    private String messageType;

    /**
     * 目标id
     */
    private Integer targetId;

    /**
     * 姓名
     */
    private String name;

    /**
     * 头像
     */
    private String photo;

    /**
     * 时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedTime;

    /**
     * 当前在线状态
     */
    private String onlineStatus;


}
