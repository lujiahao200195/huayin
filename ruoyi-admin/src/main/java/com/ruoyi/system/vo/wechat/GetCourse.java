package com.ruoyi.system.vo.wechat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@NoArgsConstructor
@Data
public class GetCourse {

    private Long id;

    @JsonProperty("dateTime")
    private String dateTime;
    @JsonProperty("course")
    private List<CourseDTO> course;

    @NoArgsConstructor
    @Data
    public static class CourseDTO {
        @JsonProperty("startTime")
        @JsonFormat(pattern = "HH:mm")
        private Date startTime;
        @JsonProperty("endTime")
        @JsonFormat(pattern = "HH:mm")
        private Date endTime;
        @JsonProperty("subjectStrs")
        private List<String> subjectStrs;
    }
}
