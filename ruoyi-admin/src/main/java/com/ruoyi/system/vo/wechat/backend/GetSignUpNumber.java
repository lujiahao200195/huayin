package com.ruoyi.system.vo.wechat.backend;

import lombok.Data;

@Data
public class GetSignUpNumber {

    /**
     * 数量
     */
    private Integer number;

    /**
     * 类型
     */
    private String type;
}
