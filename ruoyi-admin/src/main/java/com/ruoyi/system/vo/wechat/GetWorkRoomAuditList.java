package com.ruoyi.system.vo.wechat;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class GetWorkRoomAuditList {

    /**
     * 审核表id
     */
    private Long tableId;

    /**
     * 工作室id
     */
    private Long workRoomId;
    /**
     * 教师名
     */
    private String teacherName;

    /**
     * 教师性别
     */
    private String teacherGender;

    /**
     * 教师简介
     */
    private String teacherIntro;

    /**
     * 教师老师照片链接
     */
    private String teacherPicture;

    /**
     * 教师联系电话
     */
    private String teacherPhone;

    /**
     * 教师电子邮箱
     */
    private String teacherEmail;

    /**
     * 教师院校名称
     */
    private String teacherSchool;

    /**
     * 所学专业
     */
    private String teacherMajor;

    /**
     * 当前学历
     */
    private String teacherEducation;

    /**
     * 所在国家或地区
     */
    private String teahcerCountryregion;

    /**
     * 教师政治面貌
     */
    private String teacherParty;

    /**
     * 教师民族
     */
    private String teacherNationality;

    /**
     * 教学或工作经历
     */
    private String teacherExperience;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createdTime;
}
