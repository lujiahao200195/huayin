package com.ruoyi.system.vo.wechat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

@Data
@Accessors(chain = true)
public class SignUpStudentInfoVo {

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 用户ID
     */
    @Excel(name = "用户ID")
    private Long userId;

    /**
     * 学生简介
     */
    @Excel(name = "学生简介")
    private String studentIntro;

    /**
     * 学号
     */
    @Excel(name = "学号")
    private String studentCode;

    /**
     * 作为学生的名字
     */
    @Excel(name = "作为学生的名字")
    private String studentName;

    /**
     * 学生出生日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "学生出生日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date studentBirthday;

    /**
     * 学龄
     */
    @Excel(name = "学龄")
    private String studentAge;

    /**
     * 学生手机号
     */
    @Excel(name = "学生手机号")
    private String studentPhone;

    /**
     * 学生性别
     */
    private String studentGender;

    /**
     * 学生邮箱
     */
    @Excel(name = "学生邮箱")
    private String studentEmail;

    /**
     * 学生目表分数
     */
    @Excel(name = "学生目表分数")
    private Long studentTargetScore;

    /**
     * 知情同意
     */
    @Excel(name = "知情同意")
    private String studentInformedConsent;

    /**
     * 手写签名图片
     */
    @Excel(name = "手写签名图片")
    private String studentHandwrittenSignature;

    /**
     * 审核状态
     */
    @Excel(name = "审核状态")
    private String auditStatus;

    /**
     * 报名日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createdTime;

    /**
     * 学生需要强化的科目
     */
    private List<StudentNeedSubject> studentNeedSubject;

    @Data
    @Accessors(chain = true)
    public static class StudentNeedSubject {

        /**
         * 科目Id
         */
        private Long subjectId;

        /**
         * 科目名称
         */
        private String subjectName;

        /**
         * 学习进度
         */
        private String learningProgress;

        /**
         * 当前分数
         */
        private Long currentScore;

        /**
         * 目标分数
         */
        private Long targetScore;
    }

}
