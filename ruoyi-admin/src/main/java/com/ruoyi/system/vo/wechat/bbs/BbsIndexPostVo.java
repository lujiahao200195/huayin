package com.ruoyi.system.vo.wechat.bbs;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

@Data
@Accessors(chain = true)
public class BbsIndexPostVo {

    /**
     * 文章ID
     */
    private Long postId;

    /**
     * 文章点赞数
     */
    private Long postUpNum;

    /**
     * 文章评论数
     */
    private Long postCommandNum;

    /**
     * 文章转发数
     */
    private Long postShareNum;

    /**
     * 文章阅读数
     */
    private Long postVisitNum;

    /**
     * 文章标题
     */
    private String postTitle;

    /**
     * 版块Id
     */
    private Long boardId;

    /**
     * 版块名字
     */
    private String boardName;

    /**
     * 图标
     */
    private String boardIcon;

    /**
     * 文章标题/没有标题则显示文章内容简写
     */
    private String postIntro;

    /**
     * 文章类型
     */
    private String postType;

    /**
     * 发帖时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date sendTime;

    /**
     * 文章图片列表
     */
    private List<BbsPostImageVo> postImageVoList;

}
