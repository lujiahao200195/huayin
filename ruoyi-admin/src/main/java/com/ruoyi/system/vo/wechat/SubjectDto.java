package com.ruoyi.system.vo.wechat;

import lombok.Data;

/**
 * 强化科目
 */
@Data
public class SubjectDto {
    /**
     * 科目名
     */
    private String subjectName;
    /**
     * 目前进度
     */
    private String studentRate;
    /**
     * 当前分数
     */
    private Integer currentScore;
    /**
     * 目标分数
     */
    private Integer targetScore;

}
