package com.ruoyi.system.vo.wechat.bbs;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 版块信息
 */
@Data
@Accessors(chain = true)
public class BbsBoardInfo {

    /**
     * 版块Id
     */
    private Long boardId;

    /**
     * 版块名称
     */
    private String boardName;

    /**
     * 板块图标
     */
    private String boardIcon;

    /**
     * 版块简介
     */
    private String boardIntro;

    /**
     * 版块规则文章Id
     */
    private TopPost boardRulePost;

    /**
     * 版块置顶文章Id
     */
    private List<TopPost> boardTopPostList;
}

