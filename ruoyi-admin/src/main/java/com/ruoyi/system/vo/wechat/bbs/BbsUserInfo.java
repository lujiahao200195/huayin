package com.ruoyi.system.vo.wechat.bbs;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 论坛用户信息
 */
@Data
@Accessors(chain = true)
public class BbsUserInfo {

    /**
     * 用户Id
     */
    private Long userId;

    /**
     * 论坛数据用户ID
     */
    private Long bbsUserId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 经验数
     */
    private Long expNum;

    /**
     * 积分数
     */
    private Long pointNum;

    /**
     * 用户简介
     */
    private String userIntro;

    /**
     * 用户性别
     */
    private String userGender;

    /**
     * 用户照片
     */
    private String userPhoto;
}
