package com.ruoyi.system.vo.wechat;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class LoginVo {

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 用户名
     */
    @Excel(name = "用户名")
    private String userName;

    /**
     * 性别
     */
    @Excel(name = "性别")
    private String userGender;

    /**
     * 手机号
     */
    @Excel(name = "手机号")
    private String userPhone;

    /**
     * 邮箱
     */
    @Excel(name = "邮箱")
    private String userEmail;

    /**
     * 用户出身年月
     */
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @Excel(name = "用户出身年月", width = 30, dateFormat = "yyyy-MM-dd")
    private String userBirth;

    /**
     * 用户头像链接
     */
    @Excel(name = "用户头像链接")
    private String userPhoto;


    private String wechatMiniOpenId;

    /**
     * 用户简介
     */
    private String userIntro;

    /**
     * 教师数据
     */
    private TeacherInfo teacherInfo;

    /**
     * 学生数据
     */
    private StudentInfo studentInfo;

    /**
     * 是否阅读隐私条例
     */
    private String readPrivacyPolicy;

    @Data
    @Accessors(chain = true)
    public static class TeacherInfo {

        /**
         * 教师ID
         */
        private Long teacherId;

        /**
         * 用户ID
         */
        @Excel(name = "用户ID")
        private Long userId;

        /**
         * 教师简介
         */
        @Excel(name = "教师简介")
        private String teacherIntro;

        /**
         * 作为教师时的教师名
         */
        @Excel(name = "作为教师时的教师名")
        private String teacherName;

        /**
         * 教师编号
         */
        @Excel(name = "教师编号")
        private String teacherCode;


        /**
         * 教师老师照片链接
         */
        @Excel(name = "教师老师照片链接")
        private String teacherPicture;

        /**
         * 教师联系电话
         */
        @Excel(name = "教师联系电话")
        private String teacherPhone;

        /**
         * 教师电子邮箱
         */
        @Excel(name = "教师电子邮箱")
        private String teacherEmail;

        /**
         * 教师院校名称
         */
        @Excel(name = "教师院校名称")
        private String teacherSchool;

        /**
         * 所学专业
         */
        @Excel(name = "所学专业")
        private String teacherMajor;

        /**
         * 当前学历
         */
        @Excel(name = "当前学历")
        private String teacherEducation;

        /**
         * 所在国家或地区
         */
        @Excel(name = "所在国家或地区")
        private String teacherCountryRegion;

        /**
         * 教师政治面貌
         */
        @Excel(name = "教师政治面貌")
        private String teacherParty;

        /**
         * 教师民族
         */
        @Excel(name = "教师民族")
        private String teacherNationality;

        /**
         * 教学或工作经历
         */
        @Excel(name = "教学或工作经历")
        private String teacherExperience;
    }

    @Data
    @Accessors(chain = true)
    public static class StudentInfo {

        /**
         * 学生Id
         */
        private Long studentId;

        @Excel(name = "用户ID")
        private Long userId;

        /**
         * 学号
         */
        @Excel(name = "学号")
        private String studentCode;

        /**
         * 作为学生的名字
         */
        @Excel(name = "作为学生的名字")
        private String studentName;

        /**
         * 学生出生日期
         */
        @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
        @Excel(name = "学生出生日期", width = 30, dateFormat = "yyyy-MM-dd")
        private String studentBirthday;

        /**
         * 学龄
         */
        @Excel(name = "学龄")
        private String studentAge;

        /**
         * 学生手机号
         */
        @Excel(name = "学生手机号")
        private String studentPhone;

        /**
         * 学生邮箱
         */
        @Excel(name = "学生邮箱")
        private String studentEmail;

        /**
         * 学生目表分数
         */
        @Excel(name = "学生目表分数")
        private Long studentTargetScore;

        /**
         * 学生简介
         */
        @Excel(name = "学生简介")
        private String studentIntro;
    }

}
