package com.ruoyi.system.vo.wechat;

/**
 * 工作室成员详情
 */

public class WorkRoomMemberDetails<T> {
    private String type;

    private T data;

    public WorkRoomMemberDetails() {
    }

    public WorkRoomMemberDetails(String type, T data) {
        this.type = type;
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
