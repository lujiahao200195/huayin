package com.ruoyi.system.vo.wechat.bbs;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

@Data
@Accessors(chain = true)
public class PostReplyVo {

    /**
     * 回复Id
     */
    private Long replyId;

    /**
     * 用户Id
     */
    private Long userId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 用户头像
     */
    private String userPhoto;

    /**
     * 评论内容
     */
    private String replyContent;

    /**
     * 回复点赞内容
     */
    private Long replyUpNum;

    /**
     * 回复时间
     */
    private Date replyTime;

    /**
     * 评论的回复列表
     */
    private List<ChildReplyVo> childReply;
}
