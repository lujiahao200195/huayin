package com.ruoyi.system.vo.wechat.rs;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

/**
 * 资源数据
 */
@Accessors(chain = true)
@Data
public class ResourceVo {

    /**
     * 资源Id
     */
    private Long resourceId;

    /**
     * 用户Id
     */
    private Long userId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 资源封面
     */
    private String resourceCover;

    /**
     * 资源标题
     */
    private String resourceTitle;

    /**
     * 资源简介
     */
    private String resourceIntro;

    /**
     * 资源标签列表
     */
    private List<ResourceTagVo> resourceTagVoList;

    /**
     * 资源链接
     */
    private String resourceLink;

    /**
     * 下载量
     */
    private Long downloadNum;

    /**
     * 资源大小
     */
    private String size;

    /**
     * 文件列表
     */
    private String fileType;
}
