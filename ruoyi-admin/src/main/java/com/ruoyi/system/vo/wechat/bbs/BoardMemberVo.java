package com.ruoyi.system.vo.wechat.bbs;


import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 版块成员列表
 */
@Data
@Accessors(chain = true)
public class BoardMemberVo {

    /**
     * 版块ID
     */
    private Long boardId;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 职位名称
     */
    private String jobName;
}
