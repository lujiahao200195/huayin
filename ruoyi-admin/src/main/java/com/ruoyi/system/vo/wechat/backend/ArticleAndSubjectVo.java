package com.ruoyi.system.vo.wechat.backend;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.Getter;

import java.time.LocalDateTime;

@Data
public class ArticleAndSubjectVo extends BaseEntity{



    private Integer id;
    private Integer articleId;

    private String subjectId;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime createdTime;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime updatedTime;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime deleteTime;
    private String subjectName;
    private String articleTitle;
    private String articleNote;
    private String articleImage;
    private String articleLink;
    private String articleContent;
    private Integer articleReadNum;
    private Integer articleUpNum;
    private Integer articleDownNum;
    private String auditStatus;

    private Integer userId;
    private Integer workRoomId;
    private String note;



}
