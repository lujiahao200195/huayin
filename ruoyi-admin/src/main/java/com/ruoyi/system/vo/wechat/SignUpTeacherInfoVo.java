package com.ruoyi.system.vo.wechat;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.system.vo.wechat.SignUpTeacherInfo.Subject;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

@Data
@Accessors(chain = true)
public class SignUpTeacherInfoVo {

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 用户ID
     */
    @Excel(name = "用户ID")
    private Long userId;

    /**
     * 作为教师时的教师名
     */
    @Excel(name = "作为教师时的教师名")
    private String teacherName;

    /**
     * 教师编号
     */
    @Excel(name = "教师编号")
    private String teacherCode;

    /**
     * 教师评分
     */
    @Excel(name = "教师评分")
    private Long teacherScore;

    /**
     * 教师性别
     */
    private String teacherGender;

    /**
     * 教师出生日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date teacherBirthday;

    /**
     * 教师简介
     */
    @Excel(name = "教师简介")
    private String teacherIntro;

    /**
     * 教师老师照片链接
     */
    @Excel(name = "教师老师照片链接")
    private String teacherPicture;

    /**
     * 教师联系电话
     */
    @Excel(name = "教师联系电话")
    private String teacherPhone;

    /**
     * 教师电子邮箱
     */
    @Excel(name = "教师电子邮箱")
    private String teacherEmail;

    /**
     * 教师院校名称
     */
    @Excel(name = "教师院校名称")
    private String teacherSchool;

    /**
     * 所学专业
     */
    @Excel(name = "所学专业")
    private String teacherMajor;

    /**
     * 当前学历
     */
    @Excel(name = "当前学历")
    private String teacherEducation;

    /**
     * 所在国家或地区
     */
    @Excel(name = "所在国家或地区")
    private String teacherCountryRegion;

    /**
     * 教师知情简述
     */
    @Excel(name = "教师知情简述")
    private String teacherInformedBrief;

    /**
     * 教师签名
     */
    @Excel(name = "教师签名")
    private String teacherSignature;

    /**
     * 教师政治面貌
     */
    @Excel(name = "教师政治面貌")
    private String teacherParty;

    /**
     * 教师民族
     */
    @Excel(name = "教师民族")
    private String teacherNationality;

    /**
     * 教学或工作经历
     */
    @Excel(name = "教学或工作经历")
    private String teacherExperience;

    /**
     * 备注
     */
    @Excel(name = "备注")
    private String teacherRemark;

    /**
     * 教师状态
     */
    @Excel(name = "教师状态")
    private String teacherStatus;

    /**
     * 审核状态
     */
    @Excel(name = "审核状态")
    private String auditStatus;

    /**
     * 引荐用户名
     */
    private String referrerUserName;


    /**
     * 教师证书
     */
    private List<String> teacherCertificate;

    /**
     * 教师擅长科目
     */
    private List<Subject> subjectList;

    /**
     * 教学时段
     */
    private List<TeacherSchedule> teacherScheduleList;

    /**
     * 教师优势列表
     */
    private List<TeacherAdvantage> teacherAdvantageList;

    /**
     * 报名日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createdTime;

    /**
     * 教师可安排时间
     */
    @Data
    public static class TeacherSchedule {

        /**
         * 可安排月份
         */
        private String scheduleMonth;

        /**
         * 可安排的星期
         */
        private String scheduleWeek;

        /**
         * 开始时间
         */
        @JsonFormat(pattern = "HH:mm")
        private Date startTime;

        /**
         * 结束时间
         */
        @JsonFormat(pattern = "HH:mm")
        private Date endTime;

        /**
         * 教师安排备注, 由教师自己备注(比如什么什么时间点有事情)
         */
        private String planNote;
    }


    /**
     * 教师个人优势数据
     */
    @Data
    public static class TeacherAdvantage {
        private String teacherAdvantage;

        private String advantageRemark;

    }
}
