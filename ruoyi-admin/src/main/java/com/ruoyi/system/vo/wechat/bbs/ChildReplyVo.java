package com.ruoyi.system.vo.wechat.bbs;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
public class ChildReplyVo {

    /**
     * 主键Id
     */
    private Long id;

    /**
     * 用户Id
     */
    private Long userId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 回复用户的Id
     */
    private Long replyUserId;

    /**
     * 回复用户的名字
     */
    private String replyUserName;

    /**
     * 用户头像
     */
    private String userPhoto;

    /**
     * 回复内容
     */
    private String replyContent;

    /**
     * 回复点赞数
     */
    private Long replyUpNum;

    /**
     * 回复点踩数
     */
    private Long replyDownNum;

    /**
     * 回复时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date replyTime;

}
