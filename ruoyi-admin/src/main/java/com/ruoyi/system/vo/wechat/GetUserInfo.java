package com.ruoyi.system.vo.wechat;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
public class GetUserInfo {

    /**
     * 用户名
     */
    private String username;

    /**
     * 性别
     */
    private String gender;

    /**
     * 用户电话号
     */
    private String phone;

    /**
     * 用户电子邮箱
     */
    private String email;

    /**
     * 用户出生日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthDay;

    /**
     * 用户头像数据
     */
    private String photo;

    /**
     * 用户个人简介
     */
    private String intro;


}
