package com.ruoyi.system.common.tools;

import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.system.common.config.CosPropertiesUtils;
import com.ruoyi.system.common.config.Custom;
import com.ruoyi.system.mapper.AopMapper;
import com.ruoyi.system.vo.wechat.LoginVo;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import okhttp3.*;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Component
public class NTools {

    public static Custom custom;
    /**
     * 微信公众号的AccessToken
     */
    public static String WxAccessToken = "";
    public static CosPropertiesUtils cosPropertiesUtils;
    @Resource
    AopMapper aopMapper;
    @Resource
    RedisTemplate redisTemplate;

    /**
     * 快速构造一个Map对象
     *
     * @return
     */
    public static MapP genMapP() {
        return new MapP();
    }

    /**
     * 对象转字符串
     *
     * @param o
     * @return
     */
    public static String json(Object o) {
        return JSONObject.toJSONString(o);
    }

    /**
     * 返回当前格式化时间
     * 格式(年-月-日-时-分-秒)
     *
     * @return
     */
    public static String createTime() {

//        //当前系统时间
//        Date now = new Date();
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式(年-月-日-时-分-秒)
//        String createTime = dateFormat.format(now);//格式化然后放入字符串中
//        //课程名称
//        StringBuilder stringBuilder = new StringBuilder();
//        for (int i = 0; i < subjects.length; i++) {
//            stringBuilder.append(subjects[i]);
//            if (i < subjects.length - 1) {
//                stringBuilder.append("，");
//            }
//        }
//        String subjectsstr = stringBuilder.toString();


        //当前系统时间
        Date now = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式(年-月-日-时-分-秒)
        String createTime = dateFormat.format(now);//格式化然后放入字符串中
        return createTime;
    }

    /**
     * 返回2023-09-06
     *
     * @return
     */
    public static String createBirthDay(Date date) {
        System.out.println("0:" + date);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");//设置日期格式(年-月-日-时-分-秒)
        String createTime = dateFormat.format(date);//格式化然后放入字符串中
        return createTime;
    }

    /**
     * 字符串数组转字符串
     *
     * @param subjects
     * @return
     */
    public static String arrayToStr(String[] subjects) {
        //课程名称
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < subjects.length; i++) {
            stringBuilder.append(subjects[i]);
            if (i < subjects.length - 1) {
                stringBuilder.append("，");
            }
        }
        return stringBuilder.toString();
    }

    /**
     * 随机生成8位数字字符串
     *
     * @return
     */
    public static String getRandomMath() {
        int length = 8; // 字符串长度
        // 随机数生成器
        Random random = new Random();
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            // 生成随机数字并将其转换为字符添加到字符串中
            int digit = random.nextInt(10); // 生成 0~9 之间的随机整数
            sb.append(digit);
        }
        String randomString = sb.toString();
        System.out.println("工作室: " + randomString);
        return randomString;
    }

    /**
     * 使用分钟
     * 创建过期时间
     *
     * @return
     */
    public static Date createExpirationTime(int time) {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
//        int minutesToAdd = time; // 需要加的分钟数
        calendar.add(Calendar.MINUTE, time);
        Date resultDate = calendar.getTime();
        System.out.println(resultDate);
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式(年-月-日-时-分-秒)
//        String createTime = dateFormat.format(resultDate);//格式化然后放入字符串中
//        System.out.println("现在  "+dateFormat.format(date));
//        System.out.println("新增  "+dateFormat.format(resultDate));
        return resultDate;
    }

    /**
     * 创建令牌
     *
     * @param subject 令牌主题, 自定义对外公布的信息
     * @return 返回加密的令牌
     */
    public static String generateToken(String subject) {
        long expirationTimeInMillis = 1000 * 60 * 60 * 48; // 设置过期时间为48小时

        return Jwts.builder()
                .setSubject(subject)
                .setExpiration(new Date(System.currentTimeMillis() + expirationTimeInMillis))
                .signWith(SignatureAlgorithm.HS512, custom.SECRET_KEY)
                .compact();
    }

    /**
     * 提前令牌主题
     *
     * @param token 令牌字符串
     * @return 返回主题字符串
     */
    public static String extractSubjectFromToken(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(custom.SECRET_KEY)
                .parseClaimsJws(token)
                .getBody();
        return claims.getSubject();
    }

    /**
     * 验证令牌
     *
     * @param token 令牌字符串
     * @return 返回是否验证成功
     */
    public static boolean validateToken(String token) {
        try {
            Jwts.parser().setSigningKey(custom.SECRET_KEY).parseClaimsJws(token);
            return true;
        } catch (JwtException | IllegalArgumentException e) {
            // 处理token过期或者验证失败的情况
            return false;
        }
    }

    /**
     * 截取字符串, 如果结束字符串大于字符串的长度,则截取到
     *
     * @param beginIndex
     * @param endIndex
     * @return
     */
    public static String subString(String oldStr, int beginIndex, int endIndex) {
        if (endIndex > oldStr.length()) {
            endIndex = oldStr.length();
        }
        return oldStr.substring(beginIndex, endIndex);
    }

    /**
     * 判断是否为空并进行类型转换, 出了问题抛异常
     *
     * @param o
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T mustData(Object o, Class<T> clazz) {
        if (o == null) {
            throw new RuntimeException("数据为空");
        }
        T rs = null;
        try {
            rs = clazz.cast(o);
        } catch (Exception e) {
            throw new RuntimeException("数据类型异常");
        }
        return rs;
    }

    /**
     * 判断是否为空并进行类型转换, 出了问题抛异常
     *
     * @param o
     * @param clazz
     * @param errorMsg
     * @param <T>
     * @return
     */
    public static <T> T mustData(Object o, Class<T> clazz, String errorMsg) {
        if (o == null) {
            throw new RuntimeException(errorMsg);
        }
        T rs = null;
        try {
            if (o.getClass() == Integer.class) {
                rs = (T) (Long.valueOf(((Integer) o).longValue()));
            } else {
                rs = clazz.cast(o);
            }
        } catch (Exception e) {
            throw new RuntimeException(errorMsg);
        }
        return rs;
    }

    /**
     * 判断是否为空并进行类型转换
     *
     * @param o
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T data(Object o, Class<T> clazz) {
        if (o == null) {
            return null;
        }
        T rs = null;
        try {
            if (o.getClass() == Integer.class) {
                rs = (T) (Long.valueOf(((Integer) o).longValue()));
            } else {
                rs = clazz.cast(o);
            }
        } catch (Exception e) {
            throw new RuntimeException("数据类型异常");
        }
        return rs;
    }

    /**
     * 判断是否为空并进行类型转换
     *
     * @param o
     * @param clazz
     * @param errorMsg
     * @param <T>
     * @return
     */
    public static <T> T data(Object o, Class<T> clazz, String errorMsg) {
        if (o == null) {
            return null;
        }
        T rs = null;
        try {
            rs = clazz.cast(o);
        } catch (Exception e) {
            throw new RuntimeException(errorMsg);
        }
        return rs;
    }

    @Resource
    public void custom(Custom custom) {
        NTools.custom = custom;
    }

    @Resource
    public void CosPropertiesUtils(CosPropertiesUtils cosPropertiesUtils) {
        NTools.cosPropertiesUtils = cosPropertiesUtils;
    }

    public void deleteLoginCache() {
        redisTemplate.delete("wxmini:user-info:" + getLoginInfo().getWechatMiniOpenId());
    }

    /**
     * 从请求头中获取token
     *
     * @return
     */
    public String getToken() {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = null;
        if (requestAttributes != null) {
            request = requestAttributes.getRequest();
        }
        //从请求头获取token
        String token = request.getHeader("wechat-mini-token");
        if (token == null || "".equals(token)) {
            throw new RuntimeException("用户登入信息错误, 请登入");
        }
        return token;
    }

    /**
     * 获取当前请求的ip地址
     *
     * @return
     */
    public String getIp() {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = null;
        if (requestAttributes != null) {
            request = requestAttributes.getRequest();
        }
        //从请求头获取ip
        String remoteAddr = request.getRemoteAddr();
        return remoteAddr;
    }

    /**
     * 从request中 根据小程序openid查询用户名
     *
     * @return
     */
    public String getUserName() {
        String openId = getMiniOpenId();
        //redis 根据openId 取userName
        String userName = (String) redisTemplate.opsForValue().get("wxmini:username:" + openId);
        if (userName == null || "".equals(userName)) {
            userName = aopMapper.selectUserNameByOpenId(openId);
            redisTemplate.opsForValue().set("wxmini:username:" + openId, userName, 10, TimeUnit.MINUTES);
        }
        return userName;
    }

    /**
     * 获取登入数据
     *
     * @return
     */
    public LoginVo getLoginInfo() {
        String subjectJson = extractSubjectFromToken(getToken());
        if (subjectJson == null || "".equals(subjectJson)) {
            throw new RuntimeException("未登入, 请登入.");
        }
        LoginVo loginVo = JSONObject.parseObject(subjectJson, LoginVo.class);
        if (loginVo == null) {
            throw new RuntimeException("用户数据异常");
        }
        return loginVo;
    }

    /**
     * 获取教师数据
     *
     * @return
     */
    public LoginVo.TeacherInfo getTeacherInfo() {
        LoginVo loginInfo = getLoginInfo();
        LoginVo.TeacherInfo teacherInfo = loginInfo.getTeacherInfo();
        if (teacherInfo == null) {
            throw new RuntimeException("教师数据异常");
        }
        return teacherInfo;
    }

    /**
     * 获取学生数据
     *
     * @return
     */
    public LoginVo.StudentInfo getStudentInfo() {
        LoginVo loginInfo = getLoginInfo();
        LoginVo.StudentInfo studentInfo = loginInfo.getStudentInfo();
        if (studentInfo == null) {
            throw new RuntimeException("学生数据异常");
        }
        return studentInfo;
    }

    /**
     * 从request头中 获取openid
     *
     * @return
     */
    public String getMiniOpenId() {
        // 返回openId
        return getLoginInfo().getWechatMiniOpenId();
    }

    /**
     * 发送微信小程序客服消息给用户
     *
     * @param userOpenId 用户微信小程序openId
     * @param msg        要发送的消息
     * @throws IOException
     */
    public void sendCustomerService(String userOpenId, String msg) throws IOException {
        String url = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=" + getWechatMiniAccessToken();
        String requestDatas = "{\n" +
                "  \"touser\":\"" + userOpenId + "\",\n" +
                "  \"msgtype\":\"text\",\n" +
                "  \"text\":\n" +
                "  {\n" +
                "    \"content\":\"" + msg + "\"\n" +
                "  }\n" +
                "}";
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), requestDatas);
        Request request = new Request.Builder()
                .url(url)
                .post(requestBody)
                .build();
        Call call = OkHttp3Tools.client.newCall(request);
        //4.执行同步请求，获取响应体Response对象
        Response response = call.execute();
        JSONObject jsonObject = JSONObject.parseObject(response.body().string());
        Integer errcode = jsonObject.getInteger("errcode");
        if (errcode != 0) {
            throw new RuntimeException("发送失败: " + jsonObject.getString("errmsg"));
        }
    }

    public void send() throws IOException {
        String requestDatas = "{\n" +
                "    \"touser\": \"ooL2_t76f_SvEAX5k7e3Cgy2RG-I\",\n" +
                "    \"template_id\": \"mK_KygM-lEju9NhL5Hwa36vKvh-9hMBnLM0Pp62iBpc\",\n" +
//                "    \"url\": \"http://weixin.qq.com/download\",\n" +
                "    \"miniprogram\": {\n" +
                "        \"appid\": \"wx61311d73fb1285c5\"\n"+
                "     }\n"+
                "    \"data\": {\n" +
                "        \"keyword1\": {\n" +
                "            \"value\": \"小明\"\n" +
                "        },\n" +
                "        \"keyword2\": {\n" +
                "            \"value\": \"12345678912\"\n" +
                "        },\n" +
                "        \"keyword3\": {\n" +
                "            \"value\": \"数学，语文\"\n" +
                "        },\n" +
                "        \"keyword4\": {\n" +
                "            \"value\": \"2022年1月1日 22:22:22\"\n" +
                "        }\n" +
                "    }\n" +
                "}";
        System.out.println(requestDatas);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), requestDatas);
        String accessToken = getWechatMiniAccessToken();
        Request request = new Request.Builder()
                .url("https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" + accessToken)
                .post(requestBody)
                .build();
        Call call = OkHttp3Tools.client.newCall(request);
        //4.执行同步请求，获取响应体Response对象
        Response response = call.execute();
        System.out.println(response.body().string());
    }

    public static String getWechatArticleAccessToken() throws IOException {

        String appId = "wx61311d73fb1285c5";
        String secret = "7b87a59c24acc46a481f7cfefd8430e0";

        Request request = new Request.Builder()
                .url("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appId + "&secret=" + secret)
                .get()
                .build();

        Call call = OkHttp3Tools.client.newCall(request);
        Response response = call.execute();
        JSONObject jsonObject = JSONObject.parseObject(response.body().string());
        return jsonObject.getString("access_token");
    }

    /**
     * 获取小程序AccessToken
     *
     * @return
     * @throws IOException
     */
    public String getWechatMiniAccessToken() throws IOException {
        String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + custom.WechatMiniAppID + "&secret=" + custom.WechatMiniSecret;
        Request request = new Request.Builder()
                .url(url)
                .get()
                .build();
        Call call = OkHttp3Tools.client.newCall(request);
        //4.执行同步请求，获取响应体Response对象
        Response response = call.execute();
        return JSONObject.parseObject(response.body().string()).getString("access_token");

    }

    /**
     * 传回当前日期的一周的时间
     * @param dateStr
     * @return
     * @throws ParseException
     */
    public static List<String> printWeekdays(String dateStr) throws ParseException {
        SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
        Date reDate =  dateFormat2.parse(dateStr);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(reDate);
//        从周日开始
//        while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY) {
//            calendar.add(Calendar.DATE, -1);
//        }
        //从周一开始
        while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY) {
            calendar.add(Calendar.DATE, -1);
        }
        List<String> dateList = new ArrayList<String>();
        for (int i = 0; i < 7; i++) {
            String dayTemp = dateFormat2.format(calendar.getTime());
            dateList.add(dayTemp);
            calendar.add(Calendar.DATE, 1);
        }
        return dateList;
    }


}
