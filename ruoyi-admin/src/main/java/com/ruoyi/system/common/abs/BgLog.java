package com.ruoyi.system.common.abs;

import java.lang.annotation.*;

/**
 * 后台管理日志
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface BgLog {

    /**
     * 要执行的操作类型
     **/
    String methodType() default "";

    /**
     * 要执行的操作内容
     **/
    String moduleContent() default "";

}
