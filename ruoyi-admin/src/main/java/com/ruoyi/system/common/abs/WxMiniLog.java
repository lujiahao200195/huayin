package com.ruoyi.system.common.abs;

import java.lang.annotation.*;

/**
 * 用户小程序日志
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WxMiniLog {
    /**
     * 要执行的操作类型
     **/
    String methodDesc() default "";

    /**
     * 要执行的操作名称
     **/
    String moduleName() default "";

}
