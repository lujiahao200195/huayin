package com.ruoyi.system.common.wechatmsg;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@NoArgsConstructor
@Data
public class Tag {

    @JsonProperty("tags")
    private List<TagsDTO> tags;

    @NoArgsConstructor
    @Data
    public static class TagsDTO {
        @JsonProperty("id")
        private Integer id;
        @JsonProperty("name")
        private String name;
        @JsonProperty("count")
        private Integer count;
    }
}
