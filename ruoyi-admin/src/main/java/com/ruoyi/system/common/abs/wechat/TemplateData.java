package com.ruoyi.system.common.abs.wechat;

import com.alibaba.fastjson2.JSONObject;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/**
 * 公众号通知data
 */
public interface TemplateData {

    default String json() {
        return JSONObject.toJSONString(this);
    }

    /**
     * 使用放射整合通知模板数据
     * 公众号openid
     *
     * @param openId
     * @param weChatTemplateEnum
     * @throws IllegalAccessException
     */
    default String msgTemplateData(String openId, String weChatTemplateEnum, HashMap<String, String> data) throws IllegalAccessException {
        Map<String, Object> map = new HashMap<>();
        //包含private的字段 getDeclaredFields
        Field[] fields = this.getClass().getDeclaredFields();

        Map<String, Object> map1 = new HashMap<>();
        for (Field declaredField : fields) {
            declaredField.setAccessible(true);
            String name = declaredField.getName();
//            System.out.println(name);
            Object o = declaredField.get(this);
            Map<String, Object> map2 = new HashMap<>();
            if (o == null || "".equals(o)) {
                o = " ";
            }
            map2.put("value", o);
            map1.put(name, map2);
        }
        map.put("touser", openId);
        map.put("template_id", weChatTemplateEnum);
        map.put("data", map1);
        map.put("miniprogram", data);
        return JSONObject.toJSONString(map);
    }
}
