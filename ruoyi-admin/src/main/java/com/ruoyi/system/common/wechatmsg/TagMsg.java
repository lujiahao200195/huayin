package com.ruoyi.system.common.wechatmsg;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class TagMsg {

    @JsonProperty("count")
    private Integer count;
    @JsonProperty("data")
    private DataDTO data;
    @JsonProperty("next_openid")
    private String nextOpenid;

    @NoArgsConstructor
    @Data
    public static class DataDTO {
        @JsonProperty("openid")
        private List<String> openid;
    }
}
