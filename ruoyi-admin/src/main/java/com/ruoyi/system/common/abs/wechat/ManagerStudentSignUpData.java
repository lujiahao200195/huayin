package com.ruoyi.system.common.abs.wechat;

import lombok.Data;

/**
 * 公众号 管理员
 * 新学员提交报名审核消息
 */
@Data
public class ManagerStudentSignUpData implements TemplateData {
    /**
     * 姓名
     */
    private String thing5;
    /**
     * 电话
     */
    private String thing1;
    /**
     * 课程
     */
    private String phone_number8;
}
