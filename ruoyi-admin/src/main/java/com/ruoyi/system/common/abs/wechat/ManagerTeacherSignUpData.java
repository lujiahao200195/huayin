package com.ruoyi.system.common.abs.wechat;

import lombok.Data;

/**
 * 管理员
 * 教师提交报名审核消息
 */
@Data
public class ManagerTeacherSignUpData implements TemplateData {
    /**
     * 教师姓名
     */
    private String thing1;

    /**
     * 时间
     */
    private String time2;


}
