package com.ruoyi.system.common.tools;

import com.alibaba.fastjson2.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MapP {

    private Map<String, Object> map;

    public MapP() {
        this.map = new HashMap<>();
    }

    public MapP put(String key, Object value) {
        this.map.put(key, value);
        return this;
    }

    public MapP delete(String key) {
        this.map.remove(key);
        return this;
    }

    public String json() {
        return JSONObject.toJSONString(this.map);
    }
}
