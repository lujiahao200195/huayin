package com.ruoyi.system.common;

import lombok.Data;

/**
 * 解析微信订阅消息返回值对象
 */
@Data
public class WxReturnedValue {
    private int errcode;
    private String errmsg;
    private Long msgid;

    @Override
    public String toString() {
        return "WxReturnedValue{" +
                "errcode=" + errcode +
                ", errmsg='" + errmsg + '\'' +
                ", msgid=" + msgid +
                '}';
    }
}
