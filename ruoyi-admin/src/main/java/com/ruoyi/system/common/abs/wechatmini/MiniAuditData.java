package com.ruoyi.system.common.abs.wechatmini;

import lombok.Data;

/**
 * 审核结果通知
 */
@Data
public class MiniAuditData implements TemplateData {

    /**
     * 申请人
     */
    private String thing8;
    /**
     * 审核结果
     */
    private String phrase1;
    /**
     * 备注
     */
    private String thing3;
    /**
     * 审核时间
     */
    private String time2;

}
