package com.ruoyi.system.common.config;

import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.region.Region;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class CosPropertiesUtils {


    @Value("${tencent.cos.secret-id}")
    private String secretId;

    @Value("${tencent.cos.secret-key}")
    private String secretKey;

    @Value("${tencent.cos.region}")
    private String region;

    @Getter
    @Value("${tencent.cos.bucket-name}")
    private String bucketName;
    @Getter
    @Value("${tencent.cos.base-url}")
    private String baseUrl;

    private COSClient cosClient;
    @Value("${tencent.private-cos.secret-id}")
    private String secretIdPrivate;
    @Value("${tencent.private-cos.secret-key}")
    private String secretKeyPrivate;
    @Value("${tencent.private-cos.region}")
    private String regionPrivate;
    @Getter
    @Value("${tencent.private-cos.bucket-name}")
    private String bucketNamePrivate;
    @Getter
    @Value("${tencent.private-cos.base-url}")
    private String baseUrlPrivate;
    private COSClient cosPrivateClient;

    public COSClient getCOSClient() {
        // 初始化 COS 客户端
        ClientConfig clientConfig = new ClientConfig(new Region(region));
        BasicCOSCredentials credentials = new BasicCOSCredentials(secretId, secretKey);
        cosClient = new COSClient(credentials, clientConfig);
        return cosClient;
    }

    public COSClient getCosPrivateClient() {
        // 初始化 COS 客户端
        ClientConfig clientConfig = new ClientConfig(new Region(regionPrivate));
        BasicCOSCredentials credentials = new BasicCOSCredentials(secretIdPrivate, secretKeyPrivate);
        cosPrivateClient = new COSClient(credentials, clientConfig);
        return cosPrivateClient;
    }

}
