package com.ruoyi.system.common.abs.wechatmini;

import lombok.Data;

/**
 * 小程序报名成功通知data
 */
@Data
public class MiniStudentSignUpData implements TemplateData {

    /**
     * 学生名
     */
    private String name3;

    /**
     * 学生报名的课程字符串
     */
    private String thing1;

    /**
     * 申请时间
     */
    private String time12;

    /**
     * 学生联系方式
     */
    private String phone_number5;

    /**
     * 父母联系方式
     */
    private String phone_number13;

}
