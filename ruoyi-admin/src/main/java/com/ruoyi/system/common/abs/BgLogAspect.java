package com.ruoyi.system.common.abs;

import com.ruoyi.common.core.domain.model.LoginUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.TBackendLog;
import com.ruoyi.system.mapper.TBackendLogMapper;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.lang.reflect.Method;

/**
 * 后台管理日志 aop
 */
@Aspect
@Component
public class BgLogAspect {

    private static final Logger log = LoggerFactory.getLogger(BgLogAspect.class);
    @Resource
    private TBackendLogMapper tBackendLogMapper;

    //Controller层切点
    @Pointcut("@annotation(com.ruoyi.system.common.abs.BgLog)")
    public void controllerAspect() {
    }

    /**
     * 处理完请求后执行
     *
     * @param joinPoint 切点
     */
    @AfterReturning(pointcut = "controllerAspect()", returning = "jsonResult")
    public void doAfterReturning(JoinPoint joinPoint, Object jsonResult) {
        // 获取目标方法
        Method targetMethod = ((MethodSignature) joinPoint.getSignature()).getMethod();
        // 获取方法上的注解
        BgLog annotation = targetMethod.getAnnotation(BgLog.class);
        // 获取注解里的值
        //要执行的操作类型
        String methodType = annotation.methodType();
        //要执行的操作内容
        String moduleContent = annotation.moduleContent();
        // 获取当前的用户
        LoginUser loginUser = SecurityUtils.getLoginUser();
        //构建插入数据库的日志对象
        TBackendLog tBackendLog = new TBackendLog();
        tBackendLog.setUserName(loginUser.getUsername());
        tBackendLog.setUserId(loginUser.getUserId());
        tBackendLog.setLogType(methodType);
        tBackendLog.setLogContent(moduleContent);
        tBackendLog.setHandleStatus("操作成功");
        tBackendLogMapper.insertTBackendLog(tBackendLog);
    }


    /**
     * 拦截异常操作
     *
     * @param joinPoint 切点
     * @param e         异常
     */
    @AfterThrowing(value = "controllerAspect()", throwing = "e")
    public void doAfterThrowing(JoinPoint joinPoint, Exception e) {
        // 获取目标方法
        Method targetMethod = ((MethodSignature) joinPoint.getSignature()).getMethod();
        // 获取方法上的注解
        BgLog annotation = targetMethod.getAnnotation(BgLog.class);
        // 获取注解里的值
        //要执行的操作类型
        String methodType = annotation.methodType();
        //要执行的操作内容
        String moduleContent = annotation.moduleContent();
        // 获取当前的用户
        LoginUser loginUser = SecurityUtils.getLoginUser();
        //构建插入数据库的日志对象
        TBackendLog tBackendLog = new TBackendLog();
        tBackendLog.setUserName(loginUser.getUsername());
        tBackendLog.setUserId(loginUser.getUserId());
        tBackendLog.setLogType(methodType);
        tBackendLog.setLogContent(moduleContent);
        tBackendLog.setHandleStatus("操作失败");
        tBackendLogMapper.insertTBackendLog(tBackendLog);
    }


}
