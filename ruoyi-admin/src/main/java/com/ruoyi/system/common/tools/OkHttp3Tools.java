package com.ruoyi.system.common.tools;

import com.alibaba.fastjson2.JSONObject;
import okhttp3.OkHttpClient;
import okhttp3.Response;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class OkHttp3Tools {

    // OkHttp请求
    final public static OkHttpClient client = new OkHttpClient.Builder()
            .connectTimeout(2, TimeUnit.SECONDS)//链接超时为2秒，单位为秒
            .writeTimeout(2, TimeUnit.SECONDS)//写入超时
            .readTimeout(2, TimeUnit.SECONDS)//读取超时
            .build();

    // 吧响应转换成Map
    public static JSONObject responseToMap(Response response) throws IOException {
        if (response.code() == 200) {
            String string = response.body().string();
            JSONObject jsonObject = JSONObject.parseObject(string);
            return jsonObject;
        }
        return null;
    }
}
