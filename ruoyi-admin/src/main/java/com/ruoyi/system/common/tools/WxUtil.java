package com.ruoyi.system.common.tools;

import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.system.common.WxReturnedValue;
import com.ruoyi.system.common.config.Custom;
import com.ruoyi.system.common.wechatmsg.Tag;
import com.ruoyi.system.common.wechatmsg.TagMsg;
import lombok.Getter;
import okhttp3.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 微信订阅消息工具类
 */
@Component
public class WxUtil {

    @Resource
    private Custom custom;

    @Value("${custom.tag}")
    @Getter
    private String tag;

    @Resource
    private RedisTemplate redisTemplate;


    /**
     * 获取微信公众号access_token
     */
    public String getWeChatAccessToken() throws IOException {

//        String appId = "wxa6ac992b3135d1fa";
//        String secret = "6031c09a011deb4b094e44313ddce771";
        String appId = custom.WechatAppID;
        String secret = custom.WechatSecret;

        Request request = new Request.Builder()
                .url("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appId + "&secret=" + secret)
                .get()
                .build();

        Call call = OkHttp3Tools.client.newCall(request);
        Response response = call.execute();
        JSONObject jsonObject = JSONObject.parseObject(response.body().string());
//        System.out.println(jsonObject);
        String accessToken = jsonObject.getString("access_token");
        if (accessToken == null) throw new RuntimeException("获取access_token失败: "+jsonObject);
        redisTemplate.opsForValue().set("wechat:accesstoken:" + appId, accessToken, 3600, TimeUnit.SECONDS);


        return accessToken;
    }

    /**
     * 微信小程序AccessToken
     *
     * @return
     * @throws IOException
     */
    public String getWeChatMiniAccessToken() throws IOException {

        String appId = custom.WechatMiniAppID;
        String secret = custom.WechatMiniSecret;
//        String appId = "wxe697bb38d4b86ed7";
//        String secret = "fd572fd24d04011fd6fbb85949d892a2";

        Request request = new Request.Builder()
                .url("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appId + "&secret=" + secret)
                .get()
                .build();
        Call call = OkHttp3Tools.client.newCall(request);
        Response response = call.execute();
        JSONObject jsonObject = JSONObject.parseObject(response.body().string());
//        String accessToken = jsonObject.getString("access_token");
        String accessToken = null;
        try {
            accessToken = jsonObject.getString("access_token");
            redisTemplate.opsForValue().set("wechatmini:accesstoken:" + appId, accessToken, 3600, TimeUnit.SECONDS);
        } catch (Exception e) {
            throw new RuntimeException("获取access_token失败:" + jsonObject);
        }

        return accessToken;
    }

    /**
     * 公众号通知
     */
    public WxReturnedValue weChatMsg(String str) throws IOException {
        System.out.println(str);
//        String accessToken = getWeChatAccessToken();
        String accessToken = (String) redisTemplate.opsForValue().get("wechat:accesstoken:" + custom.WechatAppID);
        if (accessToken == null || accessToken.length() == 0) {
            accessToken = getWeChatAccessToken();
            redisTemplate.opsForValue().set("wechat:accesstoken:" + custom.WechatAppID, accessToken, 3600, TimeUnit.SECONDS);
        }

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), str);
        Request request = new Request.Builder()
                .url("https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" + accessToken)
                .post(requestBody)
                .build();
        Call call = OkHttp3Tools.client.newCall(request);
        //4.执行同步请求，获取响应体Response对象
        Response response = call.execute();
        String string = response.body().string();
        WxReturnedValue wxReturnedValue = JSONObject.parseObject(string, WxReturnedValue.class);
        System.out.println(wxReturnedValue);
        return wxReturnedValue;
    }


    /**
     * 小程序通知
     */
    public WxReturnedValue weChatMiniMsg(String str) throws IOException {
        System.out.println(str);
//        String accessToken = getWeChatMiniAccessToken();
        String accessToken = (String) redisTemplate.opsForValue().get("wechatmini:accesstoken:" + custom.WechatMiniAppID);
        if (accessToken == null || accessToken.length() == 0) {
            accessToken = getWeChatMiniAccessToken();
            redisTemplate.opsForValue().set("wechatmini:accesstoken:" + custom.WechatMiniAppID, accessToken, 3600, TimeUnit.SECONDS);
        }

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), str);
        Request request = new Request.Builder()
                .url("https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=" + accessToken)
                .post(requestBody)
                .build();
        Call call = OkHttp3Tools.client.newCall(request);
        //4.执行同步请求，获取响应体Response对象
        Response response = call.execute();
        String string = response.body().string();
        WxReturnedValue wxReturnedValue = JSONObject.parseObject(string, WxReturnedValue.class);
        System.out.println(wxReturnedValue);
        return wxReturnedValue;

    }


    /**
     * 管理员列表
     */
    public List<String> getManagerOpenIds() throws IOException {


        String requestDatas = "{\n" +
                "    \"tagid\": " + getTagId() + ",\n" +
                "    \"next_openid\": \"\"\n" +
                "}";

//        String requestDatas = "{\n" +
//                "    \"tagid\": " + 101 + ",\n" +
//                "    \"next_openid\": \"\"\n" +
//                "}";

//        System.out.println(requestDatas);

        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), requestDatas);
//        String accessToken = getWeChatAccessToken();

        String accessToken = (String) redisTemplate.opsForValue().get("wechat:accesstoken:" + custom.WechatAppID);
        if (accessToken == null || accessToken.length() == 0) {
            accessToken = getWeChatAccessToken();
            redisTemplate.opsForValue().set("wechat:accesstoken:" + custom.WechatAppID, accessToken, 3600, TimeUnit.SECONDS);
        }
        Request request = new Request.Builder()
                .url("https://api.weixin.qq.com/cgi-bin/user/tag/get?access_token=" + accessToken)//URL地址
                .post(requestBody)
                .build();//构建

        Call call = OkHttp3Tools.client.newCall(request);
        //4.执行同步请求，获取响应体Response对象
        Response response = call.execute();
//        System.out.println(response.body().string());
        TagMsg tagMsg = JSONObject.parseObject(response.body().string(), TagMsg.class);
        List<String> openids = tagMsg.getData().getOpenid();
        return openids;
    }


    public Integer getTagId() throws IOException {

        if (tag == null) {
            throw new RuntimeException("发送管理员通知失败,标签tag为空");
        }

//        String accessToken = getWeChatAccessToken();
        String accessToken = (String) redisTemplate.opsForValue().get("wechat:accesstoken:" + custom.WechatAppID);

        if (accessToken == null || accessToken.length() == 0) {
            accessToken = getWeChatAccessToken();
            redisTemplate.opsForValue().set("wechat:accesstoken:" + custom.WechatAppID, accessToken, 3600, TimeUnit.SECONDS);
        }

        Request request = new Request.Builder()
                .url("https://api.weixin.qq.com/cgi-bin/tags/get?access_token=" + accessToken)//URL地址
                .get()
                .build();//构建

        Call call = OkHttp3Tools.client.newCall(request);
        //4.执行同步请求，获取响应体Response对象
        Response response = call.execute();
        Tag tag1 = JSONObject.parseObject(response.body().string(), Tag.class);
        for (int i = 0; i < tag1.getTags().size(); i++) {
            if (tag1.getTags().get(i).getName().equals(tag)) {
                return tag1.getTags().get(i).getId();
            }
        }
        return null;
    }


    /**
     * 小程序
     * 学员报名成功通知
     * 模板 SbrB1nbVaa8TCOqXe8s4HNr21xYGfQwUZePhW3syERs
     */
//    public void weChatMiniStudentSignUpMsg(String openId, String userName, String[] subjects, String tel, String parentTel) {
//        //openid
//
//        //报名学生
//        //报名时间
//        Date now = new Date();
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式(年-月-日-时-分-秒)
//        String createTime = dateFormat.format(now);//格式化然后放入字符串中
//        //课程名称
//        StringBuilder stringBuilder = new StringBuilder();
//
//        for (int i = 0; i < subjects.length; i++) {
//            stringBuilder.append(subjects[i]);
//            if (i < subjects.length - 1) {
//                stringBuilder.append("，");
//            }
//        }
//        String subjectsstr = stringBuilder.toString();
//        //联系方式
//        //家长电话
//
//        HttpURLConnection httpConn = null;
//        InputStream is = null;
//        BufferedReader rd = null;
//        String accessToken = null;
//        String str = null;
//        try {
//            //1.就要有一在小程序中允许通知的openid
//            //2.调用接口
//            //获取token  小程序全局唯一后台接口调用凭据
//            accessToken = getWeChatMiniAccessToken();
//
//            JSONObject xmlData = new JSONObject();
//            xmlData.put("touser", openId);//接收者（用户）的 openid
//            xmlData.put("template_id", "SbrB1nbVaa8TCOqXe8s4HNr21xYGfQwUZePhW3syERs");//所需下发的订阅模板id
//            xmlData.put("page", "/pages/index/index");//点击模板卡片后的跳转页面，仅限本小程序内的页面该字段不填则模板无跳转
//            xmlData.put("miniprogram_state", "developer");//跳转小程序类型：developer为开发版；trial为体验版；formal为正式版；默认为正式版
//            xmlData.put("lang", "zh_CN");//进入小程序查看”的语言类型，支持zh_CN(简体中文)、en_US(英文)、zh_HK(繁体中文)、zh_TW(繁体中文)，默认为zh_CN返回值
//
//            JSONObject data = new JSONObject();
//            //报名学生
//            JSONObject name3 = new JSONObject();//
//            name3.put("value", userName);
//            data.put("name3", name3);
//            //课程名称
//            JSONObject thing1 = new JSONObject();
//            thing1.put("value", subjectsstr);
//            data.put("thing1", thing1);
//            //报名时间
//            JSONObject time12 = new JSONObject();
//            time12.put("value", createTime);
//            data.put("time12", time12);
//            //联系方式
//            JSONObject phone_number5 = new JSONObject();
//            phone_number5.put("value", tel);
//            data.put("phone_number5", phone_number5);
//            //家长电话
//            JSONObject phone_number13 = new JSONObject();
//            phone_number13.put("value", parentTel);
//            data.put("phone_number13", phone_number13);
//
//            xmlData.put("data", data);//小程序模板数据
//
//            RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), xmlData.toString());
//
//            Request request = new Request.Builder()
//                    .url("https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=" + accessToken)
//                    .post(requestBody)
//                    .build();
//
//            Call call = OkHttp3Tools.client.newCall(request);
//            //4.执行同步请求，获取响应体Response对象
//            Response response = call.execute();
//
//            System.out.println(response.body().string());
//
//        } catch (Exception e) {
//            System.out.println("发送模板消息失败.." + e.getMessage());
//        }
//
//
//    }

//    /**
//     * 小程序
//     * 学员报名审核通知
//     * 模板 F9UiQnCPgWgBMffssKdn7wmLw6l6Y27N0avJL3mDwX0
//     */
//    public void weChatMiniStudentAuditMsg(String str) throws IOException {
//        String accessToken = null;
//        //1.就要有一在小程序中允许通知的openid
//        //2.调用接口
//        //获取token  小程序全局唯一后台接口调用凭据
//        accessToken = getWeChatMiniAccessToken();
//        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), str);
//        Request request = new Request.Builder()
//                .url("https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=" + accessToken)
//                .post(requestBody)
//                .build();
//        Call call = OkHttp3Tools.client.newCall(request);
//        //4.执行同步请求，获取响应体Response对象
//        Response response = call.execute();
//        System.out.println(response.body().string());
//    }

    /**
     * 小程序
     * 学员报名审核通知
     * 模板 F9UiQnCPgWgBMffssKdn7wmLw6l6Y27N0avJL3mDwX0
     */
//    public void weChatMiniStudentAuditMsg(String openId, String userName, String[] subjects, String tel, String auditResult) throws IOException {
//
//        //当前系统时间
//        Date now = new Date();
//        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式(年-月-日-时-分-秒)
//        String createTime = dateFormat.format(now);//格式化然后放入字符串中
//        //课程名称
//        StringBuilder stringBuilder = new StringBuilder();
//        for (int i = 0; i < subjects.length; i++) {
//            stringBuilder.append(subjects[i]);
//            if (i < subjects.length - 1) {
//                stringBuilder.append("，");
//            }
//        }
//        String subjectsstr = stringBuilder.toString();
//
//        String accessToken = null;
//
//        //1.就要有一在小程序中允许通知的openid
//        //2.调用接口
//        //获取token  小程序全局唯一后台接口调用凭据
//        accessToken = getWeChatMiniAccessToken();
//
//        JSONObject xmlData = new JSONObject();
//        xmlData.put("touser", openId);//接收者（用户）的 openid
//        xmlData.put("template_id", "F9UiQnCPgWgBMffssKdn7wmLw6l6Y27N0avJL3mDwX0");//所需下发的订阅模板id
//        xmlData.put("page", "/pages/index/index");//点击模板卡片后的跳转页面，仅限本小程序内的页面该字段不填则模板无跳转
//        xmlData.put("miniprogram_state", "developer");//跳转小程序类型：developer为开发版；trial为体验版；formal为正式版；默认为正式版
//        xmlData.put("lang", "zh_CN");//进入小程序查看”的语言类型，支持zh_CN(简体中文)、en_US(英文)、zh_HK(繁体中文)、zh_TW(繁体中文)，默认为zh_CN返回值
//
//        JSONObject data = new JSONObject();
//        //报名学生
//        JSONObject name2 = new JSONObject();//
//        name2.put("value", userName);
//        data.put("name2", name2);
//        //课程名称
//        JSONObject thing1 = new JSONObject();
//        thing1.put("value", subjectsstr);
//        data.put("thing1", thing1);
//        //联系电话
//        JSONObject phone_number4 = new JSONObject();
//        phone_number4.put("value", tel);
//        data.put("phone_number4", phone_number4);
//        //审核结果
//        JSONObject phrase7 = new JSONObject();
//        phrase7.put("value", auditResult);
//        data.put("phrase7", phrase7);
//        //审核时间
//        JSONObject time6 = new JSONObject();
//        time6.put("value", createTime);
//        data.put("time6", time6);
//
//        xmlData.put("data", data);//小程序模板数据
//
////           System.out.println("发送模板消息xmlData:" + xmlData);
//
//        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), xmlData.toString());
//
//        Request request = new Request.Builder()
//                .url("https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=" + accessToken)
//                .post(requestBody)
//                .build();
//
//        Call call = OkHttp3Tools.client.newCall(request);
//        //4.执行同步请求，获取响应体Response对象
//        Response response = call.execute();
//
//        System.out.println(response.body().string());
//
//
//    }


}