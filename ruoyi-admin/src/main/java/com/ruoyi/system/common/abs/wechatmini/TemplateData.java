package com.ruoyi.system.common.abs.wechatmini;

import com.alibaba.fastjson2.JSONObject;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * 小程序通知data
 */
public interface TemplateData {

    default String json() {
        return JSONObject.toJSONString(this);
    }

    /**
     * 通知
     * 小程序openid
     *
     * @param openId
     * @param weChatMiniTemplateEnum
     * @return
     * @throws IllegalAccessException
     */
    default String msgTemplateData(String openId, String weChatMiniTemplateEnum, String lang,String page, String miniprogramState) throws IllegalAccessException {
        Map<String, Object> map = new HashMap<>();
        Field[] fields = this.getClass().getDeclaredFields();
        Map<String, Map<String, Object>> map1 = new HashMap<>();
        for (Field declaredField : fields) {
            declaredField.setAccessible(true);
            String name = declaredField.getName();
//            System.out.println(name);
            Object o = declaredField.get(this);
            Map<String, Object> map2 = new HashMap<>();
            if (o == null || "".equals(o)) {
                o = " ";
            }
            map2.put("value", o);
            map1.put(name, map2);
        }
        map.put("touser", openId);
        map.put("template_id", weChatMiniTemplateEnum);
        map.put("page", page);
        map.put("miniprogram_state", miniprogramState);
        map.put("lang", lang);
        map.put("data", map1);
        return JSONObject.toJSONString(map);
    }
}
