package com.ruoyi.system.common.abs;

import com.ruoyi.system.common.tools.NTools;
import com.ruoyi.system.domain.TWechatMiniLog;
import com.ruoyi.system.mapper.TWechatMiniLogMapper;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.lang.reflect.Method;

/**
 * 用户小程序日志管理 aop
 */
@Aspect
@Component
public class WxMiniLogAspect {

    private static final Logger log = LoggerFactory.getLogger(WxMiniLogAspect.class);
    @Resource
    private NTools nTools;
    @Resource
    private TWechatMiniLogMapper tWechatMiniLogMapper;

    //Controller层切点
    @Pointcut("@annotation(com.ruoyi.system.common.abs.WxMiniLog)")
    public void controllerAspect() {
    }

    /**
     * 处理完请求后执行
     *
     * @param joinPoint 切点
     */
    @AfterReturning(pointcut = "controllerAspect()", returning = "jsonResult")
    public void doAfterReturning(JoinPoint joinPoint, Object jsonResult) {
        // 获取目标方法
        Method targetMethod = ((MethodSignature) joinPoint.getSignature()).getMethod();

        // 获取方法上的注解
        WxMiniLog annotation = targetMethod.getAnnotation(WxMiniLog.class);

        // 获取注解里的值
        String methodDesc = annotation.methodDesc();
        String moduleName = annotation.moduleName();

        //小程序日志实体类写入数据库
        TWechatMiniLog tWechatMiniLog = new TWechatMiniLog();
        tWechatMiniLog.setUserName(nTools.getUserName());
        tWechatMiniLog.setWechatMiniOpenId(nTools.getMiniOpenId());
        tWechatMiniLog.setLogType(methodDesc);
        tWechatMiniLog.setLogContent(moduleName);
        tWechatMiniLog.setHandleStatus("操作成功");
        tWechatMiniLogMapper.insertTWechatMiniLog(tWechatMiniLog);

    }


    /**
     * 拦截异常操作
     *
     * @param joinPoint 切点
     * @param e         异常
     */
    @AfterThrowing(value = "controllerAspect()", throwing = "e")
    public void doAfterThrowing(JoinPoint joinPoint, Exception e) {

        // 获取目标方法
        Method targetMethod = ((MethodSignature) joinPoint.getSignature()).getMethod();

        // 获取方法上的注解
        WxMiniLog annotation = targetMethod.getAnnotation(WxMiniLog.class);

        // 获取注解里的值
        String methodDesc = annotation.methodDesc();
        String moduleName = annotation.moduleName();

        //小程序日志实体类写入数据库
        TWechatMiniLog tWechatMiniLog = new TWechatMiniLog();
        tWechatMiniLog.setUserName(nTools.getUserName());
        tWechatMiniLog.setWechatMiniOpenId(nTools.getMiniOpenId());
        tWechatMiniLog.setLogType(methodDesc);
        tWechatMiniLog.setLogContent(moduleName);
        tWechatMiniLog.setHandleStatus("操作失败");
        tWechatMiniLogMapper.insertTWechatMiniLog(tWechatMiniLog);

    }

}
