package com.ruoyi.system.common.tools;

import com.qcloud.cos.COSClient;
import com.qcloud.cos.model.GetObjectRequest;
import com.qcloud.cos.model.ObjectMetadata;
import com.qcloud.cos.model.PutObjectRequest;
import com.qcloud.cos.transfer.TransferManager;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.exception.file.FileNameLengthLimitExceededException;
import com.ruoyi.common.exception.file.FileSizeLimitExceededException;
import com.ruoyi.common.exception.file.InvalidExtensionException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.MimeTypeUtils;
import com.ruoyi.common.utils.uuid.Seq;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.math3.analysis.function.Abs;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Objects;

import static com.ruoyi.system.common.tools.NTools.cosPropertiesUtils;

/**
 * 文件上传工具类
 *
 * @author ruoyi
 */
public class CosFileUploadUtils {

    /**
     * 默认大小 50M
     */
    public static final long DEFAULT_MAX_SIZE = 50 * 1024 * 1024;

    /**
     * 默认的文件名最大长度 100
     */
    public static final int DEFAULT_FILE_NAME_LENGTH = 100;

    /**
     * 默认上传的地址
     */
    private static String defaultBaseDir = RuoYiConfig.getProfile();

    public static String getDefaultBaseDir() {
        return defaultBaseDir;
    }

    public static void setDefaultBaseDir(String defaultBaseDir) {
        CosFileUploadUtils.defaultBaseDir = defaultBaseDir;
    }

    /**
     * 以默认配置进行文件上传
     *
     * @param file 上传的文件
     * @return 文件名称
     * @throws Exception
     */
    public static final String upload(MultipartFile file) throws IOException {
        try {
            return upload(getDefaultBaseDir(), false, file, MimeTypeUtils.DEFAULT_ALLOWED_EXTENSION);
        } catch (Exception e) {
            throw new IOException(e.getMessage(), e);
        }
    }

    /**
     * 以默认配置进行文件上传, 上传至私有存储桶
     *
     * @param file 上传的文件
     * @return 文件名称
     * @throws Exception
     */
    public static final String uploadPrivate(MultipartFile file) throws IOException {
        try {
            return upload(getDefaultBaseDir(), true, file, MimeTypeUtils.DEFAULT_ALLOWED_EXTENSION);
        } catch (Exception e) {
            throw new IOException(e.getMessage(), e);
        }
    }

    /**
     * 根据文件路径上传
     *
     * @param baseDir 相对应用的基目录
     * @param file    上传的文件
     * @return 文件名称
     * @throws IOException
     */
    public static final String upload(String baseDir, MultipartFile file) throws IOException {
        try {
            return upload(baseDir, false, file, MimeTypeUtils.DEFAULT_ALLOWED_EXTENSION);
        } catch (Exception e) {
            throw new IOException(e.getMessage(), e);
        }
    }

    /**
     * 文件上传
     *
     * @param baseDir          相对应用的基目录
     * @param file             上传的文件
     * @param allowedExtension 上传文件类型
     * @return 返回上传成功的文件名
     * @throws FileSizeLimitExceededException       如果超出最大大小
     * @throws FileNameLengthLimitExceededException 文件名太长
     * @throws IOException                          比如读写文件出错时
     * @throws InvalidExtensionException            文件校验异常
     */
    public static final String upload(String baseDir, Boolean isPrivate, MultipartFile file, String[] allowedExtension)
            throws FileSizeLimitExceededException, IOException, FileNameLengthLimitExceededException,
            InvalidExtensionException {
        int fileNamelength = Objects.requireNonNull(file.getOriginalFilename()).length();
        if (fileNamelength > CosFileUploadUtils.DEFAULT_FILE_NAME_LENGTH) {
            throw new FileNameLengthLimitExceededException(CosFileUploadUtils.DEFAULT_FILE_NAME_LENGTH);
        }

        assertAllowed(file, allowedExtension);

        String fileName = extractFilename(file);

//
        // 保存文件到本地临时路径
        File tempFile = File.createTempFile("temp", null);
        file.transferTo(tempFile);
        COSClient cosClient = null;
        String bucketName = null;
        if (isPrivate) {
            cosClient = cosPropertiesUtils.getCosPrivateClient();
            bucketName = cosPropertiesUtils.getBucketNamePrivate();
        } else {
            cosClient = cosPropertiesUtils.getCOSClient();
            bucketName = cosPropertiesUtils.getBucketName();
        }
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, defaultBaseDir + fileName, tempFile);
        cosClient.putObject(putObjectRequest);

        // 关闭 COS 客户端
        cosClient.shutdown();
        // 删除本地临时文件
        tempFile.delete();

        //上传
        return defaultBaseDir + fileName;
    }

    /**
     * 下载文件
     *
     * @param cosFileUrl 腾讯云对象存储需要的cos的Url路径
     * @param response   需要提供下载的响应体对象
     */
    public static final void download(String cosFileUrl, HttpServletResponse response) throws IOException {
        COSClient cosPrivateClient = cosPropertiesUtils.getCosPrivateClient();
        String bucketNamePrivate = cosPropertiesUtils.getBucketNamePrivate();
        String baseUrlPrivate = cosPropertiesUtils.getBaseUrlPrivate();
        String path = cosFileUrl.replace(baseUrlPrivate, "");
        // 创建临时文件
        File tempFile = File.createTempFile("output/", getFileExt(path));
        GetObjectRequest getObjectRequest = new GetObjectRequest(bucketNamePrivate, path);
        cosPrivateClient.getObject(getObjectRequest, tempFile);

        File rFile = new File(tempFile.getAbsolutePath());
        byte[] bytes = fileToByteArray(rFile);
        response.getOutputStream().write(bytes);
    }


    /**
     * 读取文档
     * @param file
     * @return
     * @throws IOException
     */
    public static byte[] fileToByteArray(File file) throws IOException {
        byte[] byteArray = new byte[(int) file.length()];
        try (FileInputStream fis = new FileInputStream(file)) {
            fis.read(byteArray);
        }
        return byteArray;
    }

    /**
     * 获取文件扩展名
     * @param fileNameStr
     * @return
     */
    public static String getFileExt(String fileNameStr){

        // 获取最后一个"."的索引
        int dotIndex = fileNameStr.lastIndexOf(".");

        // 判断是否存在后缀名
        if (dotIndex != -1 && dotIndex < fileNameStr.length() - 1) {
            // 获取文件后缀名
            return fileNameStr.substring(dotIndex + 1);
        } else {
            return "";
        }

    }


    /**
     * 编码文件名
     */
    public static final String extractFilename(MultipartFile file) {
        return StringUtils.format("{}/{}_{}.{}", DateUtils.datePath(),
                FilenameUtils.getBaseName(file.getOriginalFilename()), Seq.getId(Seq.uploadSeqType), getExtension(file));
    }

    public static final File getAbsoluteFile(String uploadDir, String fileName) throws IOException {
        File desc = new File(uploadDir + File.separator + fileName);

        if (!desc.exists()) {
            if (!desc.getParentFile().exists()) {
                desc.getParentFile().mkdirs();
            }
        }
        return desc;
    }

    public static final String getPathFileName(String uploadDir, String fileName) throws IOException {
        int dirLastIndex = RuoYiConfig.getProfile().length() + 1;
        String currentDir = StringUtils.substring(uploadDir, dirLastIndex);
        return Constants.RESOURCE_PREFIX + "/" + currentDir + "/" + fileName;
    }

    /**
     * 文件大小校验
     *
     * @param file 上传的文件
     * @return
     * @throws FileSizeLimitExceededException 如果超出最大大小
     * @throws InvalidExtensionException
     */
    public static final void assertAllowed(MultipartFile file, String[] allowedExtension)
            throws FileSizeLimitExceededException, InvalidExtensionException {
        long size = file.getSize();
        if (size > DEFAULT_MAX_SIZE) {
            throw new FileSizeLimitExceededException(DEFAULT_MAX_SIZE / 1024 / 1024);
        }

        String fileName = file.getOriginalFilename();
        String extension = getExtension(file);
        if (allowedExtension != null && !isAllowedExtension(extension, allowedExtension)) {
            if (allowedExtension == MimeTypeUtils.IMAGE_EXTENSION) {
                throw new InvalidExtensionException.InvalidImageExtensionException(allowedExtension, extension,
                        fileName);
            } else if (allowedExtension == MimeTypeUtils.FLASH_EXTENSION) {
                throw new InvalidExtensionException.InvalidFlashExtensionException(allowedExtension, extension,
                        fileName);
            } else if (allowedExtension == MimeTypeUtils.MEDIA_EXTENSION) {
                throw new InvalidExtensionException.InvalidMediaExtensionException(allowedExtension, extension,
                        fileName);
            } else if (allowedExtension == MimeTypeUtils.VIDEO_EXTENSION) {
                throw new InvalidExtensionException.InvalidVideoExtensionException(allowedExtension, extension,
                        fileName);
            } else {
                throw new InvalidExtensionException(allowedExtension, extension, fileName);
            }
        }
    }

    /**
     * 判断MIME类型是否是允许的MIME类型
     *
     * @param extension
     * @param allowedExtension
     * @return
     */
    public static final boolean isAllowedExtension(String extension, String[] allowedExtension) {
        for (String str : allowedExtension) {
            if (str.equalsIgnoreCase(extension)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 获取文件名的后缀
     *
     * @param file 表单文件
     * @return 后缀名
     */
    public static final String getExtension(MultipartFile file) {
        String extension = FilenameUtils.getExtension(file.getOriginalFilename());
        if (StringUtils.isEmpty(extension)) {
            extension = MimeTypeUtils.getExtension(Objects.requireNonNull(file.getContentType()));
        }
        return extension;
    }

    /**
     * 获取文件大小
     *
     * @param file
     * @return
     */
    public static final String getFileSize(MultipartFile file) {
        long fileSizeBytes = file.getSize();
        return getFileSize(fileSizeBytes);
    }

    /**
     * 获取文件单位
     *
     * @param fileSizeBytes
     * @return
     */
    public static String getFileSize(long fileSizeBytes) {
        DecimalFormat df = new DecimalFormat("#.###");
        if (fileSizeBytes <= 1024) {
            return df.format(fileSizeBytes) + " B";
        }
        double fileSizeKB = (double) fileSizeBytes / 1024;
        if (fileSizeKB <= 1024) {
            return df.format(fileSizeKB) + " KB";
        }
        double fileSizeMB = (double) fileSizeBytes / (1024 * 1024); // 换算为MB
        if (fileSizeMB <= 1024) {
            return df.format(fileSizeMB) + " MB";
        }
        double fileSizeGB = (double) fileSizeBytes / (1024 * 1024 * 1024); // 换算为GB
        if (fileSizeGB <= 1024) {
            return df.format(fileSizeGB) + " GB";
        }
        return fileSizeGB + " GB";
    }
}
