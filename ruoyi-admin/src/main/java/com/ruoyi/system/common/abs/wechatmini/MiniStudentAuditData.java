package com.ruoyi.system.common.abs.wechatmini;

import lombok.Data;

@Data
public class MiniStudentAuditData implements TemplateData {

    /**
     * 报名学生名
     */
    private String name2;

    /**
     * 报名课程的数组字符串
     */
    private String thing1;

    /**
     * 学生联系方式
     */
    private String phone_number4;

    /**
     * 是否通过
     */
    private String phrase7;

    /**
     * 时间
     */
    private String time6;
}
