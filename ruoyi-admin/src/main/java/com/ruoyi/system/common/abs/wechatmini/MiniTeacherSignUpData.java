package com.ruoyi.system.common.abs.wechatmini;

import lombok.Data;

/**
 * 小程序
 * 教师 报名成功通知
 */
@Data
public class MiniTeacherSignUpData implements TemplateData {

    /**
     * 教师名
     */
    private String name9;
    /**
     * 课程名称
     */
    private String thing1;
    /**
     * 备注
     */
    private String thing11;
    /**
     * 生成时间
     */
    private String time28;
}
