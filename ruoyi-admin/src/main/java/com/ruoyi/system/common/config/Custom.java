package com.ruoyi.system.common.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Custom {

    // 微信小程序AppID
    @Value("${custom.WechatMiniAppID}")
    public String WechatMiniAppID;

    // 微信小程序密钥
    @Value("${custom.WechatMiniSecret}")
    public String WechatMiniSecret;

    @Value("${custom.SecretKey}")
    public String SECRET_KEY;

    @Value("${custom.WechatAppID}")
    public String WechatAppID;

    @Value("${custom.WechatSecret}")
    public String WechatSecret;


    // 小程序模板

    // 学生报名成功模板ID
    @Value("${custom.template.wechatMini.StudentSignUpSuccess}")
    public String StudentSignUpSuccess;

    // 教师报名成功
    @Value("${custom.template.wechatMini.TeacherSignUpSuccess}")
    public String TeacherSignUpSuccess;

    // 教师报名审核
    @Value("${custom.template.wechatMini.AuditSuccess}")
    public String AuditSuccess;

    // 小程序通知语言
    @Value("${custom.template.wechatMini.lang}")
    public String wechatMiniInfoLang;

    // 小程序通知跳转页面
    @Value("${custom.template.wechatMini.page}")
    public String wechatMiniPage;

    // 小程序通知跳转的类型
    @Value("${custom.template.wechatMini.miniprogramState}")
    public String miniprogramState;


    // 公众号通知模板

    // 管理员通知学生报名
    @Value("${custom.template.wechatArticle.ManagerStudentSignUp}")
    public String ManagerStudentSignUp;

    // 管理员通知教师报名
    @Value("${custom.template.wechatArticle.ManagerTeacherSignUp}")
    public String ManagerTeacherSignUp;
}
