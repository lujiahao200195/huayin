package com.ruoyi.system.bo.wechat;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 搜索资源
 */
@Data
@Accessors(chain = true)
public class SearchResourceDo {
    /**
     * 搜索词
     */
    private String search;

    /**
     * 搜索类型
     */
    private String searchType;

    /**
     * 标签Id列表
     */
//    private List<Long> tagIdList;
    private String tagId;
}
