package com.ruoyi.system.bo.wechat;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class UpdateWorkRoomDo {


    /**
     * 工作室ID
     */
    private Long workRoomId;

    /**
     * 工作室名字
     */
    private String workRoomName;

    /**
     * 工作室链接
     */
    private String workRoomLink;

    /**
     * 工作室联系电话
     */
    private String workRoomPhone;

    /**
     * 工作室邮箱
     */
    private String workRoomEmail;


    /**
     * 工作室详情
     */
    private String workRoomDetails;

    /**
     * 工作室图标
     */
    private String workRoomIcon;

    /**
     * 工作室照片
     */
    private String workRoomImage;

    /**
     * 文章标题
     */
    private String articleTitle;

    /**
     * 工作室文章内容
     */
    private String articleContent;

    /**
     * 简介
     */
    private String workRoomIntro;

    /**
     * 工作室科目列表
     */
    private List<Long> subjectIdList;

}
