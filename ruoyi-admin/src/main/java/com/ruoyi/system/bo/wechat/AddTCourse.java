package com.ruoyi.system.bo.wechat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@NoArgsConstructor
@Data
public class AddTCourse {

    private Long id;
    @JsonProperty("studentId")
    private Long studentId;
    @JsonProperty("studentName")
    private String studentName;
    @JsonProperty("teacherId")
    private Long teacherId;
    @JsonProperty("teacherName")
    private String teacherName;
    @JsonProperty("dateTime")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dateTime;
    @JsonProperty("course")
    private List<CourseDTO> course;

    @NoArgsConstructor
    @Data
    public static class CourseDTO {
        @JsonProperty("startTime")
        @JsonFormat(pattern = "HH:mm:SS")
        private Date startTime;
        @JsonProperty("endTime")
        @JsonFormat(pattern = "HH:mm:SS")
        private Date endTime;
        @JsonProperty("subjectIds")
        private List<Long> subjectIds;
    }
}
