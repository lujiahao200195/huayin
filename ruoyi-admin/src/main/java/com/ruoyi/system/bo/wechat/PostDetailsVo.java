package com.ruoyi.system.bo.wechat;

import com.ruoyi.system.vo.wechat.bbs.BbsBoardInfo;
import com.ruoyi.system.vo.wechat.bbs.BbsPostImageVo;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class PostDetailsVo {

    /**
     * 版块信息
     */
    private BbsBoardInfo boardInfo;

    /**
     * 文章作者头像
     */
    private String postOwnUserPhoto;

    /**
     * 文章作者用户Id
     */
    private Long postOwnUserId;

    /**
     * 文章作者名字
     */
    private String postOwnUserName;

    /**
     * 文章标题
     */
    private String postTitle;

    /**
     * 文章内容
     */
    private String postContent;

    /**
     * 文章点赞数
     */
    private Long postUp;

    /**
     * 文章类型
     */
    private String postType;

    /**
     * 文章图片列表
     */
    private List<BbsPostImageVo> imageVoList;
}
