package com.ruoyi.system.bo.wechat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

/**
 * 学生报名后端接收
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
@Accessors(chain = true)
public class StudentSignUp {

    /**
     * 学生姓名
     */
    @JsonProperty("name")

    private String name;
    /**
     * 学生电话
     */
    @JsonProperty("phone")
    private String phone;
    /**
     * 学生邮箱
     */
    @JsonProperty("email")
    private String email;

    /**
     * 学龄
     */
    private String studentAge;

    /**
     * 学生性别
     */
    @JsonProperty("gender")
    private String gender;
    /**
     * 学生科目 弃用
     */
    @JsonProperty("subject")
    private List<SubjectDTO> subject;
    /**
     * 学生科目
     */
    @JsonProperty("learning")
    private List<LearningDTO> learning;

    /**
     * 目标总分
     */
    @JsonProperty("targetScore")
    private String targetScore;

    /**
     * 学生出生日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date studentBirthday;

    /**
     * 知情确认
     */
    private String studentInformedConsent;

    /**
     * 知情确认图片
     */
    @JsonProperty("sign")
    private String sign;

    /**
     * 用户openid
     */

    @NoArgsConstructor
    @Data
    public static class SubjectDTO {
        @JsonProperty("subjectName")
        private String subjectName;
        @JsonProperty("id")
        private Integer id;
        @JsonProperty("checked")
        private Boolean checked;
    }

    @NoArgsConstructor
    @Data
    public static class LearningDTO {
        /**
         * 课程名
         */
        @JsonProperty("subjectName")
        private String subjectName;
        /**
         * 课程id
         */
        @JsonProperty("id")
        private Integer id;
        /**
         *
         */
        @JsonProperty("checked")
        private Boolean checked;
        /**
         * 目前进度
         */
        @JsonProperty("rate")
        private String rate;
        /**
         * 当前分数
         */
        private Long targetScore;
        /**
         * 目标分数
         */
        @JsonProperty("currentScore")
        private Long currentScore;
    }
}
