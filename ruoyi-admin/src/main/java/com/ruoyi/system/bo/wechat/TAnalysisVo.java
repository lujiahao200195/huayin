package com.ruoyi.system.bo.wechat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TAnalysisVo {
    private Long id;

    private Long questionId;

    private String analysis;

    private String correctAnswer;
}
