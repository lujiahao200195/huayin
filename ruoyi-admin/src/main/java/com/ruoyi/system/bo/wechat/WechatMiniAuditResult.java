package com.ruoyi.system.bo.wechat;

import lombok.Data;

/**
 * 报名管理员审核信息
 */
@Data
public class WechatMiniAuditResult {

    /**
     * id
     */
    private Long id;
    /**
     * 名字
     */
    private String name;
    /**
     * 类别 学生/教师
     */
    private String type;
    /**
     * 结果
     */
    private String result;
    /**
     * 结果原因
     */
    private String resultStr;

}
