package com.ruoyi.system.bo.wechat;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * 上传资源文件Do
 */
@Data
@Accessors(chain = true)
public class UploadResourceDo {

    /**
     * 资源标签Id列表
     */
    private List<Long> resourceTagList;

    /**
     * 资源标题
     */
    private String resourceTitle;

    /**
     * 资源封面图片
     */
    private String resourceCover;

    /**
     * 资源描述
     */
    private String resourceDetails;

    /**
     * 数据MIME类型
     */
    private String resourceMime;

    /**
     * 文件连接
     */
    private String resourceLink;

    /**
     * 资源大小
     */
    private Long resourceSize;

    /**
     * 文件扩展名
     */
    private String fileExt;

    /**
     * 公开状态
     */
    private String publicStatus;
}
