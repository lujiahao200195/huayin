package com.ruoyi.system.bo.wechat;

import com.ruoyi.common.annotation.Excel;
import lombok.Data;

/**
 * 添加新的科目文章
 */
@Data
public class AddSubjectArticleDo {

    /**
     * 文章id
     */
    private Long subjectId;

    /**
     * 文章标题
     */
    @Excel(name = "文章标题")
    private String articleTitle;

    /**
     * 文章简介
     */
    @Excel(name = "文章简介")
    private String articleNote;

    /**
     * 文章图片
     */
    @Excel(name = "文章图片")
    private String articleImage;

    /**
     * 文章链接
     */
    @Excel(name = "文章链接")
    private String articleLink;

    /**
     * 文章内容
     */
    @Excel(name = "文章内容")
    private String articleContent;
}
