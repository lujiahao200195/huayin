package com.ruoyi.system.bo.wechat;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class ChangeUserInfo {


    /**
     * 用户名
     */
    private String userName;
    /**
     * 性别
     */
    private String userGender;
    /**
     * 用户电话号
     */
    private String userPhone;
    /**
     * 用户电子邮箱
     */
    private String userEmail;
    /**
     * 用户出生日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date userBirth;
    /**
     * 用户头像数据
     */
    private String userPhoto;
    /**
     * 用户个人简介
     */
    private String userIntro;


}
