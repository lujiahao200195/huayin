package com.ruoyi.system.bo.wechat;

import lombok.Data;
import lombok.experimental.Accessors;


/**
 * 创建版块数据
 */
@Data
@Accessors
public class BbsCreateBoardDo {

    /**
     * 版块名
     */
    private String boardName;

    /**
     * 版块图标
     */
    private String boardPhoto;

    /**
     * 版块简介
     */
    private String boardIntro;
}
