package com.ruoyi.system.bo.wechat;

import lombok.Builder;
import lombok.Data;

@Data
public class QueryArticleDo {
    private Integer subjectId;
    private Integer pageNum;
    private Integer pageSize;
    //
    private Integer articleTwoTypeId;
}
