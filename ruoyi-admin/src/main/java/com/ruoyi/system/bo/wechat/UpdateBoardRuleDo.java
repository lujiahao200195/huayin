package com.ruoyi.system.bo.wechat;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UpdateBoardRuleDo {

    /**
     * 版块Id
     */
    private Long boardId;

    /**
     * 规则内容
     */
    private String boardContent;
}
