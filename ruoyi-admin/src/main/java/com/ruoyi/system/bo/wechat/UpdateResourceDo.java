package com.ruoyi.system.bo.wechat;


import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class UpdateResourceDo {

    /**
     * 资源Id
     */
    private Long resourceId;

    /**
     * 资源标题
     */
    private String resourceTitle;

    /**
     * 资源简介
     */
    private String resourceIntro;

    /**
     * 资源标签列表
     */
    private List<Long> resourceIdList;

}
