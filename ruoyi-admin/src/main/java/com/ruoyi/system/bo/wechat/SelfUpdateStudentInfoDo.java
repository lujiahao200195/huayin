package com.ruoyi.system.bo.wechat;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
public class SelfUpdateStudentInfoDo {

    /**
     * 学生简介
     */
    @Excel(name = "学生简介")
    private String studentIntro;

    /**
     * 作为学生的名字
     */
    @Excel(name = "作为学生的名字")
    private String studentName;

    /**
     * 学生出生日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "学生出生日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date studentBirthday;

    /**
     * 学龄
     */
    @Excel(name = "学龄")
    private String studentAge;

    /**
     * 学生手机号
     */
    @Excel(name = "学生手机号")
    private String studentPhone;

    /**
     * 学生邮箱
     */
    @Excel(name = "学生邮箱")
    private String studentEmail;

    /**
     * 学生目表分数
     */
    @Excel(name = "学生目标分数")
    private Long studentTargetScore;

}
