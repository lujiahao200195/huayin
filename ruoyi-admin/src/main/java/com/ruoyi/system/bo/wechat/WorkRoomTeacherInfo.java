package com.ruoyi.system.bo.wechat;

import lombok.Data;

/**
 * 修改/新增工作室教师信息
 */
@Data
public class WorkRoomTeacherInfo {

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 工作室ID
     */
    private Long workRoomId;

    /**
     * 教师名
     */
    private String teacherName;

    /**
     * 教师简介
     */
    private String teacherIntro;

    /**
     * 教师照片1
     */
    private String teacherPhoto1;

    /**
     * 教师照片2
     */
    private String teacherPhoto2;

    /**
     * 教师照片3
     */
    private String teacherPhoto3;

    /**
     * 教师详情
     */
    private String teacherDetails;

    /**
     * 教师联系电话
     */
    private String teacherPhone;

    /**
     * 教师联系邮箱
     */
    private String teacherEmail;

}
