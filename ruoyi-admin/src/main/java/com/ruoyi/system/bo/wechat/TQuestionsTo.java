package com.ruoyi.system.bo.wechat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.system.domain.TDifficultyLevel;
import com.ruoyi.system.domain.TOption;
import com.ruoyi.system.domain.TQuestionType;
import com.ruoyi.system.domain.TQuestions;

import java.util.Date;
import java.util.List;

public class TQuestionsTo {

    /** 试题id */
    private Long id;

    /** 题目 */
    @Excel(name = "题目")
    private String questionText;

    /** 正确答案 */
    @Excel(name = "正确答案")
    private String correctAnswer;

    /** 难度级别 */
    @Excel(name = "难度级别")
    private Long difficultyLevelId;

    /** 题目类型 */
    @Excel(name = "题目类型")
    private Long questionTypeId;

    /** 创建人 */
    @Excel(name = "创建人")
    private Long createdBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdTime;

    /** 更新人 */
    @Excel(name = "更新人")
    private Long updatedBy;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date updatedTime;

    /** 删除人 */
    @Excel(name = "删除人")
    private Long deleteBy;

    /** 删除时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date deleteTime;

    /** 备注 */
    private String note;

    private TDifficultyLevel tDifficultyLevel;

    private List<TOption> tOptionList;

    private TQuestionType tQuestionType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String correctAnswer) {
        this.correctAnswer = correctAnswer;
    }

    public Long getDifficultyLevelId() {
        return difficultyLevelId;
    }

    public void setDifficultyLevelId(Long difficultyLevelId) {
        this.difficultyLevelId = difficultyLevelId;
    }

    public Long getQuestionTypeId() {
        return questionTypeId;
    }

    public void setQuestionTypeId(Long questionTypeId) {
        this.questionTypeId = questionTypeId;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Long getDeleteBy() {
        return deleteBy;
    }

    public void setDeleteBy(Long deleteBy) {
        this.deleteBy = deleteBy;
    }

    public Date getDeleteTime() {
        return deleteTime;
    }

    public void setDeleteTime(Date deleteTime) {
        this.deleteTime = deleteTime;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public TDifficultyLevel gettDifficultyLevel() {
        return tDifficultyLevel;
    }

    public void settDifficultyLevel(TDifficultyLevel tDifficultyLevel) {
        this.tDifficultyLevel = tDifficultyLevel;
    }

    public List<TOption> gettOptionList() {
        return tOptionList;
    }

    public void settOptionList(List<TOption> tOptionList) {
        this.tOptionList = tOptionList;
    }

    public TQuestionType gettQuestionType() {
        return tQuestionType;
    }

    public void settQuestionType(TQuestionType tQuestionType) {
        this.tQuestionType = tQuestionType;
    }
}
