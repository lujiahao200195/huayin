package com.ruoyi.system.bo.wechat;


import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class BbsSendPostDo {

    /**
     * 论坛版块ID
     */
    private Long boardId;

    /**
     * 贴子标题
     */
    private String postTitle;

    /**
     * 帖子内容
     */
    private String postContent;

    /**
     * 帖子类型
     */
    private String postType;

    /**
     * 悬赏积分数
     */
    private Long rewardPoint;

    /**
     * 帖子图片列表
     */
    private List<String> imageList;

}
