package com.ruoyi.system.bo.wechat;


import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


@Data
@Accessors(chain = true)
public class JoinWorkRoom {

    /**
     * 加入类型(案例: 教师, 学生, 用户)
     */
    @NotBlank(message = "加入类型不能为空")
    private String joinType;

    /**
     * 工作室Id
     */
    @NotNull(message = "工作室Id不能为空")
    private Long workRoomId;

    /**
     * 工作室内名字
     */
    @NotBlank(message = "工作室内名字不能为空")
    private String workRoomSideName;
}
