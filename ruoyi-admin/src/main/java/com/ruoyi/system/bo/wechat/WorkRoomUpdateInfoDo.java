package com.ruoyi.system.bo.wechat;


import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 工作室修改工作室信息
 */
@Data
@Accessors(chain = true)
public class WorkRoomUpdateInfoDo {

}
