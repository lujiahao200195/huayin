package com.ruoyi.system.bo.wechat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * 添加教师证书业务数据
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class AddTeacherCertificate {

    /**
     * 微信小程序openId
     */
    private String openId;

    /**
     * 证书Id
     */
    private Long certificatesId;

    /**
     * 教师Id
     */
    private Long teacherId;

    /**
     * 证件照1
     */
    private String certificatesPhoto1;

    /**
     * 证件照2
     */
    private String certificatesPhoto2;

    /**
     * 证件照3
     */
    private String certificatesPhoto3;

    /**
     * 证件照4
     */
    private String certificatesPhoto4;

    /**
     * 证件照5
     */
    private String certificatesPhoto5;

}
