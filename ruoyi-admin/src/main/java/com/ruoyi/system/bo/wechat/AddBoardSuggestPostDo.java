package com.ruoyi.system.bo.wechat;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class AddBoardSuggestPostDo {

    /**
     * 版块ID
     */
    private Long boardId;

    /**
     * 文章Id
     */
    private Long postId;

    /**
     * 推荐类型: 置顶/推荐
     */
    private String suggestType;

    /**
     * 版块内标签列表
     */
    private List<Long> tagIdList;

}
