package com.ruoyi.system.bo.wechat;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UpdateBoardInfoDo {

    /**
     * 版块Id
     */
    private Long boardId;

    /**
     * 版块名称
     */
    private String boardName;

    /**
     * 版块图标
     */
    private String boardIcon;

    /**
     * 版块简介
     */
    private String boardIntro;
}
