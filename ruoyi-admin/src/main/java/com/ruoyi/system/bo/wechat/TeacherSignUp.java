package com.ruoyi.system.bo.wechat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.system.domain.TTeacherCertificates;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class TeacherSignUp {


    /**
     * 教师信息
     */
    private String username;

    /**
     * 教师性别
     */
    private String gender;

    /**
     * 出生日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date birthday;

    /**
     * 教师所学专业
     */
    private String major;

    /**
     * 当前学历
     */
    private String currentEducation;

    /**
     * 所在地区
     */
    private String address;

    /**
     * 电子邮箱
     */
    private String email;

    /**
     * 知请确认事项概述
     */
    private String confirmation;

    /**
     * 签名链接
     */
    private String signature;

    /**
     * 教师照片
     */
    private String picture;

    /**
     * 教师学校
     */
    private String school;

    /**
     * 教师政治面貌
     */
    private String party;

    /**
     * 教师民族
     */
    private String nationality;

    /**
     * 教师教学或工作经历
     */
    private String experience;

    /**
     * 教师意向课程
     */
    private List<Subject> subject;

    /**
     * 教师个人优势数据
     */
    private List<TeacherAdvantage> teacherAdvantageList;

    /**
     * 教师可安排时间
     */
    private List<teacherSchedule> teacherScheduleList;

    /**
     * 教师电话
     */
    private String teacherPhone;
//
    /**
     * 推荐人教师邀请码
     */
    private String referrerCode;

//
    /**
     * 教师证书
     */
    private List<TTeacherCertificates> certificates;


    /**
     * 教师教学课程
     */
    @Data
    public static class Subject {
        /**
         * 课程Id
         */
        private Long subjectId;

        /**
         * 教学年段
         */
        private Long teachingYearId;

        /**
         * 教学备注
         */
        private String teachingRemark;
    }


    /**
     * 教师个人优势数据
     */
    @Data
    public static class TeacherAdvantage {

        private Long teacherAdvantageTypeId;

        private String advantageRemark;

    }


    /**
     * 教师可安排时间
     */
    @Data
    public static class teacherSchedule {

        /**
         * 可安排月份
         */
        private String scheduleMonth;

        /**
         * 可安排的星期
         */
        private String scheduleWeek;

        /**
         * 开始时间
         */
        @JsonFormat(pattern = "HH:mm")
        private Date startTime;

        /**
         * 结束时间
         */
        @JsonFormat(pattern = "HH:mm")
        private Date endTime;

        /**
         * 教师安排备注, 由教师自己备注(比如什么什么时间点有事情)
         */
        private String planNote;
    }


}
