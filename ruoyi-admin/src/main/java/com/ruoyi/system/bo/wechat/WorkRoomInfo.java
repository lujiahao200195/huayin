package com.ruoyi.system.bo.wechat;

import lombok.Data;

@Data
public class WorkRoomInfo {

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 工作室名字
     */
    private String workRoomName;

    /**
     * 工作室简介
     */
    private String workRoomIntro;

    /**
     * 工作室联系电话
     */
    private String workRoomPhone;

    /**
     * 工作室电子邮箱
     */
    private String workRoomEmail;

    /**
     * 工作室图标
     */
    private String workRoomIcon;

    /**
     * 工作室图片
     */
    private String workRoomImage;


}
