package com.ruoyi.system.bo.wechat;


import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class WorkRoomSearch {
    /**
     * 搜索关键字
     */
    private String search;

    /**
     * 学科ID列表
     */
    private List<Long> subjectIdList;

    /**
     * 地址Id列表
     */
    private List<Long> addressIdList;

}
