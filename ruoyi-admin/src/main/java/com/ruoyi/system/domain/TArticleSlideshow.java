package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 轮播图对象 t_article_slideshow
 * 
 * @author ruoyi
 * @date 2024-05-11
 */
public class TArticleSlideshow extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private Long id;

    /** 轮播图名称 */
    @Excel(name = "轮播图名称")
    private String slideshowName;

    /** 轮播图图片链接 */
    @Excel(name = "轮播图图片链接")
    private String slideshowImage;

    /** 轮播图跳转链接 */
    @Excel(name = "轮播图跳转链接")
    private String slideshowLink;

    /** 轮播图状态 */
    @Excel(name = "轮播图状态")
    private String slideshowStatus;

    /** 创建人 */
    @Excel(name = "创建人")
    private Long createdBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdTime;

    /** 更新人 */
    @Excel(name = "更新人")
    private Long updatedBy;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedTime;

    /** 删除人 */
    @Excel(name = "删除人")
    private Long deleteBy;

    /** 删除时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "删除时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date deleteTime;

    /** 备注 */
    @Excel(name = "备注")
    private String note;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setSlideshowName(String slideshowName) 
    {
        this.slideshowName = slideshowName;
    }

    public String getSlideshowName() 
    {
        return slideshowName;
    }
    public void setSlideshowImage(String slideshowImage) 
    {
        this.slideshowImage = slideshowImage;
    }

    public String getSlideshowImage() 
    {
        return slideshowImage;
    }
    public void setSlideshowLink(String slideshowLink) 
    {
        this.slideshowLink = slideshowLink;
    }

    public String getSlideshowLink() 
    {
        return slideshowLink;
    }
    public void setSlideshowStatus(String slideshowStatus) 
    {
        this.slideshowStatus = slideshowStatus;
    }

    public String getSlideshowStatus() 
    {
        return slideshowStatus;
    }
    public void setCreatedBy(Long createdBy) 
    {
        this.createdBy = createdBy;
    }

    public Long getCreatedBy() 
    {
        return createdBy;
    }
    public void setCreatedTime(Date createdTime) 
    {
        this.createdTime = createdTime;
    }

    public Date getCreatedTime() 
    {
        return createdTime;
    }
    public void setUpdatedBy(Long updatedBy) 
    {
        this.updatedBy = updatedBy;
    }

    public Long getUpdatedBy() 
    {
        return updatedBy;
    }
    public void setUpdatedTime(Date updatedTime) 
    {
        this.updatedTime = updatedTime;
    }

    public Date getUpdatedTime() 
    {
        return updatedTime;
    }
    public void setDeleteBy(Long deleteBy) 
    {
        this.deleteBy = deleteBy;
    }

    public Long getDeleteBy() 
    {
        return deleteBy;
    }
    public void setDeleteTime(Date deleteTime) 
    {
        this.deleteTime = deleteTime;
    }

    public Date getDeleteTime() 
    {
        return deleteTime;
    }
    public void setNote(String note) 
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("slideshowName", getSlideshowName())
            .append("slideshowImage", getSlideshowImage())
            .append("slideshowLink", getSlideshowLink())
            .append("slideshowStatus", getSlideshowStatus())
            .append("createdBy", getCreatedBy())
            .append("createdTime", getCreatedTime())
            .append("updatedBy", getUpdatedBy())
            .append("updatedTime", getUpdatedTime())
            .append("deleteBy", getDeleteBy())
            .append("deleteTime", getDeleteTime())
            .append("note", getNote())
            .toString();
    }
}
