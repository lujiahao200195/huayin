package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 用户对象 t_user
 *
 * @author ruoyi
 * @date 2023-09-12
 */
public class TUser extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 用户名
     */
    @Excel(name = "用户名")
    private String userName;

    /**
     * 性别
     */
    @Excel(name = "性别")
    private String userGender;

    /**
     * 手机号
     */
    @Excel(name = "手机号")
    private String userPhone;

    /**
     * 邮箱
     */
    @Excel(name = "邮箱")
    private String userEmail;

    /**
     * 用户出身年月
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "用户出身年月", width = 30, dateFormat = "yyyy-MM-dd")
    private Date userBirth;

    /**
     * 用户头像链接
     */
    @Excel(name = "用户头像链接")
    private String userPhoto;

    /**
     * 微信小程序openId
     */
    @Excel(name = "微信小程序openId")
    private String userWxMiniOpenid;

    /**
     * 微信公众号openId
     */
    @Excel(name = "微信公众号openId")
    private String userWxActicleOpenid;

    /**
     * 微信会话key
     */
    @Excel(name = "微信会话key")
    private String userWxMiniSessionkey;

    /**
     * 密码
     */
    @Excel(name = "密码")
    private String userPassword;

    /**
     * 注册IP
     */
    @Excel(name = "注册IP")
    private String registerIp;

    /**
     * 最后登入时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "最后登入时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date lastLoginTime;

    /**
     * 最后登入IP
     */
    @Excel(name = "最后登入IP")
    private String lastLoginIp;

    /**
     * 用户是否阅读隐私条例
     */
    @Excel(name = "用户是否阅读隐私条例")
    private String readPrivacyPolicy;

    /**
     * 用户个人简介
     */
    @Excel(name = "用户个人简介")
    private String userIntro;

    /**
     * 创建人
     */
    @Excel(name = "创建人")
    private Long createdBy;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdTime;

    /**
     * 更新人
     */
    @Excel(name = "更新人")
    private Long updatedBy;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedTime;

    /**
     * 删除人
     */
    @Excel(name = "删除人")
    private Long deleteBy;

    /**
     * 删除时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "删除时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date deleteTime;

    /**
     * 备注
     */
    @Excel(name = "备注")
    private String note;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserGender() {
        return userGender;
    }

    public void setUserGender(String userGender) {
        this.userGender = userGender;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public Date getUserBirth() {
        return userBirth;
    }

    public void setUserBirth(Date userBirth) {
        this.userBirth = userBirth;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    public String getUserWxMiniOpenid() {
        return userWxMiniOpenid;
    }

    public void setUserWxMiniOpenid(String userWxMiniOpenid) {
        this.userWxMiniOpenid = userWxMiniOpenid;
    }

    public String getUserWxActicleOpenid() {
        return userWxActicleOpenid;
    }

    public void setUserWxActicleOpenid(String userWxActicleOpenid) {
        this.userWxActicleOpenid = userWxActicleOpenid;
    }

    public String getUserWxMiniSessionkey() {
        return userWxMiniSessionkey;
    }

    public void setUserWxMiniSessionkey(String userWxMiniSessionkey) {
        this.userWxMiniSessionkey = userWxMiniSessionkey;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getRegisterIp() {
        return registerIp;
    }

    public void setRegisterIp(String registerIp) {
        this.registerIp = registerIp;
    }

    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }

    public String getReadPrivacyPolicy() {
        return readPrivacyPolicy;
    }

    public void setReadPrivacyPolicy(String readPrivacyPolicy) {
        this.readPrivacyPolicy = readPrivacyPolicy;
    }

    public String getUserIntro() {
        return userIntro;
    }

    public void setUserIntro(String userIntro) {
        this.userIntro = userIntro;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Long getDeleteBy() {
        return deleteBy;
    }

    public void setDeleteBy(Long deleteBy) {
        this.deleteBy = deleteBy;
    }

    public Date getDeleteTime() {
        return deleteTime;
    }

    public void setDeleteTime(Date deleteTime) {
        this.deleteTime = deleteTime;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("userName", getUserName())
                .append("userGender", getUserGender())
                .append("userPhone", getUserPhone())
                .append("userEmail", getUserEmail())
                .append("userBirth", getUserBirth())
                .append("userPhoto", getUserPhoto())
                .append("userWxMiniOpenid", getUserWxMiniOpenid())
                .append("userWxActicleOpenid", getUserWxActicleOpenid())
                .append("userWxMiniSessionkey", getUserWxMiniSessionkey())
                .append("userPassword", getUserPassword())
                .append("registerIp", getRegisterIp())
                .append("lastLoginTime", getLastLoginTime())
                .append("lastLoginIp", getLastLoginIp())
                .append("readPrivacyPolicy", getReadPrivacyPolicy())
                .append("userIntro", getUserIntro())
                .append("createdBy", getCreatedBy())
                .append("createdTime", getCreatedTime())
                .append("updatedBy", getUpdatedBy())
                .append("updatedTime", getUpdatedTime())
                .append("deleteBy", getDeleteBy())
                .append("deleteTime", getDeleteTime())
                .append("note", getNote())
                .toString();
    }
}
