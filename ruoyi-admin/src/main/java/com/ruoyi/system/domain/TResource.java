package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 资源对象 t_resource
 * 
 * @author ruoyi
 * @date 2023-09-21
 */
public class TResource extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private Long id;

    /** 创建资源的用户ID */
    @Excel(name = "创建资源的用户ID")
    private Long userId;

    /** 资源封面 */
    @Excel(name = "资源封面")
    private String resourceCover;

    /** 资源标题 */
    @Excel(name = "资源标题")
    private String resourceTitle;

    /** 资源简介 */
    @Excel(name = "资源简介")
    private String resourceIntro;

    /** 资源MIME */
    @Excel(name = "资源MIME")
    private String resourceMime;

    /** 资源连接(必须要用连接加token的方式不是无限制下载) */
    @Excel(name = "资源连接(必须要用连接加token的方式不是无限制下载)")
    private String resourceLink;

    /** 文件格式 */
    @Excel(name = "文件格式")
    private String fileType;

    /** 下载量 */
    @Excel(name = "下载量")
    private Long downloadNum;

    /** 资源大小 */
    @Excel(name = "资源大小")
    private String resourceSize;

    /** 公开状态 */
    @Excel(name = "公开状态")
    private String publicStatus;

    /** 审核状态 */
    @Excel(name = "审核状态")
    private String auditStatus;

    /** 创建人 */
    @Excel(name = "创建人")
    private Long createdBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdTime;

    /** 更新人 */
    @Excel(name = "更新人")
    private Long updatedBy;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedTime;

    /** 删除人 */
    @Excel(name = "删除人")
    private Long deleteBy;

    /** 删除时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "删除时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date deleteTime;

    /** 备注 */
    @Excel(name = "备注")
    private String note;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setResourceCover(String resourceCover) 
    {
        this.resourceCover = resourceCover;
    }

    public String getResourceCover() 
    {
        return resourceCover;
    }
    public void setResourceTitle(String resourceTitle) 
    {
        this.resourceTitle = resourceTitle;
    }

    public String getResourceTitle() 
    {
        return resourceTitle;
    }
    public void setResourceIntro(String resourceIntro) 
    {
        this.resourceIntro = resourceIntro;
    }

    public String getResourceIntro() 
    {
        return resourceIntro;
    }
    public void setResourceMime(String resourceMime) 
    {
        this.resourceMime = resourceMime;
    }

    public String getResourceMime() 
    {
        return resourceMime;
    }
    public void setResourceLink(String resourceLink) 
    {
        this.resourceLink = resourceLink;
    }

    public String getResourceLink() 
    {
        return resourceLink;
    }
    public void setFileType(String fileType) 
    {
        this.fileType = fileType;
    }

    public String getFileType() 
    {
        return fileType;
    }
    public void setDownloadNum(Long downloadNum) 
    {
        this.downloadNum = downloadNum;
    }

    public Long getDownloadNum() 
    {
        return downloadNum;
    }
    public void setResourceSize(String resourceSize) 
    {
        this.resourceSize = resourceSize;
    }

    public String getResourceSize() 
    {
        return resourceSize;
    }
    public void setPublicStatus(String publicStatus) 
    {
        this.publicStatus = publicStatus;
    }

    public String getPublicStatus() 
    {
        return publicStatus;
    }
    public void setAuditStatus(String auditStatus) 
    {
        this.auditStatus = auditStatus;
    }

    public String getAuditStatus() 
    {
        return auditStatus;
    }
    public void setCreatedBy(Long createdBy) 
    {
        this.createdBy = createdBy;
    }

    public Long getCreatedBy() 
    {
        return createdBy;
    }
    public void setCreatedTime(Date createdTime) 
    {
        this.createdTime = createdTime;
    }

    public Date getCreatedTime() 
    {
        return createdTime;
    }
    public void setUpdatedBy(Long updatedBy) 
    {
        this.updatedBy = updatedBy;
    }

    public Long getUpdatedBy() 
    {
        return updatedBy;
    }
    public void setUpdatedTime(Date updatedTime) 
    {
        this.updatedTime = updatedTime;
    }

    public Date getUpdatedTime() 
    {
        return updatedTime;
    }
    public void setDeleteBy(Long deleteBy) 
    {
        this.deleteBy = deleteBy;
    }

    public Long getDeleteBy() 
    {
        return deleteBy;
    }
    public void setDeleteTime(Date deleteTime) 
    {
        this.deleteTime = deleteTime;
    }

    public Date getDeleteTime() 
    {
        return deleteTime;
    }
    public void setNote(String note) 
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("resourceCover", getResourceCover())
            .append("resourceTitle", getResourceTitle())
            .append("resourceIntro", getResourceIntro())
            .append("resourceMime", getResourceMime())
            .append("resourceLink", getResourceLink())
            .append("fileType", getFileType())
            .append("downloadNum", getDownloadNum())
            .append("resourceSize", getResourceSize())
            .append("publicStatus", getPublicStatus())
            .append("auditStatus", getAuditStatus())
            .append("createdBy", getCreatedBy())
            .append("createdTime", getCreatedTime())
            .append("updatedBy", getUpdatedBy())
            .append("updatedTime", getUpdatedTime())
            .append("deleteBy", getDeleteBy())
            .append("deleteTime", getDeleteTime())
            .append("note", getNote())
            .toString();
    }
}
