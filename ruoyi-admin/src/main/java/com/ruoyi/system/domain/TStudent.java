package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 学生对象 t_student
 *
 * @author ruoyi
 * @date 2023-08-31
 */
public class TStudent extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 用户ID
     */
    @Excel(name = "用户ID")
    private Long userId;

    /**
     * 学生简介
     */
    @Excel(name = "学生简介")
    private String studentIntro;

    /**
     * 学号
     */
    @Excel(name = "学号")
    private String studentCode;

    /**
     * 作为学生的名字
     */
    @Excel(name = "作为学生的名字")
    private String studentName;

    /**
     * 学生出生日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "学生出生日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date studentBirthday;

    /**
     * 学龄
     */
    @Excel(name = "学龄")
    private String studentAge;

    /**
     * 学生手机号
     */
    @Excel(name = "学生手机号")
    private String studentPhone;

    /**
     * 学生性别
     */
    @Excel(name = "学生性别")
    private String studentGender;

    /**
     * 学生邮箱
     */
    @Excel(name = "学生邮箱")
    private String studentEmail;

    /**
     * 学生目表分数
     */
    @Excel(name = "学生目表分数")
    private Long studentTargetScore;

    /**
     * 知情同意
     */
    @Excel(name = "知情同意")
    private String studentInformedConsent;

    /**
     * 手写签名图片
     */
    @Excel(name = "手写签名图片")
    private String studentHandwrittenSignature;

    /**
     * 审核状态
     */
    @Excel(name = "审核状态")
    private String auditStatus;

    /**
     * 审核结果原因
     */
    @Excel(name = "审核结果原因")
    private String auditResult;

    /**
     * 创建人
     */
    @Excel(name = "创建人")
    private Long createdBy;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdTime;

    /**
     * 更新人
     */
    @Excel(name = "更新人")
    private Long updatedBy;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedTime;

    /**
     * 删除人
     */
    @Excel(name = "删除人")
    private Long deleteBy;

    /**
     * 删除时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "删除时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date deleteTime;

    /**
     * 备注
     */
    @Excel(name = "备注")
    private String note;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getStudentIntro() {
        return studentIntro;
    }

    public void setStudentIntro(String studentIntro) {
        this.studentIntro = studentIntro;
    }

    public String getStudentCode() {
        return studentCode;
    }

    public void setStudentCode(String studentCode) {
        this.studentCode = studentCode;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public Date getStudentBirthday() {
        return studentBirthday;
    }

    public void setStudentBirthday(Date studentBirthday) {
        this.studentBirthday = studentBirthday;
    }

    public String getStudentAge() {
        return studentAge;
    }

    public void setStudentAge(String studentAge) {
        this.studentAge = studentAge;
    }

    public String getStudentPhone() {
        return studentPhone;
    }

    public void setStudentPhone(String studentPhone) {
        this.studentPhone = studentPhone;
    }

    public String getStudentGender() {
        return studentGender;
    }

    public void setStudentGender(String studentGender) {
        this.studentGender = studentGender;
    }

    public String getStudentEmail() {
        return studentEmail;
    }

    public void setStudentEmail(String studentEmail) {
        this.studentEmail = studentEmail;
    }

    public Long getStudentTargetScore() {
        return studentTargetScore;
    }

    public void setStudentTargetScore(Long studentTargetScore) {
        this.studentTargetScore = studentTargetScore;
    }

    public String getStudentInformedConsent() {
        return studentInformedConsent;
    }

    public void setStudentInformedConsent(String studentInformedConsent) {
        this.studentInformedConsent = studentInformedConsent;
    }

    public String getStudentHandwrittenSignature() {
        return studentHandwrittenSignature;
    }

    public void setStudentHandwrittenSignature(String studentHandwrittenSignature) {
        this.studentHandwrittenSignature = studentHandwrittenSignature;
    }

    public String getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(String auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getAuditResult() {
        return auditResult;
    }

    public void setAuditResult(String auditResult) {
        this.auditResult = auditResult;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Long getDeleteBy() {
        return deleteBy;
    }

    public void setDeleteBy(Long deleteBy) {
        this.deleteBy = deleteBy;
    }

    public Date getDeleteTime() {
        return deleteTime;
    }

    public void setDeleteTime(Date deleteTime) {
        this.deleteTime = deleteTime;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("userId", getUserId())
                .append("studentIntro", getStudentIntro())
                .append("studentCode", getStudentCode())
                .append("studentName", getStudentName())
                .append("studentBirthday", getStudentBirthday())
                .append("studentAge", getStudentAge())
                .append("studentPhone", getStudentPhone())
                .append("studentGender", getStudentGender())
                .append("studentEmail", getStudentEmail())
                .append("studentTargetScore", getStudentTargetScore())
                .append("studentInformedConsent", getStudentInformedConsent())
                .append("studentHandwrittenSignature", getStudentHandwrittenSignature())
                .append("auditStatus", getAuditStatus())
                .append("auditResult", getAuditResult())
                .append("createdBy", getCreatedBy())
                .append("createdTime", getCreatedTime())
                .append("updatedBy", getUpdatedBy())
                .append("updatedTime", getUpdatedTime())
                .append("deleteBy", getDeleteBy())
                .append("deleteTime", getDeleteTime())
                .append("note", getNote())
                .toString();
    }
}
