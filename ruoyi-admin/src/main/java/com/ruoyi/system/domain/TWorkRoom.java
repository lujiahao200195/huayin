package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 工作室对象 t_work_room
 *
 * @author ruoyi
 * @date 2023-08-22
 */
public class TWorkRoom extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 工作室名字
     */
    @Excel(name = "工作室名字")
    private String workRoomName;

    /**
     * 工作室链接
     */
    @Excel(name = "工作室链接")
    private String workRoomLink;

    /**
     * 工作室简介
     */
    @Excel(name = "工作室简介")
    private String workRoomIntro;

    /**
     * 工作室联系电话
     */
    @Excel(name = "工作室联系电话")
    private String workRoomPhone;

    /**
     * 工作室电子邮箱
     */
    @Excel(name = "工作室电子邮箱")
    private String workRoomEmail;

    /**
     * 工作室详情
     */
    @Excel(name = "工作室详情")
    private String workRoomDetails;

    /**
     * 工作室图标
     */
    @Excel(name = "工作室图标")
    private String workRoomIcon;

    /**
     * 工作室图片
     */
    @Excel(name = "工作室图片")
    private String workRoomImage;

    /**
     * 工作室展示文章ID
     */
    @Excel(name = "工作室展示文章ID")
    private Long showArticleId;

    /**
     * 工作室负责教师ID
     */
    @Excel(name = "工作室负责教师ID")
    private Long principalTeacherId;

    /**
     * 工作室咨询教师ID
     */
    @Excel(name = "工作室咨询教师ID")
    private Long consultTeacherId;

    /**
     * 工作室审核状态
     */
    @Excel(name = "工作室审核状态")
    private String auditStatus;

    /**
     * 创建人
     */
    @Excel(name = "创建人")
    private Long createdBy;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdTime;

    /**
     * 更新人
     */
    @Excel(name = "更新人")
    private Long updatedBy;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedTime;

    /**
     * 删除人
     */
    @Excel(name = "删除人")
    private Long deleteBy;

    /**
     * 删除时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "删除时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date deleteTime;

    /**
     * 备注
     */
    @Excel(name = "备注")
    private String note;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWorkRoomName() {
        return workRoomName;
    }

    public void setWorkRoomName(String workRoomName) {
        this.workRoomName = workRoomName;
    }

    public String getWorkRoomLink() {
        return workRoomLink;
    }

    public void setWorkRoomLink(String workRoomLink) {
        this.workRoomLink = workRoomLink;
    }

    public String getWorkRoomIntro() {
        return workRoomIntro;
    }

    public void setWorkRoomIntro(String workRoomIntro) {
        this.workRoomIntro = workRoomIntro;
    }

    public String getWorkRoomPhone() {
        return workRoomPhone;
    }

    public void setWorkRoomPhone(String workRoomPhone) {
        this.workRoomPhone = workRoomPhone;
    }

    public String getWorkRoomEmail() {
        return workRoomEmail;
    }

    public void setWorkRoomEmail(String workRoomEmail) {
        this.workRoomEmail = workRoomEmail;
    }

    public String getWorkRoomDetails() {
        return workRoomDetails;
    }

    public void setWorkRoomDetails(String workRoomDetails) {
        this.workRoomDetails = workRoomDetails;
    }

    public String getWorkRoomIcon() {
        return workRoomIcon;
    }

    public void setWorkRoomIcon(String workRoomIcon) {
        this.workRoomIcon = workRoomIcon;
    }

    public String getWorkRoomImage() {
        return workRoomImage;
    }

    public void setWorkRoomImage(String workRoomImage) {
        this.workRoomImage = workRoomImage;
    }

    public Long getShowArticleId() {
        return showArticleId;
    }

    public void setShowArticleId(Long showArticleId) {
        this.showArticleId = showArticleId;
    }

    public Long getPrincipalTeacherId() {
        return principalTeacherId;
    }

    public void setPrincipalTeacherId(Long principalTeacherId) {
        this.principalTeacherId = principalTeacherId;
    }

    public Long getConsultTeacherId() {
        return consultTeacherId;
    }

    public void setConsultTeacherId(Long consultTeacherId) {
        this.consultTeacherId = consultTeacherId;
    }

    public String getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(String auditStatus) {
        this.auditStatus = auditStatus;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Long getDeleteBy() {
        return deleteBy;
    }

    public void setDeleteBy(Long deleteBy) {
        this.deleteBy = deleteBy;
    }

    public Date getDeleteTime() {
        return deleteTime;
    }

    public void setDeleteTime(Date deleteTime) {
        this.deleteTime = deleteTime;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("workRoomName", getWorkRoomName())
                .append("workRoomLink", getWorkRoomLink())
                .append("workRoomIntro", getWorkRoomIntro())
                .append("workRoomPhone", getWorkRoomPhone())
                .append("workRoomEmail", getWorkRoomEmail())
                .append("workRoomDetails", getWorkRoomDetails())
                .append("workRoomIcon", getWorkRoomIcon())
                .append("workRoomImage", getWorkRoomImage())
                .append("showArticleId", getShowArticleId())
                .append("principalTeacherId", getPrincipalTeacherId())
                .append("consultTeacherId", getConsultTeacherId())
                .append("auditStatus", getAuditStatus())
                .append("createdBy", getCreatedBy())
                .append("createdTime", getCreatedTime())
                .append("updatedBy", getUpdatedBy())
                .append("updatedTime", getUpdatedTime())
                .append("deleteBy", getDeleteBy())
                .append("deleteTime", getDeleteTime())
                .append("note", getNote())
                .toString();
    }
}
