package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 教师和教师证书关系对象 t_relation_teacher_certificates
 *
 * @author ruoyi
 * @date 2023-08-17
 */
public class TRelationTeacherCertificates extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 教师基本证件ID
     */
    @Excel(name = "教师基本证件ID")
    private Long teacherCertificates;

    /**
     * 教师ID
     */
    @Excel(name = "教师ID")
    private Long teacherId;

    /**
     * 证件照1
     */
    @Excel(name = "证件照1")
    private String certificatesPhoto1;

    /**
     * 证件照2
     */
    @Excel(name = "证件照2")
    private String certificatesPhoto2;

    /**
     * 证件照3
     */
    @Excel(name = "证件照3")
    private String certificatesPhoto3;

    /**
     * 证件照4
     */
    @Excel(name = "证件照4")
    private String certificatesPhoto4;

    /**
     * 证件照5
     */
    @Excel(name = "证件照5")
    private String certificatesPhoto5;

    /**
     * 审核状态
     */
    @Excel(name = "审核状态")
    private String auditStatus;

    /**
     * 创建人
     */
    @Excel(name = "创建人")
    private Long createdBy;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdTime;

    /**
     * 更新人
     */
    @Excel(name = "更新人")
    private Long updatedBy;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedTime;

    /**
     * 删除人
     */
    @Excel(name = "删除人")
    private Long deleteBy;

    /**
     * 删除时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "删除时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date deleteTime;

    /**
     * 备注
     */
    @Excel(name = "备注")
    private String note;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTeacherCertificates() {
        return teacherCertificates;
    }

    public void setTeacherCertificates(Long teacherCertificates) {
        this.teacherCertificates = teacherCertificates;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public String getCertificatesPhoto1() {
        return certificatesPhoto1;
    }

    public void setCertificatesPhoto1(String certificatesPhoto1) {
        this.certificatesPhoto1 = certificatesPhoto1;
    }

    public String getCertificatesPhoto2() {
        return certificatesPhoto2;
    }

    public void setCertificatesPhoto2(String certificatesPhoto2) {
        this.certificatesPhoto2 = certificatesPhoto2;
    }

    public String getCertificatesPhoto3() {
        return certificatesPhoto3;
    }

    public void setCertificatesPhoto3(String certificatesPhoto3) {
        this.certificatesPhoto3 = certificatesPhoto3;
    }

    public String getCertificatesPhoto4() {
        return certificatesPhoto4;
    }

    public void setCertificatesPhoto4(String certificatesPhoto4) {
        this.certificatesPhoto4 = certificatesPhoto4;
    }

    public String getCertificatesPhoto5() {
        return certificatesPhoto5;
    }

    public void setCertificatesPhoto5(String certificatesPhoto5) {
        this.certificatesPhoto5 = certificatesPhoto5;
    }

    public String getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(String auditStatus) {
        this.auditStatus = auditStatus;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Long getDeleteBy() {
        return deleteBy;
    }

    public void setDeleteBy(Long deleteBy) {
        this.deleteBy = deleteBy;
    }

    public Date getDeleteTime() {
        return deleteTime;
    }

    public void setDeleteTime(Date deleteTime) {
        this.deleteTime = deleteTime;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("teacherCertificates", getTeacherCertificates())
                .append("teacherId", getTeacherId())
                .append("certificatesPhoto1", getCertificatesPhoto1())
                .append("certificatesPhoto2", getCertificatesPhoto2())
                .append("certificatesPhoto3", getCertificatesPhoto3())
                .append("certificatesPhoto4", getCertificatesPhoto4())
                .append("certificatesPhoto5", getCertificatesPhoto5())
                .append("auditStatus", getAuditStatus())
                .append("createdBy", getCreatedBy())
                .append("createdTime", getCreatedTime())
                .append("updatedBy", getUpdatedBy())
                .append("updatedTime", getUpdatedTime())
                .append("deleteBy", getDeleteBy())
                .append("deleteTime", getDeleteTime())
                .append("note", getNote())
                .toString();
    }
}
