package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * 文章对象 t_article
 *
 * @author ruoyi
 * @date 2023-08-17
 */
public class TArticle extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 发布文章的用户ID
     */
    @Excel(name = "发布文章的用户ID")
    private Long userId;

    /**
     * 工作室ID
     */
    @Excel(name = "工作室ID")
    private Long workRoomId;

    @Excel(name = "类型")
    private Long articleTwoTypeId;

    /**
     * 文章标题
     */
    @Excel(name = "文章标题")
    @NotBlank(message = "文章标题为必填项")
    private String articleTitle;

    /**
     * 文章简介
     */
    @Excel(name = "文章简介")
    private String articleNote;

    /**
     * 文章图片
     */
    @Excel(name = "文章图片")
    private String articleImage;

    /**
     * 文章链接
     */
    @Excel(name = "文章链接")
    private String articleLink;

    /**
     * 文章内容
     */
    @Excel(name = "文章内容")
    private String articleContent;

    /**
     * 文章阅读数量
     */
    @Excel(name = "文章阅读数量")
    private Long articleReadNum;

    /**
     * 文章点赞数量
     */
    @Excel(name = "文章点赞数量")
    private Long articleUpNum;

    /**
     * 文章点踩数量
     */
    @Excel(name = "文章点踩数量")
    private Long articleDownNum;

    /**
     * 审核状态
     */
    @Excel(name = "审核状态")
    private String auditStatus;

    /**
     * 创建人
     */
    @Excel(name = "创建人")
    private Long createdBy;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdTime;

    /**
     * 更新人
     */
    @Excel(name = "更新人")
    private Long updatedBy;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedTime;

    /**
     * 删除人
     */
    @Excel(name = "删除人")
    private Long deleteBy;

    /**
     * 删除时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "删除时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date deleteTime;

    /**
     * 备注
     */
    @Excel(name = "备注")
    private String note;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getArticleTwoTypeId() {
        return articleTwoTypeId;
    }

    public void setArticleTwoTypeId(Long articleTwoTypeId) {
        this.articleTwoTypeId = articleTwoTypeId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getWorkRoomId() {
        return workRoomId;
    }

    public void setWorkRoomId(Long workRoomId) {
        this.workRoomId = workRoomId;
    }

    public String getArticleTitle() {
        return articleTitle;
    }

    public void setArticleTitle(String articleTitle) {
        this.articleTitle = articleTitle;
    }

    public String getArticleNote() {
        return articleNote;
    }

    public void setArticleNote(String articleNote) {
        this.articleNote = articleNote;
    }

    public String getArticleImage() {
        return articleImage;
    }

    public void setArticleImage(String articleImage) {
        this.articleImage = articleImage;
    }

    public String getArticleLink() {
        return articleLink;
    }

    public void setArticleLink(String articleLink) {
        this.articleLink = articleLink;
    }

    public String getArticleContent() {
        return articleContent;
    }

    public void setArticleContent(String articleContent) {
        this.articleContent = articleContent;
    }

    public Long getArticleReadNum() {
        return articleReadNum;
    }

    public void setArticleReadNum(Long articleReadNum) {
        this.articleReadNum = articleReadNum;
    }

    public Long getArticleUpNum() {
        return articleUpNum;
    }

    public void setArticleUpNum(Long articleUpNum) {
        this.articleUpNum = articleUpNum;
    }

    public Long getArticleDownNum() {
        return articleDownNum;
    }

    public void setArticleDownNum(Long articleDownNum) {
        this.articleDownNum = articleDownNum;
    }

    public String getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(String auditStatus) {
        this.auditStatus = auditStatus;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Long getDeleteBy() {
        return deleteBy;
    }

    public void setDeleteBy(Long deleteBy) {
        this.deleteBy = deleteBy;
    }

    public Date getDeleteTime() {
        return deleteTime;
    }

    public void setDeleteTime(Date deleteTime) {
        this.deleteTime = deleteTime;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("userId", getUserId())
                .append("workRoomId", getWorkRoomId())
                .append("articleTitle", getArticleTitle())
                .append("articleNote", getArticleNote())
                .append("articleImage", getArticleImage())
                .append("articleLink", getArticleLink())
                .append("articleContent", getArticleContent())
                .append("articleReadNum", getArticleReadNum())
                .append("articleUpNum", getArticleUpNum())
                .append("articleDownNum", getArticleDownNum())
                .append("auditStatus", getAuditStatus())
                .append("createdBy", getCreatedBy())
                .append("createdTime", getCreatedTime())
                .append("updatedBy", getUpdatedBy())
                .append("updatedTime", getUpdatedTime())
                .append("deleteBy", getDeleteBy())
                .append("deleteTime", getDeleteTime())
                .append("note", getNote())
                .toString();
    }
}
