package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 教师对象 t_teacher
 *
 * @author ruoyi
 * @date 2023-09-13
 */
public class TTeacher extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 用户ID
     */
    @Excel(name = "用户ID")
    private Long userId;

    /**
     * 教师性别
     */
    @Excel(name = "教师性别")
    private String teacherGender;

    /**
     * 教师生日
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "教师生日", width = 30, dateFormat = "yyyy-MM-dd")
    private Date teacherBirthday;

    /**
     * 作为教师时的教师名
     */
    @Excel(name = "作为教师时的教师名")
    private String teacherName;

    /**
     * 教师编号
     */
    @Excel(name = "教师编号")
    private String teacherCode;

    /**
     * 教师评分
     */
    @Excel(name = "教师评分")
    private Long teacherScore;

    /**
     * 教师简介
     */
    @Excel(name = "教师简介")
    private String teacherIntro;

    /**
     * 教师老师照片链接
     */
    @Excel(name = "教师老师照片链接")
    private String teacherPicture;

    /**
     * 教师联系电话
     */
    @Excel(name = "教师联系电话")
    private String teacherPhone;

    /**
     * 教师电子邮箱
     */
    @Excel(name = "教师电子邮箱")
    private String teacherEmail;

    /**
     * 教师院校名称
     */
    @Excel(name = "教师院校名称")
    private String teacherSchool;

    /**
     * 所学专业
     */
    @Excel(name = "所学专业")
    private String teacherMajor;

    /**
     * 当前学历
     */
    @Excel(name = "当前学历")
    private String teacherEducation;

    /**
     * 所在国家或地区
     */
    @Excel(name = "所在国家或地区")
    private String teahcerCountryregion;

    /**
     * 教师知情简述
     */
    @Excel(name = "教师知情简述")
    private String teacherInformedbrief;

    /**
     * 教师签名
     */
    @Excel(name = "教师签名")
    private String teacherSignature;

    /**
     * 教师政治面貌
     */
    @Excel(name = "教师政治面貌")
    private String teacherParty;

    /**
     * 教师民族
     */
    @Excel(name = "教师民族")
    private String teacherNationality;

    /**
     * 教学或工作经历
     */
    @Excel(name = "教学或工作经历")
    private String teacherExperience;

    /**
     * 备注
     */
    @Excel(name = "备注")
    private String teacherRemark;

    /**
     * 教师状态
     */
    @Excel(name = "教师状态")
    private String teahcerStatus;

    /**
     * 审核状态
     */
    @Excel(name = "审核状态")
    private String auditStatus;

    /**
     * 审核结果原因
     */
    @Excel(name = "审核结果原因")
    private String auditResult;

    /**
     * 引荐用户ID
     */
    @Excel(name = "引荐用户ID")
    private Long referrerUserId;

    /**
     * 教师自己的邀请码
     */
    @Excel(name = "教师自己的邀请码")
    private String referrerCode;

    /**
     * 创建人
     */
    @Excel(name = "创建人")
    private Long createdBy;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdTime;

    /**
     * 更新人
     */
    @Excel(name = "更新人")
    private Long updatedBy;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedTime;

    /**
     * 删除人
     */
    @Excel(name = "删除人")
    private Long deleteBy;

    /**
     * 删除时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "删除时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date deleteTime;

    /**
     * 备注
     */
    @Excel(name = "备注")
    private String note;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getTeacherGender() {
        return teacherGender;
    }

    public void setTeacherGender(String teacherGender) {
        this.teacherGender = teacherGender;
    }

    public Date getTeacherBirthday() {
        return teacherBirthday;
    }

    public void setTeacherBirthday(Date teacherBirthday) {
        this.teacherBirthday = teacherBirthday;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getTeacherCode() {
        return teacherCode;
    }

    public void setTeacherCode(String teacherCode) {
        this.teacherCode = teacherCode;
    }

    public Long getTeacherScore() {
        return teacherScore;
    }

    public void setTeacherScore(Long teacherScore) {
        this.teacherScore = teacherScore;
    }

    public String getTeacherIntro() {
        return teacherIntro;
    }

    public void setTeacherIntro(String teacherIntro) {
        this.teacherIntro = teacherIntro;
    }

    public String getTeacherPicture() {
        return teacherPicture;
    }

    public void setTeacherPicture(String teacherPicture) {
        this.teacherPicture = teacherPicture;
    }

    public String getTeacherPhone() {
        return teacherPhone;
    }

    public void setTeacherPhone(String teacherPhone) {
        this.teacherPhone = teacherPhone;
    }

    public String getTeacherEmail() {
        return teacherEmail;
    }

    public void setTeacherEmail(String teacherEmail) {
        this.teacherEmail = teacherEmail;
    }

    public String getTeacherSchool() {
        return teacherSchool;
    }

    public void setTeacherSchool(String teacherSchool) {
        this.teacherSchool = teacherSchool;
    }

    public String getTeacherMajor() {
        return teacherMajor;
    }

    public void setTeacherMajor(String teacherMajor) {
        this.teacherMajor = teacherMajor;
    }

    public String getTeacherEducation() {
        return teacherEducation;
    }

    public void setTeacherEducation(String teacherEducation) {
        this.teacherEducation = teacherEducation;
    }

    public String getTeahcerCountryregion() {
        return teahcerCountryregion;
    }

    public void setTeahcerCountryregion(String teahcerCountryregion) {
        this.teahcerCountryregion = teahcerCountryregion;
    }

    public String getTeacherInformedbrief() {
        return teacherInformedbrief;
    }

    public void setTeacherInformedbrief(String teacherInformedbrief) {
        this.teacherInformedbrief = teacherInformedbrief;
    }

    public String getTeacherSignature() {
        return teacherSignature;
    }

    public void setTeacherSignature(String teacherSignature) {
        this.teacherSignature = teacherSignature;
    }

    public String getTeacherParty() {
        return teacherParty;
    }

    public void setTeacherParty(String teacherParty) {
        this.teacherParty = teacherParty;
    }

    public String getTeacherNationality() {
        return teacherNationality;
    }

    public void setTeacherNationality(String teacherNationality) {
        this.teacherNationality = teacherNationality;
    }

    public String getTeacherExperience() {
        return teacherExperience;
    }

    public void setTeacherExperience(String teacherExperience) {
        this.teacherExperience = teacherExperience;
    }

    public String getTeacherRemark() {
        return teacherRemark;
    }

    public void setTeacherRemark(String teacherRemark) {
        this.teacherRemark = teacherRemark;
    }

    public String getTeahcerStatus() {
        return teahcerStatus;
    }

    public void setTeahcerStatus(String teahcerStatus) {
        this.teahcerStatus = teahcerStatus;
    }

    public String getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(String auditStatus) {
        this.auditStatus = auditStatus;
    }

    public String getAuditResult() {
        return auditResult;
    }

    public void setAuditResult(String auditResult) {
        this.auditResult = auditResult;
    }

    public Long getReferrerUserId() {
        return referrerUserId;
    }

    public void setReferrerUserId(Long referrerUserId) {
        this.referrerUserId = referrerUserId;
    }

    public String getReferrerCode() {
        return referrerCode;
    }

    public void setReferrerCode(String referrerCode) {
        this.referrerCode = referrerCode;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Long getDeleteBy() {
        return deleteBy;
    }

    public void setDeleteBy(Long deleteBy) {
        this.deleteBy = deleteBy;
    }

    public Date getDeleteTime() {
        return deleteTime;
    }

    public void setDeleteTime(Date deleteTime) {
        this.deleteTime = deleteTime;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("userId", getUserId())
                .append("teacherGender", getTeacherGender())
                .append("teacherBirthday", getTeacherBirthday())
                .append("teacherName", getTeacherName())
                .append("teacherCode", getTeacherCode())
                .append("teacherScore", getTeacherScore())
                .append("teacherIntro", getTeacherIntro())
                .append("teacherPicture", getTeacherPicture())
                .append("teacherPhone", getTeacherPhone())
                .append("teacherEmail", getTeacherEmail())
                .append("teacherSchool", getTeacherSchool())
                .append("teacherMajor", getTeacherMajor())
                .append("teacherEducation", getTeacherEducation())
                .append("teahcerCountryregion", getTeahcerCountryregion())
                .append("teacherInformedbrief", getTeacherInformedbrief())
                .append("teacherSignature", getTeacherSignature())
                .append("teacherParty", getTeacherParty())
                .append("teacherNationality", getTeacherNationality())
                .append("teacherExperience", getTeacherExperience())
                .append("teacherRemark", getTeacherRemark())
                .append("teahcerStatus", getTeahcerStatus())
                .append("auditStatus", getAuditStatus())
                .append("auditResult", getAuditResult())
                .append("referrerUserId", getReferrerUserId())
                .append("referrerCode", getReferrerCode())
                .append("createdBy", getCreatedBy())
                .append("createdTime", getCreatedTime())
                .append("updatedBy", getUpdatedBy())
                .append("updatedTime", getUpdatedTime())
                .append("deleteBy", getDeleteBy())
                .append("deleteTime", getDeleteTime())
                .append("note", getNote())
                .toString();
    }
}
