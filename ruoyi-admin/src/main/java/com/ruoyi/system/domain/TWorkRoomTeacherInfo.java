package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 工作室教师信息卡对象 t_work_room_teacher_info
 *
 * @author ruoyi
 * @date 2023-08-18
 */
public class TWorkRoomTeacherInfo extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 工作室ID
     */
    @Excel(name = "工作室ID")
    private Long workRoomId;

    /**
     * 教师名
     */
    @Excel(name = "教师名")
    private String teacherName;

    /**
     * 教师简介
     */
    @Excel(name = "教师简介")
    private String teacherIntro;

    /**
     * 教师照片1
     */
    @Excel(name = "教师照片1")
    private String teacherPhoto1;

    /**
     * 教师照片2
     */
    @Excel(name = "教师照片2")
    private String teacherPhoto2;

    /**
     * 教师照片3
     */
    @Excel(name = "教师照片3")
    private String teacherPhoto3;

    /**
     * 教师详情
     */
    @Excel(name = "教师详情")
    private String teacherDetails;

    /**
     * 教师联系电话
     */
    @Excel(name = "教师联系电话")
    private String teacherPhone;

    /**
     * 教师联系邮箱
     */
    @Excel(name = "教师联系邮箱")
    private String teacherEmail;

    /**
     * 信息卡隐藏状态
     */
    @Excel(name = "信息卡隐藏状态")
    private String hiddenStatus;

    /**
     * 审核状态
     */
    @Excel(name = "审核状态")
    private String auditStatus;

    /**
     * 创建人
     */
    @Excel(name = "创建人")
    private Long createdBy;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date createdTime;

    /**
     * 更新人
     */
    @Excel(name = "更新人")
    private Long updatedBy;

    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updatedTime;

    /**
     * 删除人
     */
    @Excel(name = "删除人")
    private Long deleteBy;

    /**
     * 删除时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "删除时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date deleteTime;

    /**
     * 备注
     */
    @Excel(name = "备注")
    private String note;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getWorkRoomId() {
        return workRoomId;
    }

    public void setWorkRoomId(Long workRoomId) {
        this.workRoomId = workRoomId;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getTeacherIntro() {
        return teacherIntro;
    }

    public void setTeacherIntro(String teacherIntro) {
        this.teacherIntro = teacherIntro;
    }

    public String getTeacherPhoto1() {
        return teacherPhoto1;
    }

    public void setTeacherPhoto1(String teacherPhoto1) {
        this.teacherPhoto1 = teacherPhoto1;
    }

    public String getTeacherPhoto2() {
        return teacherPhoto2;
    }

    public void setTeacherPhoto2(String teacherPhoto2) {
        this.teacherPhoto2 = teacherPhoto2;
    }

    public String getTeacherPhoto3() {
        return teacherPhoto3;
    }

    public void setTeacherPhoto3(String teacherPhoto3) {
        this.teacherPhoto3 = teacherPhoto3;
    }

    public String getTeacherDetails() {
        return teacherDetails;
    }

    public void setTeacherDetails(String teacherDetails) {
        this.teacherDetails = teacherDetails;
    }

    public String getTeacherPhone() {
        return teacherPhone;
    }

    public void setTeacherPhone(String teacherPhone) {
        this.teacherPhone = teacherPhone;
    }

    public String getTeacherEmail() {
        return teacherEmail;
    }

    public void setTeacherEmail(String teacherEmail) {
        this.teacherEmail = teacherEmail;
    }

    public String getHiddenStatus() {
        return hiddenStatus;
    }

    public void setHiddenStatus(String hiddenStatus) {
        this.hiddenStatus = hiddenStatus;
    }

    public String getAuditStatus() {
        return auditStatus;
    }

    public void setAuditStatus(String auditStatus) {
        this.auditStatus = auditStatus;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Long getDeleteBy() {
        return deleteBy;
    }

    public void setDeleteBy(Long deleteBy) {
        this.deleteBy = deleteBy;
    }

    public Date getDeleteTime() {
        return deleteTime;
    }

    public void setDeleteTime(Date deleteTime) {
        this.deleteTime = deleteTime;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("workRoomId", getWorkRoomId())
                .append("teacherName", getTeacherName())
                .append("teacherIntro", getTeacherIntro())
                .append("teacherPhoto1", getTeacherPhoto1())
                .append("teacherPhoto2", getTeacherPhoto2())
                .append("teacherPhoto3", getTeacherPhoto3())
                .append("teacherDetails", getTeacherDetails())
                .append("teacherPhone", getTeacherPhone())
                .append("teacherEmail", getTeacherEmail())
                .append("hiddenStatus", getHiddenStatus())
                .append("auditStatus", getAuditStatus())
                .append("createdBy", getCreatedBy())
                .append("createdTime", getCreatedTime())
                .append("updatedBy", getUpdatedBy())
                .append("updatedTime", getUpdatedTime())
                .append("deleteBy", getDeleteBy())
                .append("deleteTime", getDeleteTime())
                .append("note", getNote())
                .toString();
    }
}
