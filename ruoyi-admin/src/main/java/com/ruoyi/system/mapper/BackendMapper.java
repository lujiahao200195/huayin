package com.ruoyi.system.mapper;

import com.ruoyi.system.vo.wechat.GetStudentAndOpenId;
import com.ruoyi.system.vo.wechat.SubjectDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BackendMapper {


    /**
     * 判断是否存在该id的数据
     *
     * @return
     */
    Integer count(@Param("tbName") String tableName, @Param("id") Integer id);

    /**
     * 根据学生表Id修改学生审核状态码
     *
     * @return
     */
    Integer updateStudentStateById(@Param("id") Integer id, @Param("state") String state);

    /**
     * 根据学生表Id查学生名和电话 微信小程序openid
     */
    GetStudentAndOpenId selectStudentById(@Param("id") Integer id);

    /**
     * 根据学生表userId查学生课程
     */
    List<String> selectSubjectsById(@Param("uid") Integer uId);

    /**
     * 根据教师表Id修改教师审核状态码
     *
     * @return
     */
    Integer updateTeacherStateById(@Param("id") Integer id, @Param("state") String state);

    /**
     * 根据教师表id查教师姓名
     *
     * @param id
     * @return
     */
    String selectTeacherNameById(@Param("id") Integer id);

    /**
     * 根据教师id查教师消除openid
     *
     * @param id
     * @return
     */
    String selectTeacherOpenIdById(@Param("id") Integer id);

    /**
     * 根据学生id查学生优势的科目
     *
     * @param studentId
     * @return
     */
    String[] selectObjectsByStudentId(@Param("studentId") Long studentId);

    /**
     * 根据学生id 查需强化的科目
     *
     * @param studentId
     * @return
     */
    List<SubjectDto> selectNeedSubjectByStudentId(@Param("studentId") Long studentId);

}
