package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TBbsHelp;

import java.util.List;

/**
 * 论坛求助Mapper接口
 *
 * @author ruoyi
 * @date 2023-08-30
 */
public interface TBbsHelpMapper {
    /**
     * 查询论坛求助
     *
     * @param id 论坛求助主键
     * @return 论坛求助
     */
    public TBbsHelp selectTBbsHelpById(Long id);

    /**
     * 查询论坛求助列表
     *
     * @param tBbsHelp 论坛求助
     * @return 论坛求助集合
     */
    public List<TBbsHelp> selectTBbsHelpList(TBbsHelp tBbsHelp);

    /**
     * 新增论坛求助
     *
     * @param tBbsHelp 论坛求助
     * @return 结果
     */
    public int insertTBbsHelp(TBbsHelp tBbsHelp);

    /**
     * 修改论坛求助
     *
     * @param tBbsHelp 论坛求助
     * @return 结果
     */
    public int updateTBbsHelp(TBbsHelp tBbsHelp);

    /**
     * 删除论坛求助
     *
     * @param id 论坛求助主键
     * @return 结果
     */
    public int deleteTBbsHelpById(Long id);

    /**
     * 批量删除论坛求助
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTBbsHelpByIds(Long[] ids);
}
