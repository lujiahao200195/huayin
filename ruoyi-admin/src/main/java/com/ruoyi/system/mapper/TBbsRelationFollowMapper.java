package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TBbsRelationFollow;

import java.util.List;

/**
 * 关注关系Mapper接口
 *
 * @author ruoyi
 * @date 2023-08-29
 */
public interface TBbsRelationFollowMapper {
    /**
     * 查询关注关系
     *
     * @param id 关注关系主键
     * @return 关注关系
     */
    public TBbsRelationFollow selectTBbsRelationFollowById(Long id);

    /**
     * 查询关注关系列表
     *
     * @param tBbsRelationFollow 关注关系
     * @return 关注关系集合
     */
    public List<TBbsRelationFollow> selectTBbsRelationFollowList(TBbsRelationFollow tBbsRelationFollow);

    /**
     * 新增关注关系
     *
     * @param tBbsRelationFollow 关注关系
     * @return 结果
     */
    public int insertTBbsRelationFollow(TBbsRelationFollow tBbsRelationFollow);

    /**
     * 修改关注关系
     *
     * @param tBbsRelationFollow 关注关系
     * @return 结果
     */
    public int updateTBbsRelationFollow(TBbsRelationFollow tBbsRelationFollow);

    /**
     * 删除关注关系
     *
     * @param id 关注关系主键
     * @return 结果
     */
    public int deleteTBbsRelationFollowById(Long id);

    /**
     * 批量删除关注关系
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTBbsRelationFollowByIds(Long[] ids);
}
