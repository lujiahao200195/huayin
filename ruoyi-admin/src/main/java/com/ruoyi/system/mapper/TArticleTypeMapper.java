package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TArticleType;

import java.util.List;

/**
 * 文章分类Mapper接口
 *
 * @author ruoyi
 * @date 2023-08-15
 */
public interface TArticleTypeMapper {
    /**
     * 查询文章分类
     *
     * @param id 文章分类主键
     * @return 文章分类
     */
    public TArticleType selectTArticleTypeById(Long id);

    /**
     * 查询文章分类列表
     *
     * @param tArticleType 文章分类
     * @return 文章分类集合
     */
    public List<TArticleType> selectTArticleTypeList(TArticleType tArticleType);

    /**
     * 新增文章分类
     *
     * @param tArticleType 文章分类
     * @return 结果
     */
    public int insertTArticleType(TArticleType tArticleType);

    /**
     * 修改文章分类
     *
     * @param tArticleType 文章分类
     * @return 结果
     */
    public int updateTArticleType(TArticleType tArticleType);

    /**
     * 删除文章分类
     *
     * @param id 文章分类主键
     * @return 结果
     */
    public int deleteTArticleTypeById(Long id);

    /**
     * 批量删除文章分类
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTArticleTypeByIds(Long[] ids);
}
