package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TUser;

import java.util.List;

/**
 * 用户Mapper接口
 *
 * @author ruoyi
 * @date 2023-09-12
 */
public interface TUserMapper {
    /**
     * 查询用户
     *
     * @param id 用户主键
     * @return 用户
     */
    public TUser selectTUserById(Long id);

    /**
     * 查询用户列表
     *
     * @param tUser 用户
     * @return 用户集合
     */
    public List<TUser> selectTUserList(TUser tUser);

    /**
     * 新增用户
     *
     * @param tUser 用户
     * @return 结果
     */
    public int insertTUser(TUser tUser);

    /**
     * 修改用户
     *
     * @param tUser 用户
     * @return 结果
     */
    public int updateTUser(TUser tUser);

    /**
     * 删除用户
     *
     * @param id 用户主键
     * @return 结果
     */
    public int deleteTUserById(Long id);

    /**
     * 批量删除用户
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTUserByIds(Long[] ids);
}
