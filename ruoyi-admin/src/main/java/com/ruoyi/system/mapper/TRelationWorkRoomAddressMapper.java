package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TRelationWorkRoomAddress;

import java.util.List;

/**
 * 工作室与地区关系Mapper接口
 *
 * @author ruoyi
 * @date 2023-08-21
 */
public interface TRelationWorkRoomAddressMapper {
    /**
     * 查询工作室与地区关系
     *
     * @param id 工作室与地区关系主键
     * @return 工作室与地区关系
     */
    public TRelationWorkRoomAddress selectTRelationWorkRoomAddressById(Long id);

    /**
     * 查询工作室与地区关系列表
     *
     * @param tRelationWorkRoomAddress 工作室与地区关系
     * @return 工作室与地区关系集合
     */
    public List<TRelationWorkRoomAddress> selectTRelationWorkRoomAddressList(TRelationWorkRoomAddress tRelationWorkRoomAddress);

    /**
     * 新增工作室与地区关系
     *
     * @param tRelationWorkRoomAddress 工作室与地区关系
     * @return 结果
     */
    public int insertTRelationWorkRoomAddress(TRelationWorkRoomAddress tRelationWorkRoomAddress);

    /**
     * 修改工作室与地区关系
     *
     * @param tRelationWorkRoomAddress 工作室与地区关系
     * @return 结果
     */
    public int updateTRelationWorkRoomAddress(TRelationWorkRoomAddress tRelationWorkRoomAddress);

    /**
     * 删除工作室与地区关系
     *
     * @param id 工作室与地区关系主键
     * @return 结果
     */
    public int deleteTRelationWorkRoomAddressById(Long id);

    /**
     * 批量删除工作室与地区关系
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTRelationWorkRoomAddressByIds(Long[] ids);
}
