package com.ruoyi.system.mapper.backend;

import com.ruoyi.system.domain.TUser;

public interface UserMapper {

    /**
     * 根据用户id查用户信息
     *
     * @param id
     * @return
     */
    TUser getUserById(Long id);
}
