package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TStudentSchedule;

import java.util.List;

/**
 * 学生可安排时间Mapper接口
 *
 * @author ruoyi
 * @date 2023-08-17
 */
public interface TStudentScheduleMapper {
    /**
     * 查询学生可安排时间
     *
     * @param id 学生可安排时间主键
     * @return 学生可安排时间
     */
    public TStudentSchedule selectTStudentScheduleById(Long id);

    /**
     * 查询学生可安排时间列表
     *
     * @param tStudentSchedule 学生可安排时间
     * @return 学生可安排时间集合
     */
    public List<TStudentSchedule> selectTStudentScheduleList(TStudentSchedule tStudentSchedule);

    /**
     * 新增学生可安排时间
     *
     * @param tStudentSchedule 学生可安排时间
     * @return 结果
     */
    public int insertTStudentSchedule(TStudentSchedule tStudentSchedule);

    /**
     * 修改学生可安排时间
     *
     * @param tStudentSchedule 学生可安排时间
     * @return 结果
     */
    public int updateTStudentSchedule(TStudentSchedule tStudentSchedule);

    /**
     * 删除学生可安排时间
     *
     * @param id 学生可安排时间主键
     * @return 结果
     */
    public int deleteTStudentScheduleById(Long id);

    /**
     * 批量删除学生可安排时间
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTStudentScheduleByIds(Long[] ids);
}
