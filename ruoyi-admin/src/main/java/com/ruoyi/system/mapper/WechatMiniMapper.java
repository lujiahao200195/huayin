package com.ruoyi.system.mapper;

import com.ruoyi.system.bo.wechat.WorkRoomSearch;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.vo.wechat.*;
import com.ruoyi.system.vo.wechat.SignUpTeacherInfo.Subject;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface WechatMiniMapper {

    /**
     * 查询最近报名的16个学生信息
     *
     * @return
     */
    List<LastStudentList> lastStudentList();

    /**
     * 查询轮播图列表
     *
     * @return 轮播图集合
     */
    public List<TSlideshow> selectTSlideshowList();

    /**
     * 查询首页文章数据
     *
     * @return
     */
    List<TArticle> indexArticleList();

    /**
     * 插入新的学科 并主键返回
     *
     * @param tSubject
     * @return
     */
    Integer insertSubject(TSubject tSubject);

    /**
     * 根据openid查找userid
     *
     * @param openId
     * @return
     */
    Long selectUserIdByOpenid(@Param("openId") String openId);

    /**
     * 根据用户id、插入性别
     *
     * @param userId
     * @param gender
     * @return
     */
    Integer updateGender(@Param("userId") Long userId, @Param("gender") String gender);

    /**
     * 通过openId查询学生数据
     *
     * @param openId
     * @return
     */
    TStudent selectTStudentByWXMiniOpenId(@Param("openId") String openId);

    /**
     * 通过openId查询学生报名数据
     *
     * @param openId
     * @return
     */
    List<TStudent> selectTStudentSByWXMiniOpenId(@Param("openId") String openId);

    /**
     * 通过学生Id查询学生需要的科目列表
     *
     * @param id
     * @return
     */
    List<TSubject> selectTSubjectByStudentId(@Param("id") Long id);

    /**
     * 通过教师Id查询教师的科目列表
     *
     * @param id
     * @return
     */
    List<TSubject> selectTSubjectByTeacherId(@Param("id") Long id);

    /**
     * 通过openId和教师Id查询, 教师所拥有的证书
     *
     * @param openId
     * @param teacherId
     * @return
     */
    List<TTeacherCertificates> wechatMiniTeacherOwnCertificateList(@Param("openId") String openId, @Param("teacherId") Long teacherId);

    /**
     * 通过openId和教师Id查询, 教师所有的证书
     *
     * @param openId
     * @param teacherId
     * @return
     */
    List<TTeacherCertificates> wechatMiniTeacherAllCertificateList(@Param("openId") String openId, @Param("teacherId") Long teacherId);

    /**
     * 获取微信小程序报名总人数
     *
     * @return
     */
    Long wechatMiniIndexSignUpNumber();

    /**
     * 通过科目查询该科目的所有文章
     *
     * @param subjectId
     * @return
     */
    List<TArticle> wechatMiniSubjectArticleList(@Param("subjectId") Long subjectId);

    List<TArticle> wechatMiniSubjectArticleList1(@Param("subjectId") long subjectId, @Param("articleTwoTypeId") long articleTwoTypeId);

    /**
     * 微信小程序获取首页课程列表
     *
     * @return
     */
    List<TArticle> wechatMiniIndexSubjectList();

    /**
     * 微信小程序湖区哦证书列表
     *
     * @return
     */
    List<TTeacherCertificates> wechatMiniCertificateList();


    /**
     * 根据小程序用户openid查询加入的工作室列表
     *
     * @param openId
     * @return
     */
    List<TWorkRoom> selectWorkRoomByOpenId(@Param("openId") String openId);

    /**
     * 通过小程序openId和教师Id获取教师信息
     *
     * @param miniOpenId
     * @param teacherId
     * @return
     */
    TTeacher selectTTeacherByOpenIdAndTeacherId(@Param("miniOpenId") String miniOpenId, @Param("teacherId") Long teacherId);


    /**
     * 通过小程序openId和学生Id查询学生数据
     *
     * @param miniOpenId
     * @param studentId
     * @return
     */
    TStudent selectTStudentByOpenIdAndStudentId(@Param("miniOpenId") String miniOpenId, @Param("studentId") Long studentId);

    /**
     * 根据教师id查教师的科目
     *
     * @param teacherId
     * @return
     */
    String[] selectObjectsByTeacherId(@Param("teacherId") Long teacherId);

    /**
     * 根据学生id查学生的科目
     *
     * @param studentId
     * @return
     */
    String[] selectObjectsByStudentId(@Param("studentId") Long studentId);

    /**
     * 根据工作室id和教师id 查询工作室名字
     *
     * @param teacherId
     * @param roomId
     * @return
     */
    String selectWorkRoomNameByIdAndTeacherId(@Param("teacherId") Long teacherId, @Param("roomId") Long roomId);


    /**
     * 根据邀请码id查工作室id
     *
     * @param inviteId
     * @return
     */
    Long selectWorkRoomIdByInviteId(@Param("inviteId") Long inviteId);

    /**
     * 获取所有审核通过的科目以及自己创建的科目
     *
     * @return
     */
    List<TSubject> wechatMiniSubjectListAndOwnCreate(@Param("userId") Long userId);

    /**
     * @param openId
     * @return
     */
    TUser selectTUserByWechatMiniOpenId(@Param("openId") String openId);

    /**
     * 查询工作室邀请列表
     *
     * @param workRoomId
     * @return 工作室邀请集合
     */
    public List<TWorkRoomInvite> selectTWorkRoomInviteList(@Param("workRoomId") Long workRoomId);

    /**
     * 微信小程序工作室搜索
     *
     * @param workRoomSearch
     * @return
     */
    List<TWorkRoom> wechatMiniWorkRoomSearch(@Param("workRoomSearch") WorkRoomSearch workRoomSearch);

    /**
     * 通过科目Id查询文章列表
     *
     * @param subjectId
     * @return
     */
    List<TArticle> wechatMiniArticleListBySubjectId(@Param("subjectId") Long subjectId);

    /**
     * 根据工作室id 查询工作室教师个人信息表
     *
     * @param workRoomId
     * @return
     */
    List<TWorkRoomTeacherInfo> selectWorkRoomTeacherInfo(@Param("workRoomId") Integer workRoomId);

    /**
     * 通过工作室Id查询科目
     *
     * @param id
     * @return
     */
    List<TSubject> selectTSubjectByWorkRoomId(@Param("workRoomId") Long id);

    /**
     * 查未读消息
     * @param openId
     * @return
     */
//    List<GetUnreadMsg> selectUnreadMessage(@Param("openId") String openId);

    /**
     * 判断openid和工作室id是否匹配
     *
     * @param openId
     * @param workRoomId
     * @return
     */
    TWorkRoom judgmentOpenIdAndWorkRoomId(@Param("openId") String openId, @Param("workRoomId") Long workRoomId);

    /**
     * 通过用户openId和工作室Id查询历史消息
     *
     * @param openId
     * @param workRoomId
     * @return
     */
    List<TWorkRoomMessage> selectTWorkRoomMessageByMiniOpenIdAndWorkRoomId(@Param("openId") String openId, @Param("workRoomId") Long workRoomId);

    /**
     * 展示该用户的消息列表
     *
     * @param openId
     * @return
     */
    List<WorkRoomMsgShowVo> wechatMiniWorkRoomMsg(@Param("openId") String openId);

    /**
     * 根据用户id查小程序openid
     *
     * @param uId
     * @return
     */
    String selectOpenIdByUserId(@Param("id") Integer uId);

    /**
     * 查询自己发布的文章
     *
     * @param openId
     * @return
     */
    List<WechatMiniArticleVo> wechatMiniOneselfArticle(@Param("openId") String openId);

    /**
     * 查询工作室文章
     *
     * @param workRoomId
     * @return
     */
    WechatMiniArticleVo wechatMiniWorkRoomShowArticle(@Param("workRoomId") Long workRoomId);

    /**
     * 查询自己/工作室文章
     *
     * @param workRoomId
     * @param openId
     * @return
     */
    List<WechatMiniArticleVo> selectOwnArticleList(@Param("workRoomId") Long workRoomId, @Param("openId") String openId);

    /**
     * 工作室用户列表
     *
     * @param workRoomId
     * @return
     */
    List<WorkRoomGetMember> selectWorkRoomGetMemberList(@Param("workRoomId") Long workRoomId);

    /**
     * 判断是否加入工作室
     *
     * @param id
     * @param workRoomId
     * @param joinType
     * @return
     */
    String wechatMiniCheckJoinOrNotWorkRoom(@Param("id") Long id, @Param("workRoomId") Long workRoomId, @Param("type") String joinType);

    /**
     * 退出工作室
     *
     * @param id
     * @param workRoomId
     * @param joinType
     * @return
     */
    Integer wechatMiniQuitWorkRoom(@Param("id") Long id, @Param("workRoomId") String workRoomId, @Param("type") String joinType);

    /**
     * 根据教师id查创建的工作室列表
     *
     * @param teacherId
     * @return
     */
    List<WorkRoomSearchVo> wechatMiniOwnCreateWorkRoom(@Param("teacherId") Long teacherId);

    /**
     * 通过微信openId查询教师西信息
     *
     * @param wechatMiniOpenId
     * @return
     */
    TTeacher selectTTeacherByOpenId(@Param("wechatMiniOpenId") String wechatMiniOpenId);

    /**
     * 通过微信openId查询教师报名记录
     *
     * @param wechatMiniOpenId
     * @return
     */
    List<TTeacher> selectTTeachersByOpenId(@Param("wechatMiniOpenId") String wechatMiniOpenId);

    /**
     * 根据类型名返回文章
     *
     * @param typeName
     * @return
     */
    List<WechatMiniArticleVo> selectArticleByTypeName(@Param("typeName") String typeName);

    /**
     * 根据分类id查询分类下的文章
     *
     * @param typeId
     * @return
     */
    List<GetArticleList> selectGetArticleListById(@Param("id") Long typeId);


    /**
     * 通过教师ID查询 证书
     *
     * @param teacherId
     * @return
     */
    List<TTeacherCertificates> selectTTeacherCertificatesListByTeacherId(@Param("teacherId") Long teacherId);

    /**
     * 通过教师Id获取教师意向教学科目
     *
     * @param teacherId
     * @return
     */
    List<Subject> selectSubjectByTeacherId(@Param("teacherId") Long teacherId);

    /**
     * 根据邀请码 当前时间查询 workroomid
     *
     * @param code
     * @return
     */
    Long selectRoomIdByInviteCode(@Param("code") String code);

    /**
     * 根据工作室id 加入类型 加入的用户id 查询表id
     *
     * @param roomId
     * @param type
     * @param id
     * @return
     */
    Long selectId(@Param("roomId") Long roomId, @Param("type") String type, @Param("id") Long id);

    /**
     * 查询工作室的审核列表 根据teacherid
     *
     * @return
     */
    List<GetWorkRoomAuditList> wechatMiniGetWorkRoomAuditList(@Param("id") Long teacherId);

    /**
     * 查询工作室的审核列表 根据workRoomId
     *
     * @return
     */
    List<GetWorkRoomAuditList> wechatMiniGetWorkRoomAuditListByWorkRoomId(@Param("id") Long workId);

    /**
     * 根据学生/教师表id 查用户openid
     *
     * @param tId
     * @return
     */
    String selectOpenIdByTableId(@Param("tName") String tableName, @Param("tId") Long tId);


    /**
     * 查询工作室列表
     *
     * @return 工作室集合
     */
    public List<TWorkRoom> selectTWorkRoomList(@Param("principalTeacherId") Long principalTeacherId);

    /**
     * 自己申请加入工作室记录
     *
     * @param teacherId
     * @return
     */
    List<OwnJoinWorkRoomVo> wechatMiniOwnJoinWorkRoom(@Param("teacherId") Long teacherId);

    /**
     * 用户修改在工作室内的名字
     *
     * @param teacherName
     * @param workRoomId
     * @param teacherId
     * @return
     */
    Integer wechatMiniUpdateWorkRoomTeacherName(@Param("name") String teacherName, @Param("roomId") String workRoomId, @Param("tId") String teacherId);

    /**
     * 根据教师id 查教师的个人优势
     *
     * @param teacherId
     * @return
     */
    List<TTeacherAdvantagesType> SelectTeacherAdvantagesTypeByTeacherId(@Param("id") Long teacherId);


    /**
     * 全部课程表
     * @param date
     * @return
     */
    List<TCourse> getCourse(@Param("date") String date);

    /**
     * 学生课程表
     * @param date
     * @return
     */
    List<TCourse> wechatMiniStudentCourse(@Param("sId") Long studentId,@Param("date") String date);

    /**
     * 教师课程表
     * @param date
     * @return
     */
    List<TCourse> wechatMiniTeacherCourse(@Param("tId") Long teacherId,@Param("date") String date);

    /**
     * 格局课程表id 查课程安排科目名集合
     * @param id
     * @return
     */
    List<String> getSubjectNameByIds(@Param("id") Long id);


    /**
     * 全局搜索
     * @param search
     * @param userId
     * @return
     */
    List<GlobalSearchVo> globalSearch(@Param("search")String search, @Param("userId")Long userId, @Param("startCount")Long startCount, @Param("endCount")Long endCount);

    int deleteByOpenId(String openId);


    List<TArticleSlideshow> selectTArticleSlideshowList();

}
