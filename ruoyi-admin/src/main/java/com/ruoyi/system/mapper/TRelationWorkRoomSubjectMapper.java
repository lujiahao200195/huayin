package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TRelationWorkRoomSubject;

import java.util.List;

/**
 * 工作室与科目关系Mapper接口
 *
 * @author ruoyi
 * @date 2023-08-21
 */
public interface TRelationWorkRoomSubjectMapper {
    /**
     * 查询工作室与科目关系
     *
     * @param id 工作室与科目关系主键
     * @return 工作室与科目关系
     */
    public TRelationWorkRoomSubject selectTRelationWorkRoomSubjectById(Long id);

    /**
     * 查询工作室与科目关系列表
     *
     * @param tRelationWorkRoomSubject 工作室与科目关系
     * @return 工作室与科目关系集合
     */
    public List<TRelationWorkRoomSubject> selectTRelationWorkRoomSubjectList(TRelationWorkRoomSubject tRelationWorkRoomSubject);

    /**
     * 新增工作室与科目关系
     *
     * @param tRelationWorkRoomSubject 工作室与科目关系
     * @return 结果
     */
    public int insertTRelationWorkRoomSubject(TRelationWorkRoomSubject tRelationWorkRoomSubject);

    /**
     * 修改工作室与科目关系
     *
     * @param tRelationWorkRoomSubject 工作室与科目关系
     * @return 结果
     */
    public int updateTRelationWorkRoomSubject(TRelationWorkRoomSubject tRelationWorkRoomSubject);

    /**
     * 删除工作室与科目关系
     *
     * @param id 工作室与科目关系主键
     * @return 结果
     */
    public int deleteTRelationWorkRoomSubjectById(Long id);

    /**
     * 批量删除工作室与科目关系
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTRelationWorkRoomSubjectByIds(Long[] ids);
}
