package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TBbsBoardMember;

import java.util.List;

/**
 * 版块成员Mapper接口
 *
 * @author ruoyi
 * @date 2023-08-28
 */
public interface TBbsBoardMemberMapper {
    /**
     * 查询版块成员
     *
     * @param id 版块成员主键
     * @return 版块成员
     */
    public TBbsBoardMember selectTBbsBoardMemberById(Long id);

    /**
     * 查询版块成员列表
     *
     * @param tBbsBoardMember 版块成员
     * @return 版块成员集合
     */
    public List<TBbsBoardMember> selectTBbsBoardMemberList(TBbsBoardMember tBbsBoardMember);

    /**
     * 新增版块成员
     *
     * @param tBbsBoardMember 版块成员
     * @return 结果
     */
    public int insertTBbsBoardMember(TBbsBoardMember tBbsBoardMember);

    /**
     * 修改版块成员
     *
     * @param tBbsBoardMember 版块成员
     * @return 结果
     */
    public int updateTBbsBoardMember(TBbsBoardMember tBbsBoardMember);

    /**
     * 删除版块成员
     *
     * @param id 版块成员主键
     * @return 结果
     */
    public int deleteTBbsBoardMemberById(Long id);

    /**
     * 批量删除版块成员
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTBbsBoardMemberByIds(Long[] ids);
}
