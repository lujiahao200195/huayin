package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TRelationWorkRoomUser;

import java.util.List;

/**
 * 工作室用户关系Mapper接口
 *
 * @author ruoyi
 * @date 2023-08-30
 */
public interface TRelationWorkRoomUserMapper {
    /**
     * 查询工作室用户关系
     *
     * @param id 工作室用户关系主键
     * @return 工作室用户关系
     */
    public TRelationWorkRoomUser selectTRelationWorkRoomUserById(Long id);

    /**
     * 查询工作室用户关系列表
     *
     * @param tRelationWorkRoomUser 工作室用户关系
     * @return 工作室用户关系集合
     */
    public List<TRelationWorkRoomUser> selectTRelationWorkRoomUserList(TRelationWorkRoomUser tRelationWorkRoomUser);

    /**
     * 新增工作室用户关系
     *
     * @param tRelationWorkRoomUser 工作室用户关系
     * @return 结果
     */
    public int insertTRelationWorkRoomUser(TRelationWorkRoomUser tRelationWorkRoomUser);

    /**
     * 修改工作室用户关系
     *
     * @param tRelationWorkRoomUser 工作室用户关系
     * @return 结果
     */
    public int updateTRelationWorkRoomUser(TRelationWorkRoomUser tRelationWorkRoomUser);

    /**
     * 删除工作室用户关系
     *
     * @param id 工作室用户关系主键
     * @return 结果
     */
    public int deleteTRelationWorkRoomUserById(Long id);

    /**
     * 批量删除工作室用户关系
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTRelationWorkRoomUserByIds(Long[] ids);
}
