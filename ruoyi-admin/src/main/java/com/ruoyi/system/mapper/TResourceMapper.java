package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.TResource;

/**
 * 资源Mapper接口
 * 
 * @author ruoyi
 * @date 2023-09-21
 */
public interface TResourceMapper 
{
    /**
     * 查询资源
     * 
     * @param id 资源主键
     * @return 资源
     */
    public TResource selectTResourceById(Long id);

    /**
     * 查询资源列表
     * 
     * @param tResource 资源
     * @return 资源集合
     */
    public List<TResource> selectTResourceList(TResource tResource);

    /**
     * 新增资源
     * 
     * @param tResource 资源
     * @return 结果
     */
    public int insertTResource(TResource tResource);

    /**
     * 修改资源
     * 
     * @param tResource 资源
     * @return 结果
     */
    public int updateTResource(TResource tResource);

    /**
     * 删除资源
     * 
     * @param id 资源主键
     * @return 结果
     */
    public int deleteTResourceById(Long id);

    /**
     * 批量删除资源
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTResourceByIds(Long[] ids);
}
