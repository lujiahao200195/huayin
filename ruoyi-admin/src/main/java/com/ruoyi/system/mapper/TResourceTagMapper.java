package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TResourceTag;

import java.util.List;

/**
 * 资源标签Mapper接口
 *
 * @author ruoyi
 * @date 2023-09-08
 */
public interface TResourceTagMapper {
    /**
     * 查询资源标签
     *
     * @param id 资源标签主键
     * @return 资源标签
     */
    public TResourceTag selectTResourceTagById(Long id);

    /**
     * 查询资源标签列表
     *
     * @param tResourceTag 资源标签
     * @return 资源标签集合
     */
    public List<TResourceTag> selectTResourceTagList(TResourceTag tResourceTag);

    /**
     * 新增资源标签
     *
     * @param tResourceTag 资源标签
     * @return 结果
     */
    public int insertTResourceTag(TResourceTag tResourceTag);

    /**
     * 修改资源标签
     *
     * @param tResourceTag 资源标签
     * @return 结果
     */
    public int updateTResourceTag(TResourceTag tResourceTag);

    /**
     * 删除资源标签
     *
     * @param id 资源标签主键
     * @return 结果
     */
    public int deleteTResourceTagById(Long id);

    /**
     * 批量删除资源标签
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTResourceTagByIds(Long[] ids);
}
