package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TWorkRoomNotice;

import java.util.List;

/**
 * 工作室公告Mapper接口
 *
 * @author ruoyi
 * @date 2023-08-18
 */
public interface TWorkRoomNoticeMapper {
    /**
     * 查询工作室公告
     *
     * @param id 工作室公告主键
     * @return 工作室公告
     */
    public TWorkRoomNotice selectTWorkRoomNoticeById(Long id);

    /**
     * 查询工作室公告列表
     *
     * @param tWorkRoomNotice 工作室公告
     * @return 工作室公告集合
     */
    public List<TWorkRoomNotice> selectTWorkRoomNoticeList(TWorkRoomNotice tWorkRoomNotice);

    /**
     * 新增工作室公告
     *
     * @param tWorkRoomNotice 工作室公告
     * @return 结果
     */
    public int insertTWorkRoomNotice(TWorkRoomNotice tWorkRoomNotice);

    /**
     * 修改工作室公告
     *
     * @param tWorkRoomNotice 工作室公告
     * @return 结果
     */
    public int updateTWorkRoomNotice(TWorkRoomNotice tWorkRoomNotice);

    /**
     * 删除工作室公告
     *
     * @param id 工作室公告主键
     * @return 结果
     */
    public int deleteTWorkRoomNoticeById(Long id);

    /**
     * 批量删除工作室公告
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTWorkRoomNoticeByIds(Long[] ids);
}
