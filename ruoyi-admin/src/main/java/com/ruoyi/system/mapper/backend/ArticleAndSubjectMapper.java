package com.ruoyi.system.mapper.backend;

import com.ruoyi.system.domain.TArticle;
import com.ruoyi.system.vo.wechat.backend.ArticleAndSubjectVo;

import java.util.List;

public interface ArticleAndSubjectMapper {

    /*文章与科目关联查询*/
    public List<ArticleAndSubjectVo> queryArticleAndSubject(ArticleAndSubjectVo articleAndSubjectVo);

    /*文章添加*/
    public int insertTArticle(ArticleAndSubjectVo articleAndSubjectVo);


    /*查询文章根据ID*/
    public ArticleAndSubjectVo selectArticleById(Long id);


    /*删除文章*/
    public int deleteArticle(Long[] ids);

}
