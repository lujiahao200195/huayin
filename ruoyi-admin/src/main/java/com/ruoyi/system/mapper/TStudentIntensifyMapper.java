package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TStudentIntensify;

import java.util.List;

/**
 * 学生强化科目Mapper接口
 *
 * @author ruoyi
 * @date 2023-08-30
 */
public interface TStudentIntensifyMapper {
    /**
     * 查询学生强化科目
     *
     * @param id 学生强化科目主键
     * @return 学生强化科目
     */
    public TStudentIntensify selectTStudentIntensifyById(Long id);

    /**
     * 查询学生强化科目列表
     *
     * @param tStudentIntensify 学生强化科目
     * @return 学生强化科目集合
     */
    public List<TStudentIntensify> selectTStudentIntensifyList(TStudentIntensify tStudentIntensify);

    /**
     * 新增学生强化科目
     *
     * @param tStudentIntensify 学生强化科目
     * @return 结果
     */
    public int insertTStudentIntensify(TStudentIntensify tStudentIntensify);

    /**
     * 修改学生强化科目
     *
     * @param tStudentIntensify 学生强化科目
     * @return 结果
     */
    public int updateTStudentIntensify(TStudentIntensify tStudentIntensify);

    /**
     * 删除学生强化科目
     *
     * @param id 学生强化科目主键
     * @return 结果
     */
    public int deleteTStudentIntensifyById(Long id);

    /**
     * 批量删除学生强化科目
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTStudentIntensifyByIds(Long[] ids);
}
