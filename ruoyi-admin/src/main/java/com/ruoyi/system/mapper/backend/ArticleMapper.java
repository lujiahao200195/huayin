package com.ruoyi.system.mapper.backend;

import com.ruoyi.system.domain.TArticle;
import com.ruoyi.system.vo.wechat.backend.ExportSchoolArticle;
import org.apache.ibatis.annotations.Param;


import java.util.List;

/**
 * 文章Mapper接口
 */
public interface ArticleMapper {
    /**
     * 查询文章
     *
     * @param id 文章主键
     * @return 文章
     */
    public TArticle selectTArticleById(Long id);

    /**
     * 查询文章列表
     *
     * @param tArticle 文章
     * @return 文章集合
     */
    public List<TArticle> selectTArticleList(TArticle tArticle);



    /**
     * 新增文章
     *
     * @param tArticle 文章
     * @return 结果
     */
    public int insertTArticle(TArticle tArticle);

    /**
     * 修改文章
     *
     * @param tArticle 文章
     * @return 结果
     */
    public int updateTArticle(TArticle tArticle);

    /**
     * 删除文章
     *
     * @param id 文章主键
     * @return 结果
     */
    public int deleteTArticleById(Long id);

    /**
     * 批量删除文章
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTArticleByIds(Long[] ids);

    /**
     * 根据类型名称查对应的文章
     *
     * @return
     */
    List<TArticle> getXXArticle(@Param("article") TArticle article, @Param("tName") String typeNamem);

    /**
     * 导出学校文章
     * @return
     */
    List<ExportSchoolArticle> exportSchoolArticle(@Param("ids") Long[] ids);

    /**
     * 添加文章与类型的关系表
     *
     * @param articleId
     * @param typeName
     * @return
     */
    int insertArticleTypeRelation(@Param("aId") Long articleId, @Param("tName") String typeName);

    /**
     * 删除文章与类型的关系表
     *
     * @param ids
     * @param typeName
     * @return
     */
    int deleteArticleTypeRelation(@Param("ids") Long[] ids, @Param("tName") String typeName);

}
