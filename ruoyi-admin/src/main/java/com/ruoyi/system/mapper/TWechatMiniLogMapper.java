package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TWechatMiniLog;

import java.util.List;

/**
 * 微信小程序日志Mapper接口
 *
 * @author ruoyi
 * @date 2023-08-18
 */
public interface TWechatMiniLogMapper {
    /**
     * 查询微信小程序日志
     *
     * @param id 微信小程序日志主键
     * @return 微信小程序日志
     */
    public TWechatMiniLog selectTWechatMiniLogById(Long id);

    /**
     * 查询微信小程序日志列表
     *
     * @param tWechatMiniLog 微信小程序日志
     * @return 微信小程序日志集合
     */
    public List<TWechatMiniLog> selectTWechatMiniLogList(TWechatMiniLog tWechatMiniLog);

    /**
     * 新增微信小程序日志
     *
     * @param tWechatMiniLog 微信小程序日志
     * @return 结果
     */
    public int insertTWechatMiniLog(TWechatMiniLog tWechatMiniLog);

    /**
     * 修改微信小程序日志
     *
     * @param tWechatMiniLog 微信小程序日志
     * @return 结果
     */
    public int updateTWechatMiniLog(TWechatMiniLog tWechatMiniLog);

    /**
     * 删除微信小程序日志
     *
     * @param id 微信小程序日志主键
     * @return 结果
     */
    public int deleteTWechatMiniLogById(Long id);

    /**
     * 批量删除微信小程序日志
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTWechatMiniLogByIds(Long[] ids);
}
