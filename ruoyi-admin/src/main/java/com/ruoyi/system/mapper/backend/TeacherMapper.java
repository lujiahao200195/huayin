package com.ruoyi.system.mapper.backend;

import com.ruoyi.system.domain.TTeacher;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 教师Mapper接口
 */
public interface TeacherMapper {
    /**
     * 查询教师
     *
     * @param id 教师主键
     * @return 教师
     */
    public TTeacher selectTTeacherById(Long id);

    /**
     * 查询教师列表
     *
     * @param tTeacher 教师
     * @return 教师集合
     */
    public List<TTeacher> selectTTeacherList(TTeacher tTeacher);

    /**
     * 新增教师
     *
     * @param tTeacher 教师
     * @return 结果
     */
    public int insertTTeacher(TTeacher tTeacher);

    /**
     * 修改教师
     *
     * @param tTeacher 教师
     * @return 结果
     */
    public int updateTTeacher(TTeacher tTeacher);

    /**
     * 删除教师
     *
     * @param id 教师主键
     * @return 结果
     */
    public int deleteTTeacherById(Long id);

    /**
     * 批量删除教师
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTTeacherByIds(Long[] ids);

    /**
     * 判断是否存在该id的数据
     *
     * @return
     */
    Integer count(@Param("tbName") String tableName, @Param("id") Integer id);

    /**
     * 根据教师表Id修改教师审核状态码
     *
     * @return
     */
    Integer updateTeacherStateById(@Param("id") Integer id, @Param("state") String state, @Param("stateResult") String stateResult);

    /**
     * 根据教师表id查教师姓名
     *
     * @param id
     * @return
     */
    String selectTeacherNameById(@Param("id") Integer id);

    /**
     * 根据教师id查教师消除openid
     *
     * @param id
     * @return
     */
    String selectTeacherOpenIdById(@Param("id") Integer id);

}
