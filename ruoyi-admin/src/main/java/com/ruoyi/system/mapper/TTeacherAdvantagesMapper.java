package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TTeacherAdvantages;

import java.util.List;

/**
 * 教师个人优势Mapper接口
 *
 * @author ruoyi
 * @date 2023-08-16
 */
public interface TTeacherAdvantagesMapper {
    /**
     * 查询教师个人优势
     *
     * @param id 教师个人优势主键
     * @return 教师个人优势
     */
    public TTeacherAdvantages selectTTeacherAdvantagesById(Long id);

    /**
     * 查询教师个人优势列表
     *
     * @param tTeacherAdvantages 教师个人优势
     * @return 教师个人优势集合
     */
    public List<TTeacherAdvantages> selectTTeacherAdvantagesList(TTeacherAdvantages tTeacherAdvantages);

    /**
     * 新增教师个人优势
     *
     * @param tTeacherAdvantages 教师个人优势
     * @return 结果
     */
    public int insertTTeacherAdvantages(TTeacherAdvantages tTeacherAdvantages);

    /**
     * 修改教师个人优势
     *
     * @param tTeacherAdvantages 教师个人优势
     * @return 结果
     */
    public int updateTTeacherAdvantages(TTeacherAdvantages tTeacherAdvantages);

    /**
     * 删除教师个人优势
     *
     * @param id 教师个人优势主键
     * @return 结果
     */
    public int deleteTTeacherAdvantagesById(Long id);

    /**
     * 批量删除教师个人优势
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTTeacherAdvantagesByIds(Long[] ids);

    /**
     * 根据课程id 查课程名
     *
     * @param sId
     * @return
     */
    String selectSubjectById(Long sId);

}
