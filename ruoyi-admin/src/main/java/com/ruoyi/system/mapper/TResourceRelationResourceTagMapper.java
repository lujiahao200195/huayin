package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TResourceRelationResourceTag;

import java.util.List;

/**
 * 资源标签关系Mapper接口
 *
 * @author ruoyi
 * @date 2023-09-08
 */
public interface TResourceRelationResourceTagMapper {
    /**
     * 查询资源标签关系
     *
     * @param id 资源标签关系主键
     * @return 资源标签关系
     */
    public TResourceRelationResourceTag selectTResourceRelationResourceTagById(Long id);

    /**
     * 查询资源标签关系列表
     *
     * @param tResourceRelationResourceTag 资源标签关系
     * @return 资源标签关系集合
     */
    public List<TResourceRelationResourceTag> selectTResourceRelationResourceTagList(TResourceRelationResourceTag tResourceRelationResourceTag);

    /**
     * 新增资源标签关系
     *
     * @param tResourceRelationResourceTag 资源标签关系
     * @return 结果
     */
    public int insertTResourceRelationResourceTag(TResourceRelationResourceTag tResourceRelationResourceTag);

    /**
     * 修改资源标签关系
     *
     * @param tResourceRelationResourceTag 资源标签关系
     * @return 结果
     */
    public int updateTResourceRelationResourceTag(TResourceRelationResourceTag tResourceRelationResourceTag);

    /**
     * 删除资源标签关系
     *
     * @param id 资源标签关系主键
     * @return 结果
     */
    public int deleteTResourceRelationResourceTagById(Long id);

    /**
     * 批量删除资源标签关系
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTResourceRelationResourceTagByIds(Long[] ids);
}
