package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TBbsNotice;

import java.util.List;

/**
 * 版块公告Mapper接口
 *
 * @author ruoyi
 * @date 2023-08-28
 */
public interface TBbsNoticeMapper {
    /**
     * 查询版块公告
     *
     * @param id 版块公告主键
     * @return 版块公告
     */
    public TBbsNotice selectTBbsNoticeById(Long id);

    /**
     * 查询版块公告列表
     *
     * @param tBbsNotice 版块公告
     * @return 版块公告集合
     */
    public List<TBbsNotice> selectTBbsNoticeList(TBbsNotice tBbsNotice);

    /**
     * 新增版块公告
     *
     * @param tBbsNotice 版块公告
     * @return 结果
     */
    public int insertTBbsNotice(TBbsNotice tBbsNotice);

    /**
     * 修改版块公告
     *
     * @param tBbsNotice 版块公告
     * @return 结果
     */
    public int updateTBbsNotice(TBbsNotice tBbsNotice);

    /**
     * 删除版块公告
     *
     * @param id 版块公告主键
     * @return 结果
     */
    public int deleteTBbsNoticeById(Long id);

    /**
     * 批量删除版块公告
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTBbsNoticeByIds(Long[] ids);
}
