package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TTeacherAdvantagesType;

import java.util.List;

/**
 * 教师个人优势分类Mapper接口
 *
 * @author ruoyi
 * @date 2023-08-15
 */
public interface TTeacherAdvantagesTypeMapper {
    /**
     * 查询教师个人优势分类
     *
     * @param id 教师个人优势分类主键
     * @return 教师个人优势分类
     */
    public TTeacherAdvantagesType selectTTeacherAdvantagesTypeById(Long id);

    /**
     * 查询教师个人优势分类列表
     *
     * @param tTeacherAdvantagesType 教师个人优势分类
     * @return 教师个人优势分类集合
     */
    public List<TTeacherAdvantagesType> selectTTeacherAdvantagesTypeList(TTeacherAdvantagesType tTeacherAdvantagesType);

    /**
     * 新增教师个人优势分类
     *
     * @param tTeacherAdvantagesType 教师个人优势分类
     * @return 结果
     */
    public int insertTTeacherAdvantagesType(TTeacherAdvantagesType tTeacherAdvantagesType);

    /**
     * 修改教师个人优势分类
     *
     * @param tTeacherAdvantagesType 教师个人优势分类
     * @return 结果
     */
    public int updateTTeacherAdvantagesType(TTeacherAdvantagesType tTeacherAdvantagesType);

    /**
     * 删除教师个人优势分类
     *
     * @param id 教师个人优势分类主键
     * @return 结果
     */
    public int deleteTTeacherAdvantagesTypeById(Long id);

    /**
     * 批量删除教师个人优势分类
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTTeacherAdvantagesTypeByIds(Long[] ids);
}
