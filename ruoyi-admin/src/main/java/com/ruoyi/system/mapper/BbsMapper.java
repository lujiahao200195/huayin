package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TBbsBoard;
import com.ruoyi.system.domain.TBbsPost;
import com.ruoyi.system.domain.TBbsRelationReply;
import com.ruoyi.system.domain.TBbsUser;
import com.ruoyi.system.vo.wechat.bbs.*;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface BbsMapper {

    /**
     * 查询最新工作室类型
     *
     * @return
     */
    List<BbsIndexPostVo> selectTBbsPostByLast(@Param("pageIndex") Long pageIndex, @Param("pageSize") Long pageSize);

    /**
     * 通过文章id获取文章评论数
     *
     * @param postId
     * @return
     */
    Long selectCountByPostId(@Param("postId") Long postId);

    /**
     * 通过版块Id获取置顶文章Id列表
     *
     * @param boardId
     * @return
     */
    List<Long> selectTopPostIdListByBoardId(@Param("boardId") Long boardId);

    /**
     * 通过userId查询论坛BbsUser数据
     *
     * @param userId
     * @return
     */
    TBbsUser selectTBbsUserByUserId(@Param("userId") Long userId);

    /**
     * 通过文章的评论Id查询回复
     *
     * @param replyId
     * @return
     */
    List<ChildReplyVo> selectReplyVoListByReplyId(@Param("replyId") Long replyId);

    /**
     * 通过版块Id查询版块信息
     *
     * @param boardId
     * @return
     */
    BbsBoardInfo selectBbsBoardInfoByBoardId(@Param("boardId") Long boardId);

    /**
     * 通过版块Id获取文章列表
     *
     * @param boardId
     * @return
     */
    List<BbsIndexPostVo> selectTBbsPostByBoardId(@Param("boardId") Long boardId);

    /**
     * 通过版块Id获取版块推荐文章
     *
     * @param boardId
     */
    List<BbsIndexPostVo> selectBoardSuggestByBoardId(@Param("boardId") Long boardId);

    /**
     * 通过用户Id和版块Id查询规则文章
     *
     * @param boardId
     * @param userId
     * @return
     */
    TBbsPost selectRuleTPostByBoardId(@Param("boardId") Long boardId, @Param("userId") Long userId);

    /**
     * 通过用户Id和版块Id查询版块信息
     *
     * @param boardId
     * @param userId
     * @return
     */
    TBbsBoard selectTBbsBoardByUserIdAndBoardId(@Param("boardId") Long boardId, @Param("userId") Long userId);

    /**
     * 查询版块列表
     *
     * @return
     */
    @MapKey("id")
    List<Map> selectBoardList();

    /**
     * 通过版块ID查询版块成员列表
     *
     * @param boardId
     * @return
     */
    List<BoardMemberVo> selectBoardMemberVoListByBoardId(@Param("boardId") Long boardId);

    /**
     * 通过板块Id查询
     *
     * @param boardId
     * @return
     */
    List<BoardMemberJoinVo> selectBoardMemberJoinVoListByBoardId(@Param("boardId") Long boardId);

    /**
     * 查询版块成员加入信息
     *
     * @param boardMemberId
     * @return
     */
    BoardMemberJoinVo selectBoardMemberJoinVoByBoardMemberIdAndUserId(@Param("userId") Long userId, @Param("boardMemberId") Long boardMemberId);

    /**
     * 通过文章Id获取回复列表
     *
     * @param postId
     * @return
     */
    List<TBbsRelationReply> selectTBbsRelationReplyListByPostIdAnd(@Param("postId") Long postId);
}
