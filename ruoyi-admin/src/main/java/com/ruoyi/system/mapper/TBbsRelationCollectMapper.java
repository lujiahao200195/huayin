package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TBbsRelationCollect;

import java.util.List;

/**
 * 收藏关系Mapper接口
 *
 * @author ruoyi
 * @date 2023-08-28
 */
public interface TBbsRelationCollectMapper {
    /**
     * 查询收藏关系
     *
     * @param id 收藏关系主键
     * @return 收藏关系
     */
    public TBbsRelationCollect selectTBbsRelationCollectById(Long id);

    /**
     * 查询收藏关系列表
     *
     * @param tBbsRelationCollect 收藏关系
     * @return 收藏关系集合
     */
    public List<TBbsRelationCollect> selectTBbsRelationCollectList(TBbsRelationCollect tBbsRelationCollect);

    /**
     * 新增收藏关系
     *
     * @param tBbsRelationCollect 收藏关系
     * @return 结果
     */
    public int insertTBbsRelationCollect(TBbsRelationCollect tBbsRelationCollect);

    /**
     * 修改收藏关系
     *
     * @param tBbsRelationCollect 收藏关系
     * @return 结果
     */
    public int updateTBbsRelationCollect(TBbsRelationCollect tBbsRelationCollect);

    /**
     * 删除收藏关系
     *
     * @param id 收藏关系主键
     * @return 结果
     */
    public int deleteTBbsRelationCollectById(Long id);

    /**
     * 批量删除收藏关系
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTBbsRelationCollectByIds(Long[] ids);
}
