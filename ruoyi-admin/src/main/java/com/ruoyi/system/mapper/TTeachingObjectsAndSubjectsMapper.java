package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TTeachingObjectsAndSubjects;

import java.util.List;

/**
 * 教师意向教学Mapper接口
 *
 * @author ruoyi
 * @date 2023-08-17
 */
public interface TTeachingObjectsAndSubjectsMapper {
    /**
     * 查询教师意向教学
     *
     * @param id 教师意向教学主键
     * @return 教师意向教学
     */
    public TTeachingObjectsAndSubjects selectTTeachingObjectsAndSubjectsById(Long id);

    /**
     * 查询教师意向教学列表
     *
     * @param tTeachingObjectsAndSubjects 教师意向教学
     * @return 教师意向教学集合
     */
    public List<TTeachingObjectsAndSubjects> selectTTeachingObjectsAndSubjectsList(TTeachingObjectsAndSubjects tTeachingObjectsAndSubjects);

    /**
     * 新增教师意向教学
     *
     * @param tTeachingObjectsAndSubjects 教师意向教学
     * @return 结果
     */
    public int insertTTeachingObjectsAndSubjects(TTeachingObjectsAndSubjects tTeachingObjectsAndSubjects);

    /**
     * 修改教师意向教学
     *
     * @param tTeachingObjectsAndSubjects 教师意向教学
     * @return 结果
     */
    public int updateTTeachingObjectsAndSubjects(TTeachingObjectsAndSubjects tTeachingObjectsAndSubjects);

    /**
     * 删除教师意向教学
     *
     * @param id 教师意向教学主键
     * @return 结果
     */
    public int deleteTTeachingObjectsAndSubjectsById(Long id);

    /**
     * 批量删除教师意向教学
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTTeachingObjectsAndSubjectsByIds(Long[] ids);
}
