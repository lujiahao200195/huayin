package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TTeacherCertificates;

import java.util.List;

/**
 * 教师证书Mapper接口
 *
 * @author ruoyi
 * @date 2023-08-16
 */
public interface TTeacherCertificatesMapper {
    /**
     * 查询教师证书
     *
     * @param id 教师证书主键
     * @return 教师证书
     */
    public TTeacherCertificates selectTTeacherCertificatesById(Long id);

    /**
     * 查询教师证书列表
     *
     * @param tTeacherCertificates 教师证书
     * @return 教师证书集合
     */
    public List<TTeacherCertificates> selectTTeacherCertificatesList(TTeacherCertificates tTeacherCertificates);

    /**
     * 新增教师证书
     *
     * @param tTeacherCertificates 教师证书
     * @return 结果
     */
    public int insertTTeacherCertificates(TTeacherCertificates tTeacherCertificates);

    /**
     * 修改教师证书
     *
     * @param tTeacherCertificates 教师证书
     * @return 结果
     */
    public int updateTTeacherCertificates(TTeacherCertificates tTeacherCertificates);

    /**
     * 删除教师证书
     *
     * @param id 教师证书主键
     * @return 结果
     */
    public int deleteTTeacherCertificatesById(Long id);

    /**
     * 批量删除教师证书
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTTeacherCertificatesByIds(Long[] ids);
}
