package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.TOption;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-19
 */
public interface TOptionMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public TOption selectTOptionById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param tOption 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<TOption> selectTOptionList(TOption tOption);

    /**
     * 新增【请填写功能名称】
     * 
     * @param tOption 【请填写功能名称】
     * @return 结果
     */
    public int insertTOption(TOption tOption);

    /**
     * 修改【请填写功能名称】
     * 
     * @param tOption 【请填写功能名称】
     * @return 结果
     */
    public int updateTOption(TOption tOption);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteTOptionById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTOptionByIds(Long[] ids);
}
