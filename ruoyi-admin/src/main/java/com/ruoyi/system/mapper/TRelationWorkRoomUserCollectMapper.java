package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TRelationWorkRoomUserCollect;

import java.util.List;

/**
 * 工作室用户关系-收藏用Mapper接口
 *
 * @author ruoyi
 * @date 2023-08-21
 */
public interface TRelationWorkRoomUserCollectMapper {
    /**
     * 查询工作室用户关系-收藏用
     *
     * @param id 工作室用户关系-收藏用主键
     * @return 工作室用户关系-收藏用
     */
    public TRelationWorkRoomUserCollect selectTRelationWorkRoomUserCollectById(Long id);

    /**
     * 查询工作室用户关系-收藏用列表
     *
     * @param tRelationWorkRoomUserCollect 工作室用户关系-收藏用
     * @return 工作室用户关系-收藏用集合
     */
    public List<TRelationWorkRoomUserCollect> selectTRelationWorkRoomUserCollectList(TRelationWorkRoomUserCollect tRelationWorkRoomUserCollect);

    /**
     * 新增工作室用户关系-收藏用
     *
     * @param tRelationWorkRoomUserCollect 工作室用户关系-收藏用
     * @return 结果
     */
    public int insertTRelationWorkRoomUserCollect(TRelationWorkRoomUserCollect tRelationWorkRoomUserCollect);

    /**
     * 修改工作室用户关系-收藏用
     *
     * @param tRelationWorkRoomUserCollect 工作室用户关系-收藏用
     * @return 结果
     */
    public int updateTRelationWorkRoomUserCollect(TRelationWorkRoomUserCollect tRelationWorkRoomUserCollect);

    /**
     * 删除工作室用户关系-收藏用
     *
     * @param id 工作室用户关系-收藏用主键
     * @return 结果
     */
    public int deleteTRelationWorkRoomUserCollectById(Long id);

    /**
     * 批量删除工作室用户关系-收藏用
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTRelationWorkRoomUserCollectByIds(Long[] ids);
}
