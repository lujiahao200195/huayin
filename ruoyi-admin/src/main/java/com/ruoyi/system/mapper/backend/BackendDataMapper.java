package com.ruoyi.system.mapper.backend;

import com.ruoyi.system.vo.wechat.backend.GetSignUpNumber;

import java.util.List;

public interface BackendDataMapper {
    /**
     * 获取总报名人数
     *
     * @return
     */
    List<GetSignUpNumber> getSignUpNumber();

    /**
     * 获取学生总报名人数
     *
     * @return
     */
    List<GetSignUpNumber> getStudentSignUpNumber();

    /**
     * 获取教师总报名人数
     *
     * @return
     */
    List<GetSignUpNumber> getTeacherSignUpNumber();
}
