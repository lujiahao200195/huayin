package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TWorkRoomTeacherInfo;

import java.util.List;

/**
 * 工作室教师信息卡Mapper接口
 *
 * @author ruoyi
 * @date 2023-08-18
 */
public interface TWorkRoomTeacherInfoMapper {
    /**
     * 查询工作室教师信息卡
     *
     * @param id 工作室教师信息卡主键
     * @return 工作室教师信息卡
     */
    public TWorkRoomTeacherInfo selectTWorkRoomTeacherInfoById(Long id);

    /**
     * 查询工作室教师信息卡列表
     *
     * @param tWorkRoomTeacherInfo 工作室教师信息卡
     * @return 工作室教师信息卡集合
     */
    public List<TWorkRoomTeacherInfo> selectTWorkRoomTeacherInfoList(TWorkRoomTeacherInfo tWorkRoomTeacherInfo);

    /**
     * 新增工作室教师信息卡
     *
     * @param tWorkRoomTeacherInfo 工作室教师信息卡
     * @return 结果
     */
    public int insertTWorkRoomTeacherInfo(TWorkRoomTeacherInfo tWorkRoomTeacherInfo);

    /**
     * 修改工作室教师信息卡
     *
     * @param tWorkRoomTeacherInfo 工作室教师信息卡
     * @return 结果
     */
    public int updateTWorkRoomTeacherInfo(TWorkRoomTeacherInfo tWorkRoomTeacherInfo);

    /**
     * 删除工作室教师信息卡
     *
     * @param id 工作室教师信息卡主键
     * @return 结果
     */
    public int deleteTWorkRoomTeacherInfoById(Long id);

    /**
     * 批量删除工作室教师信息卡
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTWorkRoomTeacherInfoByIds(Long[] ids);
}
