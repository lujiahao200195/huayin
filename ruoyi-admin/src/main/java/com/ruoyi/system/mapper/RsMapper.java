package com.ruoyi.system.mapper;

import com.ruoyi.system.bo.wechat.SearchResourceDo;
import com.ruoyi.system.domain.TResource;
import com.ruoyi.system.domain.TStudent;
import com.ruoyi.system.vo.wechat.rs.ResourceTagVo;
import com.ruoyi.system.vo.wechat.rs.ResourceVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RsMapper {

    /**
     * 搜索资源
     *
     * @param searchResourceDo
     * @return
     */
    List<ResourceVo> searchResourceVo(@Param("searchResourceDo") SearchResourceDo searchResourceDo, @Param("userId") Long userId, @Param("studentId")Long studentId);

    /**
     * 搜索标签
     *
     * @param searchTagName
     * @param userId
     * @return
     */
    List<ResourceTagVo> searchResourceTagVo(@Param("searchTagName") String searchTagName, @Param("userId") Long userId);

    /**
     * 通过用户Id和资源Id查询资源数据
     *
     * @param userId
     * @param resourceId
     * @return
     */
    TResource selectTResourceByUserIdAndResourceId(@Param("userId") Long userId, @Param("resourceId") Long resourceId);

    /**
     * 教师搜索学生的接口
     * @param studentName
     * @return
     */
    List<TStudent> teacherSearchStudent(@Param("studentName") String studentName);
}
