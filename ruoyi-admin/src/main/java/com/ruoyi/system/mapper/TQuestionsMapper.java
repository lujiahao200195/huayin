package com.ruoyi.system.mapper;

import java.util.List;

import com.ruoyi.system.bo.wechat.TAnalysisVo;
import com.ruoyi.system.bo.wechat.TQuestionsTo;
import com.ruoyi.system.domain.TQuestions;
import org.apache.ibatis.annotations.Param;

/**
 * 【请填写功能名称】Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-19
 */
public interface TQuestionsMapper 
{
    /**
     * 查询【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 【请填写功能名称】
     */
    public TQuestions selectTQuestionsById(Long id);

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param tQuestions 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<TQuestions> selectTQuestionsList(TQuestions tQuestions);

    /**
     * 新增【请填写功能名称】
     * 
     * @param tQuestions 【请填写功能名称】
     * @return 结果
     */
    public int insertTQuestions(TQuestions tQuestions);

    /**
     * 修改【请填写功能名称】
     * 
     * @param tQuestions 【请填写功能名称】
     * @return 结果
     */
    public int updateTQuestions(TQuestions tQuestions);

    /**
     * 删除【请填写功能名称】
     * 
     * @param id 【请填写功能名称】主键
     * @return 结果
     */
    public int deleteTQuestionsById(Long id);

    /**
     * 批量删除【请填写功能名称】
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTQuestionsByIds(Long[] ids);

    List<TQuestionsTo> selectTQuestionAll();

    int selectOne(@Param("questionId") Long questionId, @Param("option") String option);

    List<TAnalysisVo> selectTAnalysis(@Param("questionIds")List<Long> questionIds);
}
