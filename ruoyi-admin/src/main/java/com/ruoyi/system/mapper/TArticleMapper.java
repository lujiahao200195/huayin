package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TArticle;

import java.util.List;

/**
 * 文章Mapper接口
 *
 * @author ruoyi
 * @date 2023-08-17
 */
public interface TArticleMapper {
    /**
     * 查询文章
     *
     * @param id 文章主键
     * @return 文章
     */
    public TArticle selectTArticleById(Long id);

    /**
     * 查询文章列表
     *
     * @param tArticle 文章
     * @return 文章集合
     */
    public List<TArticle> selectTArticleList(TArticle tArticle);

    /**
     * 新增文章
     *
     * @param tArticle 文章
     * @return 结果
     */
    public int insertTArticle(TArticle tArticle);

    /**
     * 修改文章
     *
     * @param tArticle 文章
     * @return 结果
     */
    public int updateTArticle(TArticle tArticle);

    /**
     * 删除文章
     *
     * @param id 文章主键
     * @return 结果
     */
    public int deleteTArticleById(Long id);

    /**
     * 批量删除文章
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTArticleByIds(Long[] ids);
}
