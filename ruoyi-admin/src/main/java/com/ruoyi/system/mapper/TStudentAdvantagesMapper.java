package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TStudentAdvantages;

import java.util.List;

/**
 * 学生优势科目Mapper接口
 *
 * @author ruoyi
 * @date 2023-08-30
 */
public interface TStudentAdvantagesMapper {
    /**
     * 查询学生优势科目
     *
     * @param id 学生优势科目主键
     * @return 学生优势科目
     */
    public TStudentAdvantages selectTStudentAdvantagesById(Long id);

    /**
     * 查询学生优势科目列表
     *
     * @param tStudentAdvantages 学生优势科目
     * @return 学生优势科目集合
     */
    public List<TStudentAdvantages> selectTStudentAdvantagesList(TStudentAdvantages tStudentAdvantages);

    /**
     * 新增学生优势科目
     *
     * @param tStudentAdvantages 学生优势科目
     * @return 结果
     */
    public int insertTStudentAdvantages(TStudentAdvantages tStudentAdvantages);

    /**
     * 修改学生优势科目
     *
     * @param tStudentAdvantages 学生优势科目
     * @return 结果
     */
    public int updateTStudentAdvantages(TStudentAdvantages tStudentAdvantages);

    /**
     * 删除学生优势科目
     *
     * @param id 学生优势科目主键
     * @return 结果
     */
    public int deleteTStudentAdvantagesById(Long id);

    /**
     * 批量删除学生优势科目
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTStudentAdvantagesByIds(Long[] ids);
}
