package com.ruoyi.system.mapper.backend;

import com.ruoyi.system.domain.TStudent;
import com.ruoyi.system.vo.wechat.GetStudentAndOpenId;
import com.ruoyi.system.vo.wechat.SubjectDto;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 学生管理
 */
public interface StudentMapper {

    /**
     * 查询学生
     *
     * @param id 学生主键
     * @return 学生
     */
    public TStudent selectTStudentById(Long id);

    /**
     * 查询学生列表
     *
     * @param tStudent 学生
     * @return 学生集合
     */
    public List<TStudent> selectTStudentList(TStudent tStudent);

    /**
     * 新增学生
     *
     * @param tStudent 学生
     * @return 结果
     */
    public int insertTStudent(TStudent tStudent);

    /**
     * 修改学生
     *
     * @param tStudent 学生
     * @return 结果
     */
    public int updateTStudent(TStudent tStudent);

    /**
     * 删除学生
     *
     * @param id 学生主键
     * @return 结果
     */
    public int deleteTStudentById(Long id);

    /**
     * 批量删除学生
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTStudentByIds(Long[] ids);

    /**
     * 根据学生id查学生优势的科目
     *
     * @param studentId
     * @return
     */
    String[] selectObjectsByStudentId(@Param("studentId") Long studentId);

    /**
     * 根据学生id 查需强化的科目
     *
     * @param studentId
     * @return
     */
    List<SubjectDto> selectNeedSubjectByStudentId(@Param("studentId") Long studentId);

    /**
     * 判断是否存在该id的数据
     *
     * @return
     */
    Integer count(@Param("tbName") String tableName, @Param("id") Integer id);

    /**
     * 根据学生表Id修改学生审核状态码
     *
     * @return
     */
    Integer updateStudentStateById(@Param("id") Integer id, @Param("state") String state, @Param("stateResult") String stateResult);

    /**
     * 根据学生表Id查学生名和电话 微信小程序openid
     */
    GetStudentAndOpenId selectStudentById(@Param("id") Integer id);

    /**
     * 根据学生表userId查学生课程
     */
    List<String> selectSubjectsById(@Param("uid") Integer uId);

    /**
     * 根据userid查openid
     *
     * @param id
     * @return
     */
    String selectOpenIdById(@Param("id") Long id);


}
