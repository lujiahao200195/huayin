package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TWorkRoomInvite;

import java.util.List;

/**
 * 工作室邀请Mapper接口
 *
 * @author ruoyi
 * @date 2023-08-18
 */
public interface TWorkRoomInviteMapper {
    /**
     * 查询工作室邀请
     *
     * @param id 工作室邀请主键
     * @return 工作室邀请
     */
    public TWorkRoomInvite selectTWorkRoomInviteById(Long id);

    /**
     * 查询工作室邀请列表
     *
     * @param tWorkRoomInvite 工作室邀请
     * @return 工作室邀请集合
     */
    public List<TWorkRoomInvite> selectTWorkRoomInviteList(TWorkRoomInvite tWorkRoomInvite);

    /**
     * 新增工作室邀请
     *
     * @param tWorkRoomInvite 工作室邀请
     * @return 结果
     */
    public int insertTWorkRoomInvite(TWorkRoomInvite tWorkRoomInvite);

    /**
     * 修改工作室邀请
     *
     * @param tWorkRoomInvite 工作室邀请
     * @return 结果
     */
    public int updateTWorkRoomInvite(TWorkRoomInvite tWorkRoomInvite);

    /**
     * 删除工作室邀请
     *
     * @param id 工作室邀请主键
     * @return 结果
     */
    public int deleteTWorkRoomInviteById(Long id);

    /**
     * 批量删除工作室邀请
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTWorkRoomInviteByIds(Long[] ids);
}
