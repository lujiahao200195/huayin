package com.ruoyi.system.mapper.backend;

import com.ruoyi.system.domain.TCourse;
import com.ruoyi.system.domain.TCourseSubjectRelation;
import com.ruoyi.system.domain.TStudent;
import com.ruoyi.system.domain.TTeacher;
import com.ruoyi.system.vo.wechat.backend.CourseTeacherAndStudent;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 课程安排Mapper接口
 *
 * @author ruoyi
 * @date 2023-09-14
 */
public interface CourseMapper {
    /**
     * 查询课程安排
     *
     * @param id 课程安排主键
     * @return 课程安排
     */
    public TCourse selectTCourseById(Long id);

    /**
     * 查询课程安排列表
     *
     * @param tCourse 课程安排
     * @return 课程安排集合
     */
    public List<TCourse> selectTCourseList(TCourse tCourse);

    /**
     * 新增课程安排
     *
     * @param tCourse 课程安排
     * @return 结果
     */
    public int insertTCourse(TCourse tCourse);

    /**
     * 修改课程安排
     *
     * @param tCourse 课程安排
     * @return 结果
     */
    public int updateTCourse(TCourse tCourse);

    /**
     * 删除课程安排
     *
     * @param id 课程安排主键
     * @return 结果
     */
    public int deleteTCourseById(Long id);

    /**
     * 批量删除课程安排
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTCourseByIds(Long[] ids);

    /**
     * 批量删除课程和科目关系
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTCourseSubjectRelationByIds(Long[] ids);

    /**
     * 查询课程和科目关系列表
     *
     * @param tCourseSubjectRelation 课程和科目关系
     * @return 课程和科目关系集合
     */
    public List<TCourseSubjectRelation> selectTCourseSubjectRelationList(TCourseSubjectRelation tCourseSubjectRelation);

    /**
     * 课程表用
     * 教师列表
     * @return
     */
    List<CourseTeacherAndStudent> getTeacherInfo();

    /**
     * 课程表用
     * 学生列表
     * @return
     */
    List<CourseTeacherAndStudent> getStudentInfo();

    /**
     * 学生和教师
     * @return
     */
    List<CourseTeacherAndStudent> getStudentAndTeacher();

}
