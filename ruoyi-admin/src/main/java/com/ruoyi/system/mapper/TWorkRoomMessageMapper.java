package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TWorkRoomMessage;

import java.util.List;

/**
 * 工作室消息Mapper接口
 *
 * @author ruoyi
 * @date 2023-08-23
 */
public interface TWorkRoomMessageMapper {
    /**
     * 查询工作室消息
     *
     * @param id 工作室消息主键
     * @return 工作室消息
     */
    public TWorkRoomMessage selectTWorkRoomMessageById(Long id);

    /**
     * 查询工作室消息列表
     *
     * @param tWorkRoomMessage 工作室消息
     * @return 工作室消息集合
     */
    public List<TWorkRoomMessage> selectTWorkRoomMessageList(TWorkRoomMessage tWorkRoomMessage);

    /**
     * 新增工作室消息
     *
     * @param tWorkRoomMessage 工作室消息
     * @return 结果
     */
    public int insertTWorkRoomMessage(TWorkRoomMessage tWorkRoomMessage);

    /**
     * 修改工作室消息
     *
     * @param tWorkRoomMessage 工作室消息
     * @return 结果
     */
    public int updateTWorkRoomMessage(TWorkRoomMessage tWorkRoomMessage);

    /**
     * 删除工作室消息
     *
     * @param id 工作室消息主键
     * @return 结果
     */
    public int deleteTWorkRoomMessageById(Long id);

    /**
     * 批量删除工作室消息
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTWorkRoomMessageByIds(Long[] ids);
}
