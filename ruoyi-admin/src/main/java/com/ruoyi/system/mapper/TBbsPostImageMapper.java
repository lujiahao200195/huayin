package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TBbsPostImage;

import java.util.List;

/**
 * 帖子图片Mapper接口
 *
 * @author ruoyi
 * @date 2023-08-28
 */
public interface TBbsPostImageMapper {
    /**
     * 查询帖子图片
     *
     * @param id 帖子图片主键
     * @return 帖子图片
     */
    public TBbsPostImage selectTBbsPostImageById(Long id);

    /**
     * 查询帖子图片列表
     *
     * @param tBbsPostImage 帖子图片
     * @return 帖子图片集合
     */
    public List<TBbsPostImage> selectTBbsPostImageList(TBbsPostImage tBbsPostImage);

    /**
     * 新增帖子图片
     *
     * @param tBbsPostImage 帖子图片
     * @return 结果
     */
    public int insertTBbsPostImage(TBbsPostImage tBbsPostImage);

    /**
     * 修改帖子图片
     *
     * @param tBbsPostImage 帖子图片
     * @return 结果
     */
    public int updateTBbsPostImage(TBbsPostImage tBbsPostImage);

    /**
     * 删除帖子图片
     *
     * @param id 帖子图片主键
     * @return 结果
     */
    public int deleteTBbsPostImageById(Long id);

    /**
     * 批量删除帖子图片
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTBbsPostImageByIds(Long[] ids);
}
