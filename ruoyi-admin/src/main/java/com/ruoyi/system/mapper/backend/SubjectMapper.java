package com.ruoyi.system.mapper.backend;

import com.ruoyi.system.domain.TArticle;
import com.ruoyi.system.domain.TSubject;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 教学科目Mapper接口
 *
 * @author ruoyi
 * @date 2023-08-18
 */
public interface SubjectMapper {
    /**
     * 查询教学科目
     *
     * @param id 教学科目主键
     * @return 教学科目
     */
    public TSubject selectTSubjectById(Long id);

    /**
     * 查询教学科目列表
     *
     * @param tSubject 教学科目
     * @return 教学科目集合
     */
    public List<TSubject> selectTSubjectList(TSubject tSubject);

    /**
     * 新增教学科目
     *
     * @param tSubject 教学科目
     * @return 结果
     */
    public int insertTSubject(TSubject tSubject);

    /**
     * 修改教学科目
     *
     * @param tSubject 教学科目
     * @return 结果
     */
    public int updateTSubject(TSubject tSubject);

    /**
     * 删除教学科目
     *
     * @param id 教学科目主键
     * @return 结果
     */
    public int deleteTSubjectById(Long id);

    /**
     * 批量删除教学科目
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTSubjectByIds(Long[] ids);

    /**
     * 根科目id查文章列表
     *
     * @param id
     * @return
     */
    List<TArticle> getSubjectArticles(@Param("id") Long id);

    /**
     * 插入文章与科目关系表
     *
     * @param subjectId
     * @param articleId
     * @return
     */
    int insertSubjectArticleRelation(@Param("sId") Long subjectId, @Param("aId") Long articleId);

    /**
     * 删除文章与科目关系表
     *
     * @param subjectId
     * @param articleId
     * @return
     */
    int deleteSubjectArticleRelation(@Param("sId") Long subjectId, @Param("aId") Long articleId);

}
