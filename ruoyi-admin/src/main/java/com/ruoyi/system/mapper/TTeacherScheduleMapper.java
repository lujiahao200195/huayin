package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TTeacherSchedule;

import java.util.List;

/**
 * 教师可安排时间段Mapper接口
 *
 * @author ruoyi
 * @date 2023-08-17
 */
public interface TTeacherScheduleMapper {
    /**
     * 查询教师可安排时间段
     *
     * @param id 教师可安排时间段主键
     * @return 教师可安排时间段
     */
    public TTeacherSchedule selectTTeacherScheduleById(Long id);

    /**
     * 查询教师可安排时间段列表
     *
     * @param tTeacherSchedule 教师可安排时间段
     * @return 教师可安排时间段集合
     */
    public List<TTeacherSchedule> selectTTeacherScheduleList(TTeacherSchedule tTeacherSchedule);

    /**
     * 新增教师可安排时间段
     *
     * @param tTeacherSchedule 教师可安排时间段
     * @return 结果
     */
    public int insertTTeacherSchedule(TTeacherSchedule tTeacherSchedule);

    /**
     * 修改教师可安排时间段
     *
     * @param tTeacherSchedule 教师可安排时间段
     * @return 结果
     */
    public int updateTTeacherSchedule(TTeacherSchedule tTeacherSchedule);

    /**
     * 删除教师可安排时间段
     *
     * @param id 教师可安排时间段主键
     * @return 结果
     */
    public int deleteTTeacherScheduleById(Long id);

    /**
     * 批量删除教师可安排时间段
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTTeacherScheduleByIds(Long[] ids);
}
