package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TStudent;

import java.util.List;

/**
 * 学生Mapper接口
 *
 * @author ruoyi
 * @date 2023-08-31
 */
public interface TStudentMapper {
    /**
     * 查询学生
     *
     * @param id 学生主键
     * @return 学生
     */
    public TStudent selectTStudentById(Long id);

    /**
     * 查询学生列表
     *
     * @param tStudent 学生
     * @return 学生集合
     */
    public List<TStudent> selectTStudentList(TStudent tStudent);

    /**
     * 新增学生
     *
     * @param tStudent 学生
     * @return 结果
     */
    public int insertTStudent(TStudent tStudent);

    /**
     * 修改学生
     *
     * @param tStudent 学生
     * @return 结果
     */
    public int updateTStudent(TStudent tStudent);

    /**
     * 删除学生
     *
     * @param id 学生主键
     * @return 结果
     */
    public int deleteTStudentById(Long id);

    /**
     * 批量删除学生
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTStudentByIds(Long[] ids);

    void deleteTStudentByUserId(Long userId);
}
