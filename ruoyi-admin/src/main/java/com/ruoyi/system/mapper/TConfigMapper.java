package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.TConfig;

/**
 * 配置Mapper接口
 * 
 * @author ruoyi
 * @date 2023-09-19
 */
public interface TConfigMapper 
{
    /**
     * 查询配置
     * 
     * @param id 配置主键
     * @return 配置
     */
    public TConfig selectTConfigById(Long id);

    /**
     * 查询配置列表
     * 
     * @param tConfig 配置
     * @return 配置集合
     */
    public List<TConfig> selectTConfigList(TConfig tConfig);

    /**
     * 新增配置
     * 
     * @param tConfig 配置
     * @return 结果
     */
    public int insertTConfig(TConfig tConfig);

    /**
     * 修改配置
     * 
     * @param tConfig 配置
     * @return 结果
     */
    public int updateTConfig(TConfig tConfig);

    /**
     * 删除配置
     * 
     * @param id 配置主键
     * @return 结果
     */
    public int deleteTConfigById(Long id);

    /**
     * 批量删除配置
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTConfigByIds(Long[] ids);
}
