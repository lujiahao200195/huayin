package com.ruoyi.system.mapper;

import org.apache.ibatis.annotations.Param;

public interface AopMapper {

    /**
     * 通过小程序openId查询用户姓名
     *
     * @param openId
     * @return
     */
    public String selectUserNameByOpenId(@Param("openId") String openId);


}
