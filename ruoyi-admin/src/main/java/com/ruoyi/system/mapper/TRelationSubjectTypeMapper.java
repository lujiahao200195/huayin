package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TRelationSubjectType;

import java.util.List;

/**
 * 科目和分类的关系Mapper接口
 *
 * @author ruoyi
 * @date 2023-08-17
 */
public interface TRelationSubjectTypeMapper {
    /**
     * 查询科目和分类的关系
     *
     * @param id 科目和分类的关系主键
     * @return 科目和分类的关系
     */
    public TRelationSubjectType selectTRelationSubjectTypeById(Long id);

    /**
     * 查询科目和分类的关系列表
     *
     * @param tRelationSubjectType 科目和分类的关系
     * @return 科目和分类的关系集合
     */
    public List<TRelationSubjectType> selectTRelationSubjectTypeList(TRelationSubjectType tRelationSubjectType);

    /**
     * 新增科目和分类的关系
     *
     * @param tRelationSubjectType 科目和分类的关系
     * @return 结果
     */
    public int insertTRelationSubjectType(TRelationSubjectType tRelationSubjectType);

    /**
     * 修改科目和分类的关系
     *
     * @param tRelationSubjectType 科目和分类的关系
     * @return 结果
     */
    public int updateTRelationSubjectType(TRelationSubjectType tRelationSubjectType);

    /**
     * 删除科目和分类的关系
     *
     * @param id 科目和分类的关系主键
     * @return 结果
     */
    public int deleteTRelationSubjectTypeById(Long id);

    /**
     * 批量删除科目和分类的关系
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTRelationSubjectTypeByIds(Long[] ids);
}
