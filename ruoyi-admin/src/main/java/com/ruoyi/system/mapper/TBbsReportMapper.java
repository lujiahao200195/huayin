package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TBbsReport;

import java.util.List;

/**
 * 举报Mapper接口
 *
 * @author ruoyi
 * @date 2023-08-29
 */
public interface TBbsReportMapper {
    /**
     * 查询举报
     *
     * @param id 举报主键
     * @return 举报
     */
    public TBbsReport selectTBbsReportById(Long id);

    /**
     * 查询举报列表
     *
     * @param tBbsReport 举报
     * @return 举报集合
     */
    public List<TBbsReport> selectTBbsReportList(TBbsReport tBbsReport);

    /**
     * 新增举报
     *
     * @param tBbsReport 举报
     * @return 结果
     */
    public int insertTBbsReport(TBbsReport tBbsReport);

    /**
     * 修改举报
     *
     * @param tBbsReport 举报
     * @return 结果
     */
    public int updateTBbsReport(TBbsReport tBbsReport);

    /**
     * 删除举报
     *
     * @param id 举报主键
     * @return 结果
     */
    public int deleteTBbsReportById(Long id);

    /**
     * 批量删除举报
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTBbsReportByIds(Long[] ids);
}
