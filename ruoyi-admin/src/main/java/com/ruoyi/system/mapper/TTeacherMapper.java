package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TTeacher;

import java.util.List;

/**
 * 教师Mapper接口
 *
 * @author ruoyi
 * @date 2023-09-13
 */
public interface TTeacherMapper {
    /**
     * 查询教师
     *
     * @param id 教师主键
     * @return 教师
     */
    public TTeacher selectTTeacherById(Long id);

    /**
     * 查询教师列表
     *
     * @param tTeacher 教师
     * @return 教师集合
     */
    public List<TTeacher> selectTTeacherList(TTeacher tTeacher);

    /**
     * 新增教师
     *
     * @param tTeacher 教师
     * @return 结果
     */
    public int insertTTeacher(TTeacher tTeacher);

    /**
     * 修改教师
     *
     * @param tTeacher 教师
     * @return 结果
     */
    public int updateTTeacher(TTeacher tTeacher);

    /**
     * 删除教师
     *
     * @param id 教师主键
     * @return 结果
     */
    public int deleteTTeacherById(Long id);

    /**
     * 批量删除教师
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTTeacherByIds(Long[] ids);

    void deleteTTeacherByUserId(Long userId);

}
