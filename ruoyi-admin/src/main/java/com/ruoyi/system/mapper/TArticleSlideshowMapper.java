package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.TArticleSlideshow;

/**
 * 轮播图Mapper接口
 * 
 * @author ruoyi
 * @date 2024-05-11
 */
public interface TArticleSlideshowMapper 
{
    /**
     * 查询轮播图
     * 
     * @param id 轮播图主键
     * @return 轮播图
     */
    public TArticleSlideshow selectTArticleSlideshowById(Long id);

    /**
     * 查询轮播图列表
     * 
     * @param tArticleSlideshow 轮播图
     * @return 轮播图集合
     */
    public List<TArticleSlideshow> selectTArticleSlideshowList(TArticleSlideshow tArticleSlideshow);

    /**
     * 新增轮播图
     * 
     * @param tArticleSlideshow 轮播图
     * @return 结果
     */
    public int insertTArticleSlideshow(TArticleSlideshow tArticleSlideshow);

    /**
     * 修改轮播图
     * 
     * @param tArticleSlideshow 轮播图
     * @return 结果
     */
    public int updateTArticleSlideshow(TArticleSlideshow tArticleSlideshow);

    /**
     * 删除轮播图
     * 
     * @param id 轮播图主键
     * @return 结果
     */
    public int deleteTArticleSlideshowById(Long id);

    /**
     * 批量删除轮播图
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTArticleSlideshowByIds(Long[] ids);
}
