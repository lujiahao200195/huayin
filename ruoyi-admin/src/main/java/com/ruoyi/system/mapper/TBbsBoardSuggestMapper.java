package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.TBbsBoardSuggest;

import java.util.List;

/**
 * 版主推荐Mapper接口
 *
 * @author ruoyi
 * @date 2023-08-29
 */
public interface TBbsBoardSuggestMapper {
    /**
     * 查询版主推荐
     *
     * @param id 版主推荐主键
     * @return 版主推荐
     */
    public TBbsBoardSuggest selectTBbsBoardSuggestById(Long id);

    /**
     * 查询版主推荐列表
     *
     * @param tBbsBoardSuggest 版主推荐
     * @return 版主推荐集合
     */
    public List<TBbsBoardSuggest> selectTBbsBoardSuggestList(TBbsBoardSuggest tBbsBoardSuggest);

    /**
     * 新增版主推荐
     *
     * @param tBbsBoardSuggest 版主推荐
     * @return 结果
     */
    public int insertTBbsBoardSuggest(TBbsBoardSuggest tBbsBoardSuggest);

    /**
     * 修改版主推荐
     *
     * @param tBbsBoardSuggest 版主推荐
     * @return 结果
     */
    public int updateTBbsBoardSuggest(TBbsBoardSuggest tBbsBoardSuggest);

    /**
     * 删除版主推荐
     *
     * @param id 版主推荐主键
     * @return 结果
     */
    public int deleteTBbsBoardSuggestById(Long id);

    /**
     * 批量删除版主推荐
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTBbsBoardSuggestByIds(Long[] ids);
}
