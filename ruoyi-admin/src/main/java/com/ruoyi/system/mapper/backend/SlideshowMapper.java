package com.ruoyi.system.mapper.backend;

import com.ruoyi.system.domain.TSlideshow;

import java.util.List;

/**
 * 轮播图Mapper接口
 */
public interface SlideshowMapper {
    /**
     * 查询轮播图
     *
     * @param id 轮播图主键
     * @return 轮播图
     */
    public TSlideshow selectTSlideshowById(Long id);

    /**
     * 查询轮播图列表
     *
     * @param tSlideshow 轮播图
     * @return 轮播图集合
     */
    public List<TSlideshow> selectTSlideshowList(TSlideshow tSlideshow);

    /**
     * 新增轮播图
     *
     * @param tSlideshow 轮播图
     * @return 结果
     */
    public int insertTSlideshow(TSlideshow tSlideshow);

    /**
     * 修改轮播图
     *
     * @param tSlideshow 轮播图
     * @return 结果
     */
    public int updateTSlideshow(TSlideshow tSlideshow);

    /**
     * 删除轮播图
     *
     * @param id 轮播图主键
     * @return 结果
     */
    public int deleteTSlideshowById(Long id);

    /**
     * 批量删除轮播图
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTSlideshowByIds(Long[] ids);
}
