package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.TQuestionAnalysis;

/**
 * 试题解析Mapper接口
 * 
 * @author ruoyi
 * @date 2024-04-28
 */
public interface TQuestionAnalysisMapper 
{
    /**
     * 查询试题解析
     * 
     * @param id 试题解析主键
     * @return 试题解析
     */
    public TQuestionAnalysis selectTQuestionAnalysisById(Long id);

    /**
     * 查询试题解析列表
     * 
     * @param tQuestionAnalysis 试题解析
     * @return 试题解析集合
     */
    public List<TQuestionAnalysis> selectTQuestionAnalysisList(TQuestionAnalysis tQuestionAnalysis);

    /**
     * 新增试题解析
     * 
     * @param tQuestionAnalysis 试题解析
     * @return 结果
     */
    public int insertTQuestionAnalysis(TQuestionAnalysis tQuestionAnalysis);

    /**
     * 修改试题解析
     * 
     * @param tQuestionAnalysis 试题解析
     * @return 结果
     */
    public int updateTQuestionAnalysis(TQuestionAnalysis tQuestionAnalysis);

    /**
     * 删除试题解析
     * 
     * @param id 试题解析主键
     * @return 结果
     */
    public int deleteTQuestionAnalysisById(Long id);

    /**
     * 批量删除试题解析
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteTQuestionAnalysisByIds(Long[] ids);
}
