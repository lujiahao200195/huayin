//package com.ruoyi.web.cosfile.costool;
//
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.multipart.MultipartFile;
//
//import java.io.File;
//
//@RestController
//@RequestMapping("/system/iWeChatController")
//public class ExampleController {
//    @Value("${tencent.cos.secret-id}")
//    private String secretId;
//
//    @Value("${tencent.cos.secret-key}")
//    private String secretKey;
//
//    @Value("${tencent.cos.region}")
//    private String region;
//
//    @Value("${tencent.cos.bucket-name}")
//    private String bucketName;
//
//    @PostMapping("/upload")
//    public String uploadFile(@RequestParam("file") MultipartFile file) {
//        System.out.println("上传");
//        try {
//            // 创建 COS 工具类实例
//            TencentCosUtil cosUtil = new TencentCosUtil(secretId, secretKey, region, bucketName);
//
//            // 保存文件到本地临时路径
//            File tempFile = File.createTempFile("temp", null);
//            file.transferTo(tempFile);
//
//            // 上传文件到 COS
//            cosUtil.uploadFile(tempFile, "folder/file.jpg");
//
//            // 删除本地临时文件
//            tempFile.delete();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return "file uploaded";
//    }
//}
