package com.ruoyi.web.cosfile.costool;

import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.model.PutObjectRequest;
import com.qcloud.cos.region.Region;

import java.io.File;

public class TencentCosUtil {

    private String secretId;
    private String secretKey;
    private String region;
    private String bucketName;
    private COSClient cosClient;

    public TencentCosUtil(String secretId, String secretKey, String region, String bucketName) {
        this.secretId = secretId;
        this.secretKey = secretKey;
        this.region = region;
        this.bucketName = bucketName;

        // 初始化 COS 客户端
        ClientConfig clientConfig = new ClientConfig(new Region(region));
        BasicCOSCredentials credentials = new BasicCOSCredentials(secretId, secretKey);
        cosClient = new COSClient(credentials, clientConfig);
    }

    public void uploadFile(File filePath, String cosKey) {
//        File localFile = new File(filePath);
        PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, cosKey, filePath);
        cosClient.putObject(putObjectRequest);

    }
}
