import request from '@/utils/request'

// 查询试题解析列表
export function listAnalysis(query) {
  return request({
    url: '/tool/analysis/list',
    method: 'get',
    params: query
  })
}

// 查询试题解析详细
export function getAnalysis(id) {
  return request({
    url: '/tool/analysis/' + id,
    method: 'get'
  })
}

// 新增试题解析
export function addAnalysis(data) {
  return request({
    url: '/tool/analysis',
    method: 'post',
    data: data
  })
}

// 修改试题解析
export function updateAnalysis(data) {
  return request({
    url: '/tool/analysis',
    method: 'put',
    data: data
  })
}

// 删除试题解析
export function delAnalysis(id) {
  return request({
    url: '/tool/analysis/' + id,
    method: 'delete'
  })
}
