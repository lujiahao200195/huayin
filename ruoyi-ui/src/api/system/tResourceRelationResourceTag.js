import request from '@/utils/request'

// 查询资源标签关系列表
export function listTResourceRelationResourceTag(query) {
	return request({
		url: '/system/tResourceRelationResourceTag/list',
		method: 'get',
		params: query
	})
}

// 查询资源标签关系详细
export function getTResourceRelationResourceTag(id) {
	return request({
		url: '/system/tResourceRelationResourceTag/' + id,
		method: 'get'
	})
}

// 新增资源标签关系
export function addTResourceRelationResourceTag(data) {
	return request({
		url: '/system/tResourceRelationResourceTag',
		method: 'post',
		data: data
	})
}

// 修改资源标签关系
export function updateTResourceRelationResourceTag(data) {
	return request({
		url: '/system/tResourceRelationResourceTag',
		method: 'put',
		data: data
	})
}

// 删除资源标签关系
export function delTResourceRelationResourceTag(id) {
	return request({
		url: '/system/tResourceRelationResourceTag/' + id,
		method: 'delete'
	})
}
