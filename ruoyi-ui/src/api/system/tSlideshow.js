import request from '@/utils/request'

// 查询轮播图列表
export function listTSlideshow(query) {
	return request({
		url: '/system/tSlideshow/list',
		method: 'get',
		params: query
	})
}

// 查询轮播图详细
export function getTSlideshow(id) {
	return request({
		url: '/system/tSlideshow/' + id,
		method: 'get'
	})
}

// 新增轮播图
export function addTSlideshow(data) {
	return request({
		url: '/system/tSlideshow',
		method: 'post',
		data: data
	})
}

// 修改轮播图
export function updateTSlideshow(data) {
	return request({
		url: '/system/tSlideshow',
		method: 'put',
		data: data
	})
}

// 删除轮播图
export function delTSlideshow(id) {
	return request({
		url: '/system/tSlideshow/' + id,
		method: 'delete'
	})
}
