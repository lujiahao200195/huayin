import request from '@/utils/request'

// 查询关注关系列表
export function listTBbsRelationFollow(query) {
	return request({
		url: '/system/tBbsRelationFollow/list',
		method: 'get',
		params: query
	})
}

// 查询关注关系详细
export function getTBbsRelationFollow(id) {
	return request({
		url: '/system/tBbsRelationFollow/' + id,
		method: 'get'
	})
}

// 新增关注关系
export function addTBbsRelationFollow(data) {
	return request({
		url: '/system/tBbsRelationFollow',
		method: 'post',
		data: data
	})
}

// 修改关注关系
export function updateTBbsRelationFollow(data) {
	return request({
		url: '/system/tBbsRelationFollow',
		method: 'put',
		data: data
	})
}

// 删除关注关系
export function delTBbsRelationFollow(id) {
	return request({
		url: '/system/tBbsRelationFollow/' + id,
		method: 'delete'
	})
}
