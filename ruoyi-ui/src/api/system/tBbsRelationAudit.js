import request from '@/utils/request'

// 查询审核关系列表
export function listTBbsRelationAudit(query) {
	return request({
		url: '/system/tBbsRelationAudit/list',
		method: 'get',
		params: query
	})
}

// 查询审核关系详细
export function getTBbsRelationAudit(id) {
	return request({
		url: '/system/tBbsRelationAudit/' + id,
		method: 'get'
	})
}

// 新增审核关系
export function addTBbsRelationAudit(data) {
	return request({
		url: '/system/tBbsRelationAudit',
		method: 'post',
		data: data
	})
}

// 修改审核关系
export function updateTBbsRelationAudit(data) {
	return request({
		url: '/system/tBbsRelationAudit',
		method: 'put',
		data: data
	})
}

// 删除审核关系
export function delTBbsRelationAudit(id) {
	return request({
		url: '/system/tBbsRelationAudit/' + id,
		method: 'delete'
	})
}
