import request from '@/utils/request'

// 查询论坛版块列表
export function listTBbsBoard(query) {
	return request({
		url: '/system/tBbsBoard/list',
		method: 'get',
		params: query
	})
}

// 查询论坛版块详细
export function getTBbsBoard(id) {
	return request({
		url: '/system/tBbsBoard/' + id,
		method: 'get'
	})
}

// 新增论坛版块
export function addTBbsBoard(data) {
	return request({
		url: '/system/tBbsBoard',
		method: 'post',
		data: data
	})
}

// 修改论坛版块
export function updateTBbsBoard(data) {
	return request({
		url: '/system/tBbsBoard',
		method: 'put',
		data: data
	})
}

// 删除论坛版块
export function delTBbsBoard(id) {
	return request({
		url: '/system/tBbsBoard/' + id,
		method: 'delete'
	})
}
