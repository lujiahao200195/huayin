import request from '@/utils/request'

// 查询工作室列表
export function listTWorkRoom(query) {
	return request({
		url: '/system/tWorkRoom/list',
		method: 'get',
		params: query
	})
}

// 查询工作室详细
export function getTWorkRoom(id) {
	return request({
		url: '/system/tWorkRoom/' + id,
		method: 'get'
	})
}

// 新增工作室
export function addTWorkRoom(data) {
	return request({
		url: '/system/tWorkRoom',
		method: 'post',
		data: data
	})
}

// 修改工作室
export function updateTWorkRoom(data) {
	return request({
		url: '/system/tWorkRoom',
		method: 'put',
		data: data
	})
}

// 删除工作室
export function delTWorkRoom(id) {
	return request({
		url: '/system/tWorkRoom/' + id,
		method: 'delete'
	})
}
