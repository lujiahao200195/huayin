import request from '@/utils/request'

// 查询课程和科目关系列表
export function listTCourseSubjectRelation(query) {
  return request({
    url: '/system/tCourseSubjectRelation/list',
    method: 'get',
    params: query
  })
}

// 查询课程和科目关系详细
export function getTCourseSubjectRelation(id) {
  return request({
    url: '/system/tCourseSubjectRelation/' + id,
    method: 'get'
  })
}

// 新增课程和科目关系
export function addTCourseSubjectRelation(data) {
  return request({
    url: '/system/tCourseSubjectRelation',
    method: 'post',
    data: data
  })
}

// 修改课程和科目关系
export function updateTCourseSubjectRelation(data) {
  return request({
    url: '/system/tCourseSubjectRelation',
    method: 'put',
    data: data
  })
}

// 删除课程和科目关系
export function delTCourseSubjectRelation(id) {
  return request({
    url: '/system/tCourseSubjectRelation/' + id,
    method: 'delete'
  })
}
