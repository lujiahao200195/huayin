import request from '@/utils/request'

// 查询工作室与地区关系列表
export function listTRelationWorkRoomAddress(query) {
	return request({
		url: '/system/tRelationWorkRoomAddress/list',
		method: 'get',
		params: query
	})
}

// 查询工作室与地区关系详细
export function getTRelationWorkRoomAddress(id) {
	return request({
		url: '/system/tRelationWorkRoomAddress/' + id,
		method: 'get'
	})
}

// 新增工作室与地区关系
export function addTRelationWorkRoomAddress(data) {
	return request({
		url: '/system/tRelationWorkRoomAddress',
		method: 'post',
		data: data
	})
}

// 修改工作室与地区关系
export function updateTRelationWorkRoomAddress(data) {
	return request({
		url: '/system/tRelationWorkRoomAddress',
		method: 'put',
		data: data
	})
}

// 删除工作室与地区关系
export function delTRelationWorkRoomAddress(id) {
	return request({
		url: '/system/tRelationWorkRoomAddress/' + id,
		method: 'delete'
	})
}
