import request from '@/utils/request'

// 查询资源标签列表
export function listTResourceTag(query) {
	return request({
		url: '/system/tResourceTag/list',
		method: 'get',
		params: query
	})
}

// 查询资源标签详细
export function getTResourceTag(id) {
	return request({
		url: '/system/tResourceTag/' + id,
		method: 'get'
	})
}

// 新增资源标签
export function addTResourceTag(data) {
	return request({
		url: '/system/tResourceTag',
		method: 'post',
		data: data
	})
}

// 修改资源标签
export function updateTResourceTag(data) {
	return request({
		url: '/system/tResourceTag',
		method: 'put',
		data: data
	})
}

// 删除资源标签
export function delTResourceTag(id) {
	return request({
		url: '/system/tResourceTag/' + id,
		method: 'delete'
	})
}
