import request from '@/utils/request'

// 查询用户列表
export function listTUser(query) {
  return request({
    url: '/system/tUser/list',
    method: 'get',
    params: query
  })
}

// 查询用户详细
export function getTUser(id) {
  return request({
    url: '/system/tUser/' + id,
    method: 'get'
  })
}

// 新增用户
export function addTUser(data) {
  return request({
    url: '/system/tUser',
    method: 'post',
    data: data
  })
}

// 修改用户
export function updateTUser(data) {
  return request({
    url: '/system/tUser',
    method: 'put',
    data: data
  })
}

// 删除用户
export function delTUser(id) {
  return request({
    url: '/system/tUser/' + id,
    method: 'delete'
  })
}
