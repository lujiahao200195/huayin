import request from '@/utils/request'

// 查询科目文件关系列表
export function listTSubjectArticleRelation(query) {
	return request({
		url: '/system/tSubjectArticleRelation/list',
		method: 'get',
		params: query
	})
}

// 查询科目文件关系详细
export function getTSubjectArticleRelation(id) {
	return request({
		url: '/system/tSubjectArticleRelation/' + id,
		method: 'get'
	})
}

// 新增科目文件关系
export function addTSubjectArticleRelation(data) {
	return request({
		url: '/system/tSubjectArticleRelation',
		method: 'post',
		data: data
	})
}

// 修改科目文件关系
export function updateTSubjectArticleRelation(data) {
	return request({
		url: '/system/tSubjectArticleRelation',
		method: 'put',
		data: data
	})
}

// 删除科目文件关系
export function delTSubjectArticleRelation(id) {
	return request({
		url: '/system/tSubjectArticleRelation/' + id,
		method: 'delete'
	})
}
