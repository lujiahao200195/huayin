import request from '@/utils/request'

// 查询教师个人优势分类列表
export function listTTeacherAdvantagesType(query) {
	return request({
		url: '/system/tTeacherAdvantagesType/list',
		method: 'get',
		params: query
	})
}

// 查询教师个人优势分类详细
export function getTTeacherAdvantagesType(id) {
	return request({
		url: '/system/tTeacherAdvantagesType/' + id,
		method: 'get'
	})
}

// 新增教师个人优势分类
export function addTTeacherAdvantagesType(data) {
	return request({
		url: '/system/tTeacherAdvantagesType',
		method: 'post',
		data: data
	})
}

// 修改教师个人优势分类
export function updateTTeacherAdvantagesType(data) {
	return request({
		url: '/system/tTeacherAdvantagesType',
		method: 'put',
		data: data
	})
}

// 删除教师个人优势分类
export function delTTeacherAdvantagesType(id) {
	return request({
		url: '/system/tTeacherAdvantagesType/' + id,
		method: 'delete'
	})
}
