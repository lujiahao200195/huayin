import request from '@/utils/request'

// 查询课程安排列表
export function listTCourse(query) {
  return request({
    url: '/system/tCourse/list',
    method: 'get',
    params: query
  })
}

// 查询课程安排详细
export function getTCourse(id) {
  return request({
    url: '/system/tCourse/' + id,
    method: 'get'
  })
}

// 新增课程安排
export function addTCourse(data) {
  return request({
    url: '/system/tCourse',
    method: 'post',
    data: data
  })
}

// 修改课程安排
export function updateTCourse(data) {
  return request({
    url: '/system/tCourse',
    method: 'put',
    data: data
  })
}

// 删除课程安排
export function delTCourse(id) {
  return request({
    url: '/system/tCourse/' + id,
    method: 'delete'
  })
}
