import request from '@/utils/request'

// 查询帖子图片列表
export function listTBbsPostImage(query) {
	return request({
		url: '/system/tBbsPostImage/list',
		method: 'get',
		params: query
	})
}

// 查询帖子图片详细
export function getTBbsPostImage(id) {
	return request({
		url: '/system/tBbsPostImage/' + id,
		method: 'get'
	})
}

// 新增帖子图片
export function addTBbsPostImage(data) {
	return request({
		url: '/system/tBbsPostImage',
		method: 'post',
		data: data
	})
}

// 修改帖子图片
export function updateTBbsPostImage(data) {
	return request({
		url: '/system/tBbsPostImage',
		method: 'put',
		data: data
	})
}

// 删除帖子图片
export function delTBbsPostImage(id) {
	return request({
		url: '/system/tBbsPostImage/' + id,
		method: 'delete'
	})
}
