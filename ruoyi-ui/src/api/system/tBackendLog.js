import request from '@/utils/request'

// 查询管理端日志列表
export function listTBackendLog(query) {
	return request({
		url: '/system/tBackendLog/list',
		method: 'get',
		params: query
	})
}

// 查询管理端日志详细
export function getTBackendLog(id) {
	return request({
		url: '/system/tBackendLog/' + id,
		method: 'get'
	})
}

// 新增管理端日志
export function addTBackendLog(data) {
	return request({
		url: '/system/tBackendLog',
		method: 'post',
		data: data
	})
}

// 修改管理端日志
export function updateTBackendLog(data) {
	return request({
		url: '/system/tBackendLog',
		method: 'put',
		data: data
	})
}

// 删除管理端日志
export function delTBackendLog(id) {
	return request({
		url: '/system/tBackendLog/' + id,
		method: 'delete'
	})
}
