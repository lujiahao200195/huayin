import request from '@/utils/request'

// 查询用户收藏列表
export function listTResourceCollectUser(query) {
  return request({
    url: '/system/tResourceCollectUser/list',
    method: 'get',
    params: query
  })
}

// 查询用户收藏详细
export function getTResourceCollectUser(id) {
  return request({
    url: '/system/tResourceCollectUser/' + id,
    method: 'get'
  })
}

// 新增用户收藏
export function addTResourceCollectUser(data) {
  return request({
    url: '/system/tResourceCollectUser',
    method: 'post',
    data: data
  })
}

// 修改用户收藏
export function updateTResourceCollectUser(data) {
  return request({
    url: '/system/tResourceCollectUser',
    method: 'put',
    data: data
  })
}

// 删除用户收藏
export function delTResourceCollectUser(id) {
  return request({
    url: '/system/tResourceCollectUser/' + id,
    method: 'delete'
  })
}
