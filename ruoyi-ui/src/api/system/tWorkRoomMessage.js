import request from '@/utils/request'

// 查询工作室消息列表
export function listTWorkRoomMessage(query) {
	return request({
		url: '/system/tWorkRoomMessage/list',
		method: 'get',
		params: query
	})
}

// 查询工作室消息详细
export function getTWorkRoomMessage(id) {
	return request({
		url: '/system/tWorkRoomMessage/' + id,
		method: 'get'
	})
}

// 新增工作室消息
export function addTWorkRoomMessage(data) {
	return request({
		url: '/system/tWorkRoomMessage',
		method: 'post',
		data: data
	})
}

// 修改工作室消息
export function updateTWorkRoomMessage(data) {
	return request({
		url: '/system/tWorkRoomMessage',
		method: 'put',
		data: data
	})
}

// 删除工作室消息
export function delTWorkRoomMessage(id) {
	return request({
		url: '/system/tWorkRoomMessage/' + id,
		method: 'delete'
	})
}
