import request from '@/utils/request'

// 查询论坛帖列表
export function listTBbsPost(query) {
	return request({
		url: '/system/tBbsPost/list',
		method: 'get',
		params: query
	})
}

// 查询论坛帖详细
export function getTBbsPost(id) {
	return request({
		url: '/system/tBbsPost/' + id,
		method: 'get'
	})
}

// 新增论坛帖
export function addTBbsPost(data) {
	return request({
		url: '/system/tBbsPost',
		method: 'post',
		data: data
	})
}

// 修改论坛帖
export function updateTBbsPost(data) {
	return request({
		url: '/system/tBbsPost',
		method: 'put',
		data: data
	})
}

// 删除论坛帖
export function delTBbsPost(id) {
	return request({
		url: '/system/tBbsPost/' + id,
		method: 'delete'
	})
}
