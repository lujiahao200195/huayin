import request from '@/utils/request'

// 查询学生可安排时间列表
export function listTStudentSchedule(query) {
	return request({
		url: '/system/tStudentSchedule/list',
		method: 'get',
		params: query
	})
}

// 查询学生可安排时间详细
export function getTStudentSchedule(id) {
	return request({
		url: '/system/tStudentSchedule/' + id,
		method: 'get'
	})
}

// 新增学生可安排时间
export function addTStudentSchedule(data) {
	return request({
		url: '/system/tStudentSchedule',
		method: 'post',
		data: data
	})
}

// 修改学生可安排时间
export function updateTStudentSchedule(data) {
	return request({
		url: '/system/tStudentSchedule',
		method: 'put',
		data: data
	})
}

// 删除学生可安排时间
export function delTStudentSchedule(id) {
	return request({
		url: '/system/tStudentSchedule/' + id,
		method: 'delete'
	})
}
