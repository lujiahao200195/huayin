import request from '@/utils/request'

// 查询版块成员列表
export function listTBbsBoardMember(query) {
	return request({
		url: '/system/tBbsBoardMember/list',
		method: 'get',
		params: query
	})
}

// 查询版块成员详细
export function getTBbsBoardMember(id) {
	return request({
		url: '/system/tBbsBoardMember/' + id,
		method: 'get'
	})
}

// 新增版块成员
export function addTBbsBoardMember(data) {
	return request({
		url: '/system/tBbsBoardMember',
		method: 'post',
		data: data
	})
}

// 修改版块成员
export function updateTBbsBoardMember(data) {
	return request({
		url: '/system/tBbsBoardMember',
		method: 'put',
		data: data
	})
}

// 删除版块成员
export function delTBbsBoardMember(id) {
	return request({
		url: '/system/tBbsBoardMember/' + id,
		method: 'delete'
	})
}
