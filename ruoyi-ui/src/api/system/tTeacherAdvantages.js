import request from '@/utils/request'

// 查询教师个人优势列表
export function listTTeacherAdvantages(query) {
	return request({
		url: '/system/tTeacherAdvantages/list',
		method: 'get',
		params: query
	})
}

// 查询教师个人优势详细
export function getTTeacherAdvantages(id) {
	return request({
		url: '/system/tTeacherAdvantages/' + id,
		method: 'get'
	})
}

// 新增教师个人优势
export function addTTeacherAdvantages(data) {
	return request({
		url: '/system/tTeacherAdvantages',
		method: 'post',
		data: data
	})
}

// 修改教师个人优势
export function updateTTeacherAdvantages(data) {
	return request({
		url: '/system/tTeacherAdvantages',
		method: 'put',
		data: data
	})
}

// 删除教师个人优势
export function delTTeacherAdvantages(id) {
	return request({
		url: '/system/tTeacherAdvantages/' + id,
		method: 'delete'
	})
}
