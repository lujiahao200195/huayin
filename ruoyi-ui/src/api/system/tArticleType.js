import request from '@/utils/request'

// 查询文章分类列表
export function listTArticleType(query) {
	return request({
		url: '/system/tArticleType/list',
		method: 'get',
		params: query
	})
}

// 查询文章分类详细
export function getTArticleType(id) {
	return request({
		url: '/system/tArticleType/' + id,
		method: 'get'
	})
}

// 新增文章分类
export function addTArticleType(data) {
	return request({
		url: '/system/tArticleType',
		method: 'post',
		data: data
	})
}

// 修改文章分类
export function updateTArticleType(data) {
	return request({
		url: '/system/tArticleType',
		method: 'put',
		data: data
	})
}

// 删除文章分类
export function delTArticleType(id) {
	return request({
		url: '/system/tArticleType/' + id,
		method: 'delete'
	})
}
