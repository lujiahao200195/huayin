import request from '@/utils/request'

// 查询科目和分类的关系列表
export function listTRelationSubjectType(query) {
	return request({
		url: '/system/tRelationSubjectType/list',
		method: 'get',
		params: query
	})
}

// 查询科目和分类的关系详细
export function getTRelationSubjectType(id) {
	return request({
		url: '/system/tRelationSubjectType/' + id,
		method: 'get'
	})
}

// 新增科目和分类的关系
export function addTRelationSubjectType(data) {
	return request({
		url: '/system/tRelationSubjectType',
		method: 'post',
		data: data
	})
}

// 修改科目和分类的关系
export function updateTRelationSubjectType(data) {
	return request({
		url: '/system/tRelationSubjectType',
		method: 'put',
		data: data
	})
}

// 删除科目和分类的关系
export function delTRelationSubjectType(id) {
	return request({
		url: '/system/tRelationSubjectType/' + id,
		method: 'delete'
	})
}
