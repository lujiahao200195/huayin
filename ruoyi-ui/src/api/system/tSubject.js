import request from '@/utils/request'

// 查询教学科目列表
export function listTSubject(query) {
	return request({
		url: '/system/tSubject/list',
		method: 'get',
		params: query
	})
}

// 查询教学科目详细
export function getTSubject(id) {
	return request({
		url: '/system/tSubject/' + id,
		method: 'get'
	})
}

// 新增教学科目
export function addTSubject(data) {
	return request({
		url: '/system/tSubject',
		method: 'post',
		data: data
	})
}

// 修改教学科目
export function updateTSubject(data) {
	return request({
		url: '/system/tSubject',
		method: 'put',
		data: data
	})
}

// 删除教学科目
export function delTSubject(id) {
	return request({
		url: '/system/tSubject/' + id,
		method: 'delete'
	})
}
