import request from '@/utils/request'

// 查询积分经验关系列表
export function listTBbsRelationPointExp(query) {
	return request({
		url: '/system/tBbsRelationPointExp/list',
		method: 'get',
		params: query
	})
}

// 查询积分经验关系详细
export function getTBbsRelationPointExp(id) {
	return request({
		url: '/system/tBbsRelationPointExp/' + id,
		method: 'get'
	})
}

// 新增积分经验关系
export function addTBbsRelationPointExp(data) {
	return request({
		url: '/system/tBbsRelationPointExp',
		method: 'post',
		data: data
	})
}

// 修改积分经验关系
export function updateTBbsRelationPointExp(data) {
	return request({
		url: '/system/tBbsRelationPointExp',
		method: 'put',
		data: data
	})
}

// 删除积分经验关系
export function delTBbsRelationPointExp(id) {
	return request({
		url: '/system/tBbsRelationPointExp/' + id,
		method: 'delete'
	})
}
