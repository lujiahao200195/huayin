import request from '@/utils/request'

// 查询教师列表
export function listTTeacher(query) {
	return request({
		url: '/system/tTeacher/list',
		method: 'get',
		params: query
	})
}

// 查询教师详细
export function getTTeacher(id) {
	return request({
		url: '/system/tTeacher/' + id,
		method: 'get'
	})
}

// 新增教师
export function addTTeacher(data) {
	return request({
		url: '/system/tTeacher',
		method: 'post',
		data: data
	})
}

// 修改教师
export function updateTTeacher(data) {
	return request({
		url: '/system/tTeacher',
		method: 'put',
		data: data
	})
}

// 删除教师
export function delTTeacher(id) {
	return request({
		url: '/system/tTeacher/' + id,
		method: 'delete'
	})
}
