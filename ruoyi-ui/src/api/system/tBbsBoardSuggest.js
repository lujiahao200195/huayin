import request from '@/utils/request'

// 查询版主推荐列表
export function listTBbsBoardSuggest(query) {
	return request({
		url: '/system/tBbsBoardSuggest/list',
		method: 'get',
		params: query
	})
}

// 查询版主推荐详细
export function getTBbsBoardSuggest(id) {
	return request({
		url: '/system/tBbsBoardSuggest/' + id,
		method: 'get'
	})
}

// 新增版主推荐
export function addTBbsBoardSuggest(data) {
	return request({
		url: '/system/tBbsBoardSuggest',
		method: 'post',
		data: data
	})
}

// 修改版主推荐
export function updateTBbsBoardSuggest(data) {
	return request({
		url: '/system/tBbsBoardSuggest',
		method: 'put',
		data: data
	})
}

// 删除版主推荐
export function delTBbsBoardSuggest(id) {
	return request({
		url: '/system/tBbsBoardSuggest/' + id,
		method: 'delete'
	})
}
