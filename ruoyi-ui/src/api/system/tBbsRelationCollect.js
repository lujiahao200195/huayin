import request from '@/utils/request'

// 查询收藏关系列表
export function listTBbsRelationCollect(query) {
	return request({
		url: '/system/tBbsRelationCollect/list',
		method: 'get',
		params: query
	})
}

// 查询收藏关系详细
export function getTBbsRelationCollect(id) {
	return request({
		url: '/system/tBbsRelationCollect/' + id,
		method: 'get'
	})
}

// 新增收藏关系
export function addTBbsRelationCollect(data) {
	return request({
		url: '/system/tBbsRelationCollect',
		method: 'post',
		data: data
	})
}

// 修改收藏关系
export function updateTBbsRelationCollect(data) {
	return request({
		url: '/system/tBbsRelationCollect',
		method: 'put',
		data: data
	})
}

// 删除收藏关系
export function delTBbsRelationCollect(id) {
	return request({
		url: '/system/tBbsRelationCollect/' + id,
		method: 'delete'
	})
}
