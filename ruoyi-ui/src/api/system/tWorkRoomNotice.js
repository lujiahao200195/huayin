import request from '@/utils/request'

// 查询工作室公告列表
export function listTWorkRoomNotice(query) {
	return request({
		url: '/system/tWorkRoomNotice/list',
		method: 'get',
		params: query
	})
}

// 查询工作室公告详细
export function getTWorkRoomNotice(id) {
	return request({
		url: '/system/tWorkRoomNotice/' + id,
		method: 'get'
	})
}

// 新增工作室公告
export function addTWorkRoomNotice(data) {
	return request({
		url: '/system/tWorkRoomNotice',
		method: 'post',
		data: data
	})
}

// 修改工作室公告
export function updateTWorkRoomNotice(data) {
	return request({
		url: '/system/tWorkRoomNotice',
		method: 'put',
		data: data
	})
}

// 删除工作室公告
export function delTWorkRoomNotice(id) {
	return request({
		url: '/system/tWorkRoomNotice/' + id,
		method: 'delete'
	})
}
