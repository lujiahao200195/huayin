import request from '@/utils/request'

// 查询教师可安排时间段列表
export function listTTeacherSchedule(query) {
	return request({
		url: '/system/tTeacherSchedule/list',
		method: 'get',
		params: query
	})
}

// 查询教师可安排时间段详细
export function getTTeacherSchedule(id) {
	return request({
		url: '/system/tTeacherSchedule/' + id,
		method: 'get'
	})
}

// 新增教师可安排时间段
export function addTTeacherSchedule(data) {
	return request({
		url: '/system/tTeacherSchedule',
		method: 'post',
		data: data
	})
}

// 修改教师可安排时间段
export function updateTTeacherSchedule(data) {
	return request({
		url: '/system/tTeacherSchedule',
		method: 'put',
		data: data
	})
}

// 删除教师可安排时间段
export function delTTeacherSchedule(id) {
	return request({
		url: '/system/tTeacherSchedule/' + id,
		method: 'delete'
	})
}
