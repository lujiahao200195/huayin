import request from '@/utils/request'

// 查询科目分类列表
export function listTSubjectType(query) {
	return request({
		url: '/system/tSubjectType/list',
		method: 'get',
		params: query
	})
}

// 查询科目分类详细
export function getTSubjectType(id) {
	return request({
		url: '/system/tSubjectType/' + id,
		method: 'get'
	})
}

// 新增科目分类
export function addTSubjectType(data) {
	return request({
		url: '/system/tSubjectType',
		method: 'post',
		data: data
	})
}

// 修改科目分类
export function updateTSubjectType(data) {
	return request({
		url: '/system/tSubjectType',
		method: 'put',
		data: data
	})
}

// 删除科目分类
export function delTSubjectType(id) {
	return request({
		url: '/system/tSubjectType/' + id,
		method: 'delete'
	})
}
