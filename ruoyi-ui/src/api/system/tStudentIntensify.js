import request from '@/utils/request'

// 查询学生强化科目列表
export function listTStudentIntensify(query) {
	return request({
		url: '/system/tStudentIntensify/list',
		method: 'get',
		params: query
	})
}

// 查询学生强化科目详细
export function getTStudentIntensify(id) {
	return request({
		url: '/system/tStudentIntensify/' + id,
		method: 'get'
	})
}

// 新增学生强化科目
export function addTStudentIntensify(data) {
	return request({
		url: '/system/tStudentIntensify',
		method: 'post',
		data: data
	})
}

// 修改学生强化科目
export function updateTStudentIntensify(data) {
	return request({
		url: '/system/tStudentIntensify',
		method: 'put',
		data: data
	})
}

// 删除学生强化科目
export function delTStudentIntensify(id) {
	return request({
		url: '/system/tStudentIntensify/' + id,
		method: 'delete'
	})
}
