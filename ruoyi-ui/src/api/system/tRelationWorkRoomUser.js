import request from '@/utils/request'

// 查询工作室用户关系列表
export function listTRelationWorkRoomUser(query) {
	return request({
		url: '/system/tRelationWorkRoomUser/list',
		method: 'get',
		params: query
	})
}

// 查询工作室用户关系详细
export function getTRelationWorkRoomUser(id) {
	return request({
		url: '/system/tRelationWorkRoomUser/' + id,
		method: 'get'
	})
}

// 新增工作室用户关系
export function addTRelationWorkRoomUser(data) {
	return request({
		url: '/system/tRelationWorkRoomUser',
		method: 'post',
		data: data
	})
}

// 修改工作室用户关系
export function updateTRelationWorkRoomUser(data) {
	return request({
		url: '/system/tRelationWorkRoomUser',
		method: 'put',
		data: data
	})
}

// 删除工作室用户关系
export function delTRelationWorkRoomUser(id) {
	return request({
		url: '/system/tRelationWorkRoomUser/' + id,
		method: 'delete'
	})
}
