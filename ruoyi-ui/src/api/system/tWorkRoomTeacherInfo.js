import request from '@/utils/request'

// 查询工作室教师信息卡列表
export function listTWorkRoomTeacherInfo(query) {
	return request({
		url: '/system/tWorkRoomTeacherInfo/list',
		method: 'get',
		params: query
	})
}

// 查询工作室教师信息卡详细
export function getTWorkRoomTeacherInfo(id) {
	return request({
		url: '/system/tWorkRoomTeacherInfo/' + id,
		method: 'get'
	})
}

// 新增工作室教师信息卡
export function addTWorkRoomTeacherInfo(data) {
	return request({
		url: '/system/tWorkRoomTeacherInfo',
		method: 'post',
		data: data
	})
}

// 修改工作室教师信息卡
export function updateTWorkRoomTeacherInfo(data) {
	return request({
		url: '/system/tWorkRoomTeacherInfo',
		method: 'put',
		data: data
	})
}

// 删除工作室教师信息卡
export function delTWorkRoomTeacherInfo(id) {
	return request({
		url: '/system/tWorkRoomTeacherInfo/' + id,
		method: 'delete'
	})
}
