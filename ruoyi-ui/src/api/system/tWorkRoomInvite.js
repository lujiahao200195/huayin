import request from '@/utils/request'

// 查询工作室邀请列表
export function listTWorkRoomInvite(query) {
	return request({
		url: '/system/tWorkRoomInvite/list',
		method: 'get',
		params: query
	})
}

// 查询工作室邀请详细
export function getTWorkRoomInvite(id) {
	return request({
		url: '/system/tWorkRoomInvite/' + id,
		method: 'get'
	})
}

// 新增工作室邀请
export function addTWorkRoomInvite(data) {
	return request({
		url: '/system/tWorkRoomInvite',
		method: 'post',
		data: data
	})
}

// 修改工作室邀请
export function updateTWorkRoomInvite(data) {
	return request({
		url: '/system/tWorkRoomInvite',
		method: 'put',
		data: data
	})
}

// 删除工作室邀请
export function delTWorkRoomInvite(id) {
	return request({
		url: '/system/tWorkRoomInvite/' + id,
		method: 'delete'
	})
}
