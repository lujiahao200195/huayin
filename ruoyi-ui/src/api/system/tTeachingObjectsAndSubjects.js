import request from '@/utils/request'

// 查询教师意向教学列表
export function listTTeachingObjectsAndSubjects(query) {
	return request({
		url: '/system/tTeachingObjectsAndSubjects/list',
		method: 'get',
		params: query
	})
}

// 查询教师意向教学详细
export function getTTeachingObjectsAndSubjects(id) {
	return request({
		url: '/system/tTeachingObjectsAndSubjects/' + id,
		method: 'get'
	})
}

// 新增教师意向教学
export function addTTeachingObjectsAndSubjects(data) {
	return request({
		url: '/system/tTeachingObjectsAndSubjects',
		method: 'post',
		data: data
	})
}

// 修改教师意向教学
export function updateTTeachingObjectsAndSubjects(data) {
	return request({
		url: '/system/tTeachingObjectsAndSubjects',
		method: 'put',
		data: data
	})
}

// 删除教师意向教学
export function delTTeachingObjectsAndSubjects(id) {
	return request({
		url: '/system/tTeachingObjectsAndSubjects/' + id,
		method: 'delete'
	})
}
