import request from '@/utils/request'

// 查询版块标签关系列表
export function listTBbsRelationBoardTag(query) {
	return request({
		url: '/system/tBbsRelationBoardTag/list',
		method: 'get',
		params: query
	})
}

// 查询版块标签关系详细
export function getTBbsRelationBoardTag(id) {
	return request({
		url: '/system/tBbsRelationBoardTag/' + id,
		method: 'get'
	})
}

// 新增版块标签关系
export function addTBbsRelationBoardTag(data) {
	return request({
		url: '/system/tBbsRelationBoardTag',
		method: 'post',
		data: data
	})
}

// 修改版块标签关系
export function updateTBbsRelationBoardTag(data) {
	return request({
		url: '/system/tBbsRelationBoardTag',
		method: 'put',
		data: data
	})
}

// 删除版块标签关系
export function delTBbsRelationBoardTag(id) {
	return request({
		url: '/system/tBbsRelationBoardTag/' + id,
		method: 'delete'
	})
}
