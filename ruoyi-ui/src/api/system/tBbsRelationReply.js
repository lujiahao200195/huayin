import request from '@/utils/request'

// 查询回复关系列表
export function listTBbsRelationReply(query) {
	return request({
		url: '/system/tBbsRelationReply/list',
		method: 'get',
		params: query
	})
}

// 查询回复关系详细
export function getTBbsRelationReply(id) {
	return request({
		url: '/system/tBbsRelationReply/' + id,
		method: 'get'
	})
}

// 新增回复关系
export function addTBbsRelationReply(data) {
	return request({
		url: '/system/tBbsRelationReply',
		method: 'post',
		data: data
	})
}

// 修改回复关系
export function updateTBbsRelationReply(data) {
	return request({
		url: '/system/tBbsRelationReply',
		method: 'put',
		data: data
	})
}

// 删除回复关系
export function delTBbsRelationReply(id) {
	return request({
		url: '/system/tBbsRelationReply/' + id,
		method: 'delete'
	})
}
