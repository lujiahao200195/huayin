import request from '@/utils/request'

// 查询教师证书列表
export function listTTeacherCertificates(query) {
	return request({
		url: '/system/tTeacherCertificates/list',
		method: 'get',
		params: query
	})
}

// 查询教师证书详细
export function getTTeacherCertificates(id) {
	return request({
		url: '/system/tTeacherCertificates/' + id,
		method: 'get'
	})
}

// 新增教师证书
export function addTTeacherCertificates(data) {
	return request({
		url: '/system/tTeacherCertificates',
		method: 'post',
		data: data
	})
}

// 修改教师证书
export function updateTTeacherCertificates(data) {
	return request({
		url: '/system/tTeacherCertificates',
		method: 'put',
		data: data
	})
}

// 删除教师证书
export function delTTeacherCertificates(id) {
	return request({
		url: '/system/tTeacherCertificates/' + id,
		method: 'delete'
	})
}
