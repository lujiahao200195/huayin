import request from '@/utils/request'

// 查询微信小程序日志列表
export function listTWechatMiniLog(query) {
	return request({
		url: '/system/tWechatMiniLog/list',
		method: 'get',
		params: query
	})
}

// 查询微信小程序日志详细
export function getTWechatMiniLog(id) {
	return request({
		url: '/system/tWechatMiniLog/' + id,
		method: 'get'
	})
}

// 新增微信小程序日志
export function addTWechatMiniLog(data) {
	return request({
		url: '/system/tWechatMiniLog',
		method: 'post',
		data: data
	})
}

// 修改微信小程序日志
export function updateTWechatMiniLog(data) {
	return request({
		url: '/system/tWechatMiniLog',
		method: 'put',
		data: data
	})
}

// 删除微信小程序日志
export function delTWechatMiniLog(id) {
	return request({
		url: '/system/tWechatMiniLog/' + id,
		method: 'delete'
	})
}
