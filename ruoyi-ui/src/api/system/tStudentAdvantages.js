import request from '@/utils/request'

// 查询学生优势科目列表
export function listTStudentAdvantages(query) {
	return request({
		url: '/system/tStudentAdvantages/list',
		method: 'get',
		params: query
	})
}

// 查询学生优势科目详细
export function getTStudentAdvantages(id) {
	return request({
		url: '/system/tStudentAdvantages/' + id,
		method: 'get'
	})
}

// 新增学生优势科目
export function addTStudentAdvantages(data) {
	return request({
		url: '/system/tStudentAdvantages',
		method: 'post',
		data: data
	})
}

// 修改学生优势科目
export function updateTStudentAdvantages(data) {
	return request({
		url: '/system/tStudentAdvantages',
		method: 'put',
		data: data
	})
}

// 删除学生优势科目
export function delTStudentAdvantages(id) {
	return request({
		url: '/system/tStudentAdvantages/' + id,
		method: 'delete'
	})
}
