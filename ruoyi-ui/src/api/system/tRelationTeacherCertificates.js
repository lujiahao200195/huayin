import request from '@/utils/request'

// 查询教师和教师证书关系列表
export function listTRelationTeacherCertificates(query) {
	return request({
		url: '/system/tRelationTeacherCertificates/list',
		method: 'get',
		params: query
	})
}

// 查询教师和教师证书关系详细
export function getTRelationTeacherCertificates(id) {
	return request({
		url: '/system/tRelationTeacherCertificates/' + id,
		method: 'get'
	})
}

// 新增教师和教师证书关系
export function addTRelationTeacherCertificates(data) {
	return request({
		url: '/system/tRelationTeacherCertificates',
		method: 'post',
		data: data
	})
}

// 修改教师和教师证书关系
export function updateTRelationTeacherCertificates(data) {
	return request({
		url: '/system/tRelationTeacherCertificates',
		method: 'put',
		data: data
	})
}

// 删除教师和教师证书关系
export function delTRelationTeacherCertificates(id) {
	return request({
		url: '/system/tRelationTeacherCertificates/' + id,
		method: 'delete'
	})
}
