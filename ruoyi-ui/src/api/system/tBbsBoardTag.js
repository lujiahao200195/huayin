import request from '@/utils/request'

// 查询版块标签列表
export function listTBbsBoardTag(query) {
	return request({
		url: '/system/tBbsBoardTag/list',
		method: 'get',
		params: query
	})
}

// 查询版块标签详细
export function getTBbsBoardTag(id) {
	return request({
		url: '/system/tBbsBoardTag/' + id,
		method: 'get'
	})
}

// 新增版块标签
export function addTBbsBoardTag(data) {
	return request({
		url: '/system/tBbsBoardTag',
		method: 'post',
		data: data
	})
}

// 修改版块标签
export function updateTBbsBoardTag(data) {
	return request({
		url: '/system/tBbsBoardTag',
		method: 'put',
		data: data
	})
}

// 删除版块标签
export function delTBbsBoardTag(id) {
	return request({
		url: '/system/tBbsBoardTag/' + id,
		method: 'delete'
	})
}
