import request from '@/utils/request'

// 查询资源列表
export function listTResource(query) {
  return request({
    url: '/system/tResource/list',
    method: 'get',
    params: query
  })
}

// 查询资源详细
export function getTResource(id) {
  return request({
    url: '/system/tResource/' + id,
    method: 'get'
  })
}

// 新增资源
export function addTResource(data) {
  return request({
    url: '/system/tResource',
    method: 'post',
    data: data
  })
}

// 修改资源
export function updateTResource(data) {
  return request({
    url: '/system/tResource',
    method: 'put',
    data: data
  })
}

// 删除资源
export function delTResource(id) {
  return request({
    url: '/system/tResource/' + id,
    method: 'delete'
  })
}
