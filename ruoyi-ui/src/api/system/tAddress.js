import request from '@/utils/request'

// 查询地区列表
export function listTAddress(query) {
	return request({
		url: '/system/tAddress/list',
		method: 'get',
		params: query
	})
}

// 查询地区详细
export function getTAddress(id) {
	return request({
		url: '/system/tAddress/' + id,
		method: 'get'
	})
}

// 新增地区
export function addTAddress(data) {
	return request({
		url: '/system/tAddress',
		method: 'post',
		data: data
	})
}

// 修改地区
export function updateTAddress(data) {
	return request({
		url: '/system/tAddress',
		method: 'put',
		data: data
	})
}

// 删除地区
export function delTAddress(id) {
	return request({
		url: '/system/tAddress/' + id,
		method: 'delete'
	})
}
