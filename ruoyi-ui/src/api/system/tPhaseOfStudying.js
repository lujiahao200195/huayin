import request from '@/utils/request'

// 查询学段列表
export function listTPhaseOfStudying(query) {
	return request({
		url: '/system/tPhaseOfStudying/list',
		method: 'get',
		params: query
	})
}

// 查询学段详细
export function getTPhaseOfStudying(id) {
	return request({
		url: '/system/tPhaseOfStudying/' + id,
		method: 'get'
	})
}

// 新增学段
export function addTPhaseOfStudying(data) {
	return request({
		url: '/system/tPhaseOfStudying',
		method: 'post',
		data: data
	})
}

// 修改学段
export function updateTPhaseOfStudying(data) {
	return request({
		url: '/system/tPhaseOfStudying',
		method: 'put',
		data: data
	})
}

// 删除学段
export function delTPhaseOfStudying(id) {
	return request({
		url: '/system/tPhaseOfStudying/' + id,
		method: 'delete'
	})
}
