import request from '@/utils/request'

// 查询资源关系列表
export function listTResourceRelation(query) {
	return request({
		url: '/system/tResourceRelation/list',
		method: 'get',
		params: query
	})
}

// 查询资源关系详细
export function getTResourceRelation(id) {
	return request({
		url: '/system/tResourceRelation/' + id,
		method: 'get'
	})
}

// 新增资源关系
export function addTResourceRelation(data) {
	return request({
		url: '/system/tResourceRelation',
		method: 'post',
		data: data
	})
}

// 修改资源关系
export function updateTResourceRelation(data) {
	return request({
		url: '/system/tResourceRelation',
		method: 'put',
		data: data
	})
}

// 删除资源关系
export function delTResourceRelation(id) {
	return request({
		url: '/system/tResourceRelation/' + id,
		method: 'delete'
	})
}
