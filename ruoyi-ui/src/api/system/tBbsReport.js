import request from '@/utils/request'

// 查询举报列表
export function listTBbsReport(query) {
	return request({
		url: '/system/tBbsReport/list',
		method: 'get',
		params: query
	})
}

// 查询举报详细
export function getTBbsReport(id) {
	return request({
		url: '/system/tBbsReport/' + id,
		method: 'get'
	})
}

// 新增举报
export function addTBbsReport(data) {
	return request({
		url: '/system/tBbsReport',
		method: 'post',
		data: data
	})
}

// 修改举报
export function updateTBbsReport(data) {
	return request({
		url: '/system/tBbsReport',
		method: 'put',
		data: data
	})
}

// 删除举报
export function delTBbsReport(id) {
	return request({
		url: '/system/tBbsReport/' + id,
		method: 'delete'
	})
}
