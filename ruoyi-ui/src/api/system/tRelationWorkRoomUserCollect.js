import request from '@/utils/request'

// 查询工作室用户关系-收藏用列表
export function listTRelationWorkRoomUserCollect(query) {
	return request({
		url: '/system/tRelationWorkRoomUserCollect/list',
		method: 'get',
		params: query
	})
}

// 查询工作室用户关系-收藏用详细
export function getTRelationWorkRoomUserCollect(id) {
	return request({
		url: '/system/tRelationWorkRoomUserCollect/' + id,
		method: 'get'
	})
}

// 新增工作室用户关系-收藏用
export function addTRelationWorkRoomUserCollect(data) {
	return request({
		url: '/system/tRelationWorkRoomUserCollect',
		method: 'post',
		data: data
	})
}

// 修改工作室用户关系-收藏用
export function updateTRelationWorkRoomUserCollect(data) {
	return request({
		url: '/system/tRelationWorkRoomUserCollect',
		method: 'put',
		data: data
	})
}

// 删除工作室用户关系-收藏用
export function delTRelationWorkRoomUserCollect(id) {
	return request({
		url: '/system/tRelationWorkRoomUserCollect/' + id,
		method: 'delete'
	})
}
