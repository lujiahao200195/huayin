import request from '@/utils/request'

// 查询文章和文章分类关系列表
export function listTArticleTypeRelation(query) {
	return request({
		url: '/system/tArticleTypeRelation/list',
		method: 'get',
		params: query
	})
}

// 查询文章和文章分类关系详细
export function getTArticleTypeRelation(id) {
	return request({
		url: '/system/tArticleTypeRelation/' + id,
		method: 'get'
	})
}

// 新增文章和文章分类关系
export function addTArticleTypeRelation(data) {
	return request({
		url: '/system/tArticleTypeRelation',
		method: 'post',
		data: data
	})
}

// 修改文章和文章分类关系
export function updateTArticleTypeRelation(data) {
	return request({
		url: '/system/tArticleTypeRelation',
		method: 'put',
		data: data
	})
}

// 删除文章和文章分类关系
export function delTArticleTypeRelation(id) {
	return request({
		url: '/system/tArticleTypeRelation/' + id,
		method: 'delete'
	})
}
