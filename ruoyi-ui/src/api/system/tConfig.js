import request from '@/utils/request'

// 查询配置列表
export function listTConfig(query) {
  return request({
    url: '/system/tConfig/list',
    method: 'get',
    params: query
  })
}

// 查询配置详细
export function getTConfig(id) {
  return request({
    url: '/system/tConfig/' + id,
    method: 'get'
  })
}

// 新增配置
export function addTConfig(data) {
  return request({
    url: '/system/tConfig',
    method: 'post',
    data: data
  })
}

// 修改配置
export function updateTConfig(data) {
  return request({
    url: '/system/tConfig',
    method: 'put',
    data: data
  })
}

// 删除配置
export function delTConfig(id) {
  return request({
    url: '/system/tConfig/' + id,
    method: 'delete'
  })
}
