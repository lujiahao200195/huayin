import request from "@/utils/request";

// 查询文章列表
export function listTArticle(query) {
  return request({
    url: "/system/tArticle/list",
    method: "get",
    params: query,
  });
}

// 查询文章详细
export function getTArticle(id) {
  return request({
    url: "/system/tArticle/" + id,
    method: "get",
  });
}

// 新增文章
export function addTArticle(data) {
  return request({
    url: "/system/tArticle",
    method: "post",
    data: data,
  });
}

// 修改文章
export function updateTArticle(data) {
  return request({
    url: "/system/tArticle",
    method: "put",
    data: data,
  });
}

// 删除文章
export function delTArticle(id) {
  return request({
    url: "/system/tArticle/" + id,
    method: "delete",
  });
}

// export function getList() {
//   return request({
//     url: "/system/type/getList",
//     method: "get",
//   });
// }

export function getTwoTypeList(){
	return request({
		url: "/system/type/getList",
		method: 'get'
	})
}