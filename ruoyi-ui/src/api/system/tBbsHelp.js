import request from '@/utils/request'

// 查询论坛求助列表
export function listTBbsHelp(query) {
	return request({
		url: '/system/tBbsHelp/list',
		method: 'get',
		params: query
	})
}

// 查询论坛求助详细
export function getTBbsHelp(id) {
	return request({
		url: '/system/tBbsHelp/' + id,
		method: 'get'
	})
}

// 新增论坛求助
export function addTBbsHelp(data) {
	return request({
		url: '/system/tBbsHelp',
		method: 'post',
		data: data
	})
}

// 修改论坛求助
export function updateTBbsHelp(data) {
	return request({
		url: '/system/tBbsHelp',
		method: 'put',
		data: data
	})
}

// 删除论坛求助
export function delTBbsHelp(id) {
	return request({
		url: '/system/tBbsHelp/' + id,
		method: 'delete'
	})
}
