import request from '@/utils/request'

// 查询论坛用户数据列表
export function listTBbsUser(query) {
	return request({
		url: '/system/tBbsUser/list',
		method: 'get',
		params: query
	})
}

// 查询论坛用户数据详细
export function getTBbsUser(id) {
	return request({
		url: '/system/tBbsUser/' + id,
		method: 'get'
	})
}

// 新增论坛用户数据
export function addTBbsUser(data) {
	return request({
		url: '/system/tBbsUser',
		method: 'post',
		data: data
	})
}

// 修改论坛用户数据
export function updateTBbsUser(data) {
	return request({
		url: '/system/tBbsUser',
		method: 'put',
		data: data
	})
}

// 删除论坛用户数据
export function delTBbsUser(id) {
	return request({
		url: '/system/tBbsUser/' + id,
		method: 'delete'
	})
}
