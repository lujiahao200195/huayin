import request from '@/utils/request'

// 查询版块公告列表
export function listTBbsNotice(query) {
	return request({
		url: '/system/tBbsNotice/list',
		method: 'get',
		params: query
	})
}

// 查询版块公告详细
export function getTBbsNotice(id) {
	return request({
		url: '/system/tBbsNotice/' + id,
		method: 'get'
	})
}

// 新增版块公告
export function addTBbsNotice(data) {
	return request({
		url: '/system/tBbsNotice',
		method: 'post',
		data: data
	})
}

// 修改版块公告
export function updateTBbsNotice(data) {
	return request({
		url: '/system/tBbsNotice',
		method: 'put',
		data: data
	})
}

// 删除版块公告
export function delTBbsNotice(id) {
	return request({
		url: '/system/tBbsNotice/' + id,
		method: 'delete'
	})
}
