import request from '@/utils/request'

// 查询工作室与科目关系列表
export function listTRelationWorkRoomSubject(query) {
	return request({
		url: '/system/tRelationWorkRoomSubject/list',
		method: 'get',
		params: query
	})
}

// 查询工作室与科目关系详细
export function getTRelationWorkRoomSubject(id) {
	return request({
		url: '/system/tRelationWorkRoomSubject/' + id,
		method: 'get'
	})
}

// 新增工作室与科目关系
export function addTRelationWorkRoomSubject(data) {
	return request({
		url: '/system/tRelationWorkRoomSubject',
		method: 'post',
		data: data
	})
}

// 修改工作室与科目关系
export function updateTRelationWorkRoomSubject(data) {
	return request({
		url: '/system/tRelationWorkRoomSubject',
		method: 'put',
		data: data
	})
}

// 删除工作室与科目关系
export function delTRelationWorkRoomSubject(id) {
	return request({
		url: '/system/tRelationWorkRoomSubject/' + id,
		method: 'delete'
	})
}
