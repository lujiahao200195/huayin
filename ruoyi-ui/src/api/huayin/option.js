import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listOption(query) {
  return request({
    url: '/system/option/list',
    method: 'get',
    params: query
  })
}

// 查询【请填写功能名称】详细
export function getOption(id) {
  return request({
    url: '/system/option/' + id,
    method: 'get'
  })
}

// 新增【请填写功能名称】
export function addOption(data) {
  return request({
    url: '/system/option',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateOption(data) {
  return request({
    url: '/system/option',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delOption(id) {
  return request({
    url: '/system/option/' + id,
    method: 'delete'
  })
}
