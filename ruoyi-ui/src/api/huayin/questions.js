import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listQuestions(query) {
  return request({
    url: '/system/questions/list',
    method: 'get',
    params: query
  })
}

// 查询【请填写功能名称】详细
export function getQuestions(id) {
  return request({
    url: '/system/questions/' + id,
    method: 'get'
  })
}

// 新增【请填写功能名称】
export function addQuestions(data) {
  return request({
    url: '/system/questions',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateQuestions(data) {
  return request({
    url: '/system/questions',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delQuestions(id) {
  return request({
    url: '/system/questions/' + id,
    method: 'delete'
  })
}
