import request from "@/utils/request";

export function getCourse(data) {
  return request({
    url: "/system/backend/course/get-course",
    method: "post",
    data,
  });
}
