import request from "@/utils/request";

/**
 *
 * @param {*} data
 * @returns
 * 获取学校资讯列表
 */
export function getSchoolInfoList(data = {}) {
  return request({
    url: "/system/backend/article/school-article-list",
    method: "post",
    data,
    params: data,
  });
}
/**
 *
 * @param {*} data
 * @returns
 * 修改学校资讯
 */
export function schoolInfoEdit(data = {}) {
  return request({
    url: "/system/backend/article/article-edit",
    method: "post",
    data,
  });
}
/**
 *
 * @param {*} data
 * @returns
 * 删除学校资讯
 */
export function schoolInfoDelete(data = {}) {
  return request({
    url: "/system/backend/article/article-remove",
    method: "post",
    data,
  });
}
/**
 *
 * @param {*} data
 * @returns
 * 增加学校资讯
 */
export function schoolInfoAdd(data = {}) {
  return request({
    url: "/system/backend/article/school-article-add",
    method: "post",
    data,
  });
}
