import request from "@/utils/request";

// 查询学生列表
export function listTStudent(query) {
  return request({
    url: "/system/tStudent/list",
    method: "get",
    params: query,
  });
}

// 查询学生详细
export function getTStudent(id) {
  return request({
    url: "/system/tStudent/" + id,
    method: "get",
  });
}

// 新增学生
export function addTStudent(data) {
  return request({
    url: "/system/tStudent",
    method: "post",
    data: data,
  });
}

// 修改学生
export function updateTStudent(data) {
  return request({
    url: "/system/tStudent",
    method: "put",
    data: data,
  });
}

// 删除学生
export function delTStudent(id) {
  return request({
    url: "/system/tStudent/" + id,
    method: "delete",
  });
}
