import request from "@/utils/request";
// 获取列表
export function getList(params) {
  return request({
    url: "/api/v1/user/list",
    method: "get",
    params,
  });
}
